import fetch from 'isomorphic-fetch';
import { Util } from './util';
import ObjectAssign from 'object-assign';

export default class {
  /**
   * [GET] /my/posts -- List Standard user's posts 
   * @param ageRange (int) [1: 'Age 0-3', 2: 'Age 4-8', 3: 'Age 9-up']
   * @param postType (string) ['offer', 'wanted']
   * @param filter (string) - filtering term in posts' title or description
   * @param order (string) ['asc': older posts first, 'desc': newer first] - 'desc'
   * @param pagin (boolean) [0, 1] - 1
   * 
   * @return {data: [...], paginator: {...}}
   */
  static async getPosts(paramsObj) {
    return await Util.makeRequest('/my/posts', paramsObj);
  }

  /**
   * [GET] /my/subscriptions - Get current user subscription history
   * @param paramsObj {pagin (bool), perPage (int)}
   */
  static async getSubscriptions(paramsObj) {
    return await Util.makeRequest('/my/subscriptions', paramsObj);
  }

  /**
   * [GET] /plans --> get active subscription plans
   * @return data: []
   */
  static async getActiveSubscriptionPlans() {
    return await Util.makeRequest('/plans', {status: 'active', isPublic: 1, pagin: 0});
  }

  /**
   * [POST] /signup-complete --> complete signup process with payment and other details
   * @param body {regStep, planId, firstName, lastName, displayName, zipCode,...}
   */
  static async signupStepComplete(body) {
    return await Util.makeRequest('/signup-continue', null, 'POST', body);
  }

  /**
   * [POST] /referrals -> create new referral entry(s)
   * @param body {emails, statement}
   */
  static async createReferrals(body) {
    return await Util.makeRequest('/referrals', null, 'POST', ObjectAssign({}, body, {_req_type_json: true}));
  }

  /**
   * [POST] /reset-password - Reset password - step 1
   * @param email
   */
  static async initResetPassword(email) {
    return await Util.makeRequest('/reset-password', null, 'POST', ObjectAssign({}, {email}, {_req_type_json: true}));
  }
  /**
   * [PUT] /reset-password - Reset user password - step 2
   * @param {code: string, password: string}
   */
  static async resetPassword(data) {
    return await Util.makeRequest('/reset-password', null, 'PUT', ObjectAssign({}, data, {_req_type_urlencoded: true}));
  }

  /**
   * [PUT] /my/profile/password -- Change current user password
   * @param body {password: (string)}
   */
  static async updatePassword(body) {
    return await Util.makeRequest('/my/profile/password', null, 'PUT', ObjectAssign({}, body, {_req_type_urlencoded: true}));
  }

}

export class Messages {
  /**
   * [GET] /my/messages -- get total number of unread messages
   */
  static async totalUnread() {
    return await Util.makeRequest('/my/messages');
  }
  /**
   * [GET] /my/messages/posts - return array of post objects with additional 'totalUnreadMessages' prop
   * @param paramsObj {perPage (int), pagin (bool), page (int)} optional
   */
  static async listPosts(paramsObj) {
    return await Util.makeRequest('/my/messages/posts', paramsObj);
  }
  /**
   * [GET] /my/messages/posts/{id} - return array of user (reduced data) objects with additional 'totalUnreadMessages' prop
   * @param postId (int)
   * @param paramsObj (obj) {pagin (bool), perPage (int), page (int)} optional
   */
  static async listConversations(postId, paramsObj) {
    return await Util.makeRequest('/my/messages/posts/' + postId, paramsObj);
  }
  /**
   * [GET] /my/messages/posts/{id}/users/{id} - return array of message objects
   * @param postId (int)
   * @param userId (int)
   * @param paramsObj (obj) {pagin (bool), perPage (int), page (int)} opt
   */
  static async listMessages(postId, userId, paramsObj) {
    return await Util.makeRequest(`/my/messages/posts/${postId}/users/${userId}`, paramsObj);
  }

  /**
   * [POST] /my/messages/posts/{id}/users/{id} - create new message
   * @param postId (int)
   * @param userId (int)
   * @param body (obj) {subject (str), content (str)}
   */
  static async createMessage(postId, userId, body) {
    return await Util.makeRequest(`/my/messages/posts/${postId}/users/${userId}`, null, 'POST', ObjectAssign({}, body, {_req_type_json: true}));
  }

  /**
   * [PUT] /my/messages/{id} - mark message as read
   * @param id (int)
   * @param body (obj) {isRead (bool)}
   */
  static async updateMessageStatus(id, body) {
    return await Util.makeRequest('/my/messages/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_json: true}));
  }

  /**
   * [DELETE] /my/messages/{id} - delete message
   * @param id (int)
   */
  static async deleteMessage(id) {
    return await Util.makeRequest('/my/messages/' + id, null, 'DELETE');
  }
}