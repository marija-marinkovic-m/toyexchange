import fetch from 'isomorphic-fetch';
import { Util } from '../util';

import ObjectAssign from 'object-assign';

export default class {
  /**
   * [GET] /countries -> List Countries
   * @param paramsObj: {filter: (string), pagin: (bool)}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/countries', paramsObj);
  }

  /**
   * [PUT] /countries/{id} -> update country name or iso
   * @param id (int) 
   * @param body {name: (string), iso: (string)}
   */
  static async update(id, body) {
    return await Util.makeRequest('/countries/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_json: true}));
  }

  /**
   * [DELETE] /countries/{id} -> delete country
   * @param id (int)
   */
  static async delete(id) {
    return await Util.makeRequest('/countries/' + id, null, 'DELETE');
  }

  /**
   * [POST] /countries -> create country
   * @param body {name: (string), iso: (string)}
   */
  static async create(body) {
    return await Util.makeRequest('/countries', null, 'POST', ObjectAssign({}, body, {
      _req_type_json: true
    }));
  }
}