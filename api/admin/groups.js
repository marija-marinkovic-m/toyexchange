import fetch from 'isomorphic-fetch';
import { Util } from '../util';
import ObjectAssign from 'object-assign';

export default class {
  static async all(paramsObj) {
    return await Util.makeRequest('/groups', paramsObj);
  }

  static async getGroup(id) {
    return await Util.makeRequest('/groups/' + id);
  }

  /**
   * [POST] /groups - Create group
   * body: {zipCodes, title}
   * @param zipCodes (csv) Single or multiple zip codes which belong to group. Multiple zip codes have to be in CSV format. Every zip code item can also be represented as range in this format X-Y, meaning that every zip code in range X to Y will be included in group, for example 19602-19612. Complex example of multiple zip codes: 19786,19102,27502-27514,32101,42202-42220
   * @param title (string) group title
   */
  static async create(body) {
    return await Util.makeRequest('/groups', null, 'POST', ObjectAssign({}, body, {_req_type_json: true}));
  }

  /**
   * [PUT] /groups/{id} - Update group
   * body: {zipCodes, title}
   */
  static async update(id, body) {
    return await Util.makeRequest('/groups/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_urlencoded: true}));
  }

  /**
   * [DELETE] /groups/{id} - Delete group
   * only groups not used by any user can be deleted
   */
  static async delete(id) {
    return await Util.makeRequest('/groups/' + id, null, 'DELETE');
  }

  /**
   * [GET] /related-groups/{groupId} - Get related groups of one specific group
   * @param groupId (int)
   */
  static async related(groupId) {
    return await Util.makeRequest('/related-groups/' + groupId);
  }
  /**
   * [PUT] /related-groups/{groupId} - Update related groups of one specific group
   * @param groupId (int)
   * @param body {relatedIds: (string, csv)}
   */
  static async updateRelated(groupId, body) {
    return await Util.makeRequest('/related-groups/' + groupId, null, 'PUT', ObjectAssign({}, body, {_req_type_urlencoded: true}));
  }
  /**
   * [DELETE] /related-groups/{groupId} - delete related groups of one spec. group
   * @param groupId (int)
   */
  static async deleteRelated(groupId) {
    return await Util.makeRequest('/related-groups/' + groupId, null, 'DELETE');
  }
}