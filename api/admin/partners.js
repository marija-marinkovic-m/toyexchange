import fetch from 'isomorphic-fetch';
import { Util } from '../util';
import ObjectAssign from 'object-assign';

export default class {
  /**
   * [GET] /partners - list partners
   * @param paramsObj: {pagin: (0|1), perPage: (int), page: (int)}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/partners', paramsObj);
  }

  /**
   * [POST] /partners - create partner
   * @param body {title: (string), url: (string), imageFile: (File)}
   */
  static async create(body) {
    return await Util.makeRequest('/partners', null, 'POST', body);
  }

  /**
   * [DELETE] /partners/{id}
   * @param id (int) partner id
   */
  static async delete(id) {
    return await Util.makeRequest('/partners/' + id, null, 'DELETE');
  }

  /**
   * [PUT] /partners/{id} --- update urlencoded
   * @param id (int)
   * @param body (obj)
   */
  static async update(id, body) {
    return await Util.makeRequest('/partners/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_urlencoded: true}));
  }
  /**
   * [POST] /partners/{id} -- update multipart form-data
   * @param id (int)
   * @param body (obj)
   */
  static async updateMultipartFormData(id, body) {
    return await Util.makeRequest('/partners/' + id, null, 'POST', ObjectAssign({}, body, {
      _method: 'PUT'
    }));
  }
}