import fetch from 'isomorphic-fetch';
import { Util } from '../util';
import ObjectAssign from 'object-assign';

export default class {
  /**
   * [GET] /plans
   * @param paramsObj: {status: (string), isPublic: (bool), pagin: (bool), page: (int), perPage: (int)}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/plans', paramsObj);
  }
  /**
   * [GET] /plans/{id}
   * @param id (int)
   */
  static async one(id) {
    return await Util.makeRequest('/plans/' + id);
  }
  /**
   * [DELETE] /plans/{id}
   * @param id (int)
   */
  static async delete(id) {
    return await Util.makeRequest('/plans/' + id, null, 'DELETE');
  }
  /**
   * [POST] /plans
   * @param body {title: string, subtitle: string, description: string, price: double, payedDays: int, freeDays: int, status: string}
   */
  static async create(body) {
    return await Util.makeRequest('/plans', null, 'POST', ObjectAssign({}, body, {
      _req_type_json: true
    }));
  }
  /**
   * [PUT] /plans/{id}
   * @param id int
   * @param body {title: string, subtitle: string, description: string, price: double, payedDays: int, freeDays: int, status: string}
   */
  static async update(id, body) {
    return await Util.makeRequest('/plans/' + id, null, 'PUT', ObjectAssign({}, body, {
      _req_type_json: true
    }))
  }

}