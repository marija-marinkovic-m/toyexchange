import fetch from 'isomorphic-fetch';
import { Util } from '../util';

import ObjectAssign from 'object-assign';

export default class {
   /**
   * [GET] /posts
   * @param paramsObj: {ageRange, postType, filter, status, order, pagin, perPage}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/posts', paramsObj);
  }
  /**
   * [GET] /posts/{id}
   * @param id (int)
   */
  static async one(id) {
    return await Util.makeRequest('/posts/' + id);
  }
  /**
   * [PUT] /posts/{id} -> Update post
   * @param body {status: active | archived | suspended}
   */
  static async update(id, body) {
    return await Util.makeRequest('/posts/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_json: true}));
  }

  /**
   * [DELETE] /posts/{id} -> Delete post
   */
  static async delete(id) {
    return await Util.makeRequest('/posts/' + id, null, 'DELETE');
  }
}

export class ReportedPosts {
  /**
   * [GET] /reported-posts - list reported posts 
   * @param paramsObj: {status (pending | accepted | rejected), postId, pagin, perPage}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/reported-posts', paramsObj);
  }

  /**
   * [PUT] /reported-posts/{id} = update reported post
   * @param id (int)
   * @param body {status: 'pending' | 'accepted' | 'rejected'}
   */
  static async update(id, body) {
    return await Util.makeRequest('/reported-posts/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_json: true}));
  }

  /**
   * [GET] /reported-posts/{id} - get reported post data 
   * @param id (int)
   */
  static async one(id) {
    return await Util.makeRequest('/reported-posts/' + id);
  }

  /**
   * [DELETE] /reported-posts/{id} - delete reported post
   * @param id (int)
   */
  static async delete(id) {
    return await Util.makeRequest('/reported-posts/' + id, null, 'DELETE');
  }
}