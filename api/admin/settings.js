import fetch from 'isomorphic-fetch';
import { Util } from '../util';
import ObjectAssign from 'object-assign';

export default class {
  /**
   * [GET] /settings - list settings
   * @param paramsObj {pagin: (0|1), page: (int)}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/settings', paramsObj);
  }

  /**
   * [PUT] /settings/{id} - update setting
   * @param id (int)
   * @param body (obj {value: string})
   */
  static async update(id, body) {
    return await Util.makeRequest('/settings/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_urlencoded: true}));
  }
}