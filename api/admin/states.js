import fetch from 'isomorphic-fetch';
import { Util } from '../util';
import ObjectAssign from 'object-assign';

export default class {
  /**
   * [GET] /states -> List states
   * @param paramsObj: {countryId: (int), filter: (string), pagin: (bool)}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/states', paramsObj);
  }
  /**
   * [GET] /states/{id}
   * @param id (int)
   */
  static async one(id) {
    return await Util.makeRequest('/states/' + id);
  }
  /**
   * [DELETE] /states/{id}
   * @param id (int)
   */
  static async delete(id) {
    return await Util.makeRequest('/states/' + id, null, 'DELETE');
  }
  /**
   * [POST] /states
   * @param body {countryId: (string), name: (string), abbreviation: (string)}
   */
  static async create(body) {
    return await Util.makeRequest('/states', null, 'POST', ObjectAssign({}, body, {
      _req_type_urlencoded: true
    }));
  }
  /**
   * [PUT] /states/{id}
   * @param id int
   * @param body {name: (string), abbreviation: (string)}
   */
  static async update(id, body) {
    return await Util.makeRequest('/states/' + id, null, 'PUT', ObjectAssign({}, body, {
      _req_type_json: true
    }))
  }

}