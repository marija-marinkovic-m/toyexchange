import fetch from 'isomorphic-fetch';
import { Util } from '../util';
import ObjectAssign from 'object-assign';

import { API_URI } from '../../static/config';

export default class {
  /**
   * [GET] /users
   * @param paramsObj: {filter: (string), paymentStatus: (string), roleId: (int), status: (string), pagin: (bool)}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/users', paramsObj);
  }
  /**
   * [GET] /users/{id}
   * @param id (int)
   */
  static async one(id) {
    return await Util.makeRequest('/users/' + id);
  }
  /**
   * [DELETE] /users/{id}
   * @param id (int)
   */
  static async delete(id) {
    return await Util.makeRequest('/users/' + id, null, 'DELETE');
  }
  /**
   * [POST] /users
   * @param body {email: string, password: string}
   */
  static async create(body) {
    return await Util.makeRequest('/users', null, 'POST', ObjectAssign({}, body, {
      _req_type_urlencoded: true
    }));
  }
  /**
   * [PUT] /users/{id}
   * @param id int
   * @param body {email, firstName, lastName, displayName, zipCode, stateId, city, pickupAddress, pickupWindow, pickupPlacement, nearbyNotifications, notificationType, status}
   */
  static async update(id, body) {
    return await Util.makeRequest('/users/' + id, null, 'PUT', ObjectAssign({}, body, {
      _req_type_json: true
    }))
  }

  /**
   * [GET] /referrals - list user's referrals
   * @param paramsObj {userId, status, pagin, perPage, page}
   */
  static async listReferrals(paramsObj) {
    return await Util.makeRequest('/referrals', paramsObj);
  }

  /**
   * [DELETE] /referrals/{id} - delete user's referral
   * @param id (int)
   */
  static async deleteReferral(id) {
    return await Util.makeRequest('/referrals/' + id, null, 'DELETE');
  }

  
  static async download() {
    return await fetch(Util.getURL('/users/download', null), await Util.fetchConfig('GET'));
  }

  /**
   * Update Specified user subscription data
   * [PUT] /users/subscription/{id}
   * @param userId int
   * @param body {subStartDate, subEndDate, subPayDate, totalEndDate, paymentStatus}
   */
  static async updateSubscriptionDates(userId, body) {
    return await Util.makeRequest('/users/subscription/' + userId, null, 'PUT', ObjectAssign({}, body, {_req_type_json: true}));
  }
}

export class GroupApplicants {
  /**
   * [GET] /group-applicants - list Group Applicants
   * @param {countryId, zipCode, status, pagin, perPage}
   */
   static async list(paramsObj) {
     return await Util.makeRequest('/group-applicants', paramsObj);
   }

   /**
    * [GET] /group-applicants/{id} - get application data
    * @param id (application id)
    */
    static async one(id) {
      return await Util.makeRequest('/group-applicants/' + id);
    }
    
   /**
    * [PUT] /group-applicants/{id} - update group application
    * @param {status: (pending | notified)}
    */
    static async update(id, body) {
      return await Util.makeRequest('/group-applicants/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_json: true}));
    }

   /**
    * [DELETE] /group-applicants/{id} - delete application
    * @param id (application id)
    */
    static async delete(id) {
      return await Util.makeRequest('/group-applicants/' + id, null, 'DELETE');
    }
}