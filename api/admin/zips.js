import fetch from 'isomorphic-fetch';
import { Util } from '../util';
import ObjectAssign from 'object-assign';

export default class {
  /**
   * [GET] /zips
   * @param paramsObj: {stateId: (number), filter: (string), pagin: (bool)}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/zips', paramsObj);
  }
  /**
   * [GET] /zips/{id}
   * @param id (int)
   */
  static async one(id) {
    return await Util.makeRequest('/zips/' + id);
  }
  /**
   * [DELETE] /zips/{id} - only zips not used can be deleted
   * @param id (int)
   */
  static async delete(id) {
    return await Util.makeRequest('/zips/' + id, null, 'DELETE');
  }
  /**
   * [POST] /zips
   * @param body {code: (string), name: (string), stateId: (string)}
   */
  static async create(body) {
    return await Util.makeRequest('/zips', null, 'POST', ObjectAssign({}, body, {
      _req_type_json: true
    }));
  }
  /**
   * [PUT] /zips/{id}
   * @param id int
   * @param body {code: (string), name: (string), stateId: (string)}
   */
  static async update(id, body) {
    return await Util.makeRequest('/zips/' + id, null, 'PUT', ObjectAssign({}, body, {
      _req_type_json: true
    }))
  }

}