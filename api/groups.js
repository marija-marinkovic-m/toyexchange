import fetch from 'isomorphic-fetch';
import { Util } from './util';

export default class {
  /**
   * [GET] /groups - List Groups
   * @param filterZip (string) - only complete zip code is matched
   * @param completeZip (bool) [0, 1] - if 1 return list of groups with complete zip data
   * @param filterTitle (string) - filtering term in group's title
   * @param pagin (bool) [0, 1] - 1
   * 
   * @return {data: [], paginator: {}} (obj)
   */
  static async getGroups(paramsObj) {
    return await Util.makeRequest('/groups', paramsObj);
  }

  /**
   * [GET] /groups/{id} - Get Group Data
   * @return {data: {}}
   */
  static async getGroup(id) {
    return await Util.makeRequest('/groups/' + id);
  }
}