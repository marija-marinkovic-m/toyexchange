import fetch from 'isomorphic-fetch';
import {Util} from './util';
import ObjectAssign from 'object-assign';

export default class Locations {
  /**
   * [GET] /countries
   * @param paramsObj {filters, pagin, page}
   */
  static async listCountries(paramsObj) {
    return await Util.makeRequest('/countries', paramsObj);
  }

  /**
   * [GET] /states
   * @param paramsObj {countryId, filter, pagin, page}
   */
  static async listStates(paramsObj) {
    return await Util.makeRequest('/states', paramsObj);
  }

  /**
   * [GET] /zips
   * @param paramsObj {filter, pagin, page, stateId}
   */
  static async listZips(paramsObj) {
    return await Util.makeRequest('/zips', paramsObj);
  }

  /**
   * [GET] /zips/{id}
   * @param id int
   */
  static async getZip(id) {
    return await Util.makeRequest('/zips/' + id);
  }

  /**
   * [POST] /group-applicants - create new group applicant
   * @param body {email, countryId, zipCode, firstName, lastName}
   */
  static async createApplicant(body) {
    return await Util.makeRequest('/group-applicants', null, 'POST', ObjectAssign({}, body, {_req_type_urlencoded: true}));
  }
}