import fetch from 'isomorphic-fetch';
import { Util } from './util';

export default class {
  /**
   * [GET] /partners
   * @param paramObj {pagin: (0|1), perPage: (int), page: (int)}
   */
  static async list(paramsObj) {
    return await Util.makeRequest('/partners', paramsObj);
  }
}