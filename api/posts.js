import fetch from 'isomorphic-fetch';
import { Util } from './util';

import ObjectAssign from 'object-assign';

export default class {
  /**
   * [GET] /posts - List Posts
   * @param ageRange (int) [1: 'Age 0-3', 2: 'Age 4-8', 3: 'Age 9-up']
   * @param postType (string) ['offer', 'wanted']
   * @param filter (string) - filtering term in posts' title or description
   * @param status (string) ['active', 'archived', 'suspended']
   * @param order (string) ['asc': older posts first, 'desc': newer first] - 'desc'
   * @param pagin (boolean) [0, 1] - 1
   * 
   * @return {data: [], paginator: {}};
   */
  static async getPosts(paramsObj) {
    return await Util.makeRequest('/posts', paramsObj);
  }

  /**
   * [GET] /posts/{id} - Get post data
   * @return {data: {}}
   */
  static async getPost(id) {
    return await Util.makeRequest('/posts/' + id);
  }
}


export class MyPosts {
  /**
   * [POST] /my/posts - Create new post
   * data {title, ageRange, pickupWindow,
   * pickupAddress, pickupPlacement, description, 
   * postType, imageFile}
   */
  static async create(body) {
    return await Util.makeRequest('/my/posts', null, 'POST', body);
  }

  /**
   * [PUT] /my/posts/{id} - Update 
   * data {title, ageRange, pickupWindow,
   * pickupAddress, pickupPlacement, description, 
   * postType, imageFile}
   */
  static async update(id, body) {
    return await Util.makeRequest('/my/posts/' + id, null, 'POST', ObjectAssign({}, body, {_method: 'PUT'}));
  }

  /**
   * [DELETE] /my/posts/{id} - Delete post
   */
  static async delete(id) {
    return await Util.makeRequest('/my/posts/' + id, null, 'DELETE');
  }

  /**
   * [POST] /reported-posts - Report as inapropriate
   * body {description: (string), postId: (int)}
   */
  static async reportPost(body) {
    return await Util.makeRequest('/reported-posts', null, 'POST', ObjectAssign({}, body, {_req_type_json: true}));
  }

  /**
   * [GET] /reported-posts - Get reports as admin
   * @params {postId: (int), status, pagin, perPage}
   */
  static async reportsOnPost(paramsObj) {
    return await Util.makeRequest('/reported-posts', paramsObj);
  }

  /**
   * [PUT] /my/posts/status/{id}
   * @param id (int) post id
   * @param body {status ('active' | 'canceled' | 'completed'), givenUserId (int)}
   */
  static async updateStatus(id, body) {
    return await Util.makeRequest('/my/posts/status/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_json: true}));
  }

  static async adminDeletePost(id) {
    return await Util.makeRequest('/posts/' + id, null, 'DELETE');
  }

  /**
   * [PUT] /posts/{id}
   * @param id int
   * @param body {status: 'active' | 'canceled' | 'completed' | 'suspended' | 'expired'}
   */
  static async adminUpdateStatus(id, body) {
    return await Util.makeRequest('/posts/' + id, null, 'PUT', ObjectAssign({}, body, {_req_type_json: true}));
  }
}