import fetch from 'isomorphic-fetch';
import { Util } from './util';
import ObjectAssign from 'object-assign';
import Session from '../auth/session';

export default class {
  /**
   * [GET] /my/profile - Get current user profile
   * @return {data: {}}
   */
  static async getProfile() {
    return await Util.makeRequest('/my/profile');
  }

  /**
   * [PUT] /my/profile - Update current user profile
   * body: {email, firstName, lastName, displayName, zipCode, stateId
   * pickupAddress, pickupWindow, pickupPlacement, nearbyNotifications,
   * notificationType, avatarFile, avatarClear(???)}
   */
  static async update(body) {
    return await Util.makeRequest('/my/profile', null, 'POST', ObjectAssign({}, body, {_method: 'PUT'}))
      .then(() => {
        const SessionInstance = new Session();
        return SessionInstance.updateSession();
      });
  }
}