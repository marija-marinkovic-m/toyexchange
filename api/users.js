import fetch from 'isomorphic-fetch';
import { clientId, API_URI } from '../static/config';

export default class {
  /**
   * [POST] /login - Login user
   * @param email (string) required
   * @param password (string) required
   * @param clientId (string) required
   * 
   * @return (on success {accessToken, accessTokenExpiration, refreshToken})
   */
  static async loginUser(email, password) {
    let response = await fetch(API_URI + '/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password,
        clientId: clientId
      })
    });
    let data = await response.json();

    return data;
  }
}

export class Profile {
  /**
   * [GET] /my/profile - Get current user profile
   * @param accessToken
   * @return on success -> data object 
   */
  static async myProfile(accessToken) {
    let response = await fetch(API_URI + '/my/profile', {
      headers: {
        'Authorization': 'Bearer ' + accessToken
      }
    });
    let data = await response.json();
    return data;
  }
}
