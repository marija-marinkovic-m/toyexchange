import { API_URI } from '../static/config';
import Session from '../auth/session';

export class Util {
  static async getHeaders() {
    const SessionInstance = new Session();
    const sessionData = await SessionInstance.getSession();
    let headers = {
      'Accept': 'application/json'
    };

    if (sessionData !== null && typeof sessionData === 'object' && sessionData.user && sessionData.user.auth) {
      headers['Authorization'] = 'Bearer ' + sessionData.user.auth.accessToken;
    }

    return headers;
  }

  static async fetchConfig (method, body) {
    let headers = await Util.getHeaders();
    let config = {
      method: method || 'GET',
      headers,
      credentials: 'omit'
    };

    if (typeof body !== 'undefined') {
      if (body.hasOwnProperty('_req_type_json')) {
        // has request content type flag 
        config.headers['Content-Type'] = 'application/json';
        config.body = JSON.stringify(body);
      } else if (body.hasOwnProperty('_req_type_urlencoded')) {
        config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
        config.body = Object.keys(body).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(body[key])).join('&');
      } else {
        // config.headers['Content-Length'] = 'multipart/form-data';
        const formData = Object.keys(body).reduce((formData, key) => {
          let item = body[key];
          if (item instanceof Blob) {
            formData.append(key, item, item.filename);
          } else {
          formData.append(key, item);
          }
          return formData;
        }, new FormData());
        config.body = formData;
      }
    }

    return config;
  }

  static getURL (endpoint, paramsObj, apiUri) {
    let route = endpoint || '';
    apiUri = apiUri || API_URI;

    if ((typeof paramsObj !== 'undefined' && null !== paramsObj) && paramsObj.constructor === Object && Object.keys(paramsObj).length !== 0) {
      route += '?';
      for (let prop in paramsObj) {
        if (!paramsObj[prop]) {
          continue;
        }
        route += encodeURIComponent(prop) + '=' + encodeURIComponent(paramsObj[prop]) + '&';
      }
    }

    return apiUri + route;
  }

  static checkResponse = (response) => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }
    let error = new Error(response.status, response.statusText);
    error.response = response;
    throw error;
  }

  static async makeRequest(route, urlParams = null, method = 'GET', body) {
    return await fetch(
      Util.getURL(route, urlParams),
      await Util.fetchConfig(method, body)
      )
      .then(Util.checkResponse)
      .catch(async (error) => {
        console.log('Request failed', error);
        // edge hotfix !error.response (for some reason edge doesn't allow custom props on Error() object, so just assume we need to refresh the token)
        // also added client check (only client can start token refresh process)
        // and all protected data calls in the app should be asynchronous
        if (typeof window !== 'undefined' && (error.response.status === 401)) {
          // try to refresh the token
          // if succedes, retry the request
          // if refresh token missing from session or requrest fails logout user and redirect ;))
          const SessionInstance = new Session();
          return SessionInstance.refreshToken()
            .then(async (res) => {
              return await fetch(Util.getURL(route, urlParams), await Util.fetchConfig(method, body));
            })
            .then(Util.checkResponse)
            .catch(err => SessionInstance.logout()
                .then(res => window.location.reload())
                .catch(err => console.log(err))
            );
        }
        return await error.response.json();
      });
  }

  static formToPlainObject(form) {
    var field, l = [];
    let res = {};
    if (typeof form == 'object' && form.nodeName == "FORM") {
      var len = form.elements.length;
      for (var i=0; i<len; i++) {
        field = form.elements[i];
        if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
          if (field.type == 'select-multiple') {
            l = form.elements[i].options.length;
            res[field.name] = [];
            for (var j=0; j<l; j++) {
              if(field.options[j].selected)
                res[field.name].push(field.options[j].value);
            }
          } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
            res[field.name] = field.value;
          }
        }
      }
    }
    return res;
  }

  static dataURItoBlob(dataURI, fileName) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var theBlob = new Blob([ia], {type: mimeString});

    // I had to attach filename prop so Pedja knows the extenstion
    theBlob.filename = fileName;
    return theBlob;
  }

  static updateUrlParams(data) {
    let newUrl = window.location.search === '' ? '?' : window.location.search + (window.location.search.substring(window.location.search.length - 1) === '&' ? '' : '&');
    for (let prop in data) {
      let regExp = new RegExp(prop + "(.+?)(&|$)", "g");
      let param = data[prop] !== null && data[prop] !== '' ? `${prop}=${data[prop]}&` : '';
      newUrl = regExp.test(newUrl) ? newUrl.replace(regExp, param) : newUrl + param;
    }
    return newUrl;
  }

  static async handleResponse (context, response, callbackFunction = () => 0, successMessage = 'Success!') {
    if (!response || response.error) {
      context.addNotification({title: 'Error', message: response.error ? response.error.message : 'Request failed'});
    } else {
      context.addNotification({title: 'Success', message: response.message ? response.message : successMessage});
      await callbackFunction();
    }
    context.setState({loading: false});
  }
}