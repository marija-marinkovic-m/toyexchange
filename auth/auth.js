const bodyParser = require('body-parser');
const session = require('express-session');
const FileStore = require('session-file-store')(session);
const csrf = require('lusca').csrf();

const fetch = require('isomorphic-fetch');
const API_URI = require('../static/config').API_URI;
const clientId = require('../static/config').clientId;

const ObjectAssign = require('object-assign');

const checkResponse = (response) => {
  if (response.status >= 200 && response.status < 300) {
      return response.json();
    }
    var error = new Error(response.statusText);
    error.response = response;
    throw error;
}

process.env.SESS_PATH = process.env.SESS_PATH || './sessions';

exports.configure = ({
  app = null, // nextjs App
  server = null, // express server
  secret = 'AXy8&8pp',
  store = new FileStore({path: process.env.SESS_PATH, secret: secret}),
  maxAge = 6000 * 60 * 24 * 7 * 4,
  clientMaxAge = 60000,
  serverUrl = null
} = {}) => {
  if (app === null) {
    throw new Error('app option must be a next server instance');
  }

  // load body parser to handle POST requests 
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({extended: true}));

  // config sessions
  server.use(session({
    store: store,
    secret: secret,
    resave: false,
    rolling: true,
    saveUninitialized: true,
    httpOnly: true,
    cookie: {
      maxAge: maxAge
    }
  }));

  // add CSRF to all POST requests 
  server.use((req, res, next) => {
    csrf(req, res, next);
  });

  // login user and update current session
  server.post('/auth/login', (req, res) => {
    // perform login to api, get tokens than send my/profile 
    // request and save all the data to session
    // {user: data: {..data}, auth: {...tokens}}
    let authData;
    let userData;

    return fetch(API_URI + '/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: req.body.username,
        password: req.body.password,
        clientId: clientId
      })
    })
      .then(checkResponse)
      .then(response => {
        authData = response;
        return fetch(API_URI + '/my/profile', {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + authData.accessToken
          }
        });
      })
      .then(checkResponse)
      .then(response => {
        userData = response.data;
        req.session.user = {
          username: userData.displayName || userData.email,
          data: userData,
          auth: authData
        };

        req.session.resession = true; // tell ssrCache to act
        return res.status(200).json(req.session);
      })
      .catch(error => {
        console.log('Request failed', error);
        req.session.user = null;
        req.session.resession = true; // tell ssrCache to act
        return res.status(200).json(req.session);
      });
  });

  // signup user
  server.post('/auth/signup', (req, res) => {
    // post email,password,clientId to /signup
    // get accessToken, accessTokenExpiration, refreshToken and id back
    // get newly created userData and store to session
    let authData;
    let userData;

    let requestBody = {
      email: req.body.email,
      password: req.body.password,
      clientId
    };

    if (req.body.code && req.body.code !== '') {
      requestBody.code = req.body.code;
    }

    return fetch(API_URI + '/signup', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(requestBody)
    })
      .then(checkResponse)
      .then(response => {
        authData = response;
        return fetch(API_URI + '/my/profile', {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + authData.accessToken
          }
        });
      })
      .then(checkResponse)
      .then(response => {
        userData = response.data;
        req.session.user = {
          username: req.body.email,
          data: userData,
          auth: authData
        };

        req.session.resession = true;
        return res.status(200).json(req.session);
      })
      .catch(error => {
        console.log('Request failed', error);
        req.session.user = null;
        req.session.resession = true;
        return res.status(200).json(req.session);
      });
  });

  // logout and update server session
  server.post('/auth/logout', (req, res) => {
    if (!req.session || !req.session.user || !req.session.user.auth) {
      return res.status(400).json({success: false});
    }
    return fetch(API_URI + '/logout', {
      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + req.session.user.auth.accessToken
      }
    })
      .then(checkResponse)
      .then(response => {
        console.log(response);
        req.session.user = null;
        req.session.resession = true; // tell ssrCache to act
        res.status(200).json({sucess: true});
      })
      .catch(error => {
        console.log('Request failed', error);
        req.session.user = null;
        req.session.resession = true; // tell ssrCache to act
        res.status(400).json({sucess: false});
      });
  });

  // add route to get CSRF token via AJAX 
  server.get('/auth/csrf', (req, res) => {
    return res.json({csrfToken: res.locals._csrf});
  });

  // return session info 
  server.get('/auth/session', (req, res) => {
    let _session = {
      clientMaxAge: clientMaxAge,
      csrfToken: res.locals._csrf,
      user: req.session.user || 'no user'
    };
    return res.json(_session);
  });

  // update session user data
  server.post('/auth/update-session-user-data', (req, res) => {
    return fetch(API_URI + '/my/profile', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + req.body._accessToken
        }
      })
      .then(checkResponse)
      .then(response => {
        console.log('AAAAUUUUUUUTTTTTHH')
        console.log(response);
        req.session.user = ObjectAssign({}, req.session.user, {
          data: response.data,
          username: response.data.displayName || response.data.email
        });
        return res.status(200).json({
          user: req.session.user,
          csrfToken: res.locals._csrf,
          clientMaxAge: clientMaxAge
        });
      })
      .catch(error => {
        console.log('Requset failed', error);
        return res.status(400).json({error: {message: 'Request Failed'}});
      });
  });

  // refresh the token 
  server.post('/auth/refresh-token', (req, res) => {
    return fetch(API_URI + '/refresh-token', {
      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + req.body._accessToken,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        refreshToken: req.body._refreshToken,
        clientId: clientId
      })
    })
    .then(checkResponse)
    .then(response => {
      req.session.user.auth = response;
      return res.status(200).json({
        user: req.session.user,
        csrfToken: res.locals._csrf,
        clientMaxAge: clientMaxAge
      });
    })
    .catch(error => {
      console.log('Request failed', error);
      return res.status(400).json({error: {message: 'Request Failed'}});
    });
  });

  // update transient data
  server.post('/auth/update-transient-data', (req, res) => {
    if (!req.body._prop || !req.body._value) return res.status(400).json({error: {message: 'Request Failed. No Prop nor Value specified.'}});

    req.session.transient = ObjectAssign({}, req.session.transient, {[req.body._prop]: req.body._value});
    return res.status(200).json({transient: req.session.transient});
  });
};