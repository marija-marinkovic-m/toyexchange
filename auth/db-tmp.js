const records = [
  {
    'pickup_window': 'Friday, March 27, 2015 5:33 PM',
    'description': 'Aute mollit fugiat ipsum proident ut sint officia qui Lorem proident. Et labore excepteur sint anim fugiat ullamco eiusmod minim magna nisi eu exercitation. Cupidatat cupidatat magna Lorem adipisicing irure duis id cillum aliquip. Enim mollit non reprehenderit irure voluptate qui. Laboris ea consequat culpa cillum Lorem excepteur aliquip laborum culpa minim veniam excepteur. Non mollit fugiat nisi pariatur commodo sint quis occaecat id non amet cillum. Eiusmod nulla consectetur minim qui dolor mollit consectetur irure incididunt dolor.',
    'title': 'incididunt enim sunt',
    'post_type': 'offer',
    'age_range': 2,
    'picture': '/static/toys/toy-1.jpg',
    'id': '58c152bd2832f325ba73fd4b'
  },
  {
    'pickup_window': 'Monday, December 5, 2016 10:17 PM',
    'description': 'Dolor cupidatat nostrud id nostrud excepteur reprehenderit aliquip ea qui elit aute exercitation. Nulla amet voluptate et esse elit tempor non ea. Voluptate eiusmod tempor pariatur ex et deserunt ex adipisicing elit consectetur ea dolor.',
    'title': 'tempor quis cupidatat',
    'post_type': 'wanted',
    'age_range': 9,
    'picture': '/static/toys/toy-2.jpg',
    'id': '58c152bd976dd23feddf410b'
  },
  {
    'pickup_window': 'Wednesday, July 27, 2016 7:49 PM',
    'description': 'Qui irure qui incididunt duis culpa irure consectetur laboris laborum ex pariatur. Ullamco ex eu laborum eu velit est minim do fugiat eiusmod velit commodo in. Ex nostrud adipisicing incididunt veniam est dolor exercitation occaecat do non pariatur esse. Ex ad et sit veniam tempor id. Sit reprehenderit adipisicing do enim sit. Nulla exercitation sint adipisicing nisi aute qui enim. Sit ea cupidatat ad aliquip laborum aliquip cillum ea laboris veniam voluptate.',
    'title': 'irure nostrud velit',
    'post_type': 'wanted',
    'age_range': 2,
    'picture': '/static/toys/toy-3.jpg',
    'id': '58c152bedb9efbdf08459e0f'
  },
  {
    'pickup_window': 'Wednesday, April 13, 2016 3:40 PM',
    'description': 'Dolor nisi duis incididunt voluptate magna esse labore ad commodo velit. Sunt deserunt incididunt irure fugiat nostrud ullamco duis anim culpa qui Lorem ad voluptate. Excepteur magna sint duis veniam velit et ad duis eu nisi magna reprehenderit enim ea. Nulla voluptate sit irure ut ullamco ut excepteur. Voluptate cillum aliquip et ipsum. Irure proident cillum anim veniam culpa ex occaecat ad qui ea.',
    'title': 'ex voluptate consequat',
    'post_type': 'wanted',
    'age_range': 10,
    'picture': '/static/toys/toy-4.jpg',
    'id': '58c152beed15dd43f0547f1a'
  },
  {
    'pickup_window': 'Monday, June 8, 2015 5:50 PM',
    'description': 'Et minim consequat consectetur esse amet. Id eu nulla nostrud culpa eu in id ullamco eu et duis voluptate occaecat. Occaecat non aliquip nisi elit laborum. Aliquip ad dolore laboris ea occaecat amet reprehenderit exercitation non nulla ipsum laborum cillum. Laboris amet reprehenderit dolore non ad ullamco labore ut dolor. Anim in nisi officia velit elit aute sint fugiat ut magna proident occaecat consequat.',
    'title': 'occaecat aute magna',
    'post_type': 'offer',
    'age_range': 1,
    'picture': '/static/toys/toy-5.jpg',
    'id': '58c152be4e65c8104afe9f8a'
  },
  {
    'pickup_window': 'Thursday, December 3, 2015 4:01 PM',
    'description': 'Aliquip ut et minim voluptate irure sunt incididunt fugiat excepteur culpa proident irure minim. Amet nostrud ex consequat nulla ullamco aliqua tempor duis reprehenderit nisi. Esse amet excepteur quis excepteur nostrud sunt irure et Lorem labore. Consectetur id dolor reprehenderit eu aute.',
    'title': 'mollit laborum adipisicing',
    'post_type': 'offer',
    'age_range': 7,
    'picture': '/static/toys/toy-6.jpg',
    'id': '58c152be122c31b6742cc3d1'
  },
  {
    'pickup_window': 'Sunday, October 23, 2016 9:40 PM',
    'description': 'Quis adipisicing id cillum ut. Est consequat veniam deserunt sint. Adipisicing reprehenderit consectetur aute amet magna eu. Dolore sit officia id minim est aute nostrud cillum voluptate.',
    'title': 'magna proident sit',
    'post_type': 'wanted',
    'age_range': 9,
    'picture': '/static/toys/toy-7.jpg',
    'id': '58c152be6347144d754880f3'
  },
  {
    'pickup_window': 'Monday, May 16, 2016 1:34 PM',
    'description': 'Ut id irure voluptate ut aliquip eiusmod. Sint aliqua quis aliqua nostrud amet sit velit. Sit occaecat eu veniam ullamco qui do id officia nisi pariatur duis velit. Nostrud ullamco enim ut voluptate sunt anim Lorem labore amet eu et. Exercitation sit consequat reprehenderit ea elit est ad reprehenderit ea do labore fugiat aliquip ut.',
    'title': 'esse reprehenderit ullamco',
    'post_type': 'offer',
    'age_range': 10,
    'picture': '/static/toys/toy-8.jpg',
    'id': '58c152be2f0aea89d7a71f12'
  },
  {
    'pickup_window': 'Tuesday, May 10, 2016 1:53 PM',
    'description': 'Ad dolore veniam voluptate mollit dolore exercitation labore dolor ex non. Do consequat esse voluptate qui. Aliqua eu officia Lorem ullamco. Officia laborum cillum labore occaecat nulla officia eu. Magna mollit consectetur id ex dolore in et esse adipisicing in eiusmod quis ad.',
    'title': 'in ea quis',
    'post_type': 'wanted',
    'age_range': 9,
    'picture': '/static/toys/toy-9.jpg',
    'id': '58c152be1d2d66c840acac1d'
  },
  {
    'pickup_window': 'Monday, April 11, 2016 2:06 AM',
    'description': 'Officia ea do irure ad proident incididunt et cillum laborum reprehenderit. Ipsum est adipisicing laborum occaecat dolor. Eu irure nulla pariatur officia. Adipisicing nisi dolore aliqua enim est ad occaecat fugiat officia ex sint dolor aliqua.',
    'title': 'incididunt anim aute',
    'post_type': 'offer',
    'age_range': 4,
    'picture': '/static/toys/toy-10.jpg',
    'id': '58c152bede597ed043442693'
  },
  {
    'pickup_window': 'Tuesday, April 15, 2014 10:49 PM',
    'description': 'Cillum ad officia occaecat occaecat reprehenderit dolor excepteur quis ad minim. Incididunt minim sint dolor irure consequat mollit incididunt aute duis dolor fugiat nostrud ea. Laboris Lorem ut enim velit nulla aute eiusmod fugiat excepteur aute voluptate tempor dolor.',
    'title': 'consectetur est sit',
    'post_type': 'wanted',
    'age_range': 9,
    'picture': '/static/toys/toy-11.jpg',
    'id': '58c152bec9346d07ff199187'
  },
  {
    'pickup_window': 'Sunday, March 29, 2015 8:36 AM',
    'description': 'Fugiat cupidatat occaecat eiusmod cillum qui et nisi esse incididunt eiusmod ad. Eiusmod nostrud enim ex fugiat incididunt officia deserunt irure enim irure. Deserunt consequat enim qui aliquip veniam pariatur ipsum duis. Tempor mollit amet labore qui ex reprehenderit ea irure laborum ullamco consectetur id commodo. Cillum proident ea sunt amet mollit sit enim eu.',
    'title': 'exercitation laboris enim',
    'post_type': 'offer',
    'age_range': 0,
    'picture': '/static/toys/toy-12.jpg',
    'id': '58c152be32c84226a876d2bc'
  },
  {
    'pickup_window': 'Wednesday, February 11, 2015 5:06 AM',
    'description': 'Nostrud ad quis tempor exercitation. Reprehenderit consequat tempor labore Lorem ipsum qui esse do cupidatat labore ad id aliqua minim. Culpa reprehenderit ullamco et cillum consequat nisi laboris. Consequat amet nulla commodo ex eiusmod id ad laboris commodo elit exercitation exercitation.',
    'title': 'in nostrud culpa',
    'post_type': 'wanted',
    'age_range': 7,
    'picture': '/static/toys/toy-13.jpg',
    'id': '58c152bef3933c2a076224b2'
  },
  {
    'pickup_window': 'Wednesday, January 14, 2015 6:42 AM',
    'description': 'Aute duis id sunt consectetur id nisi veniam eu sint in. Et elit non nostrud aute minim aliquip et ad sit Lorem sint. Exercitation aute eu pariatur consequat sit esse est aliqua mollit. Duis enim excepteur enim fugiat ex irure exercitation tempor. Mollit sit excepteur ea ex in commodo commodo. Enim culpa tempor sunt esse pariatur cillum exercitation laborum proident velit et ipsum velit commodo.',
    'title': 'fugiat irure duis',
    'post_type': 'offer',
    'age_range': 9,
    'picture': '/static/toys/toy-14.jpg',
    'id': '58c152be54092277cc1c55e7'
  },
  {
    'pickup_window': 'Tuesday, September 1, 2015 9:29 AM',
    'description': 'Sunt consequat dolore consectetur mollit velit aliqua nostrud elit. Adipisicing ea nisi pariatur pariatur dolor reprehenderit nostrud sint deserunt fugiat. Adipisicing proident nisi laborum veniam esse commodo magna fugiat cillum ea quis Lorem fugiat.',
    'title': 'cillum elit adipisicing',
    'post_type': 'wanted',
    'age_range': 8,
    'picture': '/static/toys/toy-15.jpg',
    'id': '58c152beaa2dede6ef466635'
  }
];

const messages = [
  {
    'updated_at': 'Monday, July 11, 2016 9:13 PM',
    'created_at': 'Saturday, November 22, 2014 7:33 PM',
    'deleted_at': 'Tuesday, November 18, 2014 8:12 AM',
    'content': 'Lorem veniam eiusmod ex sunt ad labore dolor esse ad qui. Esse eu id pariatur magna non nulla irure proident consectetur anim. Adipisicing esse nisi deserunt fugiat esse consectetur aliqua sunt enim. Labore enim sit nostrud culpa sunt incididunt deserunt excepteur ut.\n\nEt irure fugiat labore dolor nulla ex Lorem magna aliquip adipisicing elit ut magna. Ut deserunt occaecat voluptate voluptate nostrud duis. Enim laboris fugiat adipisicing et deserunt qui reprehenderit consectetur eu pariatur sit laborum. Consequat exercitation excepteur sunt culpa esse nostrud consectetur laboris.',
    'subject': 'Apextri',
    'recipient_id': 1,
    'sender_id': 1,
    'is_deleted_recipient': true,
    'is_deleted_sender': true,
    'is_read': false,
    'post_id': 2344,
    'id': '58ea46fa78097241f988802f'
  },
  {
    'updated_at': 'Tuesday, February 9, 2016 8:43 PM',
    'created_at': 'Sunday, December 25, 2016 9:51 PM',
    'deleted_at': 'Sunday, June 12, 2016 11:44 PM',
    'content': 'Ad irure sit non duis aliqua non commodo sit. Incididunt ad sit nisi laboris. Amet nisi reprehenderit nostrud reprehenderit duis ad cillum. Reprehenderit incididunt id sunt sit dolore consectetur nostrud enim anim eu. Dolore aliqua consectetur adipisicing reprehenderit amet adipisicing ut duis exercitation velit aliqua velit. Quis labore cillum eu velit aliqua est laborum officia anim dolor.\n\nLorem sunt qui cillum nostrud reprehenderit ut proident incididunt fugiat ex exercitation ut. Nulla laborum non reprehenderit occaecat tempor in. Mollit ex sunt eiusmod sint ea magna. Laboris exercitation consectetur adipisicing sint dolore officia. Excepteur dolor magna cupidatat non quis reprehenderit minim aliqua non qui voluptate mollit aliquip consequat. Velit incididunt mollit nostrud est voluptate.',
    'subject': 'Snips',
    'recipient_id': 1,
    'sender_id': 2,
    'is_deleted_recipient': false,
    'is_deleted_sender': false,
    'is_read': false,
    'post_id': 2344,
    'id': '58ea46faaec36c0f543500fe'
  },
  {
    'updated_at': 'Monday, February 24, 2014 7:44 AM',
    'created_at': 'Friday, December 2, 2016 10:07 PM',
    'deleted_at': 'Saturday, June 4, 2016 1:08 AM',
    'content': 'Cupidatat sint quis fugiat exercitation consectetur pariatur ad id. Mollit anim do sint culpa aute fugiat elit sunt sit. Occaecat in adipisicing culpa nostrud deserunt. Fugiat magna commodo velit in irure veniam deserunt cupidatat culpa Lorem non excepteur aliqua. Sunt dolore commodo amet incididunt adipisicing labore laboris esse elit veniam eu officia cillum qui. Enim reprehenderit labore exercitation do occaecat. Nostrud voluptate ipsum in proident velit voluptate consequat et nisi.\n\nElit enim aute reprehenderit non tempor ullamco enim ullamco in ut ipsum velit sit. Nulla aliqua consequat ea in culpa laborum eu nisi. Velit in in ad aliqua cupidatat qui cupidatat sint eu ipsum eu commodo nostrud. Ut Lorem eu id quis et aliqua id minim quis non. Eiusmod aute veniam nulla consectetur sunt consectetur. Aliquip do quis et non consectetur mollit labore.',
    'subject': 'Ontality',
    'recipient_id': 2,
    'sender_id': 1,
    'is_deleted_recipient': true,
    'is_deleted_sender': false,
    'is_read': false,
    'post_id': 2344,
    'id': '58ea46fa121a3cb81b3bac9a'
  },
  {
    'updated_at': 'Friday, January 29, 2016 5:02 PM',
    'created_at': 'Thursday, November 26, 2015 2:20 PM',
    'deleted_at': 'Tuesday, August 12, 2014 4:33 PM',
    'content': 'Eiusmod ea consequat duis minim culpa cupidatat deserunt excepteur. In cupidatat adipisicing esse exercitation dolor ad esse elit nisi sunt labore sint irure. In qui nisi adipisicing minim quis. Dolor laborum in commodo ex qui eu aliquip. Commodo anim quis incididunt proident aute laboris in incididunt.\n\nEu qui ad duis dolor culpa ullamco Lorem. Magna commodo reprehenderit aliquip sunt ea deserunt irure elit est occaecat nisi voluptate voluptate sunt. Veniam sit deserunt nisi cillum ex consequat proident sit sint nulla laboris magna duis excepteur. Occaecat ea ullamco sint sit nostrud dolore veniam fugiat deserunt ex. Laborum nisi nulla ut fugiat excepteur ullamco id pariatur amet exercitation minim qui laboris eu. Occaecat Lorem veniam et culpa dolor aliqua deserunt commodo eu ut eu incididunt occaecat. Ex in culpa voluptate aliquip tempor fugiat.',
    'subject': 'Magmina',
    'recipient_id': 1,
    'sender_id': 2,
    'is_deleted_recipient': false,
    'is_deleted_sender': false,
    'is_read': true,
    'post_id': 2344,
    'id': '58ea46fa231b1d152a587579'
  },
  {
    'updated_at': 'Sunday, January 4, 2015 1:03 PM',
    'created_at': 'Friday, October 3, 2014 9:12 AM',
    'deleted_at': 'Friday, May 15, 2015 10:08 AM',
    'content': 'Duis Lorem laborum elit minim nostrud elit reprehenderit in. Excepteur est officia labore nulla magna aliquip ipsum. Sunt magna reprehenderit exercitation incididunt do irure officia. Laborum ea veniam veniam esse ad. Est mollit qui amet cillum voluptate culpa ad fugiat proident irure ullamco. Cillum laborum cupidatat voluptate ullamco laboris enim dolor et nulla anim. Esse fugiat pariatur reprehenderit consequat ipsum laboris cupidatat duis laborum occaecat sunt magna.\n\nVeniam in veniam occaecat ut officia labore ex adipisicing velit. Reprehenderit enim aliqua deserunt fugiat qui cupidatat ut dolor veniam cupidatat irure voluptate. Anim ad et officia irure consectetur non dolor nisi commodo enim mollit commodo fugiat. Exercitation Lorem eu laborum esse velit irure Lorem non Lorem consequat. Id culpa proident sint dolor sunt reprehenderit nostrud duis tempor aliquip magna ea et. Ullamco eu fugiat enim duis laboris velit sunt aute dolor laboris veniam proident officia aliquip.',
    'subject': 'Niquent',
    'recipient_id': 1,
    'sender_id': 2,
    'is_deleted_recipient': false,
    'is_deleted_sender': false,
    'is_read': false,
    'post_id': 2344,
    'id': '58ea46fa5c4e7c2ece25133b'
  },
  {
    'updated_at': 'Saturday, February 21, 2015 6:46 PM',
    'created_at': 'Tuesday, January 28, 2014 12:31 AM',
    'deleted_at': 'Monday, July 20, 2015 5:29 AM',
    'content': 'Cupidatat quis enim eiusmod exercitation tempor fugiat cupidatat commodo. Tempor laboris ut ipsum tempor cupidatat do consectetur officia reprehenderit excepteur. In laborum aliquip sint aute ipsum. Cupidatat nulla nulla labore incididunt duis amet excepteur dolore ea reprehenderit aute. Id nulla id labore culpa irure culpa id. Esse dolore commodo adipisicing fugiat reprehenderit dolore esse eu duis cupidatat in dolore elit. Cupidatat consequat magna id aliqua mollit.\n\nAute et eiusmod ullamco reprehenderit eiusmod adipisicing do anim eiusmod. Voluptate enim fugiat occaecat consequat amet ipsum veniam incididunt est laborum aliquip. Minim cupidatat voluptate fugiat ea proident ut in tempor.',
    'subject': 'Mantrix',
    'recipient_id': 1,
    'sender_id': 2,
    'is_deleted_recipient': true,
    'is_deleted_sender': true,
    'is_read': false,
    'post_id': 2344,
    'id': '58ea46fa550e18a6ae1e1d03'
  },
  {
    'updated_at': 'Thursday, February 16, 2017 3:12 PM',
    'created_at': 'Friday, November 18, 2016 3:23 AM',
    'deleted_at': 'Thursday, November 10, 2016 5:47 AM',
    'content': 'Do id incididunt velit anim sit exercitation voluptate nostrud proident officia Lorem do velit. Sit dolore in aliquip in sunt sunt in aute tempor et ullamco nulla commodo Lorem. Irure id cupidatat ipsum veniam voluptate nostrud consectetur cillum. Aliqua sit ipsum ex nostrud aliquip enim anim aliqua quis.\n\nAnim labore ea eiusmod consequat officia irure ut. Non commodo commodo anim reprehenderit. Duis eiusmod quis ad exercitation tempor cillum deserunt non irure incididunt proident veniam amet. Elit laborum sunt exercitation irure voluptate esse. Id in qui magna cillum non ipsum sint in adipisicing laboris culpa. Laboris voluptate nulla eu dolore qui reprehenderit excepteur proident Lorem ad et et magna.',
    'subject': 'Duoflex',
    'recipient_id': 1,
    'sender_id': 2,
    'is_deleted_recipient': true,
    'is_deleted_sender': true,
    'is_read': false,
    'post_id': 2344,
    'id': '58ea46fa03452997b0356240'
  },
  {
    'updated_at': 'Friday, January 22, 2016 10:30 AM',
    'created_at': 'Saturday, May 7, 2016 1:50 PM',
    'deleted_at': 'Thursday, December 18, 2014 1:56 PM',
    'content': 'Laborum reprehenderit incididunt magna et eiusmod est adipisicing id consequat nostrud elit culpa enim sint. Sint deserunt commodo ad fugiat enim culpa excepteur irure veniam id ut duis. Excepteur proident aliquip nostrud dolore excepteur laborum. Cillum reprehenderit sint sint velit id ad non culpa aliquip pariatur irure est. Consectetur ipsum irure pariatur officia excepteur minim consectetur sit labore. Enim excepteur excepteur ad Lorem voluptate consectetur tempor officia nostrud. Amet occaecat Lorem cupidatat consectetur fugiat esse ullamco non culpa adipisicing cillum.\n\nConsectetur aute esse aliqua in nisi in id. Cillum dolor dolor dolore enim. Id elit Lorem cillum dolor sint veniam esse aliquip deserunt sunt adipisicing quis. Qui anim cillum sint et voluptate do ex esse reprehenderit nostrud minim sit consectetur.',
    'subject': 'Quantalia',
    'recipient_id': 2,
    'sender_id': 1,
    'is_deleted_recipient': true,
    'is_deleted_sender': true,
    'is_read': false,
    'post_id': 2344,
    'id': '58ea46fa0fe67b5b3abbac05'
  },
  {
    'updated_at': 'Monday, August 18, 2014 3:15 PM',
    'created_at': 'Monday, June 1, 2015 11:57 PM',
    'deleted_at': 'Friday, March 25, 2016 12:51 PM',
    'content': 'Enim magna dolore laborum ullamco. Occaecat ipsum eiusmod mollit laborum esse incididunt pariatur. Ullamco officia qui elit deserunt proident fugiat voluptate reprehenderit laborum pariatur. Proident tempor magna in labore in tempor tempor voluptate eu est tempor ullamco esse ullamco. Veniam dolore pariatur aliqua enim dolor mollit ea adipisicing.\n\nExcepteur velit pariatur sint aute minim. Nisi quis aute irure deserunt voluptate ipsum in enim ut duis deserunt exercitation eiusmod nostrud. Elit velit eu cillum laboris deserunt. Id tempor eu aliquip sunt do deserunt pariatur anim consequat ut fugiat excepteur duis ullamco. Nisi anim dolore quis consectetur ut eiusmod nostrud occaecat cillum aute eiusmod dolor. Ut labore non veniam qui consectetur magna cupidatat irure pariatur quis nisi.',
    'subject': 'Ewaves',
    'recipient_id': 2,
    'sender_id': 1,
    'is_deleted_recipient': false,
    'is_deleted_sender': false,
    'is_read': false,
    'post_id': 2344,
    'id': '58ea46fa28723a642c850250'
  }
];

exports.findById = async function(id) {
  return records.find(element => element.id === id);
};

exports.filterByType = async function(type) {
 return records.filter(item => item.post_type === type);
}

exports.getAll = async function() {
  return records;
}

exports.getMessages = function() {
  return messages;
};