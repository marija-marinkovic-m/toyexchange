import React, { Component } from 'react';
import ObjectAssign from 'object-assign';

import Users from '../../api/admin/users';

export default class ButtonDownloadUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
    this.handleClick = this.handleClick.bind(this);
  }
  componentDidMount() {
    if (window.downloadFile) return;
    
    window.downloadFile = function (sUrl, fileName) {

      //iOS devices do not support downloading. We have to inform user about this.
      if (/(iP)/g.test(navigator.userAgent)) {
        //alert('Your device does not support files downloading. Please try again in desktop browser.');
        window.open(sUrl, '_blank');
        return false;
      }

      //If in Chrome or Safari - download via virtual link click
      if (window.navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || window.navigator.userAgent.toLowerCase().indexOf('safari') > -1) {
          //Creating new link node.
          var link = document.createElement('a');
          link.href = sUrl;
          link.setAttribute('target','_blank');

          if (link.download !== undefined) {
              //Set HTML5 download attribute. This will prevent file from opening if supported.
              // var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
              link.download = fileName;
          }

          //Dispatching click event.
          if (document.createEvent) {
              var e = document.createEvent('MouseEvents');
              e.initEvent('click', true, true);
              link.dispatchEvent(e);
              return true;
          }
      }

      // Force file download (whether supported by server).
      if (sUrl.indexOf('?') === -1) {
          sUrl += '?download';
      }

      window.open(sUrl, '_blank');
      return true;
    }
  }
  async handleClick(e) {
    e.preventDefault();
    this.setState({loading: true})

   
    Users.download()
      .then(data => {
        return data.text();
      })
      .then(csv => {
        const csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
        this.setState({loading: false})
        window.downloadFile(csvData, 'toy-cycle-users.csv');
      });
  }
  render() {
    const { className, ...other } = this.props;
    const classNames = className ? 'btn btn-info ' + className : 'btn btn-info';
    return (
      <button onClick={this.handleClick} type="button" className={classNames} {...other}>Download users{this.state.loading ? <span>&nbsp;&nbsp;<i className="fa fa-spin fa-circle-o-notch"></i></span> : ''}</button>
    );
  }
}