import React, { Component } from 'react';
import reactCSSTransitionGroup from 'react-addons-css-transition-group';
import cn from 'classnames';
import ObjectAssign from 'object-assign';
import FlipMove from 'react-flip-move';

export default ({onClick, loading = false}) => {
  return (
    <div className="fixed-action-btn">
      <button disabled={loading} onClick={onClick} className="btn-floating btn-large"><i className="pe-7s-close"></i></button>
    </div>
  );
}

class FloatingMenuItem extends Component {
  handleClick() {
    this.props.action();
  }
  render() {
    return <button disabled={this.props.loading} onClick={this.handleClick.bind(this)}
      className="floating-menu-item">
      {this.props.label && <label>{this.props.label}</label>}
      <div className={cn(['floating-menu-icon', ...this.props.className.split(' ')])}><i className={this.props.icon}></i></div>
    </button>;
  }
}
FloatingMenuItem.defaultProps = {
  label: '',
  icon: 'pe-7s-close',
  action: () => console.log('action not defined'),
  className: '',
  loading: false
};

export class FloatingMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggled: false
    };
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleToggle() {
    this.setState({toggled: !this.state.toggled});
  }
  render() {
    let mainBtn = ObjectAssign({}, this.props.main, {action: this.props.main.action || this.handleToggle});
    let buttons = this.state.toggled ? this.props.items : [];
    return (
      <div className="fab-container">
        <div className={cn({'floating-menu': true, 'open': this.state.toggled})}>
          <FlipMove 
            enterAnimation="elevator"
            leaveAnimation={null}
            duration={450} easing="ease-in">
            {buttons.map((b, i) => <div key={i} className="sub-items"><FloatingMenuItem loading={this.props.loading} {...b} /></div>)}
          </FlipMove>
          <FloatingMenuItem className="main-btn" {...mainBtn} />
        </div>
      </div>
    );
  }
}
FloatingMenu.defaultProps = {
  main: {label: '', icon: 'pe-7s-close', action: null},
  items: [],
  loading: false
}

/**
 * <FloatingMenu main={{label: '', icon='fa fa-circle-o-notch'}} items={[{label: '', icon='fa fa-pen', action={() => console.log('fapen')}}, {label: 'trash', icon='fa fa-trash'} action={() => console.log('fatrash')}]} />
 */

