import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import ObjectAssign from 'object-assign';
import { Util } from '../../api/util';

export default class FileUploader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgSrc: null
    }
    this.handleFile = this.handleFile.bind(this);
  }

  handleFile(acceptedFiles, rejectedFiles) {
    this.setState({
      imgSrc: null,
      defaultSrc: null,
      fileName: ''
    }, () => {
      if (acceptedFiles.length) {
        const imgSrc = acceptedFiles[0].preview;
        const fileName = acceptedFiles[0].name;

        this.props.handleFile(acceptedFiles[0], imgSrc);
        this.setState({imgSrc, fileName});
      }
    });
  }

  componentDidMount() {
    this.setState({defaultSrc: this.props.defaultSrc})
  }

  render() {
    const { defaultSrc } = this.state;

    if (defaultSrc) {
      return(
        <div className="default-src-dropzone">
          <img src={this.state.defaultSrc} />
          <Dropzone multiple={this.props.multiple} accept={this.props.accept} maxSize={this.props.maxSize} onDrop={this.handleFile} className="text-center d-flex align-items-center justify-content-center" activeClassName="alert alert-success" rejectClassName="alert alert-warning">
            <button className="btn btn-primary" type="button" title="Upload another image"><i className="fa fa-upload"></i></button>
          </Dropzone>
        </div>
      );
    }

    const { multiple, accept, maxSize, uploadLabel } = this.props;
    return(
      <div className="main-placeholder">
        <Dropzone multiple={multiple} accept={accept} maxSize={maxSize} onDrop={this.handleFile} className="dropzone-custom text-center" activeClassName="alert alert-success" rejectClassName="alert alert-warning">
          {({ isDragActive, isDragReject, acceptedFiles, rejectedFiles }) => {
            if (isDragActive) {
              return <i className="fa fa-check-circle rounded-ico lg nb" aria-hidden="true"></i>;
            }
            if (isDragReject) {
              return <i className="fa fa-times-circle rounded-ico lg nb" aria-hidden="true"></i>;
            }
            if (rejectedFiles.length) {
              return <div style={{margin: 0, height: '100%', display: 'block'}} className="alert alert-danger" role="alert"><strong>Rejected file.</strong> Max file size (1MB) exceeded. Try Again.</div>;
            }
            if (this.state.imgSrc) {
              return (
                <div className="preview-placeholder" style={{ backgroundColor: 'white', backgroundImage: `url(${this.state.imgSrc})`, width: '100%', height: '100%', backgroundSize: 'contain' }}>
                  <button className="btn btn-primary" type="button"><i className="fa fa-upload"></i></button>
                </div>
              );
            }

            return (
              <div>
                <i className="fa fa-camera rounded-ico lg d-block mx-auto mb-3 text-muted" aria-hidden="true"></i>
                <a className="text-muted">{uploadLabel}</a>
              </div>
            );
          }}
        </Dropzone>
      </div>
    );
  }
}

FileUploader.defaultProps = {
  uploadLabel: 'Upload Image',
  handleFile: (blob, base64src) => false,
  handleRemoveFile: () => false,
  defaultSrc: null,
  accept: 'image/*',
  maxSize: 10485760,
  multiple: false
}