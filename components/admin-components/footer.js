import React from 'react';

export default function () {
  return (
    <footer className="footer">
      <div className="container-fluid">
        <nav className="pull-left">
          <ul>
            <li>
              <a href="#">Home</a>
            </li>
          </ul>
        </nav>
        <p className="copyright pull-right">&copy; { new Date().getFullYear() } Toy-Exchange.org</p>
      </div>
    </footer>
  )
}