import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import React, { Component } from 'react';
import NProgress from 'nprogress';

import Session from '../../auth/session';

Router.onRouteChangeStart = url => {
  console.log(`Loading ${url}`);
  NProgress.start();
};
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

export default class Header extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <header>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>{this.props.title} | TOY-CYCLE.ORG</title>
          
          <link rel="stylesheet" type="text/css" href="/static/styles/nprogress.css" />

          <link rel="stylesheet" type="text/css" href="/static/styles/admin-styles/bootstrap.min.css" />
          <link rel="stylesheet" type="text/css" href="/static/styles/admin-styles/bootstrap.min.css" />
          <link rel="stylesheet" type="text/css" href="/static/styles/admin-styles/selectize.css" />
          <link rel="stylesheet" type="text/css" href="/static/styles/admin-styles/light-bootstrap-dashboard.css" />
          <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />
          <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,700,300" />
          <link rel="stylesheet" type="text/css" href="/static/styles/admin-styles/pe-icon-7-stroke.css" />
        </Head>
      </header>
    );
  }

}
Header.defaultProps = {
  session: {user: {userName: 'tt'}},
  title: 'Admin Dashboard'
}
