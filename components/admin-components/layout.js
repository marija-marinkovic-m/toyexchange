import React, { Component } from 'react';
import Header from './header';
import Sidebar from './sidebar';
import NavBar from './navbar';
import Footer from './footer';

export default class Layout extends Component {
  render() {
    return (
      <div className="wrapper">
        <Header />
        <Sidebar pathname={this.props.pathname} />

        <div className="main-panel">
          <NavBar pathname={this.props.pathname} csrf={this.props.session.csrfToken} />

          <div className="content">
            <div className="container-fluid">
              <div className="row">
                { this.props.children }
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}
Layout.defaultProps = {
  children: null,
  title: 'Toy-Exchange Admin Dashboard',
  pathname: ''
};