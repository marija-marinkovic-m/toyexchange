import React, { Component } from 'react';
import Link from 'next/link';
import Session from '../../auth/session';

export default class NavBar extends Component {

  handleLogout(e) {
    e.preventDefault();
    const session = new Session();
    session.logout()
      .then(res => window.location.reload())
      .catch(err => console.log(err));
  }

  render() {
    return (
      <nav className="navbar navbar-default navbar-fixed">
        <div className="container-fluid">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            {/*<Link href="/admin"><a className="navbar-brand">Dashboard</a></Link>*/}
          </div>

          <div className="collapse navbar-collapse">
            {/*<ul className="nav navbar-nav navbar-left">
              <li>
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-dashboard"></i>
                </a>
              </li>
            </ul>*/}

            <ul className="nav navbar-nav navbar-right">
              <li>
                <a>
                <form method="post" action="/auth/logout" onSubmit={this.handleLogout}>
                  <input type="_csrf" type="hidden" value={this.props.csrf} />
                  <button className="btn-link" type="submit">Logout</button>
                </form>
                </a>
              </li>
            </ul>
          </div>

        </div>
      </nav>
    );
  }
}
NavBar.defaultProps = {
  pathname: ''
}