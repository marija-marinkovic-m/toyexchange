import Page from './page';
import ObjectAssign from 'object-assign';
import Router from 'next/router';
import { Util } from '../../api/util';

/**
 * @state: {items, filters} required
 * @props: {defaultParams (obj)} optional
 * @method fetchFn required
 */

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      filters: props.defaultParams || {},
      items: {data: null, paginator: null}
    });
  }

  componentDidMount() {
    this.fetchItems();
  }

  componentWillReceiveProps(nextProps) {
    const { pathname, query } = nextProps.url;
    this.setState({filters: ObjectAssign({}, this.state.filters, query)}, () => this.fetchItems());
  }

  async fetchItems(data = null) {
    if (typeof window === 'undefined') return;
    if (data) {
      this.setState({filters: ObjectAssign({}, this.state.filters, data)}, () => Router.push(window.location.pathname + Util.updateUrlParams(data)));
    } else {
      this.setState({items: await this.fetchFn.call(null, this.state.filters)});
    }
  }

  fetchFn(filters) {
    /**
     * To be defined in child class
     */
  }
}