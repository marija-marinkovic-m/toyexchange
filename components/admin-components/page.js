import Page from '../../components/page';

export default class extends Page {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);

    if (!props.isAdmin) {
      // redirect to home if not logged in
      if (typeof window === 'undefined') {
        ctx.res && ctx.res.location('/');
        ctx.res.status(302).end();
      } else {
        location.pathname = '/';
      }
    }

    return props;
  }
}