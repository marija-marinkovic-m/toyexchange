import moment from 'moment';
import Link from 'next/link';
import cn from 'classnames';

import ReactTooltip from 'react-tooltip';

export default ({reportedPosts, handleStatusUpdate = () => false, loading = false}) => (
  <div>
    <table className="table table-hover table-striped layout-fixed">
      <colgroup>
        <col style={{width: '30%'}}></col>
        <col style={{width: '30%'}}></col>
        <col style={{width: '10%'}}></col>
        <col style={{width: '10%'}}></col>
        <col style={{width: '20%'}}></col>
      </colgroup>
      <thead>
        <tr>
          <th>Description</th>
          <th>Reported Toy</th>
          <th>User</th>
          <th>Time</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        { reportedPosts && reportedPosts.length && reportedPosts.map((post, i) => <tr key={i} className={cn({'text-bold': post.status === 'pending'})}>
            <td>
              <span className="text-truncate" data-html={true} data-tip={formatReportData(post)}>
                <span className={cn({
                  label: true, 
                  'label-warning': 'pending' === post.status,
                  'label-danger': 'rejected' === post.status,
                  'label-success': 'accepted' === post.status
                  })}>
                  {post.status}
                </span>&nbsp;{ post.description }
              </span>
            </td>
            <td>
              <Link 
                href={`/admin/reported-post?id=${post.id}`} 
                as={`/admin/reported-post/${post.id}`}>
                <a>{ post.postData && post.postData.title }</a>
              </Link>
            </td>
            <td><span data-html={true} data-tip={formatUserData(post.userData)}>{ post.userData.displayName }</span></td>
            <td>{ moment.utc(post.createdAt).fromNow() }</td>
            <td className="text-right">
              <div className="btn-group no-border">
                <button 
                  disabled={loading || post.status !== 'pending'} 
                  type="button" 
                  className="btn btn-link text-success" 
                  onClick={handleStatusUpdate.bind(null, post.id, 'accepted')}
                  data-tip="Accept">
                  <i className="fa fa-thumbs-o-up"></i>
                </button>
                <a className="btn btn-link"
                  href={`/toy/${post.postData && post.postData.slug}`}
                  target="_blank">
                  <i className="fa fa-external-link"></i>&nbsp;Open ad in new tab
                </a>
                {/*</a>
                <Link 
                  href={`/toy?id=${post.postData.slug}`} 
                  as={`/toy/${post.postData.slug}`}>
                  <button type="button" className="btn btn-link" data-tip="Open Ad"><i className="fa fa-eye"></i></button>
                </Link>
                <button 
                  disabled={loading || post.status !== 'pending'} 
                  type="button" 
                  className="btn btn-link text-danger" 
                  onClick={handleStatusUpdate.bind(null, post.id, 'rejected')}
                  data-tip="Reject">
                  <i className="fa fa-thumbs-o-down"></i>
                </button>*/}
              </div>
            </td>
          </tr>) }
      </tbody>
    </table>
    <ReactTooltip />
  </div>
);

const formatUserData = (user) => `<div>
  <p>${user.firstName} ${user.lastName}, ${user.displayName}</p>
  <span>${user.email}</span>
</div>`;

const formatReportData = (report) => {
  let trimmed = report.description.split(' ').splice(0, 40).join(' ');
  let ellipsis = trimmed.length < report.description.length ? '[...]' : '';
  let title = report && report.postData ? report.postData.title : '';
  let postType = report && report.postData ? report.postData.postType : '';
  let postDataStatus = report && report.postData ? report.postData.status : '';
  return `<div>
    <p>${title}</p>
    <small>Toy type: ${postType} <br />Toy status: ${postDataStatus}</small>
    <p></p>
    <strong>Report status</strong>: ${report.status}<br />
    <span style="display: block; max-width: 200px">${trimmed}&nbsp;${ellipsis}</span>
    <p></p>
  </div>`;
};


export const ReportedPostsWidgetReduced = ({posts}) => (
  <table className="table table-hover table-striped layout-fixed">
    <thead>
      <tr>
        <th>Post</th>
        <th>User</th>
        <th className="text-right"><i className="pe-7s-stopwatch"></i></th>
      </tr>
    </thead>
    <tbody>
      { posts && posts.length ? posts.map((p,i) => <tr key={i}>
        <td>
          <Link href={`/admin/reported-post?id=${p.id}`} as={`/admin/reported-post/${p.id}`}>
            <a>{ p.postData && p.postData.title }</a>
          </Link>
        </td>
        <td>{ p.userData && p.userData.firstName + ' ' + p.userData.lastName + ' - ' + p.userData.displayName }</td>
        <td className="text-right">{ moment.utc(p.createdAt).fromNow() }</td>  
      </tr>) : '' }

      { posts === null && <tr><td colSpan="3"><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</td></tr> }
    </tbody>
  </table>
);