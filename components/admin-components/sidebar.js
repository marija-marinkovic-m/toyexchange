import React, { Component } from 'react';
import Link from 'next/link'

import classnames from 'classnames';

import { LogoLight } from '../logo';

export default class Sidebar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      bgrImage: '' // '/static/img/home-slideshow/books-cup.jpg'
    }
  }

  componentDidMount() {
    const dataColors = ['azure', 'purple', 'blue'];
    const imgNames = ['autos', 'bag-toy', 'chess', 'pexels2photo', 'books-cup', 'lego'];
    const randImgIndex = Math.floor(Math.random() * 5) + 1;
    const randColorIndex = Math.floor(Math.random() * 2) + 1;
    this.setState({
      bgrImage: `/static/img/home-slideshow/${imgNames[randImgIndex]}.jpg`,
      bgrColor: dataColors[randColorIndex]
    });
  }

  render() {
    return (
      <div className="sidebar" data-color={this.state.bgrColor} data-image={this.state.bgrImage}>
        <div className="sidebar-wrapper">
          <div className="text-center">
            <a href="/" target="_blank"><LogoLight /></a>
          </div>

          <ul className="nav">
            <li className={classnames({'active': '/admin' === this.props.pathname})}>
              <Link href="/admin">
                <a>
                  <i className="pe-7s-graph"></i>
                  <p>Dashboard</p>
                </a>
              </Link>
            </li>
            {/*<li className={classnames({'active': '/admin/toys' === this.props.pathname})}>
              <Link href="/admin/toys">
                <a><i className="pe-7s-plugin"></i><p>Toys</p></a>
              </Link>
            </li>*/}
            <li className={classnames({'active': '/admin/plans' === this.props.pathname})}>
              <Link href="/admin/plans">
                <a><i className="pe-7s-calculator"></i><p>Subscription Plans</p></a>
              </Link>
            </li>
            <li className={classnames({'active': this.props.pathname.substr(0,11) === '/admin/user'})}>
              <Link href="/admin/users">
                <a><i className="pe-7s-users"></i><p>Users</p></a>
              </Link>
            </li>
            <li className={classnames({
              'active': this.props.pathname.substr(0,13) === '/admin/group/' || this.props.pathname.substr(0,13) === '/admin/groups'
              })}>
              <Link href="/admin/groups">
                <a>
                  <i className="pe-7s-global"></i>
                  <p>User Groups</p>
                </a>
              </Link>
            </li>

            <li className={classnames({'active': this.props.pathname === '/admin/group-applicants'})}>
              <Link href="/admin/group-applicants">
                <a><i className="pe-7s-mail-open-file"></i><p>Group Applicants</p></a>
              </Link>
            </li>

            <li className={classnames({'active': this.props.pathname.substr(0,20) === '/admin/reported-post'})}>
              <Link href="/admin/reported-posts">
                <a><i className="fa fa-exclamation"></i><p>Reported</p></a>
              </Link>
            </li>

            <li className={classnames({'active': '/admin/countries' === this.props.pathname})}>
              <Link href="/admin/countries">
                <a><i className="pe-7s-map-2"></i><p>Locations</p></a>
              </Link>
            </li>

            <li className={classnames({active: this.props.pathname.substr(0,15) === '/admin/partners'})}>
              <Link href="/admin/partners">
                <a>
                  <i className="pe-7s-medal"></i>
                  <p>Partners</p>
                </a>
              </Link>
            </li>

            <li className={classnames({active: this.props.pathname.substr(0,16) === '/admin/referrals'})}>
              <Link href="/admin/referrals">
                <a>
                  <i className="pe-7s-speaker"></i>
                  <p>Referrals</p>
                </a>
              </Link>
            </li>

            <li className={classnames({active: this.props.pathname.substr(0,15) === '/admin/settings'})}>
              <Link href="/admin/settings">
                <a>
                  <i className="pe-7s-settings"></i>
                  <p>Settings</p>
                </a>
              </Link>
            </li>
          </ul>

        </div>

        <div className="sidebar-background" style={{backgroundImage: `url(${this.state.bgrImage})`}}></div>
      </div>
    );
  }

}
Sidebar.defaultProps = {
  pathname: ''
}