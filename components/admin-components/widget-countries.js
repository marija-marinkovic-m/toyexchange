export default ({countries}) => {
  return (
    <div className="content table-responsive table-full-width">
      <table className="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>ISO</th>
          </tr>
        </thead>
        <tbody>
          { countries && countries.map((country, i) => <tr key={i}>
            <td>{country.id}</td>
            <td>{country.name}</td>
            <td>{country.iso}</td>
          </tr> )}
        </tbody>
      </table>
    </div>
    );
}