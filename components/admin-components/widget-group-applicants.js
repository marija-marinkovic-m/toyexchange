import { Component } from 'react';
import Link from 'next/link';
import moment from 'moment';
import cn from 'classnames';
import ReactTooltip from 'react-tooltip';

import { Modal, ModalBody, ModalFooter } from '../../components/modal';

export default class GroupApplicantsWidget extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpenModal: false,
      curr: 0
    };
  }

  render() {
    const { posts, onStatusUpdate, onDelete, loadingId } = this.props;
    return(
      <div>
        <table className="table table-hover table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Zip Code</th>
              <th>Applicant</th>
              <th>Applicant's email</th>
              <th>Status</th>
              <th>Date</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            { posts && posts.length && posts.map((p,i) => <tr key={i}>
              <td>{p.id}</td>
              <td>{p.zipCode}</td>
              <td>{p.firstName} {p.lastName}</td>
              <td>{p.email}</td>
              <td>
                <div className={cn({'btn-group warn': true, disabled: p.id == loadingId})} data-toggle="buttons">
                  <label className={cn({'btn btn-default btn-on': true, active: p.status === 'pending'})}>
                    <input type="radio" value="pending" name="status"
                      checked={p.status === 'pending'}
                      onChange={onStatusUpdate.bind(null, p.id, 'pending')} />
                    Pending
                  </label>
                  <label className={cn({'btn btn-default btn-off': true, active: p.status !== 'pending'})}>
                    <input type="radio" value="notified" name="status"
                      checked={p.status !== 'pending'}
                      onChange={onStatusUpdate.bind(null, p.id, 'notified')} />
                    Notified
                  </label>
                </div>
              </td>
              <td>{moment.utc(p.createdAt).format('MMMM Do YYYY')}</td>
              <td>
                <button 
                  disabled={loadingId == p.id} 
                  className="btn btn-simple" 
                  data-tip="Delete Application"
                  onClick={() => this.setState({isOpenModal: true, curr: p.id})}>
                  <i className="fa fa-trash"></i>
                </button>
              </td>
            </tr>) }
          </tbody>
        </table>
        <ReactTooltip />

        <Modal
          isOpen={this.state.isOpenModal}
          onClose={() => this.setState({isOpenModal: false})}
          modalTitle="Are you sure?">
          <ModalBody>Group Application will be permanently deleted?</ModalBody>
          <ModalFooter>
            <button type="button" onClick={() => this.setState({isOpenModal: false})} className="btn btn-default">Cancel</button>
            <button type="button" className="btn btn-danger" onClick={onDelete.bind(null, this.state.curr)}>Delete</button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
GroupApplicantsWidget.defaultProps = {
  onStatusUpdate: () => false,
  onDelete: () => false,
  loadingId: 0
};

export const GroupApplicantsWidgetReduced = ({posts}) => (
  <table className="table table-hover table-striped">
    <thead>
      <tr>
        <th>Zip Code</th>
        <th>Applicant</th>
        <th className="text-right"><i className="pe-7s-stopwatch"></i></th>
      </tr>
    </thead>
    <tbody>
      { posts && posts.length ? posts.map(p => <tr key={p.id}>
        <td>
          <Link href={`/admin/group-applicants?zipCode=${p.zipCode}`}>
            <a>{p.zipCode}</a>
          </Link>
        </td>
        <td>{p.firstName} {p.lastName}</td>
        <td className="text-right">{moment.utc(p.createdAt).fromNow()}</td>
      </tr>) : '' }

      { posts === null && <tr><td colSpan="3"><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</td></tr> }
    </tbody>
  </table>
);