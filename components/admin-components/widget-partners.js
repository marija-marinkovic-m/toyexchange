import ReactTooltip from 'react-tooltip';

export default ({partners, onDelete, onEdit}) => (
  partners ? <div className="content table-responsive table-full-width">
    <table className="table table-hover layout-fixed">
      <thead>
        <tr>
          <th>Image</th>
          <th>#id</th>
          <th>Title</th>
          <th>Link</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        { partners.map((p,i) => <tr key={i}>
          <td>{p.imageFile ? <img src={p.imageFile} /> : 'No image'}</td>
          <td>{p.id}</td>
          <td>{p.title}</td>
          <td>{p.url}</td>
          <td className="text-right">
            <button
              type="button"
              className="btn btn-simple btn-danger btn-xs"
              onClick={onEdit.bind(null, p)} data-tip="Edit">
              <i className="fa fa-edit"></i>
            </button>

            <button 
              type="button"
              className="btn btn-simple btn-info btn-xs"
              onClick={onDelete.bind(null, p)} data-tip="Delete partner">
              <i className="fa fa-times"></i>
            </button>
          </td>
        </tr>) }
      </tbody>
    </table>
    <ReactTooltip />
  </div> : <p>&nbsp;</p>
);