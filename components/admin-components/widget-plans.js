import ReactTooltip from 'react-tooltip';

export default ({plans, onEdit, onDelete}) => {
  return (
    <div className="content table-responsive table-full-width">
      <table className="table table-hover">
        <thead>
          <tr>
            <th className="text-center">ID</th>
            <th>Title</th>
            <th className="text-center">Price</th>
            <th className="text-center">Status</th>
            <th className="text-center">Visibility</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          { plans && plans.length ? plans.map((plan, i) => <tr key={i}>
              <td className="text-center">{ plan.id }</td>
              <td>{ plan.title }</td>
              <td className="text-center">$ { plan.price }</td>
              <td className="text-center">{ plan.status }</td>
              <td className="text-center">{ plan.isPublic ? 'public' : 'private' }</td>
              <td className="text-right">
                <button type="button" data-tip="Edit Plan" className="btn btn-simple btn-info btn-xs" onClick={onEdit.bind(null, plan)}><i className="fa fa-edit"></i></button>
                <button type="button" onClick={onDelete.bind(null, plan.id)} data-tip="Delete" className="btn btn-simple btn-danger btn-xs"><i className="fa fa-times"></i></button>
              </td>
            </tr>) : <tr><td colSpan="6">...</td></tr> }
        </tbody>
      </table>
      <ReactTooltip />
    </div>
  );
};