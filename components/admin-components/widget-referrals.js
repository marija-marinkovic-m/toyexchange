import { Component } from 'react'; 
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import moment from 'moment';

import { Modal, ModalBody, ModalFooter } from '../modal';


export default class ReferralsWidget extends Component {
  static propTypes = {
    referrals: PropTypes.array,
    onDelete: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      isOpenModal: false,
      currID: 0
    }
  }

  render () {
    const { referrals, onDelete } = this.props;
    const { isOpenModal, currID } = this.state;
    return (
      <div className="table-responsive table-full-width">
        <table className="table table-hover">
          <thead>
            <tr>
              <th>Email</th>
              <th>Status</th>
              <th>Sent</th>
              { onDelete !== null && <th>&nbsp;</th> }
            </tr>
          </thead>
          <tbody>
            { 
              referrals && referrals.map((r,i) => <tr key={i}>
                <td><span data-tip={r.statement}>{r.email}</span></td>
                <td><span data-tip={r.code}>{r.status}</span></td>
                <td>{ moment.utc(r.createdAt).fromNow() }</td>
                { onDelete !== null && <td className="text-right">
                  <button 
                    type="button" 
                    className="btn btn-simple"
                    data-tip="Delete referral"
                    onClick={() => this.setState({currID: r.id, isOpenModal: true})}>
                    <i className="fa fa-trash"></i>
                  </button>
                </td> }
              </tr>)
            }
          </tbody>
        </table>
        <Modal
          isOpen={isOpenModal}
          onClose={() => this.setState({isOpenModal: false})}
          modalTitle="Are you sure?">
          <ModalBody><p>Referral will be permanently deleted</p></ModalBody>
          <ModalFooter>
            <button
              type="button" 
              className="btn btn-default"
              onClick={() => this.setState({isOpenModal: false})}>
              Cancel
            </button>
            <button 
              type="button" 
              className="btn btn-danger"
              onClick={() => {onDelete(currID); this.setState({isOpenModal: false})}}>
              Delete
            </button>
          </ModalFooter>
        </Modal>
        <ReactTooltip />
      </div>
    );
  }

}
ReferralsWidget.defaultProps = {
  referrals: null,
  onDelete: () => false
}
