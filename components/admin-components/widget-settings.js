import { Component } from 'react';
import ObjectAssign from 'object-assign';

export default class SettingsWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentlyEditing: [],
      currentUpdate: [],
      settings: null
    };
    this.updateSetting = this.updateSetting.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.settings !== this.props.settings) {
      this.setState({settings: nextProps.settings});
    }
  }

  updateSetting(settingId) {

    const inputRef = this.refs[`setting-${settingId}`];
    const value = inputRef && inputRef.value ? inputRef.value : '';

    this.setState({
      currentUpdate: this.state.currentUpdate.concat(settingId)
    }, () => {
      this.props.onEdit(settingId, {value})
        .then(res => {
          this.setState({
            settings: this.state.settings.map(s => {
              if (s.id == settingId) {
                return ObjectAssign({}, s, {value});
              }
              return s;
            }),
            currentUpdate: this.state.currentUpdate.filter(s => s != settingId),
            currentlyEditing: this.state.currentlyEditing.filter(s => s!= settingId)
          });
        })
        .catch(err => console.log(err));
    });
  }

  render() {
    const { settings, currentlyEditing: edt, currentUpdate: upd } = this.state; 
    return (
      settings && <div className="table-responsive table-full-width">
        <table className="table table-hover">
          <thead>
            <tr>
              <th>Setting</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            { settings.map(s => <tr key={s.id}>
              <td>{s.description}</td>
              <td className="text-right">

                {
                  edt.indexOf(s.id) > -1 ? 
                  <div
                    className="form-group form-group-sm"
                    style={{marginBottom: '0px'}}>
                    <input
                      type="text"
                      disabled={upd.indexOf(s.id) > -1}
                      ref={`setting-${s.id}`}
                      className="form-control input-sm"
                      defaultValue={s.value}
                      style={{display: 'inline-block', width: 'auto'}} />
                    <button
                      type="button"
                      disabled={upd.indexOf(s.id) > -1}
                      className="btn btn-simple btn-info btn-xs"
                      onClick={() => this.setState({currentlyEditing: edt.filter(i => s.id != i)})}>
                      Cancel
                    </button>
                    <button
                      disabled={upd.indexOf(s.id) > -1}
                      type="button"
                      className="btn btn-simple btn-danger btn-xs"
                      onClick={this.updateSetting.bind(null, s.id)}>
                      { upd.indexOf(s.id) > -1 ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Save' }
                    </button>
                  </div> : 
                  <div>
                    {s.value}
                    <button 
                      type="button"
                      className="btn btn-simple btn-info btn-xs"
                      onClick={() => this.setState({currentlyEditing: edt.concat(s.id)})}>
                      <i className="fa fa-edit"></i>
                    </button>
                  </div>
                }
              </td>
            </tr>) }
          </tbody>
        </table>
      </div>
    )
  }
}
SettingsWidget.defaultProps = {
  onEdit: () => new Promise((res, rej) => setTimeout(() => res(true), 2000))
}