import config from '../../static/config';
import moment from 'moment';
import Router from 'next/router';

export default ({items = [], onDelete = null}) => {
  return(
    <table className="table table-hover table-striped">
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th>Post title</th>
          <th className="text-right"><i className="fa fa-clock-o"></i>&nbsp;Latest update</th>
          { onDelete !== null ? <th className="text-right">Delete</th> : '' }
        </tr>
      </thead>
      <tbody>
        { items.length ? items.map((p,i) => {
          const momentUpdated = moment.utc(p.updatedAt);
          const diff = moment().diff(momentUpdated, 'days');
          const latestUpdates = diff > 5 ? momentUpdated.format('ll') : momentUpdated.fromNow();

          return (
            <tr key={i}
              style={{cursor: 'pointer'}}>
              <td onClick={e => Router.push(`/toy?id=${p.slug}`, `/toy/${p.slug}`)}>
                <img width="32px" height="32px" className="rounded-circle" alt="Thumbnail" src={p.imageFile || config.defaultPostImage} />
              </td>
              <td onClick={e => Router.push(`/toy?id=${p.slug}`, `/toy/${p.slug}`)} className="align-middle">{p.title}</td>
              <td className="text-right align-middle">{latestUpdates}</td>
              {onDelete !== null ? <td className="text-right align-middle"><button className="btn simple-btn" onClick={e => onDelete(p.id)} style={{border: 'none'}}><i className="fa fa-trash"></i></button></td> : ''}
            </tr>
          );
        }) : <tr><td colSpan="3">&nbsp;</td></tr> }
      </tbody>
    </table>
  );
}