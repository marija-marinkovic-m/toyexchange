import Link from 'next/link';

export default ({users, onDelete = null}) => {
  return (
    <div className="content table-responsive table-full-width">
      <table className="table table-hover brand-bg">
        <thead>
          <tr>
            { onDelete !== null && <th className="text-center">ID</th> }
            <th className="text-center">Avatar</th>
            <th>Name</th>
            { onDelete !== null && <th>Email</th> }
            <th>Location</th>
            { onDelete !== null && <th>&nbsp;</th> }
          </tr>
        </thead>
        <tbody>
          { users && users.map((user, i) => <tr key={i}>
            { onDelete !== null && <td className="text-center">{ user.id }</td> }
            <td className="text-center">
              <Link href={`/admin/user?id=${user.id}`}>
                <a>{ user.avatarFile ? <img width="32" height="32" style={{borderRadius: '50%', border: '1px solid'}} src={user.avatarFile} /> : <i style={{fontSize: '36px'}} className="pe-7s-user"></i> }</a>
              </Link>
            </td>
            <td>
              <Link href={`/admin/user?id=${user.id}`}>
                <a>{ `${user.firstName} ${user.lastName} - ${user.displayName}` }</a>
              </Link>
            </td>
            { onDelete !== null && <td>{ user.email }</td> }
            <td>{user.zipCode} {user.stateData && user.countryData && `(${user.stateData.name}, ${user.countryData.iso})`}</td>

            { onDelete !== null && <td className="text-right">
              <button
                type="button"
                className="btn btn-simple btn-danger"
                onClick={onDelete.bind(null, user)}>
                <i className="fa fa-trash"></i>
              </button>
            </td> }
          </tr> )}


          {users === null && <tr><td colSpan="4"><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</td></tr>}
        </tbody>
      </table>
    </div>
    );
}