export default ({current, handle}) => (
  <div className="filters card text-center">
    <style jsx>{`
      .filters .col {
        cursor: pointer;
        transition: background-color 400ms ease-in;
      }
      .filters .col.active {background-color: #f7f7f9;}
      .toy-filter {background-image: url('/static/img/filters.png');}
    `}</style>
    <div className="card-header bg-inverse text-white text-uppercase">Filter posts by age</div>
    <div className="card-block row no-gutters text-uppercase p-0">
      <div onClick={() => handle('ageRange', null)} className={`col p-3 border-right-1 border-bottom-1 ${!current ? ' active' : '' }`}>
        <span className="toy-filter show-all d-block mx-auto mb-1"></span>
        Show All
      </div>
      <div onClick={() => handle('ageRange', 1)} className={`col p-3 border-bottom-1 ${current && current == 1 ? 'active' : ''}`}>
        <span className="toy-filter age-0-3 d-block mx-auto mb-1"></span>
        Age 0-3
      </div>
      <div className="w-100"></div>
      <div onClick={() => handle('ageRange', 2)} className={`col p-3 border-right-1 ${current && current == 2 ? 'active' : ''}`}>
        <span className="toy-filter age-4-8 d-block mx-auto mb-1"></span>
        Age 4-8
      </div>
      <div onClick={() => handle('ageRange', 3)} className={`col p-3 ${current && current == 3 ? 'active' : ''}`}>
        <span className="toy-filter age-9-up d-block mx-auto mb-1"></span>
        Age 9-up
      </div>
    </div>
  </div>
);