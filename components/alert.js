export default ({title = 'Alert', content = '', type = 'success'}) => (
  <div className={`alert alert-${type}`} role="alert">
    <strong>{ title }</strong> { content }
  </div>
);