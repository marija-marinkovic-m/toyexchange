/**
 * todo: put logic to detect if code is being run on
 * the server or in the browser inside the page template 
 */
import fetch from 'isomorphic-fetch';

export default class {
  static async getData() {
    let res = await fetch('//jsonplaceholder.typicode.com/posts');
    let data = await res.json();
    return data;
  }
}