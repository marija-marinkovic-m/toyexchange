import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';



Alert.defaultProps = {
  color: 'success',
  isOpen: true,
  transitionAppearTimeout: 150,
  transitionEnterTimeout: 150,
  transitionLeaveTimeout: 150
};

const Alert = (props) => {
  const { color, isOpen, transitionAppearTimeout, transitionEnterTimeout, transitionLeaveTimeout } = props;
}

export default Alert;