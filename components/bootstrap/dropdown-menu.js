import { Component } from 'react';
import Link from 'next/link';
import cn from 'classnames';

import LogoutComponent from '../logout-component';
import throttle from 'lodash.throttle';


export const MenuRenderer = (pathname = null, item, index) => {
  switch(item.type) {
    case 'divider':
      return <div key={index} className="dropdown-divider"></div>;
    case 'logout':
      return <LogoutComponent key={index} csrfToken={item.token} />
    default: // link
      return <Link key={index} href={item.href}>
        <a className={cn(['dropdown-item', {active: pathname === item.href}])}>{ item.label }</a>
      </Link>;
  }
};

export const MenuRendererLI = (pathname = null, item, index) => {
  switch(item.type) {
    case 'divider':
      return <li key={index} className="dropdown-divider"></li>;
    case 'logout':
      return (<li key={index} className="nav-item"><LogoutComponent
        csrfToken={item.token} buttonClassName="nav-link" buttonStyle={{
          border: 'none',
          width: '100%',
          textAlign: 'left',
          marginBottom: '5px'
        }} />
      </li>);
    default: // link
      return <li key={index} className="nav-item"><Link href={item.href}>
        <a className={cn(['nav-link', {active: pathname === item.href}])}>{ item.label }</a>
      </Link></li>;
  }
};

export default class DropdownMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listVisible: false
    }
    this.toggle = this.toggle.bind(this);
    this.throttleCb = throttle(this.toggle, 150);
  }

  toggle() {
    this.setState({listVisible: !this.state.listVisible}, () => {
      if (this.state.listVisible) {
        document.addEventListener('click', this.throttleCb);
      } else {
        document.removeEventListener('click', this.throttleCb);
      }
    })
  }

  render() {
    const { items, pathname } = this.props;
    return (
      <div className={cn(['dropdown'], {show: this.state.listVisible})}>
        <div
          className={cn(['account-toggle', 'dropdown-toggle'])}
          onClick={this.toggle}>
          { this.props.children }
        </div>
        <div className="dropdown-menu">
          { items && items.map(MenuRenderer.bind(null, pathname)) }
        </div>
      </div>
    );
  }
}
DropdownMenu.defaultProps = {
  items: [],
  pathname: ''
}