import React, { Component } from 'react';
import cx from 'classnames';
import paginator from 'paginator';

class Page extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {
    const { isDisabled, pageNumber } = this.props;
    e.preventDefault();
    if (isDisabled) return;
    this.props.onClick(pageNumber);
  }

  render() {
    let {
      pageText,
      pageNumber,
      activeClass,
      disabledClass,
      isActive,
      isDisabled
    } = this.props;

    const css = cx({
      'page-item': true,
      [activeClass]: isActive,
      [disabledClass]: isDisabled
    });

    return(
      <li className={css} onClick={this.handleClick}>
        <a className="page-link" href="#">{ pageText }</a>
      </li>
    );
  }
}
Page.defaultProps = {
  activeClass: 'active',
  disabledClass: 'disabled',
  isActive: false,
  isDisabled: false
}

export default class Pagination extends Component {
  constructor(props) {
    super(props);
    this.buildPages = this.buildPages.bind(this);
  }
  buildPages() {
    const pages = [];
    const {
      pagin,
      pageRangeDisplayed,
      prevPageText,
      nextPageText,
      firstPageText,
      lastPageText,
      onChange,
      activeClass,
      disabledClass,
      hideDisabled
    } = this.props;

    if (!pagin || pagin.totalPages <= 1) return false;

    const paginationInfo = new paginator(pagin.perPage, pageRangeDisplayed)
      .build(pagin.totalItems, pagin.currentPage);

    if (paginationInfo.first_page !== paginationInfo.last_page) {
      for (let i = paginationInfo.first_page; i <= paginationInfo.last_page; i++) {
        pages.push(
          <Page 
            isActive={i === pagin.currentPage}
            key={i}
            pageNumber={i}
            pageText={i + ''}
            onClick={onChange}
            activeClass={activeClass}
          />
        );
      }
    }
    (hideDisabled && !paginationInfo.has_previous_page) || pages.unshift(
      <Page
        key={'prev' + paginationInfo.previous_page}
        pageNumber={paginationInfo.previous_page}
        onClick={onChange}
        pageText={prevPageText}
        isDisabled={!paginationInfo.has_previous_page}
        disabledClass={disabledClass}
      />
    );

    (hideDisabled && !paginationInfo.has_previous_page) || pages.unshift(
      <Page
        key={'first'}
        pageNumber={1}
        onClick={onChange}
        pageText={firstPageText}
        isDisabled={paginationInfo.current_page === paginationInfo.first_page}
        disabledClass={disabledClass}
      />
    );

    (hideDisabled && !paginationInfo.has_next_page) || pages.push(
      <Page
        key={'next' + paginationInfo.next_page}
        pageNumber={paginationInfo.next_page}
        onClick={onChange}
        pageText={nextPageText}
        isDisabled={!paginationInfo.has_next_page}
        disabledClass={disabledClass}
      />
    );

    (hideDisabled && !paginationInfo.has_next_page) || pages.push(
        <Page
            key={'last'}
            pageNumber={paginationInfo.total_pages}
            onClick={onChange}
            pageText={lastPageText}
            isDisabled={paginationInfo.current_page === paginationInfo.total_pages}
            disabledClass={disabledClass}
        />
    );

    return pages;
  }

  render() {
    const pages = this.buildPages();
    return (pages && pages.length && <ul className={this.props.innerClass}>{ pages }</ul>);
  }
}
Pagination.defaultProps = {
  pagin: null,
  onChange: () => false,
  pageRangeDisplayed: 5,
  prevPageText: '⟨',
  firstPageText: '«',
  nextPageText: '⟩',
  lastPageText: '»',
  innerClass: 'pagination',
};