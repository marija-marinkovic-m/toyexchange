import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export default ({children, ssr}) => (
  <ReactCSSTransitionGroup 
    transitionName={ssr ? 'notrans' : 'fadein'} 
    transitionAppear={true}
    transitionAppearTimeout={500}
    transitionEnter={false}
    transitionLeave={false}
  >
    { children }
  </ReactCSSTransitionGroup>
);