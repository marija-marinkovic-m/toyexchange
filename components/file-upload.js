import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import Cropper from 'react-cropper';
import ObjectAssign from 'object-assign';

import { Modal, ModalBody, ModalFooter } from '../components/modal';
import Tooltip from '../components/tooltip';

import { Util } from '../api/util';

export default class FileUploader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      src: null,
      cropResult: null,
      modalOpen: false,
      mouseOverUpload: false
    };
    this.cropImage = this.cropImage.bind(this);
    this.removeImage = this.removeImage.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  handleFile(acceptedFiles, rejectedFiles) {
    console.log(acceptedFiles)
    this.setState({
      mouseOverUpload: false,
      cropResult: null,
      src: null
    }, () => {
      if (acceptedFiles.length) {
        console.log('size (file-upload.js)', acceptedFiles[0].size);
        this.setState({
          defaultSrc: null,
          src: acceptedFiles[0].preview,
          fileName: acceptedFiles[0].name
        });
      }
    });
  }

  cropImage(e) {
    if (e) e.preventDefault();
    if (!this.cropper || typeof this.cropper.getCroppedCanvas() === 'undefined') {
      return;
    }
    this.setState({
      mouseOverUpload: false,
      cropResult: this.cropper.getCroppedCanvas().toDataURL('image/jpeg', 0.2)
    }, () => {
      this.props.handleFile(Util.dataURItoBlob(this.state.cropResult, this.state.fileName), this.state.cropResult);
      if (this.state.modalOpen === true) {
        this.closeModal();
      }
    });
  }

  removeImage(e) {
    if (e) e.preventDefault();
    this.setState({
      cropResult: null,
      src: null,
      mouseOverUpload: false
    });
    this.props.handleRemoveFile();

    if (this.props.defaultSrc) {
      this.setState({defaultSrc: this.props.defaultSrc});
    }
  }

  closeModal(e) {
    if (e) e.preventDefault();
    this.setState({modalOpen: false});
  }

  componentDidMount() {
    if (this.props.defaultSrc && this.props.defaultSrc !== null && this.props.defaultSrc !== '') {
      this.setState({defaultSrc: this.props.defaultSrc});
    }
  }

  render() {
    if (this.state.defaultSrc) {
      return (
        <div className="default-src-dropzone">
          <img src={this.state.defaultSrc} />
          <Dropzone multiple={this.props.multiple} accept={this.props.accept} maxSize={this.props.maxSize} onDrop={this.handleFile} className="text-center d-flex align-items-center justify-content-center" activeClassName="alert alert-success" rejectClassName="alert alert-warning">
            <button className="btn btn-primary" type="button" title="Upload another image"><i className="fa fa-upload"></i></button>
          </Dropzone>
        </div>
      );
    }
    return(
      <div>
        <Modal modalTitle="Crop Image" isOpen={this.state.modalOpen} onClose={this.closeModal}>
          <ModalBody>
            { this.state.modalOpen && this.state.src && this.state.cropResult && <Cropper aspectRatio={1 / 1} guides={true} src={this.state.src} ref={cropper => this.cropper = cropper} center={true} modal={true} responsive={true} style={{ height: 300, width: '100%' }} /> }
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={this.cropImage}>Save</button>
            <button className="btn btn-secondary" onClick={this.closeModal}>Cancel</button>
          </ModalFooter>
        </Modal>

        <div className="main-placeholder mx-auto">
          {  
            this.state.src && !this.state.cropResult && <div className="sr-only"><Cropper aspectRatio={1 / 1} guides={false} src={this.state.src} ref={cropper => this.cropper = cropper} crop={this.cropImage} autoCrop={true} autoCropArea={1} /></div>
          }
          <Dropzone multiple={this.props.multiple} accept={this.props.accept} maxSize={this.props.maxSize} onDrop={this.handleFile} className="dropzone-custom text-center d-flex align-items-center justify-content-center" activeClassName="alert alert-success" rejectClassName="alert alert-warning">
            {({ isDragActive, isDragReject, acceptedFiles, rejectedFiles }) => {
              if (isDragActive) {
                return <i className="fa fa-check-circle rounded-ico lg nb" aria-hidden="true"></i>;
              }
              if (isDragReject) {
                return <i className="fa fa-times-circle rounded-ico lg nb" aria-hidden="true"></i>;
              }
              if (rejectedFiles.length) {
                return <div style={{margin: 0, height: '100%', display: 'block'}} className="alert alert-danger" role="alert"><strong>Rejected file.</strong> Max file size (6MB) exceeded. Try Again.</div>;
              }
              if (this.state.cropResult) {
                return (
                  <div className={`preview-placeholder ${this.state.mouseOverUpload ? 'on-hover' : ''}`} style={{ backgroundColor: 'white', backgroundImage: `url(${this.state.cropResult})`, width: '100%', height: '100%', backgroundSize: 'contain' }}>
                    <button className="btn btn-primary" type="button" onMouseEnter={e => this.setState({mouseOverUpload: true})} onMouseLeave={e => this.setState({mouseOverUpload: false})}><i className="fa fa-upload"></i></button>
                  </div>
                );
              }

              return (
                <div>
                  <i className="fa fa-camera rounded-ico lg d-block mx-auto mb-3 text-muted" aria-hidden="true"></i>
                  <a className="text-muted">{this.props.uploadLabel}</a>
                </div>
              );
            }}
          </Dropzone>
          

          {
            this.state.src && this.state.cropResult && (<div className="actions">
              <button className="btn btn-secondary mr-1" type="button" onClick={() => this.setState({modalOpen: true})}><i className="fa fa-crop"></i></button>
              <button className="btn btn-secondary" type="button" onClick={this.removeImage}><i className="fa fa-times"></i></button>
              
            </div>)
          }
        </div>
      </div>
    );
  }
}

FileUploader.defaultProps = {
  uploadLabel: 'Upload Image',
  handleFile: (blob, base64src) => false,
  handleRemoveFile: () => false,
  defaultSrc: null,
  accept: 'image/*',
  maxSize: 6291456,
  multiple: false
}