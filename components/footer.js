import Link from 'next/link';
import React from 'react';

import { SocialMenuFooter } from './social-menu';
import Logo from './logo';

export default function ({loggedIn}) {
  return (
    <footer className="mt-5">
      <div className="w-100 px-3 text-center">
        <h6>Follow Us!</h6>
        <SocialMenuFooter />
      </div>
      <hr/>
      <nav className="navbar navbar-toggleable-md navbar-light">
        <a className="navbar-brand"><Logo width="64px" color={false} /></a>
        <div className="collapse navbar-collapse show">
          <span className="navbar-text">
            Copyright &copy; {new Date().getYear() + 1900}
            <span className="text-uppercase">Toy-Exchange.org</span>. All rights reserved.
          </span>

          <ul className="navbar-nav ml-auto">
            {/* add class 'active' on li.nav-item if pathname active */}
            {/*<li className="nav-item"><Link href="/faq"><a className="nav-link">FAQ</a></Link></li>*/}
            <li className="nav-item"><Link href="/privacy-notice"><a className="nav-link">Privacy Notice</a></Link></li>
            <li className="nav-item"><Link href="/terms-of-use"><a className="nav-link">Terms of Use</a></Link></li>
            <li className="nav-item"><Link href="/posting-guidelines"><a className="nav-link">Posting Guidelines</a></Link></li>
            <li className="nav-item"><Link href="/contact"><a className="nav-link">Contact Us</a></Link></li>

            { !loggedIn && <li className="nav-item"><Link href="/browse-groups"><a className="nav-link">Browse Groups</a></Link></li> }
            { !loggedIn && <li className="nav-item"><Link href="/login"><a className="nav-link">Log In</a></Link></li> }
            { !loggedIn && <li className="nav-item"><Link href="/signup"><a className="nav-link">Sign Up</a></Link></li> }

            <li className="nav-item"><Link href="/faq"><a className="nav-link" title="Frequently Asked Questions">FAQ</a></Link></li>
          </ul>
        </div>
        
      </nav>
    </footer>
  );
}