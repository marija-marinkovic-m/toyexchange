import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import React from 'react';
import NProgress from 'nprogress';
import classnames from 'classnames';

import inlineCSS from '../css/main.scss';
import Package from '../package';

import Logo, { LogoLight } from './logo';
import SocialMenu from './social-menu';
import Session from '../auth/session';
import config from '../static/config';

import cn from 'classnames';

import DropdownMenu, { MenuRendererLI } from '../components/bootstrap/dropdown-menu';

Router.onRouteChangeStart = url => {
  console.log(`Loading: ${url}`);
  NProgress.start();
};
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();


/**
 * PROPS: {
 * session: PropTypes.object.isRequired,
 * title: PropTypes.string.isRequired,
 * loggedIn: PropTypes.bool,
 * isAdmin: PropTypes.bool
 * pathname: PropTypes.string,
 * isLight: PropTypes.bool
 * totalUnreadMessages: PropTypes.number
 */
export default class Header extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      navbarColapse: true,
      profileDropdownOpen: false
    };
    this.toggleNav = this.toggleNav.bind(this);
    this.renderProfileBlock = this.renderProfileBlock.bind(this);
    this.renderWarning = this.renderWarning.bind(this);
  }

  renderProfileBlock() {
    if (!this.props.loggedIn) return <SocialMenu />;

    let menuItems = [];
    if (this.props.isAdmin) {
      menuItems = [
        {href: '/admin', label: 'Dashboard'},
        {href: '/admin/change-password', label: 'Change Password'},
        {type: 'divider'}
      ];
    } else {
      // total unread messages 
      const msgs = this.props.totalUnreadMessages && this.props.totalUnreadMessages > 0 ? <span className="badge badge-default badge-pill">{ this.props.totalUnreadMessages }</span> : '';

      menuItems = [
        {href: '/my-account/new-post', label: 'Create new post'},
        {type: 'divider'},
        {href: '/my-account/messages', label: <span>Messages&nbsp;{ msgs }</span>},
        {href: '/my-account/posts', label: 'My Posts'},
        {href: '/my-account', label: 'Settings'},
        {href: '/my-account/change-password', label: 'Change Password'},
        {type: 'divider'}
      ];
    }
    menuItems.push({type: 'logout', csrfToken: this.props.session.csrfToken});

    return (<div>
      <div className="hidden-md-down">
        <DropdownMenu
        items={menuItems}
        pathname={this.props.pathname}>
        { this.props.session.user.username }
        <figure className="d-inline-block align-middle mb-0 rounded-circle ml-2" style={{width: '30px', height: '30px'}}>
          <img className="d-block w-100 rounded-circle" src={this.props.session.user.data.avatarFile || config.defaultAvatarImage}  />
        </figure>
        {this.props.totalUnreadMessages === false ? <span><i className="fa fa-refresh fa-spin"></i></span> : this.props.totalUnreadMessages == 0 ? '' : <span className="badge badge-default badge-pill">{this.props.totalUnreadMessages}</span>}
      </DropdownMenu>
      </div>

      <div className="hidden-lg-up">

        <ul className="navbar-nav">
          { menuItems.map(MenuRendererLI.bind(null, this.props.pathname)) }
        </ul>
      </div>
    </div>);
   
  }
  renderWarning() {
    if (!this.props.loggedIn) return;
    if (this.props.session.user.data.paymentStatus === 'paid') return;

    if (this.props.pathname.slice(0,7) === '/signup') return;
    
    let warning = <span><strong>Warning!</strong> Please finish your registration <a href="/signup?mrp">Signup</a></span>
    switch(this.props.session.user.data.paymentStatus) {
      case 'expired':
        warning = <span><strong>Warning!</strong> Your subscription expired. Please, <a href="/signup">renew your subscription</a></span>;
        break;

      default: // not_paid
        break;
    }

    return (
      <div className="alert alert-warning mt-3" role="alert">{ warning }</div>
    );
  }

  toggleNav() {
    this.setState({navbarColapse: !this.state.navbarColapse});
  }

  render() {
    // Nav Bar Status
    let navbarColapseClass = this.state.navbarColapse ? '' : 'show';

    // Stylesheets
    let stylesheet;
    if (process.env.NODE_ENV === 'production') {
      let pathToCSS = '/assets/' + Package.version + '/main.css';
      stylesheet = <link rel="stylesheet" type="text/css" href={pathToCSS} />;
    } else {
      stylesheet = <style dangerouslySetInnerHTML={{__html: inlineCSS}} />;
    }

    const brandLogo = <a className="navbar-brand" title="Toy-Exchange.org">{ this.props.isLight ? <LogoLight width="106px" /> : <Logo width="106px" /> }</a>;

    return (
      <header>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>{this.props.title} | TOY-CYCLE.ORG</title>

          <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/static/favicon/apple-touch-icon-57x57.png" />
          <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/favicon/apple-touch-icon-114x114.png" />
          <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/favicon/apple-touch-icon-72x72.png" />
          <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/favicon/apple-touch-icon-144x144.png" />
          <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/static/favicon/apple-touch-icon-60x60.png" />
          <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/favicon/apple-touch-icon-120x120.png" />
          <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/favicon/apple-touch-icon-76x76.png" />
          <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/favicon/apple-touch-icon-152x152.png" />
          <link rel="icon" type="image/png" href="/static/favicon/favicon-196x196.png" sizes="196x196" />
          <link rel="icon" type="image/png" href="/static/favicon/favicon-96x96.png" sizes="96x96" />
          <link rel="icon" type="image/png" href="/static/favicon/favicon-32x32.png" sizes="32x32" />
          <link rel="icon" type="image/png" href="/static/favicon/favicon-16x16.png" sizes="16x16" />
          <link rel="icon" type="image/png" href="/static/favicon/favicon-128.png" sizes="128x128" />
          <meta name="application-name" content="TOY-CYCLE.ORG"/>
          <meta name="msapplication-TileColor" content="#FFFFFF" />
          <meta name="msapplication-TileImage" content="/static/favicon/mstile-144x144.png" />
          <meta name="msapplication-square70x70logo" content="/static/favicon/mstile-70x70.png" />
          <meta name="msapplication-square150x150logo" content="/static/favicon/mstile-150x150.png" />
          <meta name="msapplication-wide310x150logo" content="/static/favicon/mstile-310x150.png" />
          <meta name="msapplication-square310x310logo" content="/static/favicon/mstile-310x310.png" />


          <link rel="stylesheet" type="text/css" href="/static/styles/nprogress.css" />
          <link rel="stylesheet" type="text/css" href="/static/styles/swiper.css" />
          <link rel="stylesheet" type="text/css" href="/static/styles/cropper.css" />
          <script src="https://cdn.polyfill.io/v2/polyfill.min.js" />
          {stylesheet}
        </Head>
        
        <nav className={ this.props.isLight ? 'navbar navbar-toggleable-md navbar-inverse' : 'navbar navbar-toggleable-md navbar-light bg-fadded'}>
          <button onClick={this.toggleNav} className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          { this.props.loggedIn ? brandLogo : <Link href="/">{brandLogo}</Link> }
          
          <div className={`collapse navbar-collapse ${navbarColapseClass}`}>
            <ul className="navbar-nav mr-auto">
              { this.props.loggedIn && !this.props.isAdmin && <li className="nav-item">
                <Link href="/group?id=home-group" as="/group/home-group"><a className={cn({'nav-link': true, 'active': this.props.pathname === '/group/home-hroup'})}>Home Group</a></Link>
              </li> }
              { this.props.loggedIn && this.props.isAdmin && <li className="nav-item">
                  <Link href="/browse-groups"><a className={cn({'nav-link': true, 'active': this.props.pathname === '/browse-groups'})}>User Groups</a></Link>
                </li> }
              { !this.props.loggedIn && <li className="nav-item"><Link href="/"><a className={cn({'nav-link': true, 'active': this.props.pathname === '/'})}>Home</a></Link></li> }
              <li className="nav-item">
                <Link href="/about-toy-cycle"><a className={cn({'nav-link': true, 'active': this.props.pathname === '/about-toy-cycle'})}>About Toy-Exchange</a></Link>
              </li>
              { !this.props.loggedIn && <li className="nav-item">
                <Link href="/posting-guidelines"><a className={cn({'nav-link': true, 'active': this.props.pathname === '/posting-guidelines'})}>Posting Guidelines</a></Link>
              </li> }
              <li className="nav-item">
                <Link href="/contact"><a className={cn({'nav-link': true, 'active': this.props.pathname === '/contact'})}>Contact</a></Link>
              </li>
            </ul>
            { this.renderProfileBlock() }
          </div>
        </nav>

        { this.renderWarning() }

        <style jsx>{`
          .nav-link {position: relative;}
          .nav-link.active:before {
            content: '';
            position: absolute; left: .5em; right: .5em; bottom: .3em;
            display: block; height: 2px; 
            background-color: #ff4b64;
          }
        `}</style>
      </header>
    );
  }
}