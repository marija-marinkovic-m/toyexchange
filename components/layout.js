import React from 'react';
import Header from './header';
import Footer from './footer';

export default ({children, props, session, title = 'TOY-CYCLE', pathname = '', totalUnreadMessages = 0}) => (
  <div className="container pt-3">
    <Header session={session} loggedIn={props.loggedIn} title={title} pathname={pathname !== '' ? pathname : props.pathname} isAdmin={props.isAdmin} totalUnreadMessages={totalUnreadMessages} />
    <main>{ children }</main>
    <Footer loggedIn={props.loggedIn} />
  </div>
)