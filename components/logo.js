export default ({children, width = '105px', color = true}) => (
  <div className="logo">
    <img src="/static/img/logo.png" alt="toy-cycle.org" style={{
      width: width, height: 'auto',
      filter: color ? 'url("data:image/svg+xml;utf8,&lt;svg xmlns=\'http://www.w3.org/2000/svg\'&gt;&lt;filter id=\'grayscale\'&gt;&lt;feColorMatrix type=\'matrix\' values=\'1 0 0 0 0, 0 1 0 0 0, 0 0 1 0 0, 0 0 0 1 0\'/&gt;&lt;/filter&gt;&lt;/svg&gt;#grayscale")' : 'url("data:image/svg+xml;utf8,&lt;svg xmlns=\'http://www.w3.org/2000/svg\'&gt;&lt;filter id=\'grayscale\'&gt;&lt;feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/&gt;&lt;/filter&gt;&lt;/svg&gt;#grayscale")',
      WebkitFilter: color ? 'grayscale(0%)' : 'grayscale(100%)'
    }} />
  </div>
);

export const LogoLight = ({width = '105px'}) => (
  <div className="logo">
    <img src="/static/img/logo-light.png" alt="toy-cycle.org" style={{
      width: width, height: 'auto'
    }} />
  </div>
);