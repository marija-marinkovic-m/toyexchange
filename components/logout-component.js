import { Component } from 'react';
import Session from '../auth/session';

export default class LogoutComponent extends Component {
  handleLogout(event) {
    event.preventDefault();
    const session = new Session();
    session
      .logout()
      .then(res => window.location.reload())
      .catch(err => console.log(err));
  }
  render() {
    return(
      <form method="post" action="/auth/logout" onSubmit={this.handleLogout}>
        <input type="_csrf" type="hidden" value={this.props.csrfToken} readOnly />
        <button className={this.props.buttonClassName} type="submit" style={this.props.buttonStyle}><i className="fa fa-power-off"></i>&nbsp;Logout</button>
      </form>
    );
  }
}
LogoutComponent.defaultProps = {
  csrfToken: '',
  buttonClassName: 'dropdown-item',
  buttonStyle: {display: 'block'}
}
