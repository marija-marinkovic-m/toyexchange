import { Component } from 'react';
import fetch from 'isomorphic-fetch';
import PropTypes from 'prop-types';
import ObjectAssign from 'object-assign';

import { Modal, ModalBody } from '../components/modal';

import { GoogleGeocode, GoogleApi } from '../lib/GoogleApi';
import { GoogleMap as GoogleMapComponent } from '../lib/GoogleMapComponent';

const defaultInitialCenter = {
  lat: 37.774929,
  lng: -122.419416
};

export class MapModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mapLoaded: false,
      mapProps: {
        google: null,
        visible: false
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.isOpen === this.props.isOpen) return;
    if (this.props.isOpen) {
      this.openModal();
    } else {
      this.closeModal();
    }
  }

  openModal() {
    if (typeof window === 'undefined') return;

    if (this.state.mapLoaded) {
      this.setState({mapProps: ObjectAssign({}, this.state.mapProps, {visible: true})});
      return;
    }

    if (!this.cacheAPI) {
      this.cacheAPI = require('../lib/ScriptCache');
    }
    if (!this.scriptCache) {
      this.scriptCache = this.cacheAPI.ScriptCache(window)({
        google: GoogleApi()
      });
      this.scriptCache.google.onLoad(async () => {
        this.setState({
          mapLoaded: true,
          mapProps: ObjectAssign({}, this.state.mapProps, {
            google: window.google,
            initialCenter: await this.handleGeocode(this.props.address),
            visible: true
          })
        })
      });
    }
  }

  closeModal() {
    this.setState({mapProps: ObjectAssign({}, this.state.mapProps, {visible: false})});
    if (this.props.onClose) this.props.onClose();
  }

  async handleGeocode() {
    const geocodeUrl = GoogleGeocode({address: this.props.address});
    let data = await fetch(geocodeUrl).then(r => r.json());
    if (!data || data.status !== 'OK' || !data.results.length) return defaultInitialCenter;
    const location = data.results[0].geometry.location;
    return {lat: location.lat, lng: location.lng};
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onClose={this.closeModal.bind(this)}
        modalTitle={this.props.modalTitle}
        sizeClass="modal-lg">
        <ModalBody>
          <div style={{position: 'relative', width: '100%', height: '55vh'}}>
            { 
              this.state.mapLoaded ? 
              <GoogleMapComponent {...this.state.mapProps} /> : 
              <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading map...</p> 
            }
          </div>
        </ModalBody>
      </Modal>
    );
  }
}
MapModal.propTypes = {
  address: PropTypes.string.isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  modalTitle: PropTypes.string
}
MapModal.defaultProps = {
  address: '',
  isOpen: false,
  onClose: () => false,
  modalTitle: 'Map'
}
