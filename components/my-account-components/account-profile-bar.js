import moment from 'moment';

export default ({user}) => (
  <div className="row mb-5 py-4" style={{
    backgroundImage: 'url(/static/img/no-results.png)',
    backgroundPosition: '97% 50%',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'auto 80%',
    borderRadius: '0.5rem',
    border: '1px solid #f0f3f5',
    boxShadow: '0 0 7px -3px rgba(0,0,0,0.2)'
  }}>
    <div className="col-md-1 col-sm-3">
      <img src={user.avatarFile ? user.avatarFile : '/static/img/default-avatar.png'} className="card-img-top w-100 rounded" alt="Profile Image" />
    </div>
    <div className="col-md-11 col-sm-9">
      
      <h4 className="card-title mb-0">{ user.firstName + ' ' + user.lastName + ' - ' + user.displayName }</h4>
      <p className="card-text mb-0">{ user.email }</p>
      <p className="card-text text-muted font-italic mb-0"><small>Member since: {moment.utc(user.createdAt).format('LL')}</small></p>
    </div>
  </div>
)