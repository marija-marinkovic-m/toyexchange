import React from 'react';
import Link from 'next/link';
import cn from 'classnames';

export default ({user, pathname = '', totalUnreadMessages = false}) => {
  const msgs = totalUnreadMessages > 0 ? <span className="badge badge-primary">{totalUnreadMessages}</span> : <i className="fa fa-envelope-o"></i>;

  const menuItems = [
    { href: '/new-post', label: 'New Post', icon: 'fa fa-edit' },
    { href: '/messages', label: 'Messages', additional: msgs },
    { href: '/posts', label: 'My Posts', icon: 'fa fa-folder-open-o' },
    { href: '', label: 'Profile settings', icon: 'fa fa-wrench' },
    { href: '/change-password', label: 'Change Password', icon: 'fa fa-key' },
    { href: '/referrals', label: 'Referrals', icon: 'fa fa-users' },
    { href: '/subscriptions', label: 'Subscriptions', icon: 'fa fa-handshake-o' }
  ];
  const itemDefaultClasses = [
    'list-group-item',
    'list-group-item-action',
    'justify-content-between'
  ];

  return(
    <div className="card text-center mb-4">

      <div className="list-group list-group-flush">
        { menuItems.map((item, index) => <Link
          href={`/my-account${item.href}`}
          key={index}>
          <a className={cn(
            itemDefaultClasses,
            {active: item.href !== '' ?
              pathname.substr(11, item.href.length) === item.href : 
              pathname === '/my-account'} 
            )}>
            { item.label }
            { item.icon && <i className={item.icon}></i> }
            { item.additional && item.additional }
          </a>
        </Link>) }
      </div>
    </div>
  );
}