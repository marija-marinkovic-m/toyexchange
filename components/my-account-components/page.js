import Page from '../../components/page';

export default class extends Page {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);

    if (!props.loggedIn) {
      if (typeof window === 'undefined') {
        ctx.res && ctx.res.location('/');
        ctx.res.status(302).end();
      } else {
        location.pathname = '/';
      }
      return props;
    }

    // place to redirect expired, not_paid or users who haven't completed registration steps
    // and admins
    if (props.isAdmin || (props.loggedIn && props.session.user && props.session.user.data && props.session.user.data.regStep < 3)) {
      const redirectPath = props.isAdmin ? '/admin' : '/signup';
      if (typeof window === 'undefined') {
        ctx.res && ctx.res.location(redirectPath);
        ctx.res.status(302).end();
      } else {
        location.pathname = redirectPath;
      }
    }

    return props;
  }
}