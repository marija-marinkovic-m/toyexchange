import { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import cn from 'classnames';
import moment from 'moment';
import Scroll from 'react-scroll';

import { Messages } from '../../api/account';
import config from '../../static/config';
import { Util } from '../../api/util';

import Pagination from '../../components/bootstrap/pagination';
import ComponentVisibilityTracker from '../../lib/TrackComponentVisibility';
import ObjectAssign from 'object-assign';

const Element = Scroll.Element;
const scroller = Scroll.scroller;

const defaultConversationsParams = {
  pagin: '0'
}

const defaultMessagesParams = {
  pagin: 1,
  perPage: 10,
  page: 1
}

const mockupToy = {
  givenUserData: {
    id: 16,
    displayName: 'ImUser',
    avatarFile: 'https://dev.toy-cycle.org/api/images/avatar/16_avatar.jpg'
  }
}

export default class extends Component {
  constructor(props) {
    super(props);
    let propsMessagesParams = props.page ? {page: props.page} : {};
    this.state = {
      messagesFilters: ObjectAssign({}, defaultMessagesParams, propsMessagesParams),
      conversations: {data: null, paginator: null},
      messages: null,
      otherUser: null
    };
    this.handleCreateMessage = this.handleCreateMessage.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  async componentDidMount() {
    let conversations = await Messages.listConversations(this.props.postId);
    let messages = this.props.conversationId != false ? await Messages.listMessages(this.props.postId, this.props.conversationId, this.state.messagesFilters) : {data: null, paginator: null};
    let otherUsers = this.props.conversationId && conversations.data ? conversations.data.filter(c => c.id == this.props.conversationId) : [];
    this.setState({
      conversations,
      messages,
      otherUser: otherUsers.length ? otherUsers[0] : null
    })
  }
  async componentWillReceiveProps(nextProps) {
    if (nextProps.conversationId != this.props.conversationId) {
      this.setState({messages: null});
    }
    if (nextProps.page && nextProps.page != this.state.messagesFilters.page) {
      this.setState({messagesFilters: ObjectAssign({}, this.state.messagesFilters, {page: nextProps.page})})
    }
    if (nextProps.updateSubjectRef != this.props.updateSubjectRef) {
      this.handleReply(nextProps.updateSubjectRef);
    }

    if (nextProps.reloadMessagesThread === true) {
      this.setState({messages: await Messages.listMessages(this.props.postId, this.props.conversationId, this.state.messagesFilters)});
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.conversationId == this.props.conversationId && prevProps.page == this.props.page) {
      return;
    }

    let messages = await Messages.listMessages(this.props.postId, this.props.conversationId, this.state.messagesFilters);
    let otherUsers = this.state.conversations.data ? this.state.conversations.data.filter(c => c.id == this.props.conversationId) : [];
    this.setState({
      messages,
      otherUser: otherUsers.length ? otherUsers[0] : null
    });
  }

  handleCreateMessage(e) {
    e.preventDefault();
    let theForm = e.target;
    let data = Util.formToPlainObject(theForm);

    this.setState({sending: true});

    Messages.createMessage(data.postId, data.userId, {
      subject: data.subject,
      content: data.content
    })
      .then(async res => {
        if (!res || res.error) {
          this.props.handleNotify({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
        } else {
          this.props.handleNotify({title: 'Success', message: res.message ? res.message : 'Message sent successfully'});
          this.setState({messages: await Messages.listMessages(this.props.postId, this.props.conversationId, this.state.messagesFilters)});
          theForm.reset();
        }
        this.setState({sending: false});
        this.props.handleMessageContent('');
      })
      .catch(e => this.setState({sending: false}))
  }

  handleReply(subject, e) {
    if (e) {
      e.preventDefault();
      const regExp = new RegExp(/^\bRe?\b:/, 'ig');
      this.subjectInputRef.value = regExp.test(subject) ? subject : 'Re: ' + subject;
    } else {
      this.subjectInputRef.value = subject;
    }
    scroller.scrollTo('reply-form', {duration: 600, smooth: true});
  }

  handleDelete(messageId, e) {
    e.preventDefault();
    if (confirm('Are you sure you want to remove this message. Deletion can\'t be undone. Message will still be visible to other party.')) {
      Messages.deleteMessage(messageId)
        .then(async res => {
          if (!res || res.error) {
            this.props.handleNotify({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
          } else {
            this.props.handleNotify({title: 'Success', message: res.message ? res.message : 'Message deleted successfully'});
            this.setState({messages: await Messages.listMessages(this.props.postId, this.props.conversationId, this.state.messagesFilters)});
          }
        });
    }
  }

  async handleMarkAsRead(messageId) {
    let res = await Messages.updateMessageStatus(messageId, {isRead: 1});
    if (!res || res.error) {
      this.props.handleNotify({title: 'Error', message: res.error ? res.error.message : 'Unable to mark message as read'});
      return false;
    }
    console.log(res);
    return this.setState({messages: 
      ObjectAssign({}, this.state.messages, {
        data: this.state.messages.data.map(m => {
          if (m.id == messageId) {
            return ObjectAssign({}, m, {isRead: true});
          }
          return m;
        })
      })
    });
  }

  render() {
    const { postId, conversationId, postSlug, currAvatar, toy, handleCompleteFavor, postStatus, handleMessageContent, messageContent } = this.props;
    const { conversations, messages, otherUser } = this.state;

    return (<div className="row">
      <div className="col-md-3">

        {/* loading conversations */}
        { conversations.data === null && <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</p> }

        {/* listed conversations for ad creator */}
        { conversations.data && toy.userId != conversationId && <div className="list-group">
          { conversations.data.map((c,i) => <Link key={i} href={`/toy?id=${postSlug}&conversation=${c.id}&section=reply`} as={`/toy/${postSlug}?conversation=${c.id}&section=reply`}>
            <a className={cn({
              'list-group-item justify-content-between': true,
              'active': conversationId == c.id,
              'list-group-item-action': conversationId != c.id
            })}>
              <img style={{width: '36px', height: 'auto'}} className="mr-2" src={c.avatarFile || config.defaultAvatarImage} alt={c.displayName} />&nbsp;{ c.displayName }
              { c.totalUnreadMessages > 0 ? <span className="badge badge-default badge-pill">{ c.totalUnreadMessages }</span> : <span>&nbsp;</span> }
            </a>
          </Link>) }
        </div> }

        {/* listed conversation (with ad creator) for visitor */}
        { conversations && conversationId && toy.userId == conversationId && <div className="list-group">
            <div className="list-group-item active">
              <img style={{width: '36px', height: 'auto'}} className="mr-2" src={toy.userData.avatarFile || config.defaultAvatarImage} />&nbsp;{ toy.userData.displayName }
            </div>
        </div> }

      </div>
      <div className="col-md-9">

        {/* loading messages */}
        { !messages && <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</p> }

        {/* message form */}
        { messages && conversationId && <div>
          <Element name="reply-form">
            { otherUser && <h3 className="display-3 display-sm"><MessageAvatar className="avatar-sm" imgSrc={otherUser.avatarFile} />Conversation with <strong>{otherUser.displayName}</strong></h3> }
            <form disabled={this.state.sending} onSubmit={this.handleCreateMessage} className="mb-5 reply-form">

              <div className="form-group">
                <input ref={(input) => { this.subjectInputRef = input }} type="text" name="subject" className="form-control" 
                defaultValue={toy.title}
                placeholder="Subject..." />
              </div>

              <div className="form-group">
                <textarea value={messageContent} className="form-control" name="content" placeholder="Content..." onChange={e => handleMessageContent(e.target.value)} />
              </div>

              <input type="hidden" name="userId" value={conversationId} readOnly />
              <input type="hidden" name="postId" value={postId} readOnly />

              <button type="submit" disabled={this.state.sending} className="btn btn-primary">Send Message</button>

              {toy.postType !== 'wanted' && postStatus === 'active' && otherUser && otherUser.id && otherUser.id && toy.userId != conversationId && (!toy.givenUserData || otherUser.id !== toy.givenUserData.id) && <button type="button" onClick={e => handleCompleteFavor(otherUser.id, otherUser.displayName)} className="btn btn-success ml-2">Select {otherUser.displayName} to receive this item</button>}
            </form>
          </Element>
        </div> }

        {/* listed messages */}
        { messages && messages.data && messages.data.map((m,i) => <div key={i}>
            <ComponentVisibilityTracker once={true}>
              <MessageComponent
                m={m}
                conversationId={conversationId}
                otherUser={otherUser}
                toy={toy}
                currAvatar={currAvatar}
                handleReply={this.handleReply.bind(this, m.subject)}
                handleDelete={this.handleDelete.bind(null, m.id)}
                handleUpdate={this.handleMarkAsRead.bind(this, m.id)} />
            </ComponentVisibilityTracker>
          </div>) }

        {/* if no results and on page != 1 (return to page 1) */}
        { messages && messages.data && !messages.data.length && this.state.messagesFilters.page != 1 && <p>
          <Link 
            href={`/toy?id=${postSlug}&conversation=${conversationId}&page=1&section=reply`} 
            as={`/toy/${postSlug}?conversation=${conversationId}&page=1&section=reply`}>
            <a>Go to page 1</a>
          </Link>
        </p> }

        {/* pagination */}
        { messages && conversationId && postSlug && <Pagination pagin={messages.paginator} onChange={page => Router.push(`/toy?id=${postSlug}&conversation=${conversationId}&page=${page}&section=reply`, `/toy/${postSlug}?conversation=${conversationId}&page=${page}&section=reply`)} /> }

      </div>
    </div>);
  }
}

class MessageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      css: {
        // boxShadow: '0 0 0 1px rgba(0,0,0,.15)',
        transition: 'all 600ms ease-in'
      }
    }
  }

  async componentDidMount() {
    if (this.isCurrentUserRecipient() && !this.props.m.isRead) {
      await this.props.handleUpdate() !== false && this.markAsRead();
    }
  }
  async componentWillReceiveProps(nextProps) {
    if (
      nextProps.isVisible != this.props.isVisible &&
      this.isCurrentUserRecipient() &&
      !this.props.m.isRead &&
      nextProps.isVisible
    ) {
      await this.props.handleUpdate() !== false && this.markAsRead();
    }
  }

  markAsRead() {
    console.log('marking');
    this.setState({
      css: ObjectAssign({}, this.state.css, {
        backgroundColor: '#d8ffd1'
      })
    }, () => {
      setTimeout(() => this.setState({css: ObjectAssign({}, this.state.css, {backgroundColor: '#e6e6e6'})}), 2000);
    });
  }

  isCurrentUserRecipient() {
    const { m, otherUser, toy, conversationId } = this.props;
    if (!otherUser) return false;
    return toy.userId == conversationId ? toy.userId == m.senderId : otherUser.id == m.senderId;
    // return (otherUser && m.senderId == otherUser.id) || m.recipientId == toy.userId;
  }

  render() {
    let { m, conversationId, otherUser, toy, currAvatar, handleReply, handleDelete } = this.props;

    if (toy.userId == conversationId) {
      otherUser = ObjectAssign({}, otherUser, {
        id: toy.userId,
        avatarFile: toy && toy.userData && toy.userData.avatarFile
      });
    }

    // define statusses
    const currIsRecipient = this.isCurrentUserRecipient();
    // classnames
    const wrapperClass = cn({
      'media mb-5 rounded px-3 py-4': true,
      'bg-faded': currIsRecipient
    });
    const subjectClass = cn({
      'mt-0': true,
      'float-md-left': currIsRecipient,
      'float-md-right': !currIsRecipient
    });
    const timeStampClass = cn({
      'text-muted': true,
      'float-md-left': !currIsRecipient,
      'float-md-right': currIsRecipient
    });
    const paragraphClass = cn({
      'text-sm-right': !currIsRecipient,
      'font-weight-bold': currIsRecipient && !m.isRead
    });

    return (
      <div className={wrapperClass} style={this.state.css}>
        {/* recipient's figure */}
        {currIsRecipient && <MessageAvatar 
          imgSrc={ (otherUser && otherUser.avatarFile) || config.defaultAvatarImage}
          displayName={otherUser.displayName} /> }

        {/* message body */}
        <div className="media-body">
          <div className="clearfix">
            <h5 className={subjectClass}>
              {m.subject}
              </h5>
            <p className={timeStampClass}><small>{moment.utc(m.createdAt).fromNow()}</small></p>
          </div>
          <p className={paragraphClass} style={{whiteSpace: 'pre-wrap'}}>
            <div dangerouslySetInnerHTML={{__html: m.content}} />
            
            <span className="font-weight-normal msg-actions text-muted d-block mt-3">
              { currIsRecipient && <a href="#" onClick={handleReply} className="mr-3"><i className="fa fa-reply"></i></a> }
              <a href="#" onClick={handleDelete}><i className="fa fa-trash"></i></a>
            </span>
          </p>
        </div>

        {/* sender's figure */}
        {!currIsRecipient && <MessageAvatar
          imgSrc={currAvatar || config.defaultAvatarImage}
          displayName="Me"
          className="ml-3 mb-0" />}
      </div>
    );
  }
}

export const MessageAvatar = ({className = 'mr-3 mb-0', style = {width: '75px', height: 'auto'}, imgSrc, displayName}) => (
  <figure className={className}>
    <img 
      src={imgSrc == '' ? config.defaultAvatarImage : imgSrc} 
      className="d-block avatar-msg-img" 
      style={style} />
    <p className="m-0 text-center text-muted">
      <small>{displayName}</small>
    </p>
  </figure>
);