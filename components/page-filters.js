import Page from './page';
import ObjectAssign from 'object-assign';
import Router from 'next/router';

import { Util } from '../api/util';

/**
 * @state: {items, filters} required
 * @props: {defaultParams (obj)} optional
 * @method fetchFn required
 */

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      filters: props.defaultParams || {},
      items: props.items || {data: null, paginator: null}
    });
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchItems();
  }

  componentWillReceiveProps(nextProps) {
    const { pathname, query } = nextProps.url;
    this.setState({filters: ObjectAssign({}, this.state.filters, query)}, () => this.fetchItems());
  }

  async fetchItems(data = null) {

    if (data) {
      
      if (typeof window === 'undefined') return;
      // we have specific case for /group/:id
      this.setState({filters: ObjectAssign({}, this.state.filters, data)}, () => {
        if (location.pathname.substring(0, 7) === '/group/') {
          const urlparams = Util.updateUrlParams(data);
          const searchParams = urlparams.substring(1, urlparams.length);
          const groupId = location.pathname.substring(7, location.pathname.length);
          Router.push(`/group?id=${groupId}&${searchParams}`, `/group/${groupId}?${searchParams}`);
        } else {
          Router.push(location.pathname + Util.updateUrlParams(data));
        }
      });
    } else {
      this.setState({items: await this.fetchFn.call(null, this.state.filters)});
    }
  }

  fetchFn(filters) {
    /**
     * To be defined in child class
     */
  }
}