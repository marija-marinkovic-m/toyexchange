import React from 'react';
import Router from 'next/router';
import Session from '../auth/session';

import { NotificationStack } from 'react-notification';
import { OrderedSet } from 'immutable';

import { reloadUserDataRequestParam } from '../static/config';

import { initGA, logPageView } from '../lib/Analytics';

export default class PageMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: OrderedSet(),
      totalUnreadMessages: false,
      session: props.session
    }
    this.addNotification = this.addNotification.bind(this);
    this.removeNotification = this.removeNotification.bind(this);
    this.clearAllNotifications = this.clearAllNotifications.bind(this);
    this.notificationStack = this.notificationStack.bind(this);
  }

  static async getInitialProps({req, pathname, query}) {
    const session = new Session({req});

    let sessionData = await session.getSession();
    let lastPage = '/';
    if (Router.router) {
      const queryID = Router.router.query && Router.router.query.hasOwnProperty('id') ? '/' + Router.router.query.id : '';
      lastPage = Router.router.route + queryID;
    }

    const loggedIn = sessionData && sessionData.user !== null && typeof sessionData.user === 'object';
    const isAdmin = loggedIn && sessionData.user.data && sessionData.user.data.roleId === 1;

    return {
      pathname: req && req.url || pathname,
      session: sessionData,
      query,
      loggedIn,
      isAdmin,
      lastPage: lastPage,
      ssr: Boolean(req)
    };
  }

  componentWillUnmount() {
    if (window.MyMessagesWorker && window.mainMyMessagesListener) {
      MyMessagesWorker.removeEventListener('message', window.mainMyMessagesListener);
    }
  }

  async componentDidMount() {
    
    if (!window.GA_INITIALIZED) {
      initGA()
      window.GA_INITIALIZED = true
    }
    logPageView()


    if (!this.props.loggedIn || this.props.isAdmin) {
      if (window.MyMessagesWorker) MyMessagesWorker.postMessage('stop');
      this.setState({totalUnreadMessages: 0});
      return;
    }
    const accessToken = this.props.session.user && this.props.session.user.auth ? this.props.session.user.auth.accessToken : false;

    // accessToken is required
    if (accessToken === false) return;

    if (window.Worker && !window.MyMessagesWorker) {
      // initialize worker
      // store it as a global 'MyMessagesWorker'
      window.MyMessagesWorker = new Worker('/static/workers/my-messages.js');
    }

    if (window.MyMessagesWorker) {
      window.mainMyMessagesListener = this.handleMyMessagesWorker.bind(this);
      MyMessagesWorker.addEventListener('message', window.mainMyMessagesListener);

      // send worker the valid accessToken
      MyMessagesWorker.postMessage(accessToken);
    }

    // should forceUpdate the session user data
    if (this.props.query) {
      const forceUpdate = Boolean(this.props.query[reloadUserDataRequestParam]);
      if (forceUpdate) {
        const session = new Session();
        this.setState({session: await session.updateSession()});
      }
    }
  }
  terminateMessagesWorker() {
    if (typeof window !== 'undefined' && window.MyMessagesWorker) {
      MyMessagesWorker.postMessage('stop');
    }
  }
  handleMyMessagesWorker(e) {
    console.log('MyMessagesWorker: ', e.data);
    if (e.data && !isNaN(parseInt(e.data, 10))) {
      this.setState({totalUnreadMessages: e.data});
      return;
    }
    this.setState({totalUnreadMessages: 0});
  }

  addNotification({
    title = 'Success',
    message ='',
    action = 'Dismiss',
    dismissAfter = 3500,
    onClick
  }) {
    const uid = String.fromCharCode(65 + Math.floor(Math.random() * 26)) + Date.now();
    return this.setState({
      notifications: this.state.notifications.add({
        message: message,
        key: uid,
        title: title,
        action: action,
        dismissAfter: dismissAfter,
        onClick: typeof onClick !== 'function' ? deactivate => {
          this.removeNotification(uid);
        } : onClick
      })
    });
  }

  removeNotification(uid) {
    this.setState({
      notifications: this.state.notifications.filter(n => n.key !== uid)
    })
  }

  clearAllNotifications() {
    this.setState({notifications: OrderedSet()})
  }

  notificationStack() {
    return <NotificationStack 
      notifications={this.state.notifications.toArray()}
      onDismiss={notification => this.setState({
        notifications: this.state.notifications.delete(notification)
      })}
    />
  }
}