export default ({posttype, order, handle}) => (
  <div>
    <ul className="nav text-uppercase ml-md-5 float-md-right">
      <li className="nav-item">
        <a href="#" onClick={e => {e.preventDefault(); handle('postType', null)}} className={`nav-link ${!posttype ? 'active' : ''}`}>All</a>
      </li>
      <li className="nav-item">
        <a href="#" onClick={e => {e.preventDefault(); handle('postType', 'offer')}} className={`nav-link ${posttype && posttype === 'offer' ? 'active' : ''}`}>Offer</a>
      </li>
      <li className="nav-item">
        <a href="#" onClick={e => {e.preventDefault(); handle('postType', 'wanted')}} className={`nav-link ${posttype && posttype === 'wanted' ? 'active' : ''}`}>Wanted</a>
      </li>
    </ul>
    <ul className="nav text-uppercase float-md-right">
      <li className="nav-item">
        <a href="#" onClick={e => {e.preventDefault(); handle('order', 'desc')}} className={`nav-link ${order === 'desc' ? 'active': ''}`}>Newer</a>
      </li>
      <li className="nav-item">
        <a href="#" onClick={e => {e.preventDefault(); handle('order', 'asc')}} className={`nav-link ${order && order === 'asc' ? 'active' : ''}`}>Older</a>
      </li>
    </ul>
  </div>
);