import { Component } from 'react';
import Groups from '../api/groups';
import { Group } from '../pages/browse-groups';


const defaultGroupParams = {
  pagin: 1,
  perPage: 3,
  withTotals: 1
}

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {data: null}
    }
  }
  async componentDidMount() {
    this.setState({
      items: await Groups.getGroups(defaultGroupParams)
    })
  }
  render() {
    const items = this.state.items.data === null ? 'Loading...' : this.state.items.data.map((g,i) => <Group group={g} key={i} classes="" />)
    return(
      <div>
        <h4 className="mb-4 text-muted">Popular Groups</h4>
        { items }
      </div>
    );
  }
}