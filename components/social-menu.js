import Link from 'next/link';
import React from 'react';

import socialLinks from '../static/content/social-links';

export default function() {
  return (
    <ul className="navbar-nav">
      { socialLinks.map((link, i) => (
          <li key={i} className="nav-item hidden-md-down">
            <Link href={ link.href }><a className="nav-link" target="_blank"><i className={ link.iconClass }></i><span className="sr-only">{ link.label }</span></a></Link>
          </li>
        ))
      }
      <li className="nav-item ml-3 hidden-md-down">
        <Link href="/login"><a className="nav-link">Log In</a></Link>
      </li>
      <li className="nav-item hidden-lg-up">
        <Link href="/login"><a className="nav-link">Log In</a></Link>
      </li>
      <li className="nav-item">
        <Link href="/signup"><a className="nav-link">Sign Up</a></Link>
      </li>
    </ul>
  );
}

exports.SocialMenuFooter = () => (
  <ul className="nav justify-content-center">
    { socialLinks.map((link, i) => (
        <li key={i} className="nav-item">
          <Link href={ link.href }><a className="nav-link" target="_blank"><i className={ link.iconClass }></i><span className="sr-only">{ link.label }</span></a></Link>
        </li>
      ))
    }
  </ul>
);