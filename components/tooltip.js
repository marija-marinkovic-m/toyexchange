import React from 'react';
import ReactDOM from 'react-dom';


/**
 * @PROPS = {
 * children: PropTypes.any.isRequired,
 *    content: PropTypes.oneOfType([
 *      PropTypes.string,
 *      PropTypes.array
 *    ])
 * }
 */
export default class extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };

    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.setVisibility = this.setVisibility.bind(this);
    this.handleTouch = this.handleTouch.bind(this);
    this.assignOutsideTouchHandler = this.assignOutsideTouchHandler.bind(this);
  }

  show() {
    this.setVisibility(true);
  }
  hide() {
    this.setVisibility(false);
  }
  setVisibility(visible) {
    this.setState(Object.assign({}, this.state, {visible}));
  }

  handleTouch() {
    this.show();
    this.assignOutsideTouchHandler();
  }
  assignOutsideTouchHandler() {
    const handler = e => {
      let currentNode = e.target;
      const componentNode = ReactDOM.findDOMNode(this.refs.instance);
      while(currentNode.parentNode) {
        if (currentNode === componentNode) return;
        currentNode = currentNode.parentNode;
      }
      if (currentNode !== document) return;
      this.hide();
      document.removeEventListener('touchstart', handler);
    }
    document.addEventListener('touchstart', handler);
  }

  render() {
    const {props, state, styles, show, hide, handleTouch} = this;
    return(
      <div
        onMouseEnter={show}
        onMouseLeave={hide}
        onTouchStart={handleTouch}
        ref="wrapper" className="t-wrap">
        {props.children}
        {
          state.visible &&
          <div ref="tooltip" className="tooltip show tooltip-top" role="tooltip">
            <div ref="content" className="tooltip-inner">{props.content}</div>
          </div>
        }

        <style jsx>{`
          .t-wrap {position: relative; z-index: 98; display: inline-block; cursor: help;}
          .tooltip {
            position: absolute; bottom: 100%; left: 50%; 
            opacity: 1;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            transform: translateX(-50%);
            transition: all 400ms ease-in;
          }
          .tooltip-inner {
            min-width: 200px; max-width: 50vw;
            background: #fff; color: #000;
            border: 1px solid #ccc;
            box-shadow: 0px 2px 7px -2px rgba(0, 0, 0, 0.4);
          }
          .tooltip-inner::before {
            width: 5px; height: 5px; background: #fff;
            border-width: 1px !important;
            border-color: transparent transparent #ccc #ccc !important;
            transform-origin: 0 0;
            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            -ms-transform: rotate(-45deg);
            transform: rotate(-45deg);
            box-shadow: -4px 2px 3px 0 rgba(0, 0, 0, 0.1) !important;
          }
        `}</style>
      </div>
    );
  }
}