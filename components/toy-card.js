import Router from 'next/router';
import moment from 'moment';

import { ageRangeLegend } from '../static/config';

const defaultImg = '/static/img/default.png';

export default ({toy, classes, currUser = null}) => {
  const canEdit = currUser && currUser.data && currUser.data.id ? currUser.data.id === toy.userId : false;
  return (
    <div className={`toy-card mb-3 ${classes}`} onClick={(e) => Router.push(`/toy?id=${toy.slug}`, `/toy/${toy.slug}`)}>
      <div className="card">
        <div className="card-img-top">
          <img className="rounded-top w-100" src={ toy.imageFile ? toy.imageFile : defaultImg } alt={ toy.title } />
          <span className="btn btn-secondary text-primary text-uppercase border-0">
            View{ canEdit && ' / Edit' }
          </span>
        </div>

        <div className="card-block clearfix text-muted py-2 font-weight-bold">
          <span className="float-left">by { canEdit ? 'me' : toy.userData.displayName }</span>
          <span className="float-right">{ moment(toy.updatedAt).format('LL') }</span>
        </div>

        <div className="card-block clearfix pt-2 pb-0">
          <small className="float-left text-uppercase font-weight-bold">{ toy.postType }</small>
          <small className="float-right text-uppercase font-weight-bold">{ ageRangeLegend[toy.ageRange] }</small>
        </div>

        <div className="card-block py-3">
          <h5 className="cart-title text-capitalize">{ toy.title }</h5>
          <p className="line-clamp card-text">{ toy.excerpt }</p>
        </div>
      </div>

      <style jsx>{`
        .text-muted {border-bottom: 1px solid #f0f3f5;}
        .text-muted span {font-size: 55%;}
        .btn:hover {background-color: #fff; border-color: transparent;}
      `}</style>
    </div>
  );
};