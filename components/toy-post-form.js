import React, { Component } from 'react';
import FileUpload from '../components/file-upload';
import { ageRangeLegend } from '../static/config';
import ReactTooltip from 'react-tooltip';
/**
 * @prop submitBtnLabel (string) 'Submit'
 * @prop stateData (object)
 * @prop handleFileDrop (fn)
 * @prop handleChange (fn)
 * @prop handleSubmit (fn)
 */
export default ({submitBtnLabel, stateData, handleFileDrop, handleChange, handleSubmit, loading = false}) => (
  <form onSubmit={handleSubmit ? handleSubmit : () => false}>
    <div className="row">
      <div className="col-12 col-md-auto">
        <div className="form-group">
          <FileUpload handleFile={handleFileDrop} />
        </div>
      </div>
      <div className="col">
        <div className="form-group">
          <label htmlFor="title">Post Title</label>
          <input type="text" value={stateData.title} onChange={handleChange} name="title" className="form-control" id="title" />
        </div>
        <div className="form-group">
          <label htmlFor="description">Post Description</label>
          <textarea rows="4" className="form-control" onChange={handleChange} name="description" id="description" value={stateData.description}></textarea>
        </div>
      </div>
    </div>

    <div className="row form-group">
      <div className="col">
        <label htmlFor="postType">Post Type</label>
        <select className="form-control" name="postType" id="postType" onChange={handleChange} value={stateData.postType}>
          <option value="offer">Offer</option>
          <option value="wanted">Wanted</option>
        </select>
      </div>
      <div className="col">
        <label htmlFor="ageRange">Age Range</label>
        <select className="form-control" name="ageRange" id="ageRange" onChange={handleChange} value={stateData.ageRange}>
          { Object.keys(ageRangeLegend).map((key, i) => <option key={i} value={key}>{ ageRangeLegend[key] }</option>) }
        </select>
      </div>
    </div>

    <div className="form-group" style={stateData.postType !== 'wanted' ? ({display: 'block'}) : ({display: 'none'})}>
      <label htmlFor="pickupWindow">Pick Up Window&nbsp;&nbsp;<i className="fa fa-question-circle-o" data-tip="Please indicate when you will make the item available for pickup.<br/>Example: 9:00 - 12:00 Saturday" data-html={true}></i></label>
      <input type="text" name="pickupWindow" id="pickupWindow" onChange={handleChange} value={stateData.pickupWindow} className="form-control" />
    </div>

    <div className="row form-group" style={stateData.postType !== 'wanted' ? ({opacity: 1}) : ({display: 'none'})}>
      <div className="col">
        <label htmlFor="pickupAddress">Pickup Address</label>
        <input type="text" value={stateData.pickupAddress} onChange={handleChange} name="pickupAddress" className="form-control" id="pickupAddress" />
      </div>
      <div className="col">
        <label htmlFor="pickupPlacement">Pickup Placement&nbsp;&nbsp;<i className="fa fa-question-circle-o" data-tip="Please indicate where the item can be found during the pickup window."></i></label>
        <input type="text" name="pickupPlacement" id="pickupPlacement" onChange={handleChange} value={stateData.pickupPlacement} className="form-control" />
      </div>
    </div>

    { submitBtnLabel && handleSubmit && <button type="submit" disabled={loading} className="btn btn-primary text-uppercase mb-5">{ loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : submitBtnLabel}</button> }

    <ReactTooltip type="success" />
  </form>
)

export class ToyEditFormUncontrolled extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentType: props.defaults.postType || 'offer'
    }
    this.handleCurrTypeChange = this.handleCurrTypeChange.bind(this);
  }

  handleCurrTypeChange(e) {
    this.setState({currentType: e.target.value})
  }
  render() {
    const { mainComponent, defaults, handleFileDrop } = this.props;
    return (
      <form ref={toyEditForm => mainComponent.toyEditForm = toyEditForm}>
        <div className="row">
          <div className="col-12 col-md-auto">
            <div className="form-group">
              <FileUpload handleFile={handleFileDrop} uploadLabel="Choose another toy photo" defaultSrc={defaults.imageFile ? defaults.imageFile : null} />
            </div>
          </div>
          <div className="col">
            <div className="form-group">
              <label htmlFor="title">Post Title</label>
              <input type="text" defaultValue={defaults.title} name="title" className="form-control" id="title" />
            </div>
            <div className="form-group">
              <label htmlFor="description">Post Description</label>
              <textarea rows="4" className="form-control" name="description" id="description" defaultValue={defaults.description}></textarea>
            </div>
          </div>
        </div>

        <div className="row form-group">
          <div className="col">
            <label htmlFor="postType">Post Type</label>
            <select onChange={this.handleCurrTypeChange} className="form-control" name="postType" id="postType" defaultValue={defaults.postType}>
              <option value="offer">Offer</option>
              <option value="wanted">Wanted</option>
            </select>
          </div>
          <div className="col">
            <label htmlFor="ageRange">Age Range</label>
            <select className="form-control" name="ageRange" id="ageRange" defaultValue={defaults.ageRange}>
              { Object.keys(ageRangeLegend).map((key, i) => <option key={i} value={key}>{ ageRangeLegend[key] }</option>) }
            </select>
          </div>
        </div>

        <div className="form-group" style={this.state.currentType !== 'wanted' ? ({display: 'block'}) : ({display: 'none'})}>
          <label htmlFor="pickupWindow">Pick Up Window&nbsp;&nbsp;<i className="fa fa-question-circle-o" data-tip="Please indicate when you will make the item available for pickup.<br/>Example: 9:00 - 12:00 Saturday" data-html={true}></i></label>
          <input type="text" name="pickupWindow" id="pickupWindow" defaultValue={defaults.pickupWindow} className="form-control" />
        </div>

      <div className="row form-group" style={this.state.currentType !== 'wanted' ? ({opacity: 1}) : ({display: 'none'})}>
          <div className="col">
            <label htmlFor="pickupAddress">Pickup Address</label>
            <input type="text" defaultValue={defaults.pickupAddress} name="pickupAddress" className="form-control" id="pickupAddress" />
          </div>
          <div className="col">
            <label htmlFor="pickupPlacement">Pickup Placement&nbsp;&nbsp;<i className="fa fa-question-circle-o" data-tip="Please indicate where the item can be found during the pickup window."></i></label>
            <input type="text" name="pickupPlacement" id="pickupPlacement" defaultValue={defaults.pickupPlacement} className="form-control" />
          </div>
        </div>

        <ReactTooltip type="success" />
      </form>
    );
  }
}