import Link from 'next/link';
import Data from '../static/content/widget-login-or-signup';

export default () => (
  <div className="jumbotron jumbotron-fluid text-center bg-faded rounded border-1 px-5 py-4">
    <h3 className="pb-4">{ Data.title }</h3>
    <Link href="/signup"><a className="btn btn-primary mb-3">{ Data.signUpButtonLabel }</a></Link>
    <p className="pb-0"><small>{ Data.logInTitle } <Link href="/login"><a>{ Data.logInLinkLabel }</a></Link></small></p>
  </div>
);