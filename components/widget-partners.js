import { Component } from 'react';
import Partners from '../api/partners';

export default class PartnersWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: props.items || {data: null}
    }
  }

  async componentDidMount() {
    if (this.state.items.data !== null) return;
    this.setState({
      items: await Partners.list({pagin: '0'})
    });
  }

  render() {
    const { items: partners } = this.state;

    if (!partners || !partners.data || !partners.data.length) {
      return <div />;
    }

    return (
      <section className="text-center pt-3">
        <p className="text-muted text-uppercase" style={{fontSize: '12px'}}>As Seen In</p>
        <div className="partners-list container">
          { partners && partners.data && partners.data.map(p => <a key={p.id} href={p.url} title={p.title} target="_blank" className="m-3 d-inline-block"><img className="partner-img" src={p.imageFile} alt={p.title} /></a>) }
        </div>
      </section>
    );
  }
}