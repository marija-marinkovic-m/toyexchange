const next = require('next');
const express = require('express');
const LRUCache = require('lru-cache');
const sass = require('node-sass');
const auth = require('./auth/auth');

process.env.NODE_ENV = process.env.NODE_ENV || 'production';
process.env.PORT = process.env.PORT || 8080;

const app = next({
  dir: '.',
  dev: (process.env.NODE_ENV === 'development')
});

const handle = app.getRequestHandler();

// cache our rendered HTML page 
const ssrCache = new LRUCache({
  max: 100,
  maxAge: 1000 * 60 * 60 // 1 hour
});

app.prepare()
  .then(() => {
    const server = express();

    auth.configure({
      app: app,
      server: server
    });

    // Add route to serve compiled SCSS from /assets/{build id}/main.css
    // in development it is inlined
    const sassResult = sass.renderSync({file: './css/main.scss'});
    server.get('/assets/:id/main.css', (req, res) => {
      res.setHeader('Content-Type', 'text/css');
      res.setHeader('Cache-Control', 'public, max-age=2592000');
      res.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString());
      res.send(sassResult.css);
    });

    // `renderAndCache` utility defined below is used to serve pages 
    server.get('/', (req, res) => {
      app.render(req, res, '/');
    });

    // Single GROUP custom route
    // req to '/group/{anything}' handled by 'pages/group.js'
    server.get('/group/:id', (req, res) => {
      return app.render(req, res, '/group', Object.assign({}, req.query, req.params));
    });
    // admin groups
    server.get('/admin/group/:id', (req, res) => {
      renderAndCache(req, res, '/admin/group', req.params);
    });
    // admin reported posts
    server.get('/admin/reported-post/:id', (req, res) => {
      renderAndCache(req, res, '/admin/reported-post', req.params);
    });

    // Single TOY custom route
    // Requests to '/toy/{anything}' will be handled by 'pages/toy.js'
    // and the {anything} part will be pased to the page in parameters
    server.get('/toy/:id', (req, res) => {
      return app.render(req, res, '/toy', Object.assign({}, req.params, req.query));
      // renderAndCache(req, res, '/toy', req.params);
    });

    // Default catch-all handler to allow Next.js to handle all other routes
    server.all('*', (req, res) => {
      if (req.query.refCode) {
        req.session.refCode = req.query.refCode;
      }
      return handle(req, res);
    });

    // Set vary header (good practice)
    server.use(function (req, res, nextApp) {
      res.setHeader('Vary', 'Accept-Encoding');
      next();
    });

    server.listen(process.env.PORT, err => {
      if (err) {
        throw err;
      }
      console.log('> Ready on http://localhost:' + process.env.PORT + ' [' + process.env.NODE_ENV + ']');
    });

  })
  .catch(err => {
    console.log('An error occurred, unable to start the server');
    console.log(err);
  });


  /**
   * NOTE: make sure to modify this to take into account anything that should trigger
   * an immediate page change (e.g a locale stored in req.session)
   */
  function getCacheKey(req) {
    return `${req.url}`;
  }

  function renderAndCache(req, res, pagePath, queryParams) {

    // skip cache thing on development
    if (process.env.NODE_ENV === 'development') { // @TODO: remove production check when you figure out how to handle session update
      return app.render(req, res, pagePath, queryParams);
    }

    if (req.session.resession) {
      req.session.resession = false;
      ssrCache.reset();
      return app.render(req, res, pagePath, queryParams);
    }

    const key = getCacheKey(req);

    // if we have a page in the cache, let's serve it
    if (ssrCache.has(key)) {
      console.log(`CACHE HIT: ${key}`);
      res.send(ssrCache.get(key));
      return;
    }

    // if not let's render the page into HTML
    app.renderToHTML(req, res, pagePath, queryParams)
      .then(html => {
        // let's cache this page 
        console.log(`CACHE MISS: ${key}`);
        ssrCache.set(key, html);
        res.send(html);
      })
      .catch(err => {
        app.renderError(err, req, res, pagePath, queryParams);
      });
  }