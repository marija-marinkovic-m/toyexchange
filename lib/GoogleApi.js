import ObjectAssign from 'object-assign';
import { Util } from '../api/util';
import { _GAPI_KEY, _GAPI_URI } from '../static/config';

const GoogleApi = (options = {}) => {
  const { key, libraries, v } = ObjectAssign({
    key: _GAPI_KEY,
    libraries: [],
    v: '3'
  }, options);

  return Util.getURL('/js', {
    key, v,
    libraries: libraries.join('')
  }, _GAPI_URI);
}

const GoogleGeocode = (options = {}) => {
  const { key, address } = ObjectAssign({
    key: _GAPI_KEY,
    address: ''
  }, options);
  
  return Util.getURL('/geocode/json', {key, address}, _GAPI_URI);
}

module.exports = {
  GoogleApi,
  GoogleGeocode
}