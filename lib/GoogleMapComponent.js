import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { camelize } from '../lib/String';
import { makeCancelable } from '../lib/cancelablePromise';
import ObjectAssign from 'object-assign';

const defaultStyles = {
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  },
  map: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0
  }
};
const eventNames = [
  'ready',
  'click',
  'dragend',
  'recenter',
  'bounds_changed',
  'center_changed',
  'dblclick',
  'dragstart',
  'heading_change',
  'idle',
  'maptypeid_changed',
  'mousemove',
  'mouseout',
  'mouseover',
  'projection_changed',
  'resize',
  'rightclick',
  'tilesloaded',
  'tilt_changed',
  'zoom_changed'
];

class GoogleMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLocation: {
        lat: props.initialCenter.lat,
        lng: props.initialCenter.lng
      }
    }

    this.listeners = {};
  }

  componentDidMount() {
    this.loadMap();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.google !== this.props.google) {
      this.loadMap();
    }
    if (prevProps.visible !== this.props.visible) {
      this.restyleMap();
    }
    if (prevProps.zoom !== this.props.zoom) {
      this.map.setZoom(this.props.zoom);
    }
    if (prevProps.center !== this.props.center) {
      this.setState({
        currentLocation: this.props.center
      })
    }
    if (prevState.currentLocation !== this.state.currentLocation) {
      this.recenterMap();
    }
  }

  componentWillUnmount() {
    const { google } = this.props;
    Object.keys(this.listeners).forEach(e => google.maps.event.removeListener(this.listeners[e]));
  }

  loadMap() {
    if (this.props && this.props.google) {
      const { google } = this.props;
      const maps = google.maps;

      const mapRef = this.refs.map;
      const node = ReactDOM.findDOMNode(mapRef);
      const curr = this.state.currentLocation;
      const center = new maps.LatLng(curr.lat, curr.lng);

      const mapConfig = ObjectAssign({}, this.props, {
        center
      });

      Object.keys(mapConfig).forEach(key => {
        // allow to configure mapConfig with false
        if (mapConfig[key] === null) {
          delete mapConfig[key];
        }
      });

      this.map = new maps.Map(node, mapConfig);


      let circleRad = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: this.map,
        center: center,
        radius: 1200 // meters
      });

      eventNames.forEach(e => this.listeners[e] = this.map.addListener(e, this.handleEvent(e)));
      maps.event.trigger(this.map, 'ready');
    }
  }

  handleEvent(eventName) {
    let timeout;
    const handlerName = `on${camelize(eventName)}`;

    return e => {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      timeout = setTimeout(() => {
        if (this.props[handlerName]) {
          this.props[handlerName](this.props, this.map, e);
        }
      }, 0);
    }
  }

  recenterMap() {
    const map = this.map;
    const { google } = this.props;
    const maps = google.maps;

    if (!google) return;
    if (!map) return;
    
    let center = this.state.currentLocation;
    if (!(center instanceof google.maps.LatLng)) {
      center = new google.maps.LatLng(center.lat, center.lng);
    }
    map.setCenter(center);
    maps.event.trigger(map, 'recenter');
  }

  restyleMap() {
    if (this.map) {
      const { google } = this.props;
      google.maps.event.trigger(this.map, 'resize');
    }
  }

  renderChildren() {
    const { children } = this.props;
    if (!children) return;

    return React.Children.map(children, c => {
      return React.cloneElement(c, {
        map: this.map,
        google: this.props.google,
        mapCenter: this.state.currentLocation
      });
    });
  }

  render() {
    const mapStyle = ObjectAssign({}, defaultStyles.map, this.props.style, {
      display: this.props.visible ? 'inherit' : 'none'
    });
    const containerStyle = ObjectAssign({}, defaultStyles.container, this.props.containerStyle);

    return (
      <div style={containerStyle} className={this.props.className}>
        <div style={mapStyle} ref="map">
          Loading map...
        </div>
        { this.renderChildren() }
      </div>
    );
  }
}
GoogleMap.propTypes = {
  google: PropTypes.object.isRequired,
  zoom: PropTypes.number,
  center: PropTypes.object, // {lat: string, lng: string}
  initialCenter: PropTypes.object, // {lat: string, lng: string}
  className: PropTypes.string,
  style: PropTypes.object,
  containerStyle: PropTypes.object,
  visible: PropTypes.bool
}

// @TODO: in production GoogleMap.propTypes is 'undefined'; 
// needs fix if event listeners should be enabled
// eventNames.forEach(e => GoogleMap.propTypes[camelize(e)] = PropTypes.func);

GoogleMap.defaultProps = {
  zoom: 14,
  initialCenter: {
    lat: 37.774929,
    lng: -122.419416
  },
  center: {},
  containerStyle: {},
  style: {},
  visible: true
};


module.exports = {
  GoogleMap
}