import React from 'react';
import Page from '../components/page';
import Layout from '../components/layout';

import TransHolder from '../components/component-transition-placeholder';

import Data from '../static/content/page-about';

import { NotificationStack } from 'react-notification';

// @temp
// import AsyncData from '../components/async-data';

export default class extends Page {

  static async getInitialProps({req, pathname}) {
    let props = await super.getInitialProps({req, pathname});

    // if running on server, perform Async call
    // if (typeof window === 'undefined') {
    //   props.posts = await AsyncData.getData();
    // }

    return props;
  }

  constructor(props) {
    super(props);
    // this.state = {
    //   posts: props.posts || []
    // }
  }

  // This is called after rendering, only on the client (not the server)
  // this allows to render the page on the client without delaying rendering
  // then load the data fetched via an async call in when we have title
  async componentDidMount() {
    super.componentDidMount();
    // this.setState({
    //   posts: await AsyncData.getData()
    // });
  }

  render() {
    // let loadingMessage;
    // if (this.state.posts.length === 0) {
    //   loadingMessage = <p><i>Loading content from jsonplaceholder.typicode.com…</i></p>;
    // }

    return (
      <Layout props={this.props} session={this.state.session} title={Data.title} totalUnreadMessages={this.state.totalUnreadMessages}>
        <TransHolder ssr={this.props.ssr}>
          <div className="jumbotron">
            <h5 className="mt-0 mb-3 display-4">{ Data.title }</h5>
            <hr />
            {/*{ loadingMessage }*/}

            <div dangerouslySetInnerHTML={{ __html: Data.content }}></div>

            {/*{
              this.state.posts.map((post, i) => (
                <div key={i}>
                    <strong>{post.title}</strong>
                    <p><i>{post.body}</i></p>
                </div>
              ))
            }*/}
                
          </div>
        </TransHolder>
      </Layout>
    );
  }
}