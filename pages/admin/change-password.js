import React, { Component } from 'react';
import ObjectAssign from 'object-assign';

import Layout from '../../components/admin-components/layout';
import Page from '../../components/admin-components/page';

import cn from 'classnames';
import Account from '../../api/account';

export default class extends Page {

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      loading: false,
      sent: false,
      passwordExposed: false,
      passwordOldExposed: false,
      passwordConfirmExposed: false,
      form: {
        newPassword: '',
        oldPassword: '',
        confirmNewPassword: ''
      }
    });

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputUpdate = this.handleInputUpdate.bind(this);
  }

  handleInputUpdate(e) {
    let {target} = e;
    this.setState({form: ObjectAssign({}, this.state.form, {[target.name]: target.value})});
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.loading) return;

    if (this.state.form.newPassword.trim() === '' ||
      this.state.form.oldPassword.trim() === '' ||
      this.state.form.confirmNewPassword.trim() === '') {
      this.addNotification({title: 'Error', message: 'All fields are mandatory.'});
      return;
    }

    if (this.state.form.newPassword.trim() !== this.state.form.confirmNewPassword.trim()) {
      this.addNotification({title: 'Error', message: 'Password does not match the confirm password.'});
      return;
    }

    this.clearAllNotifications();
    this.setState({loading: true});
    const exit = () => this.setState({loading: false});

    Account.updatePassword(this.state.form)
      .then(res => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request Failed'});
          return exit();
        }
        this.addNotification({title: 'Success', message: res.message ? res.message : 'Password updated'});
        exit();
        this.setState({sent: true});
      });
  }

  render () {
    return (
      <Layout {...this.props} session={this.state.session}>
        <div className="col-md-12">
          <div className="card">
            <div className="header">
              <h4 className="title" style={{marginBottom: '30px'}}><i className="pe-7s-key"></i>&nbsp;Change Password</h4>

              <form onSubmit={this.handleSubmit}>
                <div className="mb-5">
                  <div className="text-left">
                    <div className="form-group">
                      <label>Old Password</label>
                      <div className="input-group">
                        <input
                          value={this.state.form.oldPassword}
                          name="oldPassword"
                          type={this.state.passwordOldExposed ? 'text' : 'password'}
                          className="form-control"
                          onChange={this.handleInputUpdate} />

                        <div className="input-group-btn">
                          <button
                            type="button"
                            className="btn btn-secondary"
                            style={{borderTopRightRadius: '0.25rem', borderBottomRightRadius: '0.25rem'}}
                            onClick={e => this.setState({passwordOldExposed: !this.state.passwordOldExposed})}>
                            <i className={cn(['fa'], {
                              'fa-eye': !this.state.passwordOldExposed,
                              'fa-eye-slash': this.state.passwordOldExposed
                            })}></i>
                          </button>
                        </div>
                        
                      </div>
                    </div>

                    <div className="form-group">
                      <label>New password</label>
                      <div className="input-group">
                        
                        <input
                          value={this.state.form.newPassword}
                          type={this.state.passwordExposed ? 'text': 'password'}
                          name="newPassword"
                          className="form-control"
                          onChange={this.handleInputUpdate} />

                        <div className="input-group-btn">
                          <button
                            type="button"
                            className="btn btn-secondary"
                            style={{borderTopRightRadius: '0.25rem', borderBottomRightRadius: '0.25rem'}}
                            onClick={e => this.setState({passwordExposed: !this.state.passwordExposed})}>
                            <i className={cn(
                              ['fa'], {
                                'fa-eye' : !this.state.passwordExposed, 'fa-eye-slash': this.state.passwordExposed
                              })}>
                            </i>
                          </button>
                          
                        </div>
                      </div>
                    </div>

                    <div className="form-group">
                      <label>Confirm New password</label>
                      <div className="input-group">
                        
                        <input
                          value={this.state.form.confirmNewPassword}
                          type={this.state.passwordConfirmExposed ? 'text': 'password'}
                          name="confirmNewPassword"
                          className="form-control"
                          onChange={this.handleInputUpdate} />

                        <div className="input-group-btn">
                          <button
                            type="button"
                            className="btn btn-secondary"
                            style={{borderTopRightRadius: '0.25rem', borderBottomRightRadius: '0.25rem'}}
                            onClick={e => this.setState({passwordConfirmExposed: !this.state.passwordConfirmExposed})}>
                            <i className={cn(
                              ['fa'], {
                                'fa-eye' : !this.state.passwordConfirmExposed, 'fa-eye-slash': this.state.passwordConfirmExposed
                              })}>
                            </i>
                          </button>
                          
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                <button
                  type="submit"
                  className="btn btn-primary"
                  style={{marginBottom: '30px'}}
                  disabled={this.state.loading}>
                  { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Reset my password' }
                </button>
              </form>

              { this.notificationStack() }

            </div>
          </div>
        </div>
      </Layout>
    );
  }
}