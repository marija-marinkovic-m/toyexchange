import ObjectAssign from 'object-assign';
import cn from 'classnames';
import { Util } from '../../api/util';
import Page from '../../components/admin-components/page';
import Layout from '../../components/admin-components/layout';

import { FloatingMenu } from '../../components/admin-components/fab';
import CountriesWidget from '../../components/admin-components/widget-countries';
import Pagination from '../../components/bootstrap/pagination';

import Countries from '../../api/admin/countries';
import States from '../../api/admin/states';
import Zips from '../../api/admin/zips';

import { Modal, ModalBody, ModalFooter } from '../../components/modal';

export default class extends Page {

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      countries: null,
      currentCountry: null,
      states: null,
      currentState: null,
      zips: null,

      newCountryModal: false,
      newStateModal: false,
      newZipModal: false,

      loading: false
    });

    this.switchCountry = this.switchCountry.bind(this);
    this.switchState = this.switchState.bind(this);

    this.createCountry = this.createCountry.bind(this);
    this.editCountry = this.editCountry.bind(this);
    this.deleteCountry = this.deleteCountry.bind(this);

    this.createState = this.createState.bind(this);
    this.editState = this.editState.bind(this);
    this.deleteState = this.deleteState.bind(this);

    this.createZip = this.createZip.bind(this);
    this.editZip = this.editZip.bind(this);
    this.deleteZip = this.deleteZip.bind(this);
  }

  async componentDidMount() {
    let countries = await Countries.list({pagin: '0'});
    if (countries.data && countries.data.length) {
      let currentCountry = countries.data[0];
      let states = await States.list({pagin: '0', countryId: currentCountry.id});

      let currentState = null;
      let zips = {data: []};
      let currentZip = null;

      if (states && states.data && states.data.length) {
        currentState = states.data[0];
        zips = await Zips.list({pagin: '0', stateId: currentState.id});
        if (zips && zips.data && zips.data.length) {
          currentZip = zips.data[0];
        }
      }

      this.setState({
        countries,
        currentCountry,
        states,
        currentState,
        zips,
        currentZip
      })
    }
  }

  async switchCountry(currentCountry) {
    this.setState({
      states: null,
      zips: null
    }, async () => {
      let states = await States.list({pagin: '0', countryId: currentCountry.id});
      let zips = {data: []};
      let currentState = null;
      let currentZip = null;
      if (states && states.data && states.data.length) {
        currentState = states.data[0];
        zips = await Zips.list({pagin: '0', stateId: currentState.id});
        if (zips && zips.data && zips.data.length) {
          currentZip = zips.data[0];
        }
      }
      this.setState({
        currentCountry,
        states,
        currentState,
        zips,
        currentZip
      })
    })
  }

  handleResponse(res, cb = () => false, successMsg = '') {
    Util.handleResponse(this, res, cb, successMsg);
  }

  async createCountry() {
    if (!this.refs.newCountryForm || this.state.loading) return;
    const data = Util.formToPlainObject(this.refs.newCountryForm);

    // let response = await Countries.list({pagin: 0});
    let response = await Countries.create(data);
    this.handleResponse(
      response,
      async () => {
        const countries = await Countries.list({pagin: '0'});
        let updatedState = {
          newCountryModal: false,
          countries
        }
        if (!this.state.currentCountry && countries && countries.data.length) {
          updatedState.currentCountry = countries.data[0];
          updatedState.states = await States.list({pagin: '0', countryId: updatedState.currentCountry.id});
          updatedState.currentState = updatedState.states && updatedState.states.data.length ? updatedState.states.data[0] : null;
          
          if (updatedState.currentState) {
            updatedState.zips = await Zips.list({pagin: '0', stateId: currentState.id});
            updatedState.currentZip = updatedState.zips && updatedState.zips.data && updatedStates.zips.data.length ? updatedState.zips.data[0] : null;
          } else {
            updatedState.zips = {data: []};
            updatedState.currentZip = null;
          }
        }
        this.setState(updatedState);
      },
      'Country entry successfully added.'
    );
  }
  async editCountry() {
    if (!this.refs.editCountryForm || this.state.loading || !this.state.currentCountry) return;
    const data = Util.formToPlainObject(this.refs.editCountryForm);
    const currentCountry = ObjectAssign({}, this.state.currentCountry, data);

    this.setState({loading: true});
    let response = await Countries.update(this.state.currentCountry.id, data);

    this.handleResponse(response,
      async () => {
        this.setState({
          countries: await Countries.list({pagin: '0'}),
          currentCountry,
          editCountryModal: false
        })
      },
      'Country successfully updated'
    );
  }
  async deleteCountry() {
    if (this.state.loading || !this.state.currentCountry) return;
    this.setState({loading: true});

    let response = await Countries.delete(this.state.currentCountry.id);

    this.handleResponse(response,
      async () => {
        const countries = await Countries.list({pagin: '0'});
        const currentCountry = countries && countries.data && countries.data.length ? countries.data[0] : null;

        const states = currentCountry ? await States.list({pagin: '0', countryId: currentCountry.id}) : {data: []};
        const currentState = states && states.data && states.data.length ? states.data[0] : null;
        const zips = currentState ? await Zips.list({pagin: '0', stateId: currentState.id}) : {data: []};
        const currentZip = zips && zips.data && zips.data.length ? zips.data[0] : null;

        this.setState({
          countries,
          currentCountry,
          states,
          currentState,
          zips,
          currentZip,
          deleteCountryModal: false
        });
      },
      'Country sucessfully deleted'
    );
  }

  async createState() {
    if (!this.refs.newStateForm || this.state.loading || !this.state.currentCountry) return;
    this.setState({loading: true});

    const data = Util.formToPlainObject(this.refs.newStateForm);

    let response = await States.create(data);
    this.handleResponse(response,
      async() => {
        const states = await States.list({pagin: '0', countryId: this.state.currentCountry.id});
        let updatedState = {
          newStateModal: false,
          states
        };
        if (!this.state.currentState && states.data.length) {
          let currentState = states.data[0];
          let zips = currentState ? await Zips.list({pagin: '0', stateId: currentState.id}) : {data: []};
          let currentZip = zips && zips.data && zips.data.length ? zips.data[0] : null;

          updatedState = ObjectAssign({}, updatedState, {currentState, zips, currentZip});
        }
        this.setState(updatedState);
      },
      'State entry created successfully.'
    );
  }
  async editState() {
    const { states, currentState } = this.state;
    if (!this.refs.editStateForm || this.state.loading || !currentState) return;

    this.setState({loading: true});

    const data = Util.formToPlainObject(this.refs.editStateForm);

    let response = await States.update(currentState.id, data);
    this.handleResponse(response,
      () => {
        this.setState({
          states: ObjectAssign({}, states, {
            data: states.data.map(s => {
              if (s.id === currentState.id) {
                return ObjectAssign({}, s, data);
              }
              return s;
            })
          }),
          currentState: ObjectAssign({}, currentState, data),
          editStateModal: false
        })
      }
    );
  }
  async deleteState() {
    const currentState = ObjectAssign({}, this.state.currentState);
    if (!currentState || this.state.loading) return;
    this.setState({loading: true});

    let response = await States.delete(currentState.id);

    this.handleResponse(response,
      async () => {
        const states = ObjectAssign({}, this.state.states, {
          data: this.state.states.data.filter(s => s.id !== currentState.id)
        });
        const currentState = states.data.length ? states.data[0] : null;

        if (currentState) {
          const zips = await Zips.list({pagin: '0', stateId: currentState.id});
          const currentZip = zips && zips.data && zips.data.length ? zips.data[0] : null; 
        } else {
          const zips = {data: []};
          const currentZip = null;
        }

        this.setState({
          deleteStateModal: false,
          states,
          currentState,
          zips,
          currentZip
        });
      }
    );
  }

  async switchState(currentState) {
    this.setState({
      zips: {data: []},
      currentZip: null
    }, async () => {
      let zips = await Zips.list({pagin: '0', stateId: currentState.id});
      const currentZip = zips && zips.data && zips.data.length ? zips.data[0] : null;
      this.setState({
        currentState,
        zips,
        currentZip
      })
    })
  }

  async createZip() {
    if (!this.refs.newZipForm ||!this.state.currentState || this.state.loading) return;
    this.setState({loading: true});

    let data = Util.formToPlainObject(this.refs.newZipForm);

    let response = await Zips.create(data);
    this.handleResponse(response,
      async () => {
        const zips = await Zips.list({pagin: '0', stateId: this.state.currentState.id});
        let updatedState = {
          newZipModal: false,
          zips
        }
        if (!this.state.currentZip && zips && zips.data && zips.data.length) {
          updatedState.currentZip = zips.data[0];
        }
        this.setState(updatedState);
      }
    );
  }
  async editZip() {
    if (!this.refs.editZipForm || !this.state.currentZip || this.state.loading) return;
    this.setState({loading: true});

    let currentZip = ObjectAssign({}, this.state.currentZip, Util.formToPlainObject(this.refs.editZipForm));

    let response = await Zips.update(this.state.currentZip.id, currentZip);

    this.handleResponse(response,
      () => {
        this.setState({
          zips: ObjectAssign({}, this.state.zips, {
            data: this.state.zips.data.map(z => z.id === currentZip.id ? currentZip : z)
          }),
          currentZip,
          editZipModal: false
        })
      }
    );

  }
  async deleteZip() {
    if (!this.state.currentZip || this.state.loading) return;
    this.setState({loading: true});

    let response = await Zips.delete(this.state.currentZip.id);

    this.handleResponse(response,
      () => {
        let zips = {data: this.state.zips.data.filter(z => z.id !== this.state.currentZip.id)};
        let currentZip = zips.data.length ? zips.data[0] : null;
        
        this.setState({
          deleteZipModal: false,
          zips,
          currentZip
        });
      }
    );
  }

  render() {
    let floatingMenuItems = [
      {label: 'Add new Zip Code', icon: 'pe-7s-map-2', action: () => this.setState({newZipModal: true})},
      {label: 'Add new State', icon: 'pe-7s-map-marker', action: () => this.setState({newStateModal: true})},
      {label: 'Add new Country', icon: 'pe-7s-map', action: () => this.setState({newCountryModal: true})}
    ];

    const { countries, zips, states, currentCountry, currentState, currentZip } = this.state;

    return (
      <Layout {...this.props} session={this.state.session}>
        { this.notificationStack() }
        <div className="col-md-12">
          <div className="row">
            <div className={cn({'col-md-2': 1, 'bg-warning': !countries})}>
              <CountriesList
                countries={countries}
                currentCountry={currentCountry}
                onSwitchCountry={this.switchCountry}
                onEdit={() => this.setState({editCountryModal: true})}
                onDelete={() => this.setState({deleteCountryModal: true})} />
            </div>
            <div className={cn({'col-md-5': 1, 'bg-warning': states && !states.data.length})}>
              <StatesList 
                states={states}
                currentState={currentState}
                currentCountry={currentCountry}
                onSwitchState={this.switchState}
                onCreateState={() => this.setState({newStateModal: true})}
                onEdit={() => this.setState({editStateModal: true})}
                onDelete={() => this.setState({deleteStateModal: true})}
              />
            </div>
            <div className={cn({'col-md-5': 1, 'bg-info': zips && !zips.data.length})}>
              <ZipsList 
                zips={zips}
                currentCountry={currentCountry}
                currentState={currentState}
                currentZip={currentZip}
                onSwitchZip={(nextZip) => this.setState({currentZip: nextZip})}
                onCreateZip={() => this.setState({newZipModal: true})}
                onEdit={() => this.setState({editZipModal: true})}
                onDelete={() => this.setState({deleteZipModal: true})}
              />
            </div>
          </div>
        </div>
        <FloatingMenu items={floatingMenuItems} />

        {/* NEW COUNTRY MODAL */}
        <Modal
          isOpen={this.state.newCountryModal}
          onClose={() => this.setState({newCountryModal: false})}
          modalTitle="Add new country entry">
          <ModalBody>
            <form ref="newCountryForm">
              <div className="form-group">
                <label htmlFor="c-name">Name (required)</label>
                <input
                  type="text"
                  id="c-name"
                  name="name"
                  className="form-control" />
              </div>
              <div className="form-group">
                <label htmlFor="c-iso">ISO code of the country</label>
                <input
                  type="text"
                  id="c-iso"
                  name="iso"
                  className="form-control" />
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({newCountryModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-danger"
              onClick={this.createCountry}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Submit' }
            </button>
          </ModalFooter>
        </Modal>

        {/* EDIT COUNTRY MODAL */}
        <Modal
          isOpen={this.state.editCountryModal}
          onClose={() => this.setState({editCountryModal: false})}
          modalTitle="Edit selected country">
          <ModalBody>
            {currentCountry && <form ref="editCountryForm">
              <div className="form-group">
                <label htmlFor="e-name">Name (required)</label>
                <input
                  type="text"
                  id="e-name"
                  name="name"
                  className="form-control"
                  defaultValue={currentCountry.name} />
              </div>
              <div className="form-group">
                <label htmlFor="e-iso">ISO code of the country</label>
                <input
                  type="text"
                  id="e-iso"
                  name="iso"
                  className="form-control"
                  defaultValue={currentCountry.iso} />
              </div>
            </form>}
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({editCountryModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-danger"
              onClick={this.editCountry}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Save changes'}
            </button>
          </ModalFooter>
        </Modal>

        {/* DELETE COUNTRY MODAL */}
        <Modal
          isOpen={this.state.deleteCountryModal}
          onClose={() => this.setState({deleteCountryModal: false})}
          modalTitle="Are you sure?"
          sizeClass="modal-sm">
          <ModalBody>
            { currentCountry && <p>{currentCountry.name} will be permanently deleted.</p> }
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({deleteCountryModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-danger"
              onClick={this.deleteCountry}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Confirm deletion' }
            </button>
          </ModalFooter>
        </Modal>

        {/* NEW STATE MODAL */}
        <Modal
          isOpen={this.state.newStateModal}
          onClose={() => this.setState({newStateModal: false})}
          modalTitle={currentCountry ? `Add new state to ${currentCountry.name} country entry` : 'Add new state entry'}>
          <ModalBody>
            {currentCountry && <form ref="newStateForm">
              <input type="hidden" name="countryId" value={currentCountry.id} readOnly />
              <div className="form-group">
                <label htmlFor="cs-name">Name of the state</label>
                <input
                  type="text"
                  id="cs-name"
                  name="name"
                  className="form-control" />
              </div>
              <div className="form-group">
                <label htmlFor="cs-abbreviation">Abbreviation of the state</label>
                <input
                  type="text"
                  id="cs-abbreviation"
                  name="abbreviation"
                  className="form-control" />
              </div>
            </form>}
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({newStateModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-primary"
              onClick={this.createState}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Submit'}
            </button>
          </ModalFooter>
        </Modal>

        {/* EDIT STATE MODAL */}
        <Modal
          isOpen={this.state.editStateModal}
          onClose={() => this.setState({editStateModal: false})}
          modalTitle="Edit selected state">
          <ModalBody>
            { currentState && <form ref="editStateForm">
              <div className="form-group">
                <label htmlFor="es-name">Name of the state</label>
                <input 
                  type="text"
                  id="es-name"
                  name="name"
                  className="form-control"
                  defaultValue={currentState.name} />
              </div>
              <div className="form-group">
                <label htmlFor="es-abbreviation">Abbreviation of the state</label>
                <input
                  type="text"
                  id="es-abbreviation"
                  name="abbreviation"
                  className="form-control"
                  defaultValue={currentState.abbreviation} />
              </div>
            </form> }
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({editStateModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-primary"
              onClick={this.editState}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Save Changes'}
            </button>
          </ModalFooter>
        </Modal>

        {/* DELETE STATE MODAL */}
        <Modal
          isOpen={this.state.deleteStateModal}
          onClose={() => this.setState({deleteStateModal: false})}
          modalTitle="Are you sure">
          <ModalBody>
            <p>{currentState ? currentState.name + '(' + currentState.abbreviation + ')' : 'Selected state'} will be permanently deleted.</p>
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({deleteStateModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-danger"
              onClick={this.deleteState}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Confirm deletion'}
            </button>
          </ModalFooter>
        </Modal>

        {/* NEW ZIP MODAL */}
        <Modal
          isOpen={this.state.newZipModal}
          onClose={() => this.setState({newZipModal: false})}
          modalTitle="Add new zip entry">
          <ModalBody>
            { currentState && <form ref="newZipForm">
              <input type="hidden" name="stateId" value={currentState.id} readOnly />
              <div className="form-group">
                <label htmlFor="cz-name">Name of the County</label>
                <input
                  type="text"
                  id="cz-name"
                  name="name"
                  className="form-control" />
              </div>
              <div className="form-group">
                <label htmlFor="cz-code">Zip code</label>
                <input
                  type="text"
                  id="cz-code"
                  name="code"
                  className="form-control" />
              </div>
            </form> }
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({newZipModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-primary"
              onClick={this.createZip}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Submit'}
            </button>
          </ModalFooter>
        </Modal>

        {/* EDIT ZIP MODAL */}
        <Modal
          isOpen={this.state.editZipModal}
          onClose={() => this.setState({editZipModal: false})}
          modalTitle={currentZip ? `Edit ${currentZip.name} zip code` : 'Edit selected zip code'}>
          <ModalBody>
            { currentZip && <form ref="editZipForm">
              <div className="form-group">
                <label htmlFor="ez-name">Name of the County</label>
                <input
                  type="text"
                  id="ez-name"
                  name="name"
                  className="form-control"
                  defaultValue={currentZip.name} />
              </div>
              <div className="form-group">
                <label htmlFor="ez-code">Zip code</label>
                <input
                  type="text"
                  id="ez-code"
                  name="code"
                  className="form-control"
                  defaultValue={currentZip.code} />
              </div>
            </form> }
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({editZipModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-primary"
              onClick={this.editZip}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Save Changes'}
            </button>
          </ModalFooter>
        </Modal>

        {/* DELETE ZIP MODAL */}
        <Modal
          isOpen={this.state.deleteZipModal}
          onClose={() => this.setState({deleteZipModal: false})}
          modalTitle="Are you sure?">
          <ModalBody>
            { currentZip && <p>{currentZip.name} will be permanently deleted.</p> }
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              className="btn btn-default"
              onClick={() => this.setState({deleteZipModal: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              className="btn btn-danger"
              onClick={this.deleteZip}>
              {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Confirm Deletion'}
            </button>
          </ModalFooter>
        </Modal>
      </Layout>
    );
  }
}

const CountriesList = ({
  countries,
  currentCountry,
  onSwitchCountry = () => 0, 
  onDelete = () => 0,
  onEdit = () => 0
}) => {
  if (!countries) return <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</p>;
  if (countries.data && !countries.data.length) return <p>0 country entires.</p>
  return (
    <ul className="nav nav-pills nav-stacked countries">
      { countries && countries.data && countries.data.map((country, index) => {
        const onClickAction = country.id === currentCountry.id ? () => 0 : onSwitchCountry;
        return (
          <li key={index}
            role="presentation"
            className={cn({active: country.id === currentCountry.id})}>
            <a href="#" 
              onClick={(e) => {e.preventDefault(); onClickAction(country)}}>
              { country.name }
              { country.id === currentCountry.id && <span>
                <button
                  type="button"
                  onClick={onEdit}
                  className="btn btn-simple btn-default xs">
                  <i className="fa fa-edit"></i>
                </button>
                <button
                  type="button"
                  onClick={onDelete}
                  className="btn btn-simple btn-default xs">
                  <i className="fa fa-times"></i>
                </button>
              </span> }
            </a>
          </li>
        ); 
      }) }
    </ul>
  );
}

const StatesList = ({
  states,
  currentState,
  onSwitchState,
  currentCountry = null,
  onCreateState = () => 0,
  onEdit = () => 0,
  onDelete = () => 0
}) => {
  if (!states) return <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading states...</p>;
  if (states.data && !states.data.length) return <h5>0 state entries{currentCountry && <span> for {currentCountry.name}</span>}. <a href="#" onClick={e => {e.preventDefault(); onCreateState();}}><i className="pe-7s-plus"></i>&nbsp;Add state entry.</a></h5>
  return (
    <ul className="list-group list-group-flush states">
      { states.data.map((state, index) => {
        const isCurr = state.id === currentState.id;
        const onClickAction = isCurr ? () => 0 : onSwitchState.bind(null, state);
        return (
          <li key={index} 
            className={cn({'list-group-item': true, 'bg-primary': isCurr})}
            onClick={onClickAction}
            style={{cursor: 'pointer'}}>
            { state.name }, { state.abbreviation }
            { isCurr && <span>
              <button type="button" onClick={onEdit} className="btn btn-simple btn-default"><i className="fa fa-edit"></i></button>
              <button type="button" onClick={onDelete} className="btn btn-simple btn-default"><i className="fa fa-times"></i></button>
            </span> }
          </li>
        );
      }) }
    </ul>
  );
}

const ZipsList = ({
  zips,
  currentState = null,
  currentCountry = null,
  currentZip = null,
  onSwitchZip = () => 0,
  onCreateZip = () => 0,
  onEdit = () => 0,
  onDelete = () => 0
}) => {
  if (!zips) return <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading zip codes...</p>;
  if (zips.data && !zips.data.length) return <h5>No zip entries{ currentState ? <span> for {currentState.name}. <a href="#" onClick={e => {e.preventDefault(); onCreateZip();}}><i className="pe-7s-plus"></i>&nbsp;Add zip.</a></span> : '.' }</h5>
  return (
    <ul className="list-group list-group-flush states">
      {zips.data.map((zip, i) => {
        const isActive = currentZip.id === zip.id;
        const onClickAction = isActive ? () => 0 : onSwitchZip;
        return (
          <li 
            key={i}
            className={cn({'list-group-item': true, 'bg-primary': isActive})}
            onClick={onClickAction.bind(null, zip)}>
            {zip.code}, {zip.name}
            { isActive && <span>
              <button type="button" onClick={onEdit} className="btn btn-simple btn-default"><i className="fa fa-edit"></i></button>
              <button type="button" onClick={onDelete} className="btn btn-simple btn-default"><i className="fa fa-times"></i></button>
            </span> }
          </li>
        );
      })}
    </ul>
  )
}