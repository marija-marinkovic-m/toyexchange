import Page from '../../components/admin-components/page-filters';
import Layout from '../../components/admin-components/layout';
import Pagination from '../../components/bootstrap/pagination';
import GroupApplicantsWidget from '../../components/admin-components/widget-group-applicants';

import { GroupApplicants } from '../../api/admin/users';
import CountriesAPI from '../../api/admin/countries';

import ObjectAssign from 'object-assign';
import cn from 'classnames';

const defaultFilters = {
  countryId: 0,
  zipCode: '',
  status: '',
  pagin: 1,
  perPage: 15,
  page: 1
}

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      loading: 0
    });
    this.handleStatusUpdate = this.handleStatusUpdate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultFilters, ctx.query);
    return props;
  }

  fetchFn(filters) {
    return GroupApplicants.list(filters);
  }
  async handleStatusUpdate(pid, status) {
    if (!pid || !status) return;

    this.clearAllNotifications();
    this.setState({loading: pid});

    let res = await GroupApplicants.update(pid, {status});
    if (!res || res.error) {
      this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request Failed'});
      this.setState({loading: 0});
      return;
    } else {
      this.addNotification({title: 'Success', message: res.message ? res.message : 'Application successfuly updated'});
    }

    await this.updateApplicant(pid);
  }
  async handleDelete(pid) {
    if (!pid) return;
    this.setState({loading: pid});

    let res = await GroupApplicants.delete(pid);
    if (!res || res.error) {
      this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request Failed'});
      this.setState({loading: 0});
      return;
    } else {
      this.addNotification({title: 'Success', message: res.message ? res.message : 'Application successfuly deleted'});
    }

    this.fetchItems();
  }
  async updateApplicant(pid) {
    if (!pid) return;
    let updatedApplicant = await GroupApplicants.one(pid);

    this.setState({
      items: {
        data: this.state.items.data.map((a,i) => {
          if (a.id === pid && updatedApplicant.data) {
            return updatedApplicant.data;
          }
          return a;
        }),
        paginator: this.state.items.paginator
      },
      loading: 0
    });
  }

  handleFilter(prop, value, e) {
    if (e) e.preventDefault();
    this.fetchItems({
      [prop]: prop !== 'status' ? value : this.state.filters.status == value || !value ? null : value
    });
  }

  async componentDidMount() {
    super.componentDidMount();
    this.setState({
      countries: await CountriesAPI.list({pagin: '0'})
    })
  }

  render() {
    const { items, filters, loading } = this.state;
    return (
      <Layout {...this.props} session={this.state.session}>
        { this.notificationStack() }
        { !items && <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</p> }
        {
          items && items.data && 
          <div className="col-md-12 card">
            <div className="header clearfix">
              <h4 className="title pull-left">Group Applicants</h4>
              <div className="pull-right btn-group btn-group-xs">
                <button 
                  type="button"
                  onClick={this.handleFilter.bind(null, 'status', 'pending')}
                  className={cn({btn: true, 'btn-primary': filters.status === 'pending', 'btn-default': filters.status !== 'pending'})}>
                  Pending 
                </button>
                <button
                  type="button"
                  onClick={this.handleFilter.bind(null, 'status', 'notified')}
                  className={cn({btn: true, 'btn-primary': filters.status === 'notified', 'btn-default': filters.status !== 'notified'})}>
                  Notified
                </button>
              </div>

              { this.state.countries && this.state.countries.data && <div className="form-group-sm pull-right" style={{marginRight: '1rem', marginTop: '-2px'}}>
                <select
                  className="form-control"
                  defaultValue={this.state.filters.countryId}
                  onChange={e => this.handleFilter.call(null, 'countryId', e.target.value)}>
                  <option value="">Select...</option>
                  { this.state.countries.data.map((c,i) => <option key={i} value={c.id}>{c.name}</option>) }
                </select>
              </div> }

              <div className="col-sm-2 pull-right input-group input-group-sm" style={{marginRight: '1rem', marginTop: '-2px'}}>
                <style jsx>{`
                  .cancel-search {
                    position: absolute; z-index: 5;
                    top: -6px; left: -7px;
                    color: #be0000;
                    cursor: pointer;
                  }
                `}</style>
                <input 
                  type="text" 
                  className="form-control" 
                  placeholder="Enter zip..."
                  value={this.state.filters.zipCode}
                  onChange={e => this.setState({filters: ObjectAssign({}, this.state.filters, {zipCode: e.target.value})})} />
                { this.state.filters.zipCode && <i className="fa fa-times-circle cancel-search" data-name="zipFilter" onClick={this.handleFilter.bind(this, 'zipCode', '')}></i> }
                <span className="input-group-btn">
                  <button 
                    className="btn btn-default" 
                    onClick={e => this.handleFilter.call(null, 'zipCode', this.state.filters.zipCode)}
                    type="button">
                    <i className="fa fa-search"></i>
                  </button>
                </span>
              </div>

            </div>
            <div className="content table-responsive table-full-width">
              { items.data.length > 0 ? <GroupApplicantsWidget 
                posts={items.data}
                onStatusUpdate={this.handleStatusUpdate}
                onDelete={this.handleDelete}
                loadingId={loading} /> : <p>No results.</p> }
            </div>
          </div>
        }
        {
          items && items.paginator && <Pagination pagin={items.paginator} onChange={page => this.fetchItems({page})} />
        }

        {
          items.data && !items.data.length && filters.page != 1 && <p>
            <a 
              href="#" 
              onClick={e => {e.preventDefault(); this.fetchItems({page: 1})}}>
              Go to page 1
            </a>
          </p>
        }
      </Layout>
    );
  }
}
