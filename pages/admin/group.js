import Page from '../../components/admin-components/page';
import Layout from '../../components/admin-components/layout';
import DataService from '../../api/admin/groups';
import Zips from '../../api/admin/zips';

import ObjectAssign from 'object-assign';
import moment from 'moment';
import { MultiSelect } from 'react-selectize';
import { Util } from '../../api/util';

const zipMapper = (z) => ({label: `${z.code} (${z.stateAbbreviation}, ${z.countryIso})`, value: z.code});
const groupMapper = g => ({label: g.title, value: g.id});

export default class extends Page {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.groupId = ctx.query.id;

    let group = await DataService.getGroup(props.groupId);
    let related = await DataService.related(props.groupId);

    if (group.data) {
      props.group = group.data;
    }
    if (related && related.data && related.data.related) {
      props.related = related.data.related;
    } 

    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      group: props.group || null,
      formData: {
        title: this.state.group ? this.state.group.title : '',
        zipCodes: this.state.group ? this.state.group.zipCodes : ''
      },
      loading: false,
      allZips: null,
      allGroups: null,
      related: props.related || null,
      selectedGroups: [],
      searchZip: ''
    });
    this.onFormDataChange = this.onFormDataChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRelatedSubmit = this.handleRelatedSubmit.bind(this);
    this.removableValueRender = this.removableValueRender.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  async componentDidMount() {
    let group = await DataService.getGroup(this.props.groupId);
    let allZips = await Zips.list({pagin: 1});

    this.setState({
      group: group.data || null,
      formData: {
        title: group.data ? group.data.title : '',
        zipCodes: group.data ? group.data.zipCodes : ''
      },
      allZips: allZips.data ? allZips.data.map(zipMapper) : [],
      selectedZips: group.data && group.data.zips ? group.data.zips.map(zipMapper) : []
    });

    let res = await DataService.related(this.props.groupId);
    if (res && res.data && res.data.related) {
      let allGroups = await DataService.all({pagin: '0'});
      const related = res.data.related;
      this.setState({
        related,
        allGroups: allGroups.data ? allGroups.data.map(groupMapper) : [],
        selectedGroups: related.map(groupMapper)
      });
    }
  }

  onFormDataChange(e) {
    let target = e.target;
    this.setState({formData: ObjectAssign({}, this.state.formData, {
      [target.name] : target.value
    })});
  }

  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.group) return;

    this.clearAllNotifications();
    this.setState({loading: true});

    const formData = ObjectAssign({}, this.state.formData, {
      zipCodes: this.state.selectedZips ? this.state.selectedZips.map(z => z.value).join(',') : ''
    });

    DataService.update(this.state.group.id, formData)
      .then(response => {
        if (response.message) {
          this.addNotification({message: response.message});
          this.setState({group: ObjectAssign({}, this.state.group, this.state.formData)})
        } else if (response.error) {
          this.addNotification({
            title: 'Error',
            message: response.error.message,
            dismissAfter: 4500
          });
        } else {
          this.addNotification({
            title: 'Error',
            message: 'Something went wrong. Request failed. Please, try later.'
          });
        }
        this.setState({loading: false});
      });
  }

  async handleRelatedSubmit(e) {
    e.preventDefault();
    if (this.state.loadingRelated || !this.state.group) return;

    this.setState({loadingRelated: true});
    const relatedIds = this.state.selectedGroups.map(g => g.value).join(',');
    
    let response;
    if (relatedIds.trim() === '') {
      response = await DataService.deleteRelated(this.state.group.id);
    } else {
      response = await DataService.updateRelated(this.state.group.id, {relatedIds});
    }

    Util.handleResponse(this, response);

    this.setState({loadingRelated: false});
  }

  removableValueRender(item, prop) {
    return (<div className="removable simple-value">
      <button  onClick={() => this.setState({[prop]: this.state[prop].filter(z => z.value !== item.value)})} type="button" className="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <span>{item.label}</span>
    </div>);
  }

  async handleSearchChange(search) {
    this.setState({searchZip: search});
    const allZips = await Zips.list({filter: search});
    this.setState({allZips: allZips.data.map(zipMapper)})
  
  }

  render () {
    return (
      <Layout {...this.props} session={this.state.session}>
        { this.state.group && <div className="col-md-12">
          { this.notificationStack() }
          
          <h3>{ this.state.group.title } <p className="text-muted">Created: {moment(this.state.group.createdAt).format('LL')}</p></h3>
          
          <hr />

          <form className="form-horizontal" onSubmit={this.handleSubmit}>

            <div className="form-group">
              <label htmlFor="title" className="col-sm-2 control-label">Title</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="title" name="title" value={this.state.formData.title} onChange={this.onFormDataChange} />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="zipCodes" className="col-sm-2 control-label">Zip Codes</label>
              <div className="col-sm-10">

                { this.state.allZips === null && <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> }

                {this.state.allZips && <MultiSelect
                  placeholder="Select Zip Codes"
                  options={this.state.allZips}
                  values={this.state.selectedZips}
                  search={this.state.searchZip}
                  onSearchChange={this.handleSearchChange}
                  onValuesChange={(selected => this.setState({selectedZips: selected}))}
                  renderValue={item => this.removableValueRender(item, 'selectedZips')}
                 /> }

              </div>
            </div>

            <div className="form-group">
              <div className="col-sm-offset-2 col-sm-10">
                <button type="submit" className="btn btn-primary">{
                    this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : 'Save'
                  }</button>
              </div>
            </div>

          </form>


          { this.state.related && <div>
            <h3>Related Groups</h3>
            <hr />
            <form className="form-horizontal" onSubmit={this.handleRelatedSubmit}>
              <div className="form-group">
                <label htmlFor="relatedIds" className="col-sm-2 control-label">Edit related groups</label>
                <div className="col-sm-10">
                  { this.state.allGroups === null && <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> }

                  { this.state.allGroups && 
                  <MultiSelect
                    placeholder="Select Related Groups"
                    options={this.state.allGroups}
                    values={this.state.selectedGroups}
                    onValuesChange={selectedGroups => this.setState({selectedGroups})}
                    renderValue={item => this.removableValueRender(item, 'selectedGroups')} /> }
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                  <button type="submit" className="btn btn-primary">
                    { this.state.loadingRelated ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : 'Save' }
                  </button>
                </div>
              </div>
            </form>
          </div> }
        </div> }

        { !this.state.group && <p>No results</p> }
      </Layout>
    );
  }
}