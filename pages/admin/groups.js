import React, { Component } from 'react';
import Router from 'next/router';

import GroupsService from '../../api/admin/groups';
import Countries from '../../api/admin/countries';
import States from '../../api/admin/states';
import Zips from '../../api/admin/zips';

import Page from '../../components/admin-components/page-filters';
import Layout from '../../components/admin-components/layout';
import FAB, { FloatingMenu } from '../../components/admin-components/fab';

import Pagination from '../../components/bootstrap/pagination';
import { Modal, ModalBody, ModalFooter } from '../../components/modal';

import ObjectAssign from 'object-assign';
import classnames from 'classnames';
import moment from 'moment';
import { Util } from '../../api/util';

import { MultiSelect } from 'react-selectize';

const defaultParams = {
  completeZip: 0,
  filterZip: '',
  filterTitle: '',
  pagin: 1,
  perPage: 25,
  page: 1
};
const zipMapper = (z) => ({label: `${z.code} (${z.stateAbbreviation}, ${z.countryIso})`, value: z.code});

export default class extends Page {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultParams, ctx.query);
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      createModalActive: false,
      zipCodes: [],
      selectedGroup: null
    });
    this.handleFilterInputUpdate = this.handleFilterInputUpdate.bind(this);
    this.createUserGroup = this.createUserGroup.bind(this);

    this.onSwitchCountry = this.onSwitchCountry.bind(this);
    this.onSwitchState = this.onSwitchState.bind(this);

    this.removableItemRender = this.removableItemRender.bind(this);

    this.deleteSelectedGroup = this.deleteSelectedGroup.bind(this);
  }

  async componentDidMount() {
    super.componentDidMount();

    this.setState({
      countries: await Countries.list({pagin: '0'}),
      states: null,
      zips: null
    });
  }

  async fetchFn(filters) {
    return await GroupsService.all(filters);
  }

  async handleFilterInputUpdate(e) {
    this.setState({filters: ObjectAssign({}, this.state.filters, {
      [e.target.name]: e.target.value
    })})
  }

  async createUserGroup() {
    if (this.state.loading) return;
    this.setState({loading: true});

    let response = await GroupsService.create({
      title: this.refs.newUserGroupTitle.value,
      zipCodes: this.state.zipCodes.map(c => c.value).join(',')
    });

    Util.handleResponse(this, response,
      () => {
        this.setState({
          createModalActive: false,
          zipCodes: []
        }, this.fetchItems.bind(this));
      }
    );
  }

  async onSwitchCountry(e) {
    if (e.target.value !== '') {
      this.setState({states: await States.list({pagin: '0', countryId: e.target.value})});
    } else {
      this.setState({states: null});
    }
  }
  async onSwitchState(e) {
    if (e.target.value !== '') {
      this.setState({zips: await Zips.list({pagin: '0', stateId: e.target.value})});
    } else {
      this.setState({zips: null});
    }
  }

  removableItemRender(item) {
    return (<div className="removable simple-value">
      <button
        onClick={() => this.setState({zipCodes: this.state.zipCodes.filter(z => z.value !== item.value)})}
        type="button"
        className="close">
        <span>&times;</span>
      </button>
      <span>{item.label}</span>
    </div>);
  }

  async deleteSelectedGroup() {
    if (!this.state.selectedGroup || this.state.loading) return;
    this.setState({loading: true});

    let res = await GroupsService.delete(this.state.selectedGroup.id);

    Util.handleResponse(this, res,
      () => {
        this.setState({
          deleteGroupModal: false,
          selectedGroup: null
        }, this.fetchItems.bind(this));
      }
    );
  }

  render() {
    return (
      <Layout {...this.props} session={this.state.session}>
        <div className="col-md-12">
          <div className="card">
            <div className="header">
              <h4 className="title clearfix">
                <span className="pull-left">
                  User Groups&nbsp;&nbsp;
                  <label className="custom-control custom-checkbox mb-4">
                    <input type="checkbox" name="completeZip" className="custom-control-input" checked={this.state.filters.completeZip} onChange={() => this.fetchItems({completeZip: this.state.filters.completeZip ? null : 1})} />
                    <span className="custom-control-indicator"></span>
                    <span className="custom-control-description">&nbsp;Include Zips</span>
                  </label>
                </span>

                <div className="pull-right row groups-filters" style={{width: '50%'}}>
                  <style jsx>{`
                    .cancel-search {
                      position: absolute; z-index: 5;
                      top: -9px; left: -8px;
                      color: #be0000;
                      cursor: pointer;
                    }
                  `}</style>
                  {/* SEARCH BY ZIP */}
                  <form className="col-md-6" name="filter" onSubmit={e => {e.preventDefault(); this.fetchItems({filterZip: this.state.filters.filterZip})}}>
                    <div className="input-group input-group-sm">
                      <input type="text" name="filterZip" value={this.state.filters.filterZip} onChange={this.handleFilterInputUpdate} className="form-control" placeholder="Search By Zip" />
                      { this.state.filters.filterZip && <i className="fa fa-times-circle cancel-search" data-name="filterZip" onClick={this.fetchItems.bind(this, {filterZip: ''})}></i> }
                      <span className="input-group-btn">
                        <button className="btn btn-secondary" type="submit"><i className="fa fa-search" aria-hidden="true"></i></button>
                      </span>
                    </div>
                  </form>


                  {/* SEARCH BY TITLE */}
                  <form className="col-md-6 mb-4" name="filter" onSubmit={e => {e.preventDefault(); this.fetchItems({filterTitle: this.state.filterTitle})}}>
                    <div className="input-group input-group-sm">
                      <input autoComplete={false} type="text" name="filterTitle" value={this.state.filters.filterTitle} onChange={this.handleFilterInputUpdate} className="form-control" placeholder="Search By Title" />
                      { this.state.filters.filterTitle && <i className="fa fa-times-circle cancel-search" data-name="filterTitle" onClick={e => {e.preventDefault(); this.fetchItems({filterTitle: null})}}></i> }
                      <span className="input-group-btn">
                        <button className="btn btn-secondary" type="submit"><i className="fa fa-search" aria-hidden="true"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
              </h4>
            </div>
            <div className="content table-responsive table-full-width">
              <table className="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Created</th>
                    <th>Latest update</th>
                    <th className={classnames
                    ({'hidden': !this.state.filters.completeZip})}>Zip Codes</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  { this.state.items.data && this.state.items.data.map((group, i) => <tr key={i}>
                      <td>{ group.id }</td>
                      <td>
                        <a href="#"onClick={(e) => {e.preventDefault(); Router.push(`/admin/group?id=${group.id}`, `/admin/group/${group.id}`)}}>{ group.title }</a>
                      </td>
                      <td>{ moment(group.createdAt).format('LL') }</td>
                      <td>{ moment.utc(group.updatedAt).fromNow() }</td>
                      <td className={classnames({'hidden': !this.state.filters.completeZip})}>{ group.zips && group.zips.map(z => z.code).join(', ') }</td>
                      <td className="text-right">
                        <button
                          className="btn btn-simple btn-danger"
                          type="button"
                          disabled={this.state.loading}
                          onClick={e => this.setState({selectedGroup: group, deleteGroupModal: true})}><i className="fa fa-trash"></i></button>
                      </td>
                    </tr>) }

                    { this.state.items && this.state.items.data && !this.state.items.data.length && <tr>
                      <td colSpan="6">No results. { this.state.filters.page > 1 && <a href="#" onClick={e => {e.preventDefault(); this.fetchItems({page: 1})}}>Go to page 1</a> }</td>
                    </tr> }
                </tbody>
              </table>
            </div>

          </div>

          <nav className="col">
            <Pagination 
              pagin={this.state.items.paginator}
              onChange={(page) => this.fetchItems({page})}
            />
          </nav>

        </div>

        <FloatingMenu main={{action: () => this.setState({createModalActive: true})}} />


        {/* DELETE SELECTED USER GROUP MODAL */}
        <Modal
          isOpen={this.state.deleteGroupModal}
          onClose={() => this.setState({deleteGroupModal: false, selectedGroup: null})}
          modalTitle="Are you sure?">
          <ModalBody>
            { this.state.selectedGroup && <p>{ this.state.selectedGroup.title } group will be permanently deleted.</p> }
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              disabled={this.state.loading}
              onClick={() => this.setState({deleteGroupModal: false, selectedGroup: null})}
              className="btn btn-default">
              Cancel
            </button>
            <button
              type="button"
              disabled={this.state.loading}
              onClick={this.deleteSelectedGroup}
              className="btn btn-danger">
              { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Confirm Deletion' }
            </button>
          </ModalFooter>
        </Modal>

        {/* CREATE NEW USER GROUP MODAL */}
        <Modal
          isOpen={this.state.createModalActive}
          onClose={() => this.setState({createModalActive: false})}
          modalTitle="Create new user group"
          sizeClass="modal-lg modal-new-user-group">
          <ModalBody>
            <form onSubmit={e => e.preventDefault()}>
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                  type="text"
                  ref="newUserGroupTitle"
                  className="form-control" />
              </div>
              <div className="form-group">
                <label htmlFor="country">Select Country</label>
                { this.state.countries && this.state.countries.data && <select
                  className="form-control custom-select"
                  name="country"
                  onChange={this.onSwitchCountry}>
                  <option value=''>Select...</option>
                  { this.state.countries.data.map(c => <option key={c.id} value={c.id}>{c.name}</option>) }
                </select> }
              </div>

              <div className="form-group">
                <label htmlFor="state">Select State</label>
                { !this.state.states && <select className="form-control" disabled={true}>
                  <option disabled={true}>Country must be selected</option>
                </select> }
                { this.state.states && this.state.states.data && <select
                  className="form-control"
                  name="state"
                  onChange={this.onSwitchState}>
                  <option value=''>{this.state.states.data.length ? 'Select...' : 'No states available for this country.'}</option>
                  { this.state.states.data.map(s => <option key={s.id} value={s.id}>{s.name}</option>) }
                </select> }
              </div>

              <div className="form-group">
                <label htmlFor="zipCodes">Select Zip Codes (country and state have to be selected so available zip codes could be listed)</label>
                { !this.state.zips && <select className="form-control" disabled={true}><option disabled>Country and State must be selected</option></select> }

                { this.state.zips && this.state.zips.data && 
                  <MultiSelect
                    placeholder="Select Zip codes"
                    options={this.state.zips.data.map(zipMapper)}
                    values={this.state.zipCodes}
                    onValuesChange={zipCodes => this.setState({zipCodes})}
                    renderValue={this.removableItemRender}
                    hideResetButton={true} /> }
              </div>

            </form>
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              type="button"
              className="btn btn-default"
              onClick={() => this.setState({createModalActive: false})}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              type="button"
              className="btn btn-primary"
              onClick={this.createUserGroup}>
              { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Submit' }
            </button>
          </ModalFooter>
        </Modal>

        
        { this.notificationStack() }
      </Layout>
    );
  }
}