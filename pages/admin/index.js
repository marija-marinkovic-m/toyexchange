import React, { Component } from 'react';
import Link from 'next/link';
import ObjectAssign from 'object-assign';
import moment from 'moment';

import Page from '../../components/admin-components/page';
import Layout from '../../components/admin-components/layout';

import UsersWidget from '../../components/admin-components/widget-users';
import { GroupApplicantsWidgetReduced } from '../../components/admin-components/widget-group-applicants';
import { ReportedPostsWidgetReduced } from '../../components/admin-components/reported-posts-widget';
import CountriesWidget from '../../components/admin-components/widget-countries';
import ToysWidget from '../../components/admin-components/widget-toys';

import UsersData, { GroupApplicants } from '../../api/admin/users';
import { ReportedPosts } from '../../api/admin/posts';
import CountriesData from '../../api/admin/countries';
import ToysData from '../../api/admin/posts';

import DownloadUsersBtn from '../../components/admin-components/button-users-csv';

const groupApplicantsQueryParams = {
  pagin: 1,
  perPage: 6,
  status: 'pending'
};
const usersQueryParams = {
  pagin: 1,
  perPage: 15,
  status: 'active'
};
const reportedPostsQueryParams = {
  pagin: 1,
  perPage: 6,
  status: 'pending'
};
const latestToysQueryParams = {
  allGroups: 1,
  pagin: 1,
  perPage: 5,
  page: 1
};


export default class extends Page {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    if (typeof window === 'undefined') {
      props.applicants = await GroupApplicants.list(groupApplicantsQueryParams);
      props.users = await UsersData.list(usersQueryParams);
      props.reportedPosts = await ReportedPosts.list(reportedPostsQueryParams);
      props.latestToys = await ToysData.list(latestToysQueryParams);
    }
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      applicants: props.applicants || {data: null, paginator: null},
      users: props.users || {data: null, paginator: null},
      reportedPosts: props.reportedPosts || {data: null, paginator: null},
      latestToys: props.latestToys || {data: null, paginator: null}
    })
  }

  async componentDidMount() {
    this.setState({
      applicants: await GroupApplicants.list(groupApplicantsQueryParams),
      users: await UsersData.list(usersQueryParams),
      reportedPosts: await ReportedPosts.list(reportedPostsQueryParams),
      latestToys: await ToysData.list(latestToysQueryParams)
    })
  }

  render() {
    return (
      <Layout {...this.props} session={this.state.session}>
        <div className="col-md-7">

          <div className="card">
            <div className="header">
              { this.state.users.data && !this.state.users.data.length ? 
                <h4 className="text-center">
                  <i className="pe-7s-rocket"></i>&nbsp;&nbsp;There is no active users on toy-cycle.org, yet...
                </h4> : 
                <div>
                  <Link href="/admin/users">
                    <a className="pull-left"><h4 className="title"><i className="pe-7s-users"></i>&nbsp;&nbsp;Latest Users</h4></a>
                  </Link>
                </div> }
            </div>
            <div className="content">
              <UsersWidget users={this.state.users.data} />

              
              <DownloadUsersBtn style={{marginTop: '30px'}} />
            </div>
          </div>
        </div>
        <div className="col-md-5">
          <div className="card">
            <div className="header">
              { this.state.applicants.data && !this.state.applicants.data.length ?
                <h4 className="text-center">
                  <i className="pe-7s-coffee"></i>&nbsp;&nbsp;You have no pending group applicants
                </h4> : 
                <Link href="/admin/group-applicants">
                  <a><h4 className="title"><i className="pe-7s-mail-open-file"></i>&nbsp;&nbsp;Latest user group petitioners (pending)</h4></a>
                </Link> }
            </div>
            <div className="content">
              <GroupApplicantsWidgetReduced posts={this.state.applicants.data} />
            </div>
          </div>


          <div className="card">
            <div className="header">
              { this.state.reportedPosts.data && !this.state.reportedPosts.data.length ? 
                <h4 className="text-center">
                  <i className="pe-7s-like2"></i>&nbsp;&nbsp;You have no new post reports
                </h4> :
                <Link href="/admin/reported-posts">
                  <a><h4 className="title"><i className="fa fa-exclamation"></i>&nbsp;&nbsp;Pending reported posts</h4></a>
                </Link>
              }
            </div>
            <div className="content">
              <ReportedPostsWidgetReduced
                posts={this.state.reportedPosts.data} />
            </div>
          </div>

          <div className="card">
            <div className="header">
              <Link href="/admin/toys">
                <a><h4 className="title"><i className="pe-7s-plugin"></i>&nbsp;&nbsp;Latest toy ads</h4></a>
              </Link>
            </div>
            <div className="content">
              { this.state.latestToys && this.state.latestToys.data && <ToysWidget items={this.state.latestToys.data} /> }
            </div>
          </div>

        </div>
      </Layout>
    );
  }
}