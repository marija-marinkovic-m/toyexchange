import Link from 'next/link';
import PageFilters from '../../components/admin-components/page-filters';
import Layout from '../../components/admin-components/layout';
import Pagination from '../../components/bootstrap/pagination';
import Fab from '../../components/admin-components/fab';
import { Modal, ModalBody, ModalFooter } from '../../components/modal';
import { Util } from '../../api/util';
import ObjectAssign from 'object-assign';
import ReactTooltip from 'react-tooltip';

import PartnersAPI from '../../api/admin/partners';
import PartnersWidget from '../../components/admin-components/widget-partners';

import AdminFileUpload from '../../components/admin-components/file-upload';

const defaultParams = {
  pagin: 1,
  perPage: 15,
  page: 1
}

export default class extends PageFilters {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultParams, ctx.query);
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      isOpenNewModal: false,
      loading: false,
      newPartner: {
        title: '',
        url: '',
        imageFile: null
      },
      partnerPUT: null,
      partnerDELETE: null
    });

    this.createPartner = this.createPartner.bind(this);
    this.deletePartner = this.deletePartner.bind(this);
    this.updatePartner = this.updatePartner.bind(this);

    this.handleNewPartnerFile = this.handleNewPartnerFile.bind(this);
    this.handleEditPartnerFile = this.handleEditPartnerFile.bind(this);
    this.closeNewModal = this.closeNewModal.bind(this);
  }

  fetchFn(filters) {
    let dataFilters = ObjectAssign({}, filters);
    for (let prop in dataFilters) {
      if (dataFilters[prop] === null) {
        delete dataFilters[prop];
      }
    }
    return PartnersAPI.list(dataFilters);
  }

  async createPartner() {
    if (this.state.loading) return;
    const newPartner = ObjectAssign(
      {},
      this.state.newPartner,
      Util.formToPlainObject(this.refs.newPartnerForm)
    );
    this.clearAllNotifications();
    this.setState({loading: false});
    let res = await PartnersAPI.create(newPartner);

    await this.handleResponse(res, () => {
      this.setState({isOpenNewModal: false, newPartner: {title: '', url: '', imageFile: null}});
    }, 'Partner entry successfully created.');
  }

  async deletePartner() {
    if (this.state.partnerDELETE === null || this.state.loading) return;
    this.clearAllNotifications();
    this.setState({loading: true});

    const partnerId = this.state.partnerDELETE.id;

    let res = await PartnersAPI.delete(partnerId);

    await this.handleResponse(res, () => {
      this.setState({partnerDELETE: null})
    }, 'Partner entry successfully deleted.');
  }

  async updatePartner() {
    if (this.state.loading) return;
    const updatedPartner = ObjectAssign({}, this.state.partnerPUT, Util.formToPlainObject(this.refs.editPartnerForm));
    this.clearAllNotifications();
    this.setState({loading: true});

    let res;

    if (typeof updatedPartner.imageFile === 'string') {
      delete updatedPartner['imageFile'];
      res = await PartnersAPI.update(updatedPartner.id, updatedPartner);
    } else {
      res = await PartnersAPI.updateMultipartFormData(updatedPartner.id, updatedPartner);
    }

    this.handleResponse(res, () => {
      this.setState({partnerPUT: null});
    });
  }

  async handleResponse(res, cb = () => false, successMsg) {
    if (!res || res.error) {
      this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
    } else {
      this.addNotification({title: 'Success', message: res.message ? res.message : successMsg});
      cb();
    }
    await this.fetchItems();
    this.setState({loading: false});
  }

  handleNewPartnerFile(imageFile) {
    const newPartner = ObjectAssign({}, this.state.newPartner, {
      imageFile
    });
    this.setState({newPartner});
  }
  handleEditPartnerFile(imageFile) {
    this.setState({partnerPUT: ObjectAssign({}, this.state.partnerPUT, {imageFile})});
  }

  closeNewModal() {
    let newPartner = ObjectAssign(
      {},
      this.state.newPartner,
      Util.formToPlainObject(this.refs.newPartnerForm)
    );
    this.setState({newPartner, isOpenNewModal: false});
  }

  render() {
    return (
      <Layout {...this.props} session={this.state.session}>
        {this.notificationStack()}
        <div className="col-md-12">
          <div className="card">
            <div className="header">
              <h4 className="title">Partners</h4>
            </div>

            <div className="content">
              <PartnersWidget 
                partners={this.state.items.data}
                onDelete={partnerDELETE => this.setState({partnerDELETE})}
                onEdit={partnerPUT => this.setState({partnerPUT})} />

              {
                this.state.items.data && this.state.items.data.length === 0 && <div>
                  <p>No results. { this.state.filters.page != 1 && <Link href="/admin/partners?page=1"><a>Go to page 1</a></Link> }</p>
                </div>
              }

              <nav className="col">
                <Pagination 
                  pagin={this.state.items.paginator}
                  onChange={page => this.fetchItems({page})} />
              </nav>
            </div>
          </div>
        </div>

        <Fab onClick={() => this.setState({isOpenNewModal: true})} />

        {/* EDIT PARTNER MODAL */}
        <Modal
          isOpen={this.state.partnerPUT !== null}
          onClose={() => this.setState({partnerPUT: null})}
          modalTitle="Edit partner entry">
          <ModalBody>
            { this.state.partnerPUT && <form ref="editPartnerForm">
              <div className="form-group">
                <label htmlFor="e-title">Title</label>
                <input 
                  type="text"
                  id="e-title"
                  name="title"
                  className="form-control"
                  defaultValue={this.state.partnerPUT.title} />
              </div>
              <div className="form-group">
                <label htmlFor="e-url">Link</label>
                <input 
                  type="text"
                  id="e-url"
                  name="url"
                  className="form-control"
                  defaultValue={this.state.partnerPUT.url} />
              </div>
              <div className="form-group">
                <AdminFileUpload 
                  handleFile={this.handleEditPartnerFile} />
              </div>
            </form> }
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-default"
              onClick={() => this.setState({partnerPUT: null})}>
              Cancel  
            </button>
            <button
              type="button"
              className="btn btn-danger"
              disabled={this.state.loading}
              onClick={this.updatePartner}>
              { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Save Changes' }
            </button>
          </ModalFooter>
        </Modal>

        {/* DELETE PARTNER MODAL */}
        <Modal
          isOpen={this.state.partnerDELETE !== null}
          onClose={() => this.setState({partnerDELETE: null})}
          modalTitle="Are you sure?"
          sizeClass="modal-sm">
          <ModalBody>
            <p>Partner entry will be deleted permanently.</p>  
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-default"
              onClick={() => this.setState({partnerDELETE: null})}>
              Cancel
            </button>
            <button
              type="button"
              className="btn btn-danger"
              onClick={this.deletePartner}>
              { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Confirm Deletion' }
            </button>
          </ModalFooter>
        </Modal>
        
        {/* NEW PARTNER MODAL */}
        <Modal
          isOpen={this.state.isOpenNewModal}
          onClose={this.closeNewModal}
          sizeClass="modal-lg"
          modalTitle="Create new partner entry">
          <ModalBody>
            <form ref="newPartnerForm">
              <div className="form-group">
                <label htmlFor="n-title">Title</label>
                <input 
                  type="text"
                  id="n-title"
                  name="title"
                  className="form-control"
                  defaultValue={this.state.newPartner.title}
                  tabIndex="1" />
              </div>
              <div className="form-group">
                <label htmlFor="n-url">Link</label>
                <input
                  type="text"
                  id="n-url"
                  className="form-control"
                  name="url"
                  defaultValue={this.state.newPartner.url}
                  tabIndex="2" />
              </div>
              <div className="form-group">
                <AdminFileUpload 
                  handleFile={this.handleNewPartnerFile} />
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              type="button"
              className="btn btn-default"
              onClick={this.closeNewModal}>
              Cancel
            </button>
            <button
              disabled={this.state.loading}
              type="button"
              className="btn btn-primary"
              onClick={this.createPartner}>
              { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Create' }
            </button>
          </ModalFooter>
        </Modal>

        <ReactTooltip />

      </Layout>
    );
  }
}