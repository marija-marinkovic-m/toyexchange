import ObjectAssign from 'object-assign';
import PageFilters from '../../components/admin-components/page-filters';
import Layout from '../../components/admin-components/layout';
import Pagination from '../../components/bootstrap/pagination';
import Fab from '../../components/admin-components/fab';
import { Modal, ModalBody, ModalFooter } from '../../components/modal';
import { Util } from '../../api/util';
import cn from 'classnames';

import PlansAPI from '../../api/admin/plans';
import PlansWidget from '../../components/admin-components/widget-plans';

const defaultParams = {
  status: null, // active, inactive,
  isPublic: null,
  pagin: 1,
  perPage: 15,
  page: 1
};

export default class extends PageFilters {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultParams, ctx.query);
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      isOpenNewModal: false,
      loading: false,
      newPlan: {
        isPublic: '1',
        status: 'active'
      },
      planPUT: null,
      planDELETE: null
    })
    this.updateStatus = this.updateStatus.bind(this);
    this.updateVisibility = this.updateVisibility.bind(this);
    
    this.createPlan = this.createPlan.bind(this);
    this.closeNewModal = this.closeNewModal.bind(this);
    this.deletePlan = this.deletePlan.bind(this);
    this.updatePlan = this.updatePlan.bind(this);
  }

  fetchFn(filters) {
    let dataFilters = ObjectAssign({}, filters);
    for (let prop in dataFilters) {
      if (dataFilters[prop] === null) {
        delete dataFilters[prop];
      }
    }
    return PlansAPI.list(dataFilters);
  }

  updateStatus(status, e) {
    e.preventDefault();
    if (this.state.filters.status === status) {
      return this.fetchItems({status: null});
    };
    this.fetchItems({status});
  }
  updateVisibility(isPublic, e) {
    e.preventDefault();
    if (this.state.filters.isPublic == isPublic) {
      return this.fetchItems({isPublic: null});
    }
    this.fetchItems({isPublic})
  }

  updatePlan() {
    if (this.state.planPUT === null || this.state.loading || !this.refs.editPlanForm) return;
    this.clearAllNotifications();
    this.setState({loading: true});

    const updatedPlan = Util.formToPlainObject(this.refs.editPlanForm);

    PlansAPI.update(this.state.planPUT.id, updatedPlan)
      .then(async res => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
          this.setState({loading: false});
        } else {
          this.addNotification({title: 'Success', message: res.message ? res.message : 'Plan successfully updated'});
          await this.fetchItems();
          this.setState({
            loading: false,
            planPUT: null
          });
        }
      })
  }

  deletePlan() {
    if (this.state.planDELETE === null || this.state.loading) return false;
    this.clearAllNotifications();
    this.setState({loading: true});

    const planId = this.state.planDELETE;

    PlansAPI.delete(planId)
      .then(res => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request Failed'});
        } else {
          this.addNotification({title: 'Success', message: res.message ? res.message : 'Plan successfully deleted'});
        }
        this.setState({
          loading: false, 
          planDELETE: null
        });
        this.fetchItems();
      })
  }

  createPlan() {
    if (this.state.loading) return;

    const newPlan = Util.formToPlainObject(this.refs.newPlanForm);
    this.clearAllNotifications();
    this.setState({loading: true});

    PlansAPI.create(newPlan)
      .then(async (res) => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
        } else {
          this.addNotification({title: 'Success', message: res.message ? res.message : 'Plan successfully created.'});
          this.setState({isOpenNewModal: false, newPlan: {isPublic: '1', status: 'active'}});
        }
        await this.fetchItems();
        this.setState({loading: false});
      });
    
  }

  closeNewModal() {
    let newPlan = Util.formToPlainObject(this.refs.newPlanForm);
    this.setState({newPlan, isOpenNewModal: false});
  }

  render() {
    return(
      <Layout {...this.props} session={this.state.session}>
        {this.notificationStack()}
        <div className="col-md-12">

          <div className="card">
            <div className="header" style={{marginBottom: '2rem'}}>
              <h4 className="title pull-left">Subscription Plans</h4>

              <div className="pull-right btn-group btn-group-xs">
                <button
                  type="button"
                  onClick={this.updateVisibility.bind(null, '0')}
                  className={cn({btn: true, 'btn-primary': this.state.filters.isPublic == '0', 'btn-default': this.state.filters.status != '0'})}>Private</button>
                <button
                  type="button"
                  onClick={this.updateVisibility.bind(null, '1')}
                  className={cn({btn: true, 'btn-primary': this.state.filters.isPublic == '1', 'btn-default': this.state.filters.status != '1'})}>Public</button>
              </div>
              <div className="pull-right btn-group btn-group-xs" style={{marginRight: '1em'}}>
                <button 
                  type="button" 
                  onClick={this.updateStatus.bind(null, 'active')}
                  className={cn({btn: true, 'btn-primary': this.state.filters.status === 'active', 'btn-default': this.state.filters.status !== 'active'})}>
                  Active
                </button>
                <button 
                  type="button" 
                  onClick={this.updateStatus.bind(null, 'inactive')}
                  className={cn({btn: true, 'btn-primary': this.state.filters.status === 'inactive', 'btn-default': this.state.filters.status !== 'inactive'})}>
                  Inactive
                </button>
              </div>
            </div>
            <div className="content">

              <PlansWidget 
                plans={this.state.items.data}
                onEdit={planID => this.setState({planPUT: planID})}
                onDelete={planId => this.setState({planDELETE: planId})} />
              <nav className="col">
                <Pagination 
                  pagin={this.state.items.paginator}
                  onChange={page => this.fetchItems({page})} />
              </nav>

              {this.state.items.data && !this.state.items.data.length && this.state.filters.page != 1 && <p>No results. <a href="#" onClick={e => {e.preventDefault(); this.fetchItems({page: 1})}}>Go to page 1</a></p>}

            </div>
          </div>
        </div>
        <Fab onClick={() => this.setState({isOpenNewModal: true})} />

        {/* EDIT PLAN MODAL */}
        <Modal
          isOpen={this.state.planPUT !== null}
          onClose={() => this.setState({planPUT: null})}
          modalTitle="Edit subscription plan">
          <ModalBody>
            {this.state.planPUT && <form ref="editPlanForm">
                <div className="form-group">
                  <label htmlFor="e-title">Title</label>
                  <input type="text" id="e-title" name="title" className="form-control" defaultValue={this.state.planPUT.title} tabIndex="1" />
                </div>

                <div className="form-group">
                  <label htmlFor="e-subtitle">Subtitle</label>
                  <input type="text" id="e-subtitle" name="subtitle" className="form-control" defaultValue={this.state.planPUT.subtitle} tabIndex="2" />
                </div>

                <div className="form-group">
                  <label htmlFor="e-description">Description</label>
                  <textarea id="e-description" name="description" className="form-control" defaultValue={this.state.planPUT.description} tabIndex="3"></textarea>
                </div>

                <div className="form-group">
                  <label htmlFor="e-price">Price</label>
                  <input type="number" id="e-price" name="price" className="form-control" defaultValue={this.state.planPUT.price} tabIndex="4" />
                </div>

                <div className="form-group">
                  <label htmlFor="e-payedDays">Number of payed days in the plan</label>
                  <input type="number" id="e-payedDays" name="payedDays" className="form-control" defaultValue={this.state.planPUT.payedDays} tabIndex="5" />
                </div>

                <div className="form-group">
                  <label htmlFor="e-freeDays">Number of free days in the plan</label>
                  <input type="number" id="e-freeDays" name="freeDays" className="form-control" defaultValue={this.state.planPUT.freeDays} tabIndex="6" />
                </div>

                <div className="row">
                  <div className="col-md-6">
                    <label>Plan visibility</label>
                    <div className="radio">
                      <label>
                        <input type="radio" name="isPublic" value="1" defaultChecked={this.state.planPUT.isPublic == '1'} tabIndex="7" />
                        Public
                        <span className="icons">
                          <span className="first-icon fa fa-square-o"></span>
                          <span className="second-icon fa fa-check-square-o"></span>
                        </span>
                      </label>
                    </div>
                    <div className="radio">
                      <label>
                        <input type="radio" name="isPublic" value="0" defaultChecked={this.state.planPUT.isPublic == '0'} tabIndex="8" />
                        Private
                        <span className="icons">
                          <span className="first-icon fa fa-square-o"></span>
                          <span className="second-icon fa fa-check-square-o"></span>
                        </span>
                      </label>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <label>Status</label>
                    <div className="radio">
                      <label>
                        <input type="radio" name="status" value="active" defaultChecked={this.state.planPUT.status == 'active'} tabIndex="9" />
                        <span className="icons">
                          <span className="first-icon fa fa-square-o"></span>
                          <span className="second-icon fa fa-check-square-o"></span>
                        </span>
                        Active
                      </label>
                    </div>
                    <div className="radio">
                      <label>
                        <input ref="inactiveStatus" type="radio" name="status" value="inactive" defaultChecked={this.state.planPUT.status == 'inactive'} tabIndex="10" />
                        Inactive
                        <span className="icons">
                          <span className="first-icon fa fa-square-o"></span>
                          <span className="second-icon fa fa-check-square-o"></span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </form> }
          </ModalBody>
          <ModalFooter>
            <button type="button" className="btn btn-default" onClick={() => this.setState({planPUT: null})}>Cancel</button>
            <button disabled={this.state.loading} type="button" className="btn btn-primary" onClick={this.updatePlan}>{this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Save Changes' }</button>
          </ModalFooter>
        </Modal>

        {/* DELETE PLAN MODAL */}
        <Modal
          isOpen={this.state.planDELETE !== null}
          onClose={() => this.setState({planDELETE: null})}
          modalTitle="Are you sure?">
          <ModalBody>
            Subscription plan will be deleted permanently.
          </ModalBody>
          <ModalFooter>
            <button type="button" className="btn btn-default" onClick={() => this.setState({planDELETE: null})}>Cancel</button>
            <button type="button" className="btn btn-danger" onClick={this.deletePlan}>{ this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Confirm Deletion' }</button>
          </ModalFooter>
        </Modal>

        {/* NEW PLAN MODAL */}
        <Modal 
          isOpen={this.state.isOpenNewModal}
          onClose={this.closeNewModal}
          sizeClass="modal-lg"
          modalTitle="Add new subscription plan">
          <ModalBody>
            <form ref="newPlanForm">
              <div className="form-group">
                <label htmlFor="n-title">Title</label>
                <input type="text" id="n-title" name="title" className="form-control" defaultValue={this.state.newPlan.title} tabIndex="1" />
              </div>

              <div className="form-group">
                <label htmlFor="n-subtitle">Subtitle</label>
                <input type="text" id="n-subtitle" name="subtitle" className="form-control" defaultValue={this.state.newPlan.subtitle} tabIndex="2" />
              </div>

              <div className="form-group">
                <label htmlFor="n-description">Description</label>
                <textarea id="n-description" name="description" className="form-control" defaultValue={this.state.newPlan.description} tabIndex="3"></textarea>
              </div>

              <div className="form-group">
                <label htmlFor="n-price">Price</label>
                <input type="number" id="n-price" name="price" className="form-control" defaultValue={this.state.newPlan.price} tabIndex="4" />
              </div>

              <div className="form-group">
                <label htmlFor="n-payedDays">Number of payed days in the plan</label>
                <input type="number" id="n-payedDays" name="payedDays" className="form-control" defaultValue={this.state.newPlan.payedDays} tabIndex="5" />
              </div>

              <div className="form-group">
                <label htmlFor="n-freeDays">Number of free days in the plan</label>
                <input type="number" id="n-freeDays" name="freeDays" className="form-control" defaultValue={this.state.newPlan.freeDays} tabIndex="6" />
              </div>

              <div className="row">
                <div className="col-md-6">
                  <label>Plan visibility</label>
                  <div className="radio">
                    <label>
                      <input type="radio" name="isPublic" value="1" defaultChecked={this.state.newPlan.isPublic == '1'} tabIndex="7" />
                      Public
                      <span className="icons">
                        <span className="first-icon fa fa-square-o"></span>
                        <span className="second-icon fa fa-check-square-o"></span>
                      </span>
                    </label>
                  </div>
                  <div className="radio">
                    <label>
                      <input type="radio" name="isPublic" value="0" defaultChecked={this.state.newPlan.isPublic == '0'} tabIndex="8" />
                      Private
                      <span className="icons">
                        <span className="first-icon fa fa-square-o"></span>
                        <span className="second-icon fa fa-check-square-o"></span>
                      </span>
                    </label>
                  </div>
                </div>

                <div className="col-md-6">
                  <label>Status</label>
                  <div className="radio">
                    <label>
                      <input type="radio" name="status" value="active" defaultChecked={this.state.newPlan.status == 'active'} tabIndex="9" />
                      <span className="icons">
                        <span className="first-icon fa fa-square-o"></span>
                        <span className="second-icon fa fa-check-square-o"></span>
                      </span>
                      Active
                    </label>
                  </div>
                  <div className="radio">
                    <label>
                      <input ref="inactiveStatus" type="radio" name="status" value="inactive" defaultChecked={this.state.newPlan.status == 'inactive'} tabIndex="10" />
                      Inactive
                      <span className="icons">
                        <span className="first-icon fa fa-square-o"></span>
                        <span className="second-icon fa fa-check-square-o"></span>
                      </span>
                    </label>
                  </div>
                </div>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button disabled={this.state.loading} type="button" className="btn btn-default" onClick={this.closeNewModal}>Cancel</button>
            <button disabled={this.state.loading} type="button" className="btn btn-primary" onClick={this.createPlan}>{ this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Create' }</button>
          </ModalFooter>
        </Modal>
      </Layout>
    );
  }
}