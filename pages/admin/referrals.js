import React from 'react';
import ObjectAssign from 'object-assign';
import PageFilters from '../../components/admin-components/page-filters';
import Layout from '../../components/admin-components/layout';
import Pagination from '../../components/bootstrap/pagination';

import { Util } from '../../api/util';
import cn from 'classnames';

import UsersAPI from '../../api/admin/users';
import ReferralsWidget from '../../components/admin-components/widget-referrals';

const defaultParams = {
  status: null, // pending, accepted, subscribed
  pagin: 1,
  perPage: 10,
  page: 1
};

export default class extends PageFilters {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultParams, ctx.query);
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      loading: false
    });
    this.handlePagination = this.handlePagination.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
    this.deleteReferral = this.deleteReferral.bind(this);
  }

  fetchFn(filters) {
    let dataFilters = ObjectAssign({}, filters);
    for (let prop in dataFilters) {
      if (dataFilters[prop] === null) delete dataFilters[prop];
    }
    return UsersAPI.listReferrals(dataFilters);
  }

  updateStatus(status, e) {
    e.preventDefault();
    if (this.state.filters.status === status) {
      return this.fetchItems({status: null});
    }
    this.fetchItems({status});
  }

  handlePagination(page) {
    this.fetchItems({page})
  }

  deleteReferral(referralId, e) {
    if (e) e.preventDefault();

    UsersAPI.deleteReferral(referralId)
      .then(res => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed. Referral not deleted.'});
        } else {
          this.addNotification({title: 'Success', message: res.message ? res.message : 'Referral successfully deleted.'});
          this.fetchItems();
        }
      });
  }

  render() {
    return(
      <Layout {...this.props} session={this.state.session}>
        {this.notificationStack()}
        <div className="col-md-12">
          <div className="card">
            <div className="header" style={{marginBottom: '2rem'}}>
              <h4 className="title pull-left">Referrals</h4>

              <div className="pull-right btn-group btn-group-xs">
                <button
                  type="button"
                  onClick={this.updateStatus.bind(null, 'pending')}
                  className={cn({btn: true, 'btn-primary': this.state.filters.status === 'pending', 'btn-default': this.state.filters.status !== 'pending'})}>Pending</button>
                  <button
                    type="button"
                    onClick={this.updateStatus.bind(null, 'accepted')}
                    className={cn({btn: true, 'btn-primary': this.state.filters.status === 'accepted', 'btn-default': this.state.filters.status !== 'accepted'})}>Accepted</button>
                  <button
                    type="button"
                    onClick={this.updateStatus.bind(null, 'subscribed')}
                    className={cn({btn: true, 'btn-primary': this.state.filters.status === 'subscribed', 'btn-default': this.state.filters.status !== 'subscribed'})}>Subscribed</button>
              </div>
            </div>
            <div className="content">
              <ReferralsWidget referrals={this.state.items.data} onDelete={this.deleteReferral} />
              <nav className="col">
                <Pagination
                  pagin={this.state.items.paginator}
                  onChange={this.handlePagination} />
              </nav>

              {this.state.items.data && !this.state.items.data.length && this.state.filters.page != 1 && <p>No results. <a href="#" onClick={e => {e.preventDefault(); this.fetchItems({page: 1})}}>Go to page 1</a></p>}

            </div>
          </div>
        </div>
      </Layout>
    );
  }
}