import Page from '../../components/admin-components/page';
import Layout from '../../components/admin-components/layout';
import moment from 'moment';
import Link from 'next/link';
import Router from 'next/router';
import ObjectAssign from 'object-assign';
import { ReportedPosts } from '../../api/admin/posts';
import { FloatingMenu } from '../../components/admin-components/fab';

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      post: null,
      loading: null
    })
  }
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.reportId = ctx.query.id;
    return props;
  }

  componentDidMount() {
    this.fetchPost();
  }

  async fetchPost() {
    let postRes = await ReportedPosts.one(this.props.reportId);
    this.setState({
      post: postRes.data ? postRes.data : '404',
      loading: false
    });
  }

  async handleStatusUpdate(status) {
    if (!this.state.post) return;
    if (['accepted', 'pending', 'rejected'].indexOf(status) < 0) return;
    this.clearAllNotifications();
    this.setState({loading: true});

    let response = await ReportedPosts.update(this.state.post.id, {status});
    if (!response || response.error) {
      this.addNotification({title: 'Error', message: 'Error occured. Report is not updated'});
    } else {
      this.addNotification({title: 'Success', message: 'Report status updated'});
    }

    this.fetchPost();
  }

  async handleDelete() {
    if (!this.state.post) return;
    this.clearAllNotifications();
    this.setState({loading: true});

    let response = await ReportedPosts.delete(this.state.post.id);

    if (response.error || !response.message) {
      this.addNotification({title: 'Error', message: response.error, dismissAfter: 5000});
      this.setState({loading: false});
      return;
    }

    this.addNotification({message: `${response.message}. You will soon be redirected`, action: false});
    setTimeout(() => Router.push('/admin/reported-posts'), 3500);
  }

  render () {
    let { post } = this.state;
    const floatingMenuMain = {
      label: '', icon: 'fa fa-pencil', action: null
    };
    let floatingMenuItems = [
      {
        label: 'Delete report',
        icon: 'fa fa-trash',
        action: this.handleDelete.bind(this),
        className: 'bg-danger'
      }
    ];
    if (post) {
      if (post.status !== 'rejected') {
        floatingMenuItems.push({
          label: 'Mark as rejected',
          icon: 'fa fa-thumbs-o-down',
          action: this.handleStatusUpdate.bind(this, 'rejected'),
          className: 'bg-warning'
        });
      }
      if (post.status !== 'accepted') {
        floatingMenuItems.push({
          label: 'Mark as accepted',
          icon: 'fa fa-thumbs-o-up',
          action: this.handleStatusUpdate.bind(this, 'accepted'),
          className: 'bg-success'
        });
      }
      if (post.status !== 'pending') {
        floatingMenuItems.push({
          label: 'Mark as pending', 
          icon: 'pe-7s-hourglass', 
          action: this.handleStatusUpdate.bind(this, 'pending'),
          className: 'bg-info'
        });
      }
    }

    return(
      <Layout {...this.props} session={this.state.session}>
        { this.notificationStack() }
        { !post && <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> }
        { post && post === '404' && <p>Post not found</p> }

        { post && post !== '404' && <div className="col-md-12">
            <h1>{post.postData.title}</h1>
            <p><small>
              <strong>Ad status: </strong> {post.postData.status} (<a href={`/toy/${post.postId}`} target="_blank">view toy ad</a>)<br />
              <strong>Ad type: </strong> {post.postData.postType}<br /> 
              <strong>User: </strong> {post.userData.firstName + ' ' + post.userData.lastName + ', ' + post.userData.displayName} (<a href={`/admin/user?id=${post.userId}`} target="_blank">view user</a>)<br />
              <strong>Date of submition: </strong> {moment(post.createdAt).format('LL')}<br />
              <strong>Report status: </strong> {post.status}
            </small></p>

            <p>&nbsp;</p>
            
            <p style={{whiteSpace: 'pre-wrap'}}>{post.description}</p>

            <FloatingMenu 
              loading={this.state.loading}
              main={floatingMenuMain}
              items={floatingMenuItems} />
        </div> }
      </Layout>
    );
  }
}