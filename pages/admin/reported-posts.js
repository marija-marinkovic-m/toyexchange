import Page from '../../components/admin-components/page';
import Layout from '../../components/admin-components/layout';
import Pagination from '../../components/bootstrap/pagination';
import ReportedPostsWidget from '../../components/admin-components/reported-posts-widget';

import ObjectAssign from 'object-assign';

import { ReportedPosts } from '../../api/admin/posts';
const defaultParams = {
  status: null, // 'pending' || 'accepted' || 'rejected'
  pagin: 1,
  perPage: 25,
  page: 1
}

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      reqParams: ObjectAssign({}, defaultParams),
      reportedPosts: null,
      loading: false
    });

    this.handlePaginate = this.handlePaginate.bind(this);
    this.listPosts = this.listPosts.bind(this);
    this.handleStatusUpdate = this.handleStatusUpdate.bind(this);
  }

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.filters = {};
    if (ctx.query.toyId) {
      props.filters = {
        postId: ctx.query.toyId
      }
    }
    return props;
  }

  componentDidMount() {
    this.listPosts();
  }

  async listPosts() {
    this.setState({reportedPosts: await ReportedPosts.list(ObjectAssign({}, this.state.reqParams, this.props.filters))});
  }

  handlePaginate(page) {
    this.setState({
      reqParams: ObjectAssign({}, this.state.reqParams, {page})
    }, async() => await this.listPosts());
  }

  async handleStatusUpdate(postId, status, e) {
    if (e) e.preventDefault();

    if (isNaN(parseInt(postId, 10)) || ['accepted', 'pending', 'rejected'].indexOf(status) < 0) return; 

    this.clearAllNotifications();
    this.setState({loading: true});

    // make request
    let response = await ReportedPosts.update(postId, {status});

    if (!response || response.error) {
      this.addNotification({title: 'Error', message: 'Error occured. Report is not updated'});
      return this.setState({loading: false});
    } else {
      this.addNotification({title: 'Success', message: 'Report status updated'});
    }

    let updatedReportedPost = await ReportedPosts.one(postId);

    if (!updatedReportedPost.data) return this.setState({loading: false});

    this.setState({
      reportedPosts: ObjectAssign({}, this.reportedPosts, {
        data: this.state.reportedPosts.data
          .map(p => {
            if (p.id === postId) return updatedReportedPost.data;
            return p;
          })
      }),
      loading: false
    });
  }

  render() {
    const { reportedPosts } = this.state;
    return(
      <Layout {...this.props} session={this.state.session}>
        { this.notificationStack() }
        { !reportedPosts && <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</p> }
        {
          reportedPosts && reportedPosts.data &&
          <div className="col-md-12 card">
            <div className="header">
              <h4 className="title">Reported posts</h4>
              {/*<p className="category">{JSON.stringify(this.state.reqParams)}</p>*/}
            </div>
            <div className="content table-responsive table-full-width">
              { reportedPosts.data.length > 0 ? <ReportedPostsWidget 
                reportedPosts={reportedPosts.data}
                handleStatusUpdate={this.handleStatusUpdate}
                loading={this.state.loading} /> : <p className="col-md-12">{ this.props.filters.postId ? 'This post does not have any reports' : 'No results' }</p> }
            </div>
          </div>
        }
        { reportedPosts && reportedPosts.paginator && <Pagination 
          pagin={reportedPosts.paginator}
          onChange={this.handlePaginate} /> } 
      </Layout>
    );
  }
}