import PageFilters from '../../components/admin-components/page-filters';
import Layout from '../../components/admin-components/layout';
import Pagination from '../../components/bootstrap/pagination';

import ObjectAssign from 'object-assign';

import SettingsAPI from '../../api/admin/settings';
import SettingsWidget from '../../components/admin-components/widget-settings';

const defaultParams = {
  pagin: 1,
  perPage: 1,
  page: 1
}

export default class extends PageFilters {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultParams, ctx.query);
    return props;
  }

  constructor(props) {
    super(props);
    this.handleEditSetting = this.handleEditSetting.bind(this);
  }

  fetchFn(filters) {
    return SettingsAPI.list(filters);
  }

  handleEditSetting(settingId, body) {
    return new Promise(async (resolve, reject) => {
      let response = await SettingsAPI.update(settingId, body);

      if (!response || response.error) {
        this.addNotification({title: 'Error', message: response.error ? response.error.message : 'Request failed'});
        reject(response);
      } else {
        this.addNotification({title: 'Success', message: response.message ? response.message : 'Setting successfully updated.'});
        resolve(true);
      }
    });
  }

  render() {
    return (
      <Layout {...this.props} session={this.state.session}>
        { this.notificationStack() }
        <div className="col-md-12">
          <div className="card">
            <div className="header"><h4 className="title">General Application Settings</h4></div>
            <div className="content">
              <SettingsWidget
                settings={this.state.items.data}
                onEdit={this.handleEditSetting} />
            </div>

            <nav className="col">
              <Pagination 
                pagin={this.state.items.paginator}
                onChange={page => this.fetchItems({page})} />
            </nav>
          </div>
        </div>
      </Layout>
    );
  }
}