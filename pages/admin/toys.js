import ObjectAssign from 'object-assign';

import Page from '../../components/admin-components/page-filters';
import Layout from '../../components/admin-components/layout';
import Pagination from '../../components/bootstrap/pagination';
import ToysWidget from '../../components/admin-components/widget-toys';

import ToysData from '../../api/admin/posts';

const defaultFilters = {
  allGroups: 1,
  pagin: 1,
  perPage: 15,
  page: 1
}

export default class extends Page {
    static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultFilters);
    return props;
  }

  async fetchFn(filters) {
    return await ToysData.list(filters);
  }

  handleDelete(toyId) {
    if (!confirm('Are you Sure you want to delete this post?')) {
      return false;
    }
    this.clearAllNotifications();
    this.setState({deleting: true})
    ToysData.delete(toyId)
      .then(response => {
        if (response.message) {
          this.addNotification({message: response.message, action: false});
        } else if (response.error) {
          this.addNotification({
            title: 'Error',
            message: response.error.message,
            dismissAfter: 4500
          });
          this.setState({deleting: false});
        } else {
          this.addNotification({
            title: 'Error',
            message: 'Something went wrong. Request failed. Please, try later.'
          });
          this.setState({deleting: false})
        }
        this.fetchItems();
      });
  }

  render() {
    return(
      <Layout {...this.props} session={this.state.session}>
        <div className="col-md-12">
          <div className="card">
            <div className="header">
              <h4 className="title"><i className="pe-7s-plugin"></i>&nbsp;Latest on Toy-Exchange.org</h4>
              <div className="content table-responsive table-full-width">
                { (!this.state.items || !this.state.items.data) && <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> }

                { this.state.items && this.state.items.data && <ToysWidget items={this.state.items.data} onDelete={this.handleDelete.bind(this)} /> }

                <nav>
                  <Pagination 
                    pagin={this.state.items.paginator}
                    onChange={page => this.fetchItems({page})} />
                </nav>
              </div>
            </div>
          </div>
          {this.notificationStack()}
        </div>
      </Layout>
    );
  }
}