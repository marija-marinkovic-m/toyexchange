import Router from 'next/router';
import Page from '../../components/admin-components/page';
import Layout from '../../components/admin-components/layout';

import Users from '../../api/admin/users';
import Countries from '../../api/admin/countries';
import States from '../../api/admin/states';
import Zips from '../../api/admin/zips';

import { Modal, ModalBody, ModalFooter } from '../../components/modal';
import Pagination from '../../components/bootstrap/pagination';
import ReferralsWidget from '../../components/admin-components/widget-referrals';

import ObjectAssign from 'object-assign';
import cn from 'classnames';
import moment from 'moment';

const defaultAvatar = '/static/img/default-avatar.png';
const cardCoverImg = '/static/img/home-slideshow/lego.jpg';
const defaultReferralsParams = {
  pagin: 1,
  perPage: 5,
  page: 1,
  status: ''
};

const subscriptionStatuses = ['paid', 'not_paid', 'expired'];

export default class extends Page {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    if (ctx.query.id) {
      props.id = ctx.query.id;
    }
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      user: {data: null},
      countries: {data: null},
      states: {data: null},
      zips: {data:null},
      referrals: {data: null, paginator: null},
      form: {
        email: '',
        firstName: '',
        lastName: '',
        displayName: '',
        zipCode: '',
        stateId: '',
        city: '',
        pickupAddress: '',
        pickupWindow: '',
        pickupPlacement: '',
        nearbyNotifications: '',
        notificationType: '',
        status: '',
        countryId: ''
      },
      loading: false,
      loadingLocations: false,
      referralsParams: ObjectAssign({}, defaultReferralsParams),
      isOpenSubscriptionDatesModal: false,
      userSubscriptionDates: {
        subStartDate: '',
        subEndDate: '',
        subPayDate: '',
        totalEndDate: '',
        paymentStatus: 'not_paid'
      }
    });
    this.input = this.input.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.paginateReferrals = this.paginateReferrals.bind(this);
    this.toggleSubscriptionDatesModal = this.toggleSubscriptionDatesModal.bind(this);
    this.handleSubscriptionInputChange = this.handleSubscriptionInputChange.bind(this);
  }

  async handleInputChange(e) {
    let target = e.target;
    let updatedForm = ObjectAssign({}, this.state.form, {[target.name]: target.value});
    let updatedState = {};
    switch (target.name) {
      case 'countryId':
        this.setState({loadingLocations: true});
        updatedState = {
          states: await States.list({pagin: '0', countryId: target.value}),
          zips: {data: null},
          form: ObjectAssign({}, updatedForm, {
            stateId: '',
            zipCode: ''
          })
        };
        break;
      case 'stateId':
        this.setState({loadingLocations: true});      
        updatedState = {
          zips: await Zips.list({pagin: '0', stateId: target.value}),
          form: ObjectAssign({}, updatedForm, {
            zipCode: ''
          })
        };
        break;
      default: 
        updatedState = {form: updatedForm};
    }

    this.setState(updatedState, () => {
      if (target.name === 'countryId' || target.name === 'stateId') this.setState({loadingLocations: false});
    })
  }

  async handleSubmit(e) {
    e.preventDefault();
    console.log(this.state.form);

    if (!this.props.id) return;

    this.clearAllNotifications();
    this.setState({loading: true});

    const obsolete = ['countryData', 'groupData', 'stateData', 'avatarFile', 'id', 'roleId'];
    for (let i = 0; i < obsolete.length; i++) {
      if (this.state.form.hasOwnProperty([obsolete[i]])) delete this.state.form[obsolete[i]];
    }

    // make request
    let res = await Users.update(this.props.id, this.state.form);
    if (!res || res.error) {
      this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
    } else {
      this.addNotification({title: 'Success', message: res.message ? res.message : 'Profile Successfully updated.'});
    }
    this.setState({
      user: await Users.one(this.props.id),
      loading: false
    });
  }

  async handleSubscriptionDatesUpdate(e) {
    e.preventDefault();
    console.log(this.state.userSubscriptionDates);

    if (!this.props.id || this.state.loading) return;

    this.clearAllNotifications();
    this.setState({loading: true});

    // request
    let res = await Users.updateSubscriptionDates(this.props.id, this.state.userSubscriptionDates);
    if (!res || res.error) {
      this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request Failed'});
    } else {
      this.addNotification({title: 'Success', message: res.message ? res.message : 'Subscription dates successfuly updated'});
    }

    this.setState({
      user: await Users.one(this.props.id),
      loading: false,
      isOpenSubscriptionDatesModal: false
    });
  }
  handleSubscriptionInputChange(e) {
    this.setState({
      userSubscriptionDates: ObjectAssign({}, this.state.userSubscriptionDates, {[e.target.name]: e.target.value})
    });
  }

  toggleSubscriptionDatesModal() {
    this.setState({isOpenSubscriptionDatesModal: !this.state.isOpenSubscriptionDatesModal})
  }

  handleDelete() {
    this.clearAllNotifications();
    this.setState({loading: true});

    Users.delete(this.props.id)
      .then(res => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
        } else {
          this.addNotification({title: 'Success', message: res.message ? res.message : 'Profile Successfully deleted.'});
        }
        setTimeout(() => Router.push('/admin/users'), 3000);
      })
  }

  input(name, label, tabIndex = 1, type = 'text') {
    return (
      <div className="form-group">
        <label htmlFor={name}>{label}</label>
        <input type={type} name={name} id={name} value={this.state.form[name]} onChange={this.handleInputChange} className="form-control" tabIndex={tabIndex} />
      </div>
    );
  }

  paginateReferrals(page) {
    this.setState({
      referralsParams: ObjectAssign({}, this.state.referralsParams, {page})
    }, this.fetchReferrals.bind(this));
  }
  deleteReferral(referralId, e) {
    if (e) e.preventDefault();
    console.log(referralId);

    Users.deleteReferral(referralId)
      .then(res => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed. Referral not deleted.'});
        } else {
          this.addNotification({title: 'Success', message: res.message ? res.message : 'Referral successfully deleted.'});
          this.setState(
            {referralsParams: ObjectAssign({}, this.state.referralsParams, {page: 1})}, this.fetchReferrals
          );
        }
      });
  }

  async fetchReferrals() {
    let referrals = await Users.listReferrals(this.state.referralsParams);
    this.setState({referrals})
  }

  async componentDidMount() {
    let userRes = await Users.one(this.props.id);
    if (userRes && userRes.data) {
      this.setState({
        user: userRes,
        form: ObjectAssign({}, this.state.form, userRes.data),
        userSubscriptionDates: {
          subStartDate: userRes.data.subStartDate,
          subEndDate: userRes.data.subEndDate,
          subPayDate: userRes.data.subPayDate,
          totalEndDate: userRes.data.totalEndDate,
          paymentStatus: userRes.data.paymentStatus
        },
        countries: await Countries.list({pagin: '0'}),
        referralsParams: ObjectAssign({}, this.state.referralsParams, {userId: userRes.data.id})
      }, this.fetchReferrals.bind(this));
      // pick states / zips 
      if (userRes.data.countryId) {
        this.setState({
          states: await States.list({pagin: '0', countryId: userRes.data.countryId}),
          zips: userRes.data.stateId ? await Zips.list({pagin: '0', stateId: userRes.data.stateId}) : {data: null}
        });
      }
    }
  }

  render() {
    const { user, userSubscriptionDates } = this.state;
    return (
      <Layout {...this.props} session={this.state.session}>
        { 
          user.data === null ?
          <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 
          <div className="col-md-12">

            <div className="row">
              {/* THE FORM */}
              <div className="col-md-8">
                <div className="card">
                  <div className="header"><h4 className="title">Edit User Data</h4></div>
                  <div className="content">
                    <form onSubmit={this.handleSubmit}>

                      {/* email / displayName */}
                      <div className="row">
                        <div className="col-md-6">
                          {this.input('email', 'Email', 1, 'email')}
                        </div>
                        <div className="col-md-6">
                          {this.input('displayName', 'Display Name', 2)}
                        </div>
                      </div>

                      {/* firstName / lastName */}
                      <div className="row">
                        <div className="col-md-6">
                          {this.input('firstName', 'First name', 3)}
                        </div>
                        <div className="col-md-6">
                          {this.input('lastName', 'Last Name', 4)}
                        </div>
                      </div>

                      {/* countryId / stateId / zipCode */}
                      <div className="row">
                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="countryId">Country</label>
                            <select 
                              name="countryId" 
                              id="countryId" 
                              className="form-control" 
                              value={this.state.form.countryId}
                              onChange={this.handleInputChange}
                              tabIndex="5">
                              <option value="">Select</option>
                              { this.state.countries.data && this.state.countries.data.map((c,i) => <option key={i} value={c.id}>{c.name}</option>) }
                            </select>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="stateId">State</label>
                            <select 
                              name="stateId" 
                              id="stateId" 
                              className="form-control" 
                              value={this.state.form.stateId}
                              onChange={this.handleInputChange}
                              disabled={this.state.loadingLocations}
                              tabIndex="6">
                              <option value="">Select</option>
                              { this.state.states.data ? this.state.states.data.map((s,i) => <option key={i} value={s.id}>{s.name}</option>) : <option disabled>Country must be selected...</option> }
                            </select>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="zipCode">Zip Code</label>
                            <select 
                              name="zipCode" 
                              id="zipCode" 
                              className="form-control" 
                              value={this.state.form.zipCode}
                              onChange={this.handleInputChange}
                              disabled={this.state.loadingLocations}
                              tabIndex="7">
                              <option value="">Select</option>
                              { this.state.zips.data ? this.state.zips.data.map((z,i) => <option key={i} value={z.code}>{z.code} ({z.stateAbbreviation})</option>) : <option disabled>State must be selected...</option> }
                            </select>
                          </div>
                        </div>
                      </div>

                      {/* city / pickupAddress */}
                      <div className="row">
                        <div className="col-md-6">
                          {this.input('city', 'City name', 8)}
                        </div>
                        <div className="col-md-6">
                          {this.input('pickupAddress', 'Pickup address', 9)}
                        </div>
                      </div>

                      {/* pickupWindow / pickupPlacement */}
                      <div className="row">
                        <div className="col-md-6">
                          {this.input('pickupWindow', 'Pickup Window', 8)}
                        </div>
                        <div className="col-md-6">
                          {this.input('pickupPlacement', 'Pickup Placement', 9)}
                        </div>
                      </div>

                      <div className="row text-center">
                        <div className="col-md-4">
                          <div className="form-group">
                            <label>Nearby Notifications</label><br />
                            <div className="btn-group" data-toggle="buttons">
                              <label className={cn({"btn btn-default btn-on": true, "active": this.state.form.nearbyNotifications === '1'})}>
                                <input type="radio" value="1" name="nearbyNotifications" checked={this.state.form.nearbyNotifications === '1'} onChange={(e) => this.setState({form: ObjectAssign({}, this.state.form, {nearbyNotifications: '1'})})} />ON
                              </label>
                              <label className={cn({"btn btn-default btn-off": true, "active": this.state.form.nearbyNotifications !== '1'})}>
                                <input type="radio" value="0" name="nearbyNotifications" checked={this.state.form.nearbyNotifications !== '1'} onChange={(e) => this.setState({form: ObjectAssign({}, this.state.form, {nearbyNotifications: '0'})})} />OFF
                              </label>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-4">
                          <div className="form-group">
                            <label>Notification Type</label><br />
                            <div className="btn-group" data-toggle="buttons">
                              <label className={cn({"btn btn-default btn-on": true, "active": this.state.form.notificationType === 'individual'})}>
                                <input type="radio" value="individual" name="notificationType" checked={this.state.form.notificationType === 'individual'} onChange={(e) => this.setState({form: ObjectAssign({}, this.state.form, {notificationType: 'individual'})})} />Individual
                              </label>
                              <label className={cn({"btn btn-default btn-off": true, "active": this.state.form.notificationType !== 'individual'})}>
                                <input type="radio" value="daily" name="notificationType" checked={this.state.form.notificationType !== 'individual'} onChange={(e) => this.setState({form: ObjectAssign({}, this.state.form, {notificationType: 'daily'})})} />Daily
                              </label>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-4">
                          <div className="form-group">
                            <label>User Status</label><br />
                            <div className="btn-group" data-toggle="buttons">
                              <label className={cn({"btn btn-default btn-on": true, "active": this.state.form.status === 'active'})}>
                                <input type="radio" value="active" name="status" checked={this.state.form.status === 'active'} onChange={(e) => this.setState({form: ObjectAssign({}, this.state.form, {status: 'active'})})} />Active
                              </label>
                              <label className={cn({"btn btn-default btn-off": true, "active": this.state.form.status !== 'active'})}>
                                <input type="radio" value="inactive" name="status" checked={this.state.form.status !== 'active'} onChange={(e) => this.setState({form: ObjectAssign({}, this.state.form, {status: 'inactive'})})} />Inactive
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>

                      <hr />

                      <div className="text-center">
                        <button 
                          type="submit" 
                          className="btn btn-info btn-fill"
                          disabled={this.state.loading}>
                          { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Update user profile' }
                        </button>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="card">
                  <div className="header"><h5 className="title">User's Referrals</h5></div>
                  <div className="content">
                    <ReferralsWidget 
                      referrals={this.state.referrals.data}
                      onDelete={this.deleteReferral.bind(this)} />
                    <Pagination 
                      pagin={this.state.referrals.paginator}
                      onChange={this.paginateReferrals} />
                  </div>
                </div>
              </div>

              {/* THE USER CARD */}
              <div className="col-md-4">
                <div className="card card-user">
                  <div className="image">
                    <img src={cardCoverImg} alt="toy-cycle.org" />
                  </div>
                  <div className="content">
                    <div className="author">
                      <img src={user.data.avatarFile ? user.data.avatarFile : defaultAvatar} alt={user.data.displayName} className="avatar border-gray" />
                      <h4 className="title">
                        {user.data.firstName + ' ' + user.data.lastName}
                        <br />
                        <small>{user.data.displayName}</small>
                        <br />
                        <small><span className={cn({label: true, 'label-default': user.data.status !== 'active', 'label-success': user.data.status === 'active'})}>{user.data.status}</span></small>
                      </h4>
                    </div>

                    { user.data.groupData && user.data.stateData && user.data.countryData && <p className="description text-center" style={{marginTop: '10px'}}>
                      <strong>{user.data.groupData.title}</strong> user group<br />
                      { `${user.data.zipCode}, ${user.data.stateData.name} (${user.data.countryData.iso})` }
                    </p> }
                  </div>

                  <hr />

                  <div className="text-center">
                    <button type="button" 
                      className="btn btn-simple"
                      onClick={() => this.setState({isOpenConfirmDeleteModal: true})}>
                      <i className="fa fa-trash"></i>
                    </button>
                  </div>
                </div>

                <div className="panel panel-default">
                  <div className="panel-heading"><h3 className="panel-title">Subscription details</h3></div>
                  <ul className="list-group">
                    <li className="list-group-item">
                      <strong>Payment status: </strong>
                      <span className={cn({
                        label: true,
                        'label-danger': user.data.paymentStatus !== 'paid',
                        'label-success': user.data.paymentStatus === 'paid'
                      })}>{user.data.paymentStatus}</span>
                    </li>
                    <li className="list-group-item">
                      <strong>Subscription start date:</strong> {moment.utc(user.data.subStartDate).format('MMMM Do YYYY')}
                    </li>
                    <li className="list-group-item">
                      <strong>Subscription end date: </strong>{moment.utc(user.data.subEndDate).format('MMMM Do YYYY')}
                    </li>
                    <li className="list-group-item">
                      <strong>Subscription pay date: </strong>{moment.utc(user.data.subPayDate).format('MMMM Do YYYY')}
                    </li>
                    <li className="list-group-item">
                      <strong>Total end date: </strong>{moment.utc(user.data.totalEndDate).format('MMMM Do YYYY')}
                    </li>

                    <li className="list-group-item">
                      <button
                        type="button"
                        className="btn btn-warning btn-fill"
                        onClick={this.toggleSubscriptionDatesModal}>
                        <i className="pe-7s-attention"></i>&nbsp;Update subscription dates
                      </button>
                    </li>
                  </ul>
                </div>

              </div>
            </div>
            <Modal
              isOpen={this.state.isOpenConfirmDeleteModal}
              onClose={() => this.setState({isOpenConfirmDeleteModal: false})}
              modalTitle="Are You Sure?">
              <ModalBody>
                <p>User will be permanently deleted.</p>
              </ModalBody>
              <ModalFooter>
                <button 
                  type="button" 
                  className="btn bnt-default"
                  onClick={() => this.setState({isOpenConfirmDeleteModal: false})}>
                  Cancel
                </button>
                <button 
                  type="button" 
                  className="btn btn-danger"
                  onClick={this.handleDelete}
                  disabled={this.state.loading}>
                  { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Delete User' }
                </button>
              </ModalFooter>
            </Modal>

            <Modal
              isOpen={this.state.isOpenSubscriptionDatesModal}
              onClose={this.toggleSubscriptionDatesModal}
              modalTitle="Subscription Dates Update">
              <ModalBody>
                <p className="text-danger">After you submit subscription date and status changes, the member's entry might cause the system to misbehave both on functional and presentational levels. So please, be careful.</p>

                <div className="form-group">
                  <label htmlFor="sub-status">Subscription status</label>
                  <select className="form-control" name="paymentStatus" value={userSubscriptionDates.paymentStatus} onChange={this.handleSubscriptionInputChange}>
                    { subscriptionStatuses.map((s,i) => <option key={i}>{s}</option>) }
                  </select>
                </div>

                <div className="form-group">
                  <label htmlFor="sub-start">Subscription start date</label>
                  <input
                    type="date"
                    className="form-control"
                    id="sub-start"
                    name="subStartDate"
                    value={userSubscriptionDates.subStartDate}
                    onChange={this.handleSubscriptionInputChange} />
                </div>

                <div className="form-group">
                  <label htmlFor="sub-end">Subscription end date</label>
                  <input
                    type="date"
                    className="form-control"
                    id="sub-end"
                    name="subEndDate"
                    value={userSubscriptionDates.subEndDate}
                    onChange={this.handleSubscriptionInputChange} />
                </div>

                <div className="form-group">
                  <label htmlFor="sub-pay">Subscription pay date</label>
                  <input
                    type="date"
                    className="form-control"
                    id="sub-pay"
                    name="subPayDate"
                    value={userSubscriptionDates.subPayDate}
                    onChange={this.handleSubscriptionInputChange} />
                </div>

                <div className="form-group">
                  <label htmlFor="sub-total">Total end date</label>
                  <input
                    type="date"
                    className="form-control"
                    id="sub-total"
                    name="totalEndDate"
                    value={userSubscriptionDates.totalEndDate}
                    onChange={this.handleSubscriptionInputChange} />
                </div>
              </ModalBody>
              <ModalFooter>
                <button
                  type="button"
                  className="btn btn-default"
                  onClick={this.toggleSubscriptionDatesModal}>
                  Cancel
                </button>
                <button
                  type="button"
                  className="btn btn-danger"
                  onClick={this.handleSubscriptionDatesUpdate.bind(this)}
                  disabled={this.state.loading}>
                  { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Update Dates' }
                </button>
              </ModalFooter>
            </Modal>

            { this.notificationStack() }
          </div>
        }
      </Layout>
    );
  }
}