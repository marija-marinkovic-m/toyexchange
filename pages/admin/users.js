import ObjectAssign from 'object-assign';
import cn from 'classnames';

import Page from '../../components/admin-components/page-filters';
import Layout from '../../components/admin-components/layout';
import UsersWidget from '../../components/admin-components/widget-users';
import Fab from '../../components/admin-components/fab';
import Pagination from '../../components/bootstrap/pagination';

import { Modal, ModalBody, ModalFooter } from '../../components/modal';

import UsersData from '../../api/admin/users';
import Plans from '../../api/admin/plans';
import { Util } from '../../api/util';

const defaultProps = {
  filter: '',
  status: '', // active | inactive | ''
  paymentStatus: '', // not_paid | paid | expired
  roleId: 0, // 1 (admin) | 2 (standard)
  pagin: 1,
  perPage: 3,
  page: 1
};

export default class extends Page {
  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultProps, ctx.query);
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      plans: {data: null},
      loading: false,
      isOpenFormModal: false
    });
    this.handleCreateUser = this.handleCreateUser.bind(this);
    this.handleFiltersUpdate = this.handleFiltersUpdate.bind(this);
    this.deleteSelectedUser = this.deleteSelectedUser.bind(this);

    this.updateStatus = this.updateStatus.bind(this);
  }

  async fetchFn(filters) {
    return await UsersData.list(filters);
  }

  handleFiltersUpdate(e) {
    this.setState({filters: ObjectAssign({}, this.state.filters, {
      [e.target.name]: e.target.value
    })})
  }

  updateStatus(status, prop) {
    if (status === this.state.filters[prop]) {
      status = '';
    }
    this.fetchItems({[prop]: status});
  }

  async componentDidMount() {
    super.componentDidMount();
    this.setState({
      plans: await Plans.list({pagin: '0', isPublic: '0'})
    })
  }

  async handleCreateUser(e) {
    if (e) e.preventDefault();
    if (!this.refs.newUserForm) return;

    this.clearAllNotifications();
    this.setState({loading: true});

    const exit = () => {
      this.setState({isOpenFormModal: false, loading: false});
    }
    let formData = ObjectAssign({}, {
      email: '',
      password:'',
      regStep: 2,
      planId: ''
    }, Util.formToPlainObject(this.refs.newUserForm));

    if (formData.email === '' || formData.password === '') {
      this.addNotification({title: 'Email and Password are required fields', message: '', dismissAfter: 5000});
      this.setState({loading: false});
      return;
    }

    // register user
    let createdUserResponse = await UsersData.create({
      email: formData.email,
      password: formData.password,
      planId: formData.planId // ???
    });

    Util.handleResponse(this, createdUserResponse,
      () => {
        this.setState({isOpenFormModal: false}, this.fetchItems.bind(this));
      }
    );
  }

  async deleteSelectedUser() {
    if (!this.state.selectedUser || this.state.loading) return;
    this.setState({loading: true});
    let res = await UsersData.delete(this.state.selectedUser.id);
    Util.handleResponse(this, res,
      async () => {
        this.setState({
          isOpenDeleteModal: false,
          selectedUser: null
        }, this.fetchItems.bind(this));
      }
    );
  }

  render() {
    return(
      <Layout {...this.props} session={this.state.session}>
        <div className="col-md-12">
          <div className="card">
            <div className="header">
              <h4 className="title clearfix">
                <span className="pull-left">
                  Users
                  <div
                    className="pull-right btn-group btn-group-xs"
                    style={{paddingTop: '3px', marginLeft: '15px'}}>
                    <button
                      type="button"
                      onClick={this.updateStatus.bind(null, 'active', 'status')}
                      className={cn(['btn'], {'btn-primary': this.state.filters.status === 'active', 'btn-default': this.state.filters.status !== 'active'})}>
                      Active 
                    </button>
                    <button
                      type="button"
                      onClick={this.updateStatus.bind(null, 'inactive', 'status')}
                      className={cn(['btn'], {'btn-primary': this.state.filters.status === 'inactive', 'btn-default': this.state.filters.status !== 'inactive'})}>
                      Inactive 
                    </button>
                  </div>
                </span>

                <div className="pull-right row groups-filters" style={{width: '50%'}}>
                  <style jsx>{`
                    .cancel-search {
                      position: absolute; z-index: 5;
                      top: -9px; left: -8px;
                      color: #be0000;
                      cursor: pointer;
                    }
                  `}</style>
                  <div className="col-md-6" style={{paddingTop: '3px'}}>
                    <div className="pull-right btn-group btn-group-xs">
                      <button
                        type="button"
                        onClick={this.updateStatus.bind(null, 'paid', 'paymentStatus')}
                        className={cn(['btn'], {'btn-primary': this.state.filters.paymentStatus === 'paid', 'btn-default': this.state.filters.paymentStatus !== 'paid'})}>
                        Paid
                      </button>
                      <button
                        type="button"
                        onClick={this.updateStatus.bind(null, 'not_paid', 'paymentStatus')}
                        className={cn(['btn'], {'btn-primary': this.state.filters.paymentStatus === 'not_paid', 'btn-default': this.state.filters.paymentStatus !== 'not_paid'})}>
                        Not Paid
                      </button>
                      <button
                        type="button"
                        onClick={this.updateStatus.bind(null, 'expired', 'paymentStatus')}
                        className={cn(['btn'], {'btn-primary': this.state.filters.paymentStatus === 'expired', 'btn-default': this.state.filters.paymentStatus !== 'expired'})}>
                        Expired
                      </button>
                    </div>
                  </div>
                  <div className="col-md-6">
                    {/* FILTER (search) */}
                    <form
                      className="input-group input-group-sm"
                      onSubmit={e => {e.preventDefault(); this.fetchItems({filter: this.state.filters.filter})}}>
                      <input
                        type="text"
                        value={this.state.filters.filter}
                        name="filter"
                        className="form-control"
                        placeholder="Search..."
                        onChange={this.handleFiltersUpdate} />
                      { this.state.filters.filter && <i className="fa fa-times-circle cancel-search" data-name="filterZip" onClick={this.fetchItems.bind(this, {filter: ''})}></i> }
                      <span className="input-group-btn">
                        <button type="submit" className="btn btn-secondary">
                          <i className="fa fa-search"></i>
                        </button>
                      </span>
                    </form>
                  </div>
                </div>
              </h4>
            </div>
            
            { 
              this.state.items && this.state.items.data === null ? 
              <span style={{display: 'block', padding: '20px 15px'}}>
                <i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...
              </span> : 
              <UsersWidget
                users={this.state.items.data}
                onDelete={selectedUser => this.setState({selectedUser, isOpenDeleteModal: true})} />
            }
          </div>

          <Pagination
            pagin={this.state.items.paginator}
            onChange={page => this.fetchItems({page})} />
        </div>

        <Fab onClick={e => this.setState({isOpenFormModal: true})} loading={this.state.loading} />
        
        <Modal
          isOpen={this.state.isOpenFormModal}
          modalTitle="Create new user"
          onClose={() => this.setState({isOpenFormModal: false})}>
          <ModalBody>
            <form ref="newUserForm">
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input type="text" name="email" className="form-control" id="email" />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input type="text" name="password" id="password" className="form-control" />
              </div>

              <hr />

              <div className="form-group">
                <input type="hidden" name="regStep" value="2" readOnly />
                <label htmlFor="planId">Choose subscription plan</label>
                <select name="planId" id="planId" className="form-control">
                  <option value="">Select...</option>
                  { this.state.plans.data && this.state.plans.data.map((p,i) => <option value={p.id} key={i}>{p.title}</option>) }
                </select>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-default"
              disabled={this.state.loading}
              onClick={() => this.setState({isOpenFormModal: false})}>
              Cancel
            </button>
            <button className="btn btn-primary"
              disabled={this.state.loading}
              onClick={this.handleCreateUser}>
              { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Save' }
            </button>
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.isOpenDeleteModal}
          onClose={() => this.setState({selectedUser: null, isOpenDeleteModal: false})}
          modalTitle="Are you sure?">
          <ModalBody>
            <p>User {this.state.selectedUser && <strong>{this.state.selectedUser.displayName} </strong>}will be permanently deleted.</p>
          </ModalBody>
          <ModalFooter>
            <button
              disabled={this.state.loading}
              type="button"
              onClick={() => this.setState({selectedUser: null, isOpenDeleteModal: false})}
              className="btn btn-default">
              Cancel
            </button>
            <button
              type="button"
              disabled={this.state.loading}
              onClick={this.deleteSelectedUser}
              className="btn btn-danger">
              { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Confirm Deletion' }
            </button>
          </ModalFooter>
        </Modal>
        { this.notificationStack() }
      </Layout>
    );
  }
}