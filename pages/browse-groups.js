import React from 'react';
import Router from 'next/router';
import moment from 'moment';
import Page from '../components/page-filters';
import Layout from '../components/layout';
import Link from 'next/link';

import Groups from '../api/groups';

import TransHolder from '../components/component-transition-placeholder';
import Pagination from '../components/bootstrap/pagination';

import ObjectAssign from 'object-assign';
import cn from 'classnames';
import { logEvent } from '../lib/Analytics';

const defaultFilters = {
  completeZip: false,
  filterZip: '',
  filterTitle: '',
  pagin: 1,
  perPage: 18,
  page: 1,
  withTotals: 1
}

export default class extends Page {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);

    if (props.loggedIn && props.session.user.data.groupId && !props.isAdmin) {
      // redirect to /group/home-group
      if (ctx.res) {
        ctx.res.location('/group/home-group');
        ctx.res.status(302).end();
      } else {
        document.location.pathname = '/group/home-group';
      }
    }
    props.defaultParams = ObjectAssign({}, defaultFilters, ctx.query);
    // if (typeof window === 'undefined') {
      props.items = await Groups.getGroups(props.defaultParams);
    // }

    // redirect user to browse-groups if zipFilter and no results
    if ((!props.items || !props.items.data || !props.items.data.length) &&
        props.defaultParams.filterZip !== '') {
      console.log('redirect from getInitialProps')
      const redirectUrl = `/no-group?zip=${props.defaultParams.filterZip}`;
      if (typeof window === 'undefined') {
        ctx.res.location(redirectUrl);
        ctx.res.status(302).end();
      } else {
        window.location = redirectUrl;
      }
    }
    return props;
  }

  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.searchByZip = this.searchByZip.bind(this);
    this.searchByTitle = this.searchByTitle.bind(this);
  }

  handleInputChange(e) {
    this.setState({
      filters: ObjectAssign({}, this.state.filters, {
        [e.target.name]: e.target.value
      })
    });
  }

  async fetchFn(filters) {
    return await Groups.getGroups(filters);
  }

  async clearFilter(e) {
    const name = e.target.dataset.name;
    this.fetchItems({[name]: ''});
  }

  searchByZip(e) {
    e.preventDefault();
    // log event to GA
    if (window.GA_INITIALIZED) {
      logEvent('FindMyGroup', this.state.filters.filterZip, 'Search By Zip');
    }
    this.fetchItems({filterZip: this.state.filters.filterZip});
  }
  searchByTitle(e) {
    e.preventDefault();
    // log event to GA
    if (window.GA_INITIALIZED) {
      logEvent('FindMyGroup', this.state.filters.filterTitle, 'Search By Title');
    }
    this.fetchItems({filterTitle: this.state.filters.filterTitle});
  }

  render() {
    let loadingMsg;
    if (this.state.items && typeof this.state.items.data !== 'undefined') {
      if (this.state.items.data === null) {
        loadingMsg = <p className="px-3"><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</p>;
      } else if (this.state.items.data.length === 0) {
        loadingMsg = (<p className="text-center w-100">
          <span className="d-block mb-4 p-3 text-left">
            <i>Sorry, no results were found.</i>
            <br />
            {this.state.filters.filterZip !== '' && <i>If you are interested in seeing a group formed in this zip code, <Link href={`/no-group?zip=${this.state.filters.filterZip}`}><a>click here</a></Link>.</i> }
          </span>
          <img src="/static/img/no-results.png" />
        </p>);
      }
    }

    return (
      <Layout props={this.props} title="Browse Groups" totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <TransHolder ssr={this.props.ssr}>
          <div className="jumbotron">
            <h2 className="mt-0 mb-5 display-5">Browse Groups</h2>
            <div className="row">
              <style jsx>{`
                .groups-filters input.form-control {
                  border-top-left-radius: 3rem;
                  border-bottom-left-radius: 3rem;
                }
                .cancel-search {
                  position: absolute; z-index: 5;
                  top: -5px; left: 5px;
                  color: #be0000;
                  cursor: pointer;
                }
              `}</style>
              <aside className="groups-filters col-md-3 mb-3 text-center">
                {/* SEARCH BY ZIP */}
                <form className="mb-4" name="filter" onSubmit={this.searchByZip}>
                  <div className="input-group">
                    <input type="text" name="filterZip" value={this.state.filters.filterZip} onChange={this.handleInputChange} className="form-control" placeholder="Search By Zip" />
                    { this.state.filters.filterZip && <i className="fa fa-times-circle cancel-search" data-name="filterZip" onClick={this.clearFilter}></i> }
                    <span className="input-group-btn">
                      <button className="btn btn-secondary" type="submit"><i className="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                  </div>
                </form>

                {/* SEARCH BY TITLE */}
                <form className="mb-4" name="filter" onSubmit={this.searchByTitle}>
                  <div className="input-group">
                    <input autoComplete={false} type="text" name="filterTitle" value={this.state.filters.filterTitle} onChange={this.handleInputChange} className="form-control" placeholder="Search By Title" />
                    { this.state.filters.filterTitle && <i className="fa fa-times-circle cancel-search" data-name="filterTitle" onClick={this.clearFilter}></i> }
                    <span className="input-group-btn">
                      <button className="btn btn-secondary" type="submit"><i className="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                  </div>
                </form>

              </aside>

              <section className="col-md-9">
                <div className="row">
                  {loadingMsg ? loadingMsg : this.state.items.data.map((group, index) => <Group key={index} group={group} />)}
                </div>

                <div className="row"><div className="col mt-4">
                  <nav aria-label="Page navigation">
                    <Pagination
                      pagin={this.state.items.paginator}
                      onChange={(page) => this.fetchItems({page})}
                    />
                  </nav>
                </div></div>

              </section>
            </div>
          </div>
        </TransHolder>
      </Layout>
    );
  }
}


export function Group({group, classes = 'col-lg-4 col-md-6'}) {
  const updatedMoment = moment.utc(group.updatedAt);
  const diff = moment().diff(updatedMoment, 'days');

  let updatedAt = diff > 5 ? updatedMoment.format('ll') : updatedMoment.fromNow();
  
  return (
    <div className={cn([classes, 'toy-card'])}>
      <div className="card mb-4">
        <div className="card-block" onClick={(e) => Router.push(`/group?id=${group.slug}`, `/group/${group.slug}`)}>
          <h3>{group.title}</h3>
          <p className="text-muted m-0"><em>Latest activity {updatedAt}</em></p>
          <p className="m-0">{group.totalUsers} members | {group.totalPosts} results</p>
        </div>
      </div>
    </div>
  );
}