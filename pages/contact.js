import React from 'react';
import Page from '../components/page';
import Layout from '../components/layout';

import TransHolder from '../components/component-transition-placeholder';

import Data from '../static/content/page-contact';

export default class extends Page {
  render() {
    return (
      <Layout props={this.props} title={Data.title} totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <TransHolder ssr={this.props.ssr}>
          <div className="jumbotron clearfix">
            <h5 className="mt-0 mb-3 display-4">{ Data.title }</h5>
            <div className="mb-4" dangerouslySetInnerHTML={{ __html: Data.content }}></div>

            <img className="w-100" src="/static/img/cont-1.jpg" alt="" />
          </div>
        </TransHolder>
      </Layout>
    );
  }
}