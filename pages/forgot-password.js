import Page from '../components/page';
import Layout from '../components/layout';

import ObjectAssign from 'object-assign';
import TransHolder from '../components/component-transition-placeholder';
import Account from '../api/account';
import { Util } from '../api/util';

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      username: '',
      loading: false,
      mailSent: false
    })
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(e) {
    const target = e.target;
    this.setState({[target.name]: target.value});
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.loading) return;
    this.clearAllNotifications();
    this.setState({loading: true});

    Account.initResetPassword(this.state.username)
      .then(res => {
        Util.handleResponse(this, res, () => {
          this.setState({mailSent: true});
        });
      })
      .catch(err => console.log(err));
  }

  render() {
    return(
      <Layout props={this.props} title="Forgot password" totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <TransHolder ssr={this.props.ssr}>
          { this.props.loggedIn ? (
            <div className="text-center"><h2>You are already logged in</h2></div>
          ) : (
            <div className="jumbotron col-lg-5 col-md-8 mx-auto text-center">
              <h3 className="display-5">Forgot your password?</h3>

              { !this.state.mailSent ? <div>
                <p>Please enter the email address registered on your account.</p>

                <form onSubmit={this.handleSubmit}>
                  <style jsx>{`
                    .input-group-addon {background-color: rgba(255, 75, 100, 0.14)}
                  `}</style>
                  <div className="input-group mb-4">
                    <span className="input-group-addon" id="email-addon"><i className="fa fa-envelope-o"></i></span>
                    <input 
                      type="text" 
                      className="form-control" 
                      placeholder="Email"
                      aria-describedby="email-addon"
                      autoFocus
                      name="username"
                      value={this.state.username}
                      onChange={this.handleInputChange} />
                  </div>
                  <button
                    disabled={this.state.loading}
                    className="btn btn-primary w-100"
                    type="submit">
                    { this.state.loading ? <span><i className="fa fa-circle-o-notch"></i>&nbsp;&nbsp;Loading...</span> : 'Reset password' }
                  </button>
                </form>
              </div> : <div>
                <h4 className="display-6 font-weight-light mb-4 pb-4">
                  <i className="fa fa-check rounded-ico d-block mx-auto text-success md my-3"></i>
                  We sent you an email with your username and a link to reset your password.
                </h4>
              </div> }
            </div>
          ) }
          { this.notificationStack() }
        </TransHolder>
      </Layout>
    );
  }
}