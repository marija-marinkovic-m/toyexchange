import Link from 'next/link';
import React from 'react';

import FlipMove from 'react-flip-move';
import Pagination from '../components/bootstrap/pagination';

import moment from 'moment';
import ObjectAssign from 'object-assign';

import Page from '../components/page-filters';
import Layout from '../components/layout';
import LogInWidget from '../components/widget-login-or-signup';
import TransHolder from '../components/component-transition-placeholder';
import ToyCard from '../components/toy-card';
import { MapModal } from '../components/map-component';

import Posts from '../api/posts';
import Groups from '../api/groups';

import AgeRangeSidebar from '../components/age-range-filters-sidebar';
import PostTypeOrderFilters from '../components/post-type-order-filters-sidebar';

/**
 * @TODO: default filters should be object.assign of defaults and 
 * saved filters from local storage
 */
const defaultFilters = {
  ageRange: null,
  postType: null,
  order: 'desc',
  perPage: 9,
  pagin: 1,
  page: 1,
  group: '',
  groupId: 0
}

export default class extends Page {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    const isHomeGroup = 'home-group' === ctx.query.id;
    if ((!props.loggedIn && isHomeGroup) || (props.loggedIn && !props.isAdmin && !isHomeGroup && props.session.user.data.groupId) || (props.isAdmin && isHomeGroup)) {
      let pathToRedirect = props.isAdmin ? '/browse-groups' : props.loggedIn ? '/group/home-group' : '/browse-groups';
      // redirect to home if not logged in
      if (ctx.res) {
        ctx.res.location(pathToRedirect);
        ctx.res.status(302).end();
      } else {
        document.location.pathname = pathToRedirect;
      }
    }

    if (props.loggedIn && isHomeGroup && !props.session.user.data.groupId) {
      // redirect to browse-groups if user doesn't belong to any group yet
      if (ctx.res) {
        ctx.res.location('/browse-groups');
        ctx.res.status(302).end();
      } else {
        document.location.pathname = '/browse-groups';
      }
    }

    // special case for user who have not finished registration process 
    // or not paid etc
    // @TODO: props.session.user.data.paymentStatus !== 'paid'
    if ((props.loggedIn && !props.isAdmin && isHomeGroup && props.session.user && props.session.user.data) && (props.session.user.data.regStep < 3)) {
      if (ctx.res) {
        ctx.res.location('/signup');
        ctx.res.status(302).end();
      } else {
        document.location.pathname = '/signup';
      }
    }

    // 🤡
    // ctx.query.id can be string and can be int
    // if is a string can be any string and can be 'home-group'
    props.defaultParams = ObjectAssign({}, defaultFilters, ctx.query);
    let queryId = parseInt(ctx.query.id, 10);
    if (isNaN(queryId)) {
      if (isHomeGroup && props.loggedIn) {
        props.defaultParams.groupId = props.session.user && props.session.user.data.groupId;
      } else {
        props.defaultParams.group = ctx.query.id;
      }
    } else {
      props.defaultParams.groupId = queryId;
    }
    // if running on server, perform Async
    if (typeof window === 'undefined') {
      props.group = props.defaultParams && props.defaultParams.groupId ? await Groups.getGroup(props.defaultParams.groupId) : props.defaultParams && props.defaultParams.group ? await Groups.getGroup(props.defaultParams.group) : {data: null};
      props.items = await Posts.getPosts(props.defaultParams);
    }
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      group: props.group || {data: null},
      keywordInput: '',
      isMapOpen: false
    });
    this.filterToys = this.filterToys.bind(this);
    this.handleKeywordFilterFormSubmit = this.handleKeywordFilterFormSubmit.bind(this);
    this.handleKeywordInput = this.handleKeywordInput.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleShowMap = this.handleShowMap.bind(this);
  }

  async fetchFn(filters) {
    return await Posts.getPosts(filters);
  }

  // called after rendering, only on the client (not the server)
  async componentDidMount() {
    super.componentDidMount();
    this.setState({
      group: this.state.filters.groupId ? await Groups.getGroup(this.state.filters.groupId) : this.state.filters.group ? await Groups.getGroup(this.state.filters.group) : {data: null}
    });
  }

  async filterToys(key, value) {
    let newFilters = {};
    if (!value) {
      newFilters[key] = defaultFilters[key];
    } else {
      newFilters[key] = value;
    }
    if (key !== 'page' && key !== 'order') {
      newFilters.page = 1;
    }

    this.fetchItems(newFilters);
  }

  handleKeywordFilterFormSubmit(e) {
    e.preventDefault();
    let value = this.state.keywordInput;

    if (value === '') {
      value = false;
    }

    this.filterToys('filter', value);
  }

  handleKeywordInput(e) {
    const target = e.target;
    this.setState({keywordInput: target.value});
  }

  handlePageChange(page) {
    this.filterToys('page', page);
  }

  handleShowMap(e) {
    e.preventDefault();
    this.setState({isMapOpen: true});
  }

  render() {
    let loadingMsg;

    if (this.state.items && typeof this.state.items.data !== 'undefined') {
      if (this.state.items.data === null) {
        loadingMsg = <p className="px-3"><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</p>;
      } else if (this.state.items.data.length === 0) {
        loadingMsg = <p className="text-center w-100"><i className="d-block mb-4 p-3 text-left">Sorry, no results were found.</i><img src="/static/img/no-results.png" /></p>
      }
    }

    const items = this.state.items && this.state.items.data && this.state.items.data.map((item, index) => <div className="col-lg-4 col-md-6" key={index}><ToyCard toy={item} currUser={this.state.session.user} /></div>);
    // const items = this.state.items.data.map((t, i) => <div key={i}>{ t.title }</div>)
    return (
      <Layout props={this.props} title="Group" pathname={`/group/${this.props.group}`} totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
          <div className="container-fluid">
            {/* intro row */}
            {this.state.group.data && <div className="jumbotron jumbotron-fluid row">
              <div className="col-md-7">
                <h1 className="font-weight-bold">Group: { this.state.group.data.title }</h1>
                <p className="text-muted">
                  Zip codes: { this.state.group.data.zipCodes } {/*(<a href="#" onClick={this.handleShowMap}>Show Map</a>)*/} | 
                  Updated: { moment(this.state.group.data.updatedAt).format('LL') } |&nbsp;
                  { this.state.group.data.totalUsers } Members |&nbsp;
                  { this.state.group.data.totalPosts } Results
                </p>

                <MapModal 
                  isOpen={this.state.isMapOpen}
                  address={this.state.group.data.zipCodes}
                  onClose={() => this.setState({isMapOpen: false})} />
                  
              </div>
                { this.props.loggedIn && !this.props.isAdmin && <div className="col-md-5">
                  <Link href="/my-account/new-post">
                    <a className="btn btn-primary r-ico float-md-right text-uppercase font-weight-bold">Create new post&nbsp;&nbsp;<i className="fa fa-file-text"></i></a>
                  </Link>
                  <Link href="/posting-guidelines">
                    <a className="guide-link float-md-right px-3 py-2">
                      <i className="fa fa-lightbulb-o"></i>&nbsp;&nbsp;Posting Guidelines
                    </a>
                  </Link>
                </div> }
              </div>
            }

            { !this.props.loggedIn && <div className="my-5"><LogInWidget /></div> }

            {/* search and filter row */}
            <div className="row mt-5 mb-3">
              <div className="col-md-3 mb-3">
                <form onSubmit={this.handleKeywordFilterFormSubmit} className="input-group rounded-input-group">
                  <input type="text" className="form-control" placeholder="Search by keyword..." onChange={this.handleKeywordInput} />
                  <span className="input-group-btn">
                    <button className="btn btn-secondary" type="submit"><i className="fa fa-search"></i></button>
                  </span>
                </form>
              </div>
              <div className="nav-filters text-md-right text-sm-center col-md-9">
                <PostTypeOrderFilters 
                  posttype={this.state.filters.postType}
                  order={this.state.filters.order}
                  handle={this.filterToys}
                  />
              </div>
            </div>

            {/* content row */}
            <div className="row mb-5">
              {/* aside */}
              <aside className="col mb-5">
                <AgeRangeSidebar current={this.state.filters.ageRange} handle={this.filterToys} />
              </aside>

              {/* content wrap */}
              <section className="col-md-9">
                { loadingMsg ? <div className="row">{loadingMsg}</div> : <FlipMove className="row" duration={850} easing="ease-out" staggerDurationBy={150}>{ items }</FlipMove> }


                <div className="row"><div className="col mt-4">
                  <nav className="tc-navigation" aria-label="Page navigation">
                    <Pagination 
                      pagin={this.state.items.paginator}
                      onChange={this.handlePageChange}
                    />
                    </nav>
                </div></div>
              </section>
            </div>
          </div>
      </Layout>
    );
  }
}