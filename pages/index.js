import Router from 'next/router';
import Link from 'next/link';

import Session from '../auth/session';
import Page from '../components/page';
import Header from '../components/header';
import Footer from '../components/footer';
import ModalVideo, { Modal, ModalBody, ModalFooter } from '../components/modal';
import PartnersWidget from '../components/widget-partners';
import { SimpleSelect } from 'react-selectize';

import TransHolder from '../components/component-transition-placeholder';

import Intro, { VideoData, Features, CallToActionWidget, SlideshowImages } from '../static/content/page-home';

import ObjectAssign from 'object-assign';
import GroupsData from '../api/groups';
import LocationsData from '../api/locations';
import PartnersData from '../api/partners';

import { makeCancelable } from '../lib/cancelablePromise';
import VisibilityTracker from '../lib/TrackComponentVisibility.js';
import { logEvent } from '../lib/Analytics';

const zipMapper = (z) => ({label: `${z.code} - ${z.name} (${z.stateAbbreviation}, ${z.countryIso})`, value: z.code});

const defaultSwiperConfig = {
  direction: 'horizontal',
  autoplay: 3000,
  loop: true,
  effect: 'fade',
  slidesPerView: 1,
  spaceBetween: 0,
  grabCursor: true,
  preloadImages: false,
  lazyLoading: true,
  nextButton: '.swiper-button-next',
  prevButton: '.swiper-button-prev'
};

export default class extends Page {

  static async getInitialProps(ctx) {
    const pathToRedirect = '/group/home-group';
    let props = await super.getInitialProps(ctx);
    
    if (props.loggedIn && !props.isAdmin) {
      if (ctx.res) {
        ctx.res.location(pathToRedirect);
        ctx.res.status(302).end();
      } else {
        document.location.pathname = pathToRedirect;
      }
    }

    // load partners if on server 
    if (typeof window === 'undefined') {
      props.partners = await PartnersData.list({pagin: '0'});
    }

    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      searchGroupInput: '',
      isOpenVideoModal: false,
      isOpenTempModal: false,
      swiper: null,
      searchTerm: '',
      allZips: [],
      partners: props.partners || null
    });

    this.openVideoModal = this.openVideoModal.bind(this);
    this.closeVideoModal = this.closeVideoModal.bind(this);
    this.searchGroup = this.searchGroup.bind(this);
    this.handleFindMyGroup = this.handleFindMyGroup.bind(this);

    // zip search methods
    this.handleSearchChange = this.handleSearchChange.bind(this);
    // this.handleCreateFromSearch = this.handleCreateFromSearch.bind(this);
    // this.renderSearchOption = this.renderSearchOption.bind(this);
    this.renderNoResultsFound = this.renderNoResultsFound.bind(this);
  }

  openVideoModal() {
    // log event to GA
    if (window.GA_INITIALIZED) {
      logEvent('Video', 'play', 'How It Works - FrontPage Video');
    }
    this.setState({isOpenVideoModal: true});
  }
  closeVideoModal() {
    this.setState({isOpenVideoModal: false});
  }

  async componentDidMount() {
    super.componentDidMount();
    if (!this.cacheAPI) {
      this.cacheAPI = require('../lib/ScriptCache');
    }
    if (!this.scriptCache) {
      this.scriptCache = this.cacheAPI.ScriptCache(window)({
        swiper: '/static/libs/swiper.min.js'
      });
      this.scriptCache.swiper.onLoad(() => {
        new window.Swiper('.swiper-container', defaultSwiperConfig);
      });
    }
  }

  async searchGroup(item) {
    if (!item ||item.value === '') {
      return false;
    }

    // log event to GA
    if (window.GA_INITIALIZED) {
      logEvent('FindMyGroup', item.value, 'Search By Zip');
    }

    this.setState({
      loading: true
    }, async () => {
      // [GET] /groups?filterZip=this.state.searchGroupInput
      // if no results send user to 'no-group'
      if (isNaN(parseInt(item.value, 10))) {
        Router.push('/no-group?zip=' + item.value);
        return;
      }

      let groups = await GroupsData.getGroups({filterZip: item.value});

      if (!groups.data || !groups.data.length) {
        Router.push('/no-group?zip=' + item.value);
        return;
      }

      let result = groups.data[0];

      if (result && result.slug) {
        Router.push(`/group?id=${result.slug}`, `/group/${result.slug}`);
        return;
      } else {
        Router.push('/no-group?zip=' + item.value);
        return;
      }
    });
  }

  handleFindMyGroup(e) {
    e.preventDefault();
    this.searchGroup({value: this.state.searchTerm});
  }

  handleSearchChange(searchTerm) {
    // searchTerm !== '' && this.setState({searchTerm});
    this.setState({searchTerm});
    if (searchTerm.length > 0) {
      if (!!this.req) this.req.cancel();

      this.req = makeCancelable(LocationsData.listZips({filter: searchTerm}));

      this.req.promise
        .then(res => {
          if (res.data) {
            this.setState({allZips: res.data.map(zipMapper)});
          }
        })
        .catch(res => console.log(res));
    }
  }
  handleCreateFromSearch(options,search) {
    if (search.length === 0 || options.filter(opt => opt.value === search).length !== 0) return null;
    return {label: search, value: search};
  }
  renderSearchOption(item, newOption) {
    return <div className="simple-option"><span>{item.label}</span></div>
  }
  renderNoResultsFound(value, search) {
    return <div className="no-results-found" style={{fontSize: 12}}>
      {typeof this.req == 'undefined' && this.state.searchTerm.length == 0 ? 'Enter your Location by Zip to find a Local Group' : 'No results found'}
    </div>
  }

  render() {
    return (
      <TransHolder ssr={this.props.ssr}>
        <div className="toy-cycle-home">
          <section className="front-hero text-white bg-inverse">
            <div className="swiper-container">
                <div className="swiper-wrapper">
                    { SlideshowImages.map((i, index) => <div key={index} className={`swiper-slide item${index}`} style={{backgroundImage: `url(${i})`}}></div>) }
                </div>
                <div className="swiper-button-prev"></div>
                <div className="swiper-button-next"></div>
            </div>
            <div className="container pt-3">
              <Header session={this.state.session} loggedIn={this.props.loggedIn} isAdmin={this.props.isAdmin} title={ Intro.metaPageTitle } pathname="/" isLight={true} totalUnreadMessages={this.state.totalUnreadMessages} />

              <div className="col-md-8 mx-auto mb-5 pb-5 text-center">
                <div>
                  <h1>{ Intro.introHeadline }</h1>
                  
                  <div className="row my-3">
                    <p className="lead mx-auto" dangerouslySetInnerHTML={{__html: Intro.introSubHeadline}}></p>
                  </div>

                  { this.state.allZips && <div className="autocomplete-zip-search-wrapper">
                    <div className="input-find-group">
                      <SimpleSelect
                        options={this.state.allZips}
                        ref="select"
                        placeholder="Search Group By Zip"
                        theme="material"
                        search={this.state.searchTerm}
                        value={this.state.searchTerm}
                        onSearchChange={this.handleSearchChange}
                        onValueChange={this.searchGroup}
                        createFromSearch={this.handleCreateFromSearch}
                        renderOption={this.renderSearchOption}
                        renderNoResultsFound={this.renderNoSearchResults}
                        disabled={this.state.loading}
                      />
                    </div>
                    <button onClick={this.handleFindMyGroup} type="button" className="btn btn-primary text-uppercase">
                      {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : <span>{Intro.introButtonLabel}&nbsp;&nbsp;<i className="fa fa-search" aria-hidden="true"></i></span>}
                    </button>
                  </div> }
                </div>
              </div>
            </div>
          </section>

          <div className="container-fluid video-showcase">
            <div className="col-md-7 mx-auto mb-5 pb-5 text-center">
                <ModalVideo channel="vimeo" isOpen={this.state.isOpenVideoModal} videoId={VideoData.videoId} onClose={this.closeVideoModal} />

                <h5>
                  See how it works 
                  <svg className="d-inline align-middle ml-2" id="crooked-arrow" x="0px" y="0px" width="22.745px" height="40.218px" viewBox="0 0 22.745 40.218" enableBackground="new 0 0 22.745 40.218">
                    <path fill="#FF4B64" d={`M14.198,36.583c-2.991,1.358-6.284,1.88-9.188,3.557c-0.477,0.259-1.253-0.156-1.024-0.729
                      c1.213-3.27,1.735-6.796,3.354-9.919c0.309-0.575,1.359-0.452,1.297,0.276c-0.096,1.092,0.01,2.137,0.035,3.265
                      c3.301-3.667,6.765-7.179,9.489-11.31c1.479-2.23,3.071-4.828,3.087-7.57c0.003-3.307-3.552-3.663-5.993-2.89
                      c-2.12,0.681-9.261,5.535-9.976,0.843c-0.428-2.767,0.771-4.866,2.047-7.21c0.389-0.659,0.654-1.354,0.84-2.046
                      c0.742-2.688-3.247-0.932-3.957-0.261c-1.42,1.341-2.384,3.19-3.352,4.879C0.588,8-0.187,7.584,0.042,7.012
                      C1.147,4.351,3.263,0.041,6.648,0C9.59,0.015,10.004,2.218,9.01,4.512c-0.648,1.516-4.981,9.252-0.415,8.039
                      c3.005-0.793,5.497-2.778,8.631-3.214c2.21-0.281,4.341,1.175,5.193,3.16c0.983,2.424-0.477,5.46-1.597,7.557
                      c-2.819,5.223-7.066,9.73-11.241,13.913c1.618,0.264,3.321,0.687,4.827,1.317C14.98,35.587,14.677,36.405,14.198,36.583z
                      M7.423,33.037c-0.565,1.673-1.007,3.464-1.53,5.176c1.994-0.878,3.996-1.435,6.075-2.154c-1.376-0.272-2.791-0.424-3.856-1.191
                      c-0.246-0.152-0.293-0.434-0.218-0.678C7.645,33.875,7.474,33.478,7.423,33.037z`}/>
                  </svg>
                </h5>
                <div className="video-trigger my-3" onClick={this.openVideoModal}>
                  <img className="rounded img-fluid" src={VideoData.videoThumb} />
                  <i className="fa fa-play"></i>
                </div>
            </div>
          </div>
            
          <section className="w-75 m-auto mb-5">

            {Features.map((feature, i) => {
              const div2 = (i+1)%2 === 0;
              const mainPlaceholderClass = div2 ? 'my-5 no-gutters clearfix' : 'row my-5 no-gutters';
              const imgPlaceholderClass = div2 ? 'col-lg-6 p-0 float-right' : 'col-lg-6 p-0';
              const textPlaceholderClass = div2 ? 'col-lg-6 py-5 px-md-5 float-left' : 'col-lg-6 py-5 px-md-5';
              return (
                <VisibilityTracker key={i} once={true}>
                  <Feature
                    feature={feature}
                    mainClass={mainPlaceholderClass}
                    imgClass={imgPlaceholderClass}
                    textClass={textPlaceholderClass} />
                </VisibilityTracker>
              )
            })}

          </section>

          { !this.props.loggedIn && <CallToAction /> }

          {/* PARTNERS SECTION */}
          <PartnersWidget items={this.state.partners} />

          <div className="container"><Footer loggedIn={this.props.loggedIn} /></div>

        </div>
      </TransHolder>
    );
  }
}


function CallToAction() {
  return (
    <div className="jumbotron w-75 text-center p-md-5 mb-5 mx-auto">
      <div dangerouslySetInnerHTML={{ __html: CallToActionWidget.description }}></div>
    </div>
  );
}

function Feature ({feature, mainClass, imgClass, textClass, isVisible}) {
  const pulse = isVisible ? ' animated pulse' : '';
  return(
    <div className={mainClass + pulse}>
      <div className={imgClass}>
        <img className="w-100" src={ feature.imgSrc } />
      </div>
      <div className={textClass}>
        <h4 className="feature-link">{ feature.title }</h4>
        <div dangerouslySetInnerHTML={{ __html: feature.description }}></div>
      </div>
    </div>
  );
}