import Session from '../auth/session';
import Page from '../components/page';
import Layout from '../components/layout';
import Router from 'next/router';

import ObjectAssign from 'object-assign';
import TransHolder from '../components/component-transition-placeholder';

export default class extends Page {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.query = ctx.query;
    return props;
  }

  constructor(props) {
    super(props);
    const query = props.query || null;
    this.state = ObjectAssign({}, this.state, {
      username: query && query.email || '',
      password: '',
      loading: false
    });
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    if (['username', 'password'].indexOf(target.name) > -1) {
      this.setState({[target.name]: target.value});
    }
  }

  async handleLogin(event) {
    event.preventDefault();
    this.clearAllNotifications();
    if (this.state.username.trim() === '' || this.state.password.trim() === '') {
      this.addNotification({
        title: 'Error',
        message: 'Both email and password are required'
      });
      return;
    }

    this.setState({loading: true});

    const session = new Session();
    session.login(this.state.username, this.state.password)
      .then((newSessionData) => {
        console.log(newSessionData);
        if (!newSessionData.user) {
          this.addNotification({
            title: 'Error',
            message: 'The email or password is incorrect.'
          });
          this.setState({loading: false});
          return;
        }
        let redirectUrl = '/';

        // @todo: think of better solution
        if (newSessionData.user.data.roleId === 1) {
          redirectUrl = '/admin';
        } else {
          redirectUrl = this.props.lastPage && ['/login', '/signup'].indexOf(this.props.lastPage) === -1 ? this.props.lastPage : '/';
        }
        window.location.replace(redirectUrl);
      });
  }

  render() {
    return (
      <Layout props={this.props} session={this.state.session} title="Log In" totalUnreadMessages={this.state.totalUnreadMessages}>
        <TransHolder ssr={this.props.ssr} session={this.state.session}>
          { !this.props.loggedIn && (
            <div className="jumbotron col-lg-5 col-md-8 mx-auto">
              <h2 
                className="display-4 mb-5"
                style={{
                  backgroundImage: 'url(/static/img/no-results.png)',
                  backgroundRepeat: 'no-repeat',
                  backgroundPosition: 'right center',
                  backgroundSize: 'contain',
                  color: '#49671f'
                }}>
                Log in
              </h2>
              <form method="post" action="/auth/login" onSubmit={this.handleLogin}>
                <style jsx>{`
                  .input-group-addon {background-color: rgba(139, 193, 62, 0.22)}  
                `}</style>
                <div className="input-group mb-3">
                  <span className="input-group-addon" id="email-addon"><i className="fa fa-envelope-o"></i></span>
                  <input 
                    type="text" 
                    className="form-control" 
                    placeholder="Email"
                    aria-describedby="email-addon"
                    autoFocus
                    name="username"
                    value={this.state.username}
                    onChange={this.handleInputChange} />
                </div>
                <div className="input-group mb-4">
                  <span className="input-group-addon" id="pass-addon"><i className="fa fa-key"></i></span>
                  <input 
                    type="password" 
                    className="form-control" 
                    placeholder="Password"
                    aria-describedby="pass-addon"
                    name="password"
                    value={this.state.password}
                    onChange={this.handleInputChange} />
                </div>

                <div className="px-3">
                  <button 
                    disabled={this.state.loading} 
                    type="submit" 
                    className="btn btn-success w-100 mb-2"
                    style={{borderRadius: '3px'}}>
                    { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : 'Submit' }
                  </button>

                  <p className="text-center">
                    <small><button
                      disabled={this.state.loading}
                      className="text-muted btn btn-link"
                      onClick={e => Router.push('/forgot-password')}>
                      Forgot your password?
                    </button></small>
                  </p>
                </div>
              </form>
            </div>
          ) }


          { this.props.loggedIn && (
            <div className="text-center"><h2>You are already logged in</h2></div>
          ) }
        </TransHolder> 

        { this.notificationStack() }
      </Layout>
    );
  }
}