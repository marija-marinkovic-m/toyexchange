import { Component } from 'react';
import Page from '../components/page';
import Layout from '../components/layout';

import ObjectAssign from 'object-assign';

import { MapModal } from '../components/map-component';
import DropdownMenu from '../components/bootstrap/dropdown-menu';

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = {
      isMapOpen: false,
      address: '46205-46208'
    }

    this.handleShowMap = this.handleShowMap.bind(this);
  }

  handleShowMap(e) {
    e.preventDefault();
    this.setState({isMapOpen: true});
  }

  render() {

    const menuItems = [
      {
        type: 'link',
        label: 'Dashboard',
        href: '/admin'
      },
      { type: 'divider' },
      { type: 'logout', token: this.props.session.csrfToken }
    ];

    return (
      <Layout props={this.props} title="Map Test" session={this.state.session}>

        <p>{this.state.address} (<a href="#" onClick={this.handleShowMap}>Show map</a>)</p>

        <MapModal
          isOpen={this.state.isMapOpen}
          address={this.state.address}
          onClose={() => this.setState({isMapOpen: false})} />


        <div className="m-5">
          <DropdownMenu items={menuItems}>
            triggerrrr
            {/*{ this.props.session.user.username }
            <figure className="d-inline-block align-middle mb-0 rounded-circle ml-2" style={{width: '30px', height: '30px'}}>
              <img className="d-block w-100 rounded-circle" src={this.props.session.user.data.avatarFile || config.defaultAvatarImage}  />
            </figure>

            {this.props.totalUnreadMessages === false ? <span><i className="fa fa-refresh fa-spin"></i></span> : this.props.totalUnreadMessages == 0 ? '' : <span className="badge badge-default badge-pill">{this.props.totalUnreadMessages}</span>}*/}
          </DropdownMenu>
        </div>

      </Layout>
    );
  }
}