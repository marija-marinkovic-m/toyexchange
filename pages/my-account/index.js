import React from 'react';

import Link from 'next/link';
import ReactTooltip from 'react-tooltip';
import Profile from '../../api/profile';

import Page from '../../components/my-account-components/page';
import Layout from '../../components/layout';
import Sidebar from '../../components/my-account-components/account-sidebar';
import LogInWidget from '../../components/widget-login-or-signup';
import TransHolder from '../../components/component-transition-placeholder';
import ProfileBar from '../../components/my-account-components/account-profile-bar';

import FileUpload from '../../components/file-upload';

import ObjectAssign from 'object-assign';
import Locations from '../../api/locations';

export default class extends Page {

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      user: this.state.session.user && this.state.session.user.data ? this.state.session.user.data : null,
      loading: false,
      states: [],
      zipCodes: null
    });

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleAvatarFile = this.handleAvatarFile.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleAvatarFile(avatarFile) {
    const user = ObjectAssign({}, this.state.user, {
      avatarFile
    });
    this.setState({user});
  }

  async handleInputChange(event) {
    event.preventDefault();
    if (!this.state.user) return;
    const target = event.target;

    let updatedState = {};

    updatedState.user = ObjectAssign({}, this.state.user, {
      [target.name]: target.value
    });

    if (target.name === 'stateId') {
      // update zips and null user zipcode
      let statesRes = await Locations.listZips({pagin: '0', stateId: target.value});

      updatedState.user.zipCode = '';
      updatedState.zipCodes = statesRes && statesRes.data ? statesRes.data : [];
 
    }

    this.setState(updatedState);
  }

  handleFormSubmit(e) {
    e.preventDefault();
    if (!this.state.user) return;

    this.clearAllNotifications();
    this.setState({loading: true});

    // the request
    Profile.update(this.state.user)
      .then(response => {
        console.log(response);
        if (response.user && response.user.data && !response.error) {
          this.addNotification({
            message: 'Profile successfully updated.'
          });
          this.setState({session: ObjectAssign({}, this.state.session, {user: response.user})});
        } else {
          this.addNotification({
            title: 'Error',
            message: response.error.message,
            dismissAfter: 4500
          })
        }
        this.setState({loading: false});
      });
  }

  async componentDidMount() {
    super.componentDidMount();
    const { user } = this.state;
    if (user.countryId) {
      let states = await Locations.listStates({pagin: '0', countryId: user.countryId});
      states && states.data && this.setState({states: states.data})
    }
    if (user.stateId) {
      let zipCodes = await Locations.listZips({pagin: '0', stateId: user.stateId});
      zipCodes && zipCodes.data && this.setState({zipCodes: zipCodes.data});
    }
  }

  render() {
    let user = this.state.session.user.data;
    return (
      <Layout props={this.props} session={this.state.session} title="My Account Settings" totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <div className="jumbotron">
          {/*<h2 className="display-5 mb-5">Account Settings</h2>*/}

          { this.props.loggedIn && user ? (
            <div>
              <ProfileBar user={user} />
              <div className="row">
                <aside className="col-md-3 mb-3">
                  <Sidebar user={user} pathname={this.props.pathname} totalUnreadMessages={this.state.totalUnreadMessages} />
                </aside>
                <section className="col-md-9">
                  <TransHolder ssr={this.props.ssr}>
                    <form onSubmit={this.handleFormSubmit}>
                      {/* email */}
                      <div className="form-group row">
                        <label htmlFor="email" className="col-md-3 col-form-label">Email</label>
                        <div className="col-md-9">
                          <input className="form-control" type="text" value={user.email} id="email" name="email" onChange={this.handleInputChange} />
                        </div>
                      </div>

                      {/* firstName */}
                      <div className="form-group row">
                        <label htmlFor="firstName" className="col-md-3 col-form-label">First name</label>
                        <div className="col-md-9">
                          <input className="form-control" type="text" value={user.firstName} id="firstName" name="firstName" onChange={this.handleInputChange} />
                        </div>
                      </div>

                      {/* lastName */}
                      <div className="form-group row">
                        <label htmlFor="lastName" className="col-md-3 col-form-label">Last name</label>
                        <div className="col-md-9">
                          <input className="form-control" type="text" value={user.lastName} id="lastName" name="lastName" onChange={this.handleInputChange} />
                        </div>
                      </div>

                      {/* displayName */}
                      <div className="form-group row">
                        <label htmlFor="displayName" className="col-md-3 col-form-label">Display name</label>
                        <div className="col-md-9">
                          <input className="form-control" type="text" value={user.displayName} id="displayName" name="displayName" onChange={this.handleInputChange} />
                        </div>
                      </div>

                      {/* stateId */}
                      <div className="form-group row">
                        <label htmlFor="stateId" className="col-md-3 col-form-label">State</label>
                        <div className="col-md-9">
                          <select className="custom-select form-control" name="stateId" id="stateId" value={user.stateId} onChange={this.handleInputChange}>
                            { this.state.states.length && this.state.states.map(s => <option key={s.id} value={s.id}>{s.name} ({s.abbreviation})</option>) }
                          </select>
                        </div>
                      </div>

                      {/* zipCode */}
                      <div className="form-group row">
                        <label htmlFor="zipCode" className="col-md-3 col-form-label">ZIP code</label>
                        {this.state.zipCodes && <div className="col-md-9">
                          { this.state.zipCodes.length ? <select className="custom-select form-control" name="zipCode" id="zipCode" value={user.zipCode} onChange={this.handleInputChange}>
                            { this.state.zipCodes.map(z => <option key={z.id} value={z.code}>{z.code} ({z.name})</option>) }
                          </select> : <select disabled={true} className="custom-select form-control"><option>No available zip codes for this state.</option></select> }
                        </div>}
                      </div>

                      {/* city */}
                      <div className="form-group row">
                        <label htmlFor="city" className="col-md-3 col-form-label">City</label>
                        <div className="col-md-9">
                          <input className="form-control" type="text" value={user.city} id="city" name="city" onChange={this.handleInputChange} />
                        </div>
                      </div>

                      {/* pickupAddress */}
                      <div className="form-group row">
                        <label htmlFor="pickupAddress" className="col-md-3 col-form-label">Pick-up Address</label>
                        <div className="col-md-9">
                          <input className="form-control" type="text" value={user.pickupAddress} id="pickupAddress" name="pickupAddress" onChange={this.handleInputChange} />
                          <small className="form-text text-muted">Your address will only be seen by members with whom you arrange exchanges.</small>
                        </div>
                      </div>

                      {/* pickupWindow */}
                      <div className="form-group row">
                        <label htmlFor="pickupWindow" className="col-md-3 col-form-label">
                          Pick-up Window&nbsp;<i className="fa fa-question-circle" data-tip="If you would like to set a default time when you will have others pick up items from you, <br />include that here. For example: 9:00-12:00 Saturday. <br />Or you can state “Communicate with poster to coordinate“."></i>
                        </label>
                        <div className="col-md-9">
                          <input className="form-control" type="text" value={user.pickupWindow} id="pickupWindow" name="pickupWindow" onChange={this.handleInputChange} />
                        </div>
                      </div>

                      {/* pickupPlacement */}
                      <div className="form-group row">
                        <label htmlFor="pickupPlacement" className="col-md-3 col-form-label">
                          Pick-up Placement&nbsp;<i className="fa fa-question-circle" data-tip="If you will always place items in the same spot for other members, <br />you can set a default here. Example: On the front porch"></i>
                        </label>
                        <div className="col-md-9">
                          <input className="form-control" type="text" value={user.pickupPlacement} id="pickupPlacement" name="pickupPlacement" onChange={this.handleInputChange} />
                        </div>
                      </div>

                      {/* nearbyNotifications */}
                      <div className="form-group row">
                        <label className="col-md-3 col-form-label">&nbsp;</label>
                        <div className="col-md-9">
                          <label className="custom-control custom-checkbox">
                            <input type="checkbox" name="nearbyNotifications" className="custom-control-input" checked={user.nearbyNotifications ? true : false} onChange={e => {this.setState({user: ObjectAssign({}, user, {nearbyNotifications: !user.nearbyNotifications})})}} />
                            <span className="custom-control-indicator"></span>
                            <span className="custom-control-description">Receive Notifications From Nearby Groups</span>
                          </label>
                        </div>
                      </div>

                      {/* notificationType */}
                      <div className="form-group row">
                        <label className="col-md-3 col-form-label" htmlFor="notificationType">Notification type</label>
                        <div className="col-md-9">
                          <select id="notificationType" name="notificationType" className="custom-select" value={user.notificationType} onChange={this.handleInputChange}>
                            <option value="individual">Individual</option>
                            <option value="daily">Daily</option>
                          </select>
                        </div>
                      </div>

                      {/* avatarFile */}
                      <div className="form-group row">
                        <label className="col-md-3 col-form-label">Avatar</label>
                        <div className="col-md-9">
                          <label className="d-inline-block align-middle custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input" name="avatarClear"  checked={user.avatarClear ? true : false} onChange={e => {this.setState({user: ObjectAssign({}, user, {avatarClear: !user.avatarClear})})}} />
                            <span className="custom-control-indicator"></span>
                            <span className="custom-control-description">Remove avatar</span>
                          </label>
                          { !user.avatarClear && 
                          <FileUpload handleFile={this.handleAvatarFile} defaultSrc={this.props.loggedIn && this.state.session.user.data ? this.state.session.user.data.avatarFile : null} /> }
                        </div>
                      </div>

                      <div className="row mt-5">
                        <label className="col-md-2">&nbsp;</label>
                        <div className="col-md-9">
                        <button disabled={this.state.loading} type="submit" className="btn btn-primary text-uppercase mb-5">{ this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Saving...</span> : 'Update My Profile' }</button>
                        </div>
                      </div>
                      <ReactTooltip type="success" html={true} />
                    </form>
                  </TransHolder>
                </section>
              </div>
            </div>
          ) : (
            <LogInWidget />
          ) }
        </div>
        { this.notificationStack() }
      </Layout>
    );
  }
}