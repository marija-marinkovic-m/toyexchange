import React from 'react';
import Router from 'next/router';
import Link from 'next/link';

import DataService, { Messages } from '../../api/account';
import ObjectAssign from 'object-assign';
import cn from 'classnames';
import moment from 'moment';

import Page from '../../components/my-account-components/page-filters';
import Layout from '../../components/layout';
import Sidebar from '../../components/my-account-components/account-sidebar';
import LogInWidget from '../../components/widget-login-or-signup';
import TransHolder from '../../components/component-transition-placeholder';
import ProfileBar from '../../components/my-account-components/account-profile-bar';
import Pagination from '../../components/bootstrap/pagination';

import config from '../../static/config';
import ReactTooltip from 'react-tooltip';

const defaultFilters = {
  pagin: 1,
  perPage: 15,
  page: 1
}

export default class MessagesPage extends Page {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultFilters, ctx.query);
    return props;
  }

  async fetchFn(filters) {
    return await Messages.listPosts(filters);
  }

  render() {

    if (!this.props.loggedIn) return <LogInWidget />

    const user = this.state.session.user && this.state.session.user.data ? this.state.session.user.data : null;

    if (!user) return;

    const { items } = this.state;

    return(
      <Layout props={this.props} title="Messages" totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <div className="jumbotron">

          <ProfileBar user={user} />
          <div className="row">
            <aside className="col-md-3 mb-3">
              <Sidebar user={user} pathname={this.props.pathname} totalUnreadMessages={this.state.totalUnreadMessages} />
            </aside>
            
            <section className="col-md-9">
              { items.data === null && <p><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</p> }

              <style jsx>{`
                .table-inbox tbody tr {cursor:pointer}
                .table-inbox tr td {vertical-align: middle;}
              `}</style>
              <table className="table table-inbox table-striped table-hover">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Post</th>
                    <th>Author</th>
                    <th className="text-sm-center">Unread</th>
                    <th className="text-sm-right"><i className="fa fa-clock-o"></i></th>
                  </tr>
                </thead>
                <tbody>
                  { items.data && items.data.map((p,i) => {
                    return(
                      <tr
                        key={i}
                        className={cn({'font-weight-bold': p.totalUnreadMessages > 0})}
                        onClick={e => Router.push(`/toy?id=${p.slug}&section=reply`, `/toy/${p.slug}?section=reply`)}>
                        <td>
                          { 
                            p.status !== 'active' ? 
                            <img width="40px" src="/static/img/closed-stamp-sm-red.png" alt="" /> : 
                            <img width="32px" height="32px" className="rounded-circle" alt="Thumbnail" src={p.imageFile || config.defaultPostImage} />
                          }
                        </td>
                        <td className="view-message">{p.title}</td>
                        <td>
                          { p.userId === user.id ? '' : <i className="text-muted"><i className="fa fa-user-o"></i>&nbsp;&nbsp;{p.userData.displayName}</i> }
                        </td>
                        {/*<td className="view-message">{p.excerptSeo}</td>*/}
                        <td className="text-sm-center inbox-small-cells">{ p.totalUnreadMessages > 0 ? <span className="badge-pill badge badge-primary">{p.totalUnreadMessages}</span> : '' }</td>
                        <td className="view-message text-sm-right">{moment.utc(p.updatedAt).fromNow()}</td>
                      </tr>
                    );
                  }) }

                  {/*if no results*/}
                  { items.data && !items.data.length && <tr><td colSpan="5" className="text-center">You have no messages, yet...</td></tr> }
                </tbody>
              </table>

              <Pagination
                pagin={items.paginator}
                onChange={page => this.fetchItems({page})} />
            </section>
          </div>

          <ReactTooltip type="light" html={true} />
          
        </div>
      </Layout>
    );
  }

}