import React from 'react';
import Link from 'next/link';
import Router from 'next/router';

import Page from '../../components/my-account-components/page';
import Layout from '../../components/layout';
import Sidebar from '../../components/my-account-components/account-sidebar';
import LogInWidget from '../../components/widget-login-or-signup';

import ObjectAssign from 'object-assign';

import Data from '../../static/content/page-new-post';
import TransHolder from '../../components/component-transition-placeholder';

import ProfileBar from '../../components/my-account-components/account-profile-bar';
import ToyForm from '../../components/toy-post-form';
import { MyPosts } from '../../api/posts';

export default class extends Page {

  constructor(props) {
    super(props);
    this.state = ObjectAssign({loading: false}, this.state, this.formDataReset(null, this.state));
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFileDrop = this.handleFileDrop.bind(this);
  }

  handleChange(e) {
    e.preventDefault();
    let target = e.target;
    this.setState({[target.name]: target.value});
  }

  formDataReset(state, props) {
    let user = props.session.user && props.session.user.data ? props.session.user.data : null;
    return {
      imageFile: state ? state.imageFile : '',
      title: state ? state.title : '',
      ageRange: state ? state.ageRange : 1,
      pickupWindow: state ? state.pickupWindow : user ? user.pickupWindow : '',
      pickupAddress: state ? state.pickupAddress : user ? user.pickupAddress : '',
      pickupPlacement: state ? state.pickupPlacement : user ? user.pickupPlacement : '',
      description: state ? state.description : '',
      postType: state ? state.postType : 'offer'
    };
  }
  
  async handleSubmit(e) {
    e.preventDefault();
    this.clearAllNotifications();
    this.setState({loading: true});

    const postData = this.formDataReset(this.state, this.state);

    MyPosts.create(postData)
      .then(response => {
        if (response.id) {
          this.setState(ObjectAssign({}, this.state, this.formDataReset(null, this.state)));
          this.addNotification({
            message: response.message,
            action: 'View Post',
            dismissAfter: false,
            onClick: () => Router.push(`/toy?id=${response.id}`, `/toy/${response.id}`)
          });
        } else if (response.hasOwnProperty('error')) {
          console.log(response);
          this.addNotification({
            title: 'Error',
            message: response.error.message || 'File to big',
            dismissAfter: 4500
          });
        } else {
          this.addNotification({
            title: 'Error',
            message: 'An error occured. Please, try later'
          })
        }
        this.setState({loading: false});
      });
  }

  handleFileDrop(img) {
    this.setState({imageFile: img});
  }

  render() {
    let user = this.state.session.user && this.state.session.user.data ? this.state.session.user.data : null;

    return (
      <Layout props={this.props} title={Data.title} totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <div className="jumbotron">
          { this.props.loggedIn && user ? (
            <div>
              <ProfileBar user={user} />
              <div className="row">
                <aside className="col-md-3 mb-3">
                  <Sidebar 
                    user={user}
                    totalUnreadMessages={this.state.totalUnreadMessages}
                    pathname={this.props.pathname} />
                </aside>
                <section className="col-md-9">
                  <TransHolder ssr={this.props.ssr}>
                    <ToyForm
                      submitBtnLabel={Data.submitBtnLabel}
                      stateData={this.state}
                      handleFileDrop={this.handleFileDrop}
                      handleChange={this.handleChange}
                      handleSubmit={this.handleSubmit} 
                      loading={this.state.loading}
                    />
                    { this.notificationStack() }
                  </TransHolder>
                </section>
              </div>
            </div>
          ) : (
            <LogInWidget />
          ) }

        </div>
      </Layout>
    );
  }
}