import React from 'react';
import Router from 'next/router';
import moment from 'moment';
import ObjectAssign from 'object-assign';
import DataService from '../../api/account';

import Page from '../../components/my-account-components/page-filters';
import Layout from '../../components/layout';
import Sidebar from '../../components/my-account-components/account-sidebar';
import LogInWidget from '../../components/widget-login-or-signup';
import TransHolder from '../../components/component-transition-placeholder';
import AgeRangeFilters from '../../components/age-range-filters-sidebar';
import PostTypeOrderFilters from '../../components/post-type-order-filters-sidebar';

import Pagination from '../../components/bootstrap/pagination';
import ProfileBar from '../../components/my-account-components/account-profile-bar';

import config from '../../static/config';

const defaultFilters = {
  ageRange: false,
  postType: false,
  order: 'desc',
  perPage: 15,
  pagin: 1,
  page: 1
}

export default class extends Page {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultParams = ObjectAssign({}, defaultFilters, ctx.query);
    return props;
  }

  async fetchFn(filters) {
    return await DataService.getPosts(filters);
  }

  render() {
    if (!this.props.loggedIn) return <LogInWidget />;

    const user = this.state.session.user && this.state.session.user.data ? this.state.session.user.data : null;

    if (!user) return;

    let loadingMessage;

    if (this.state.items && this.state.items.data === null) {
      loadingMessage = <p className="px-3"><i className="fa fa-circle-o-notch"></i>&nbsp;&nbsp;Loading...</p>;
    } else if (this.state.items && this.state.items.data && this.state.items.data.length === 0) {
      loadingMessage = <p className="text-center w-100"><i className="d-block mb-4 p-3 text-left">Sorry, no results were found.</i><img src="/static/img/no-results.png" /></p>;
    }

    return (
      <Layout props={this.props} title="My Posts" totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <div className="jumbotron">
          <ProfileBar user={user} />
          <div className="row">
            <aside className="col-md-3 mb-3">
              <Sidebar 
                user={user}
                totalUnreadMessages={this.state.totalUnreadMessages}
                pathname={this.props.pathname} />
              <AgeRangeFilters current={this.state.filters.ageRange} handle={(key, value) => this.fetchItems({[key]: value})} />
            </aside>

            <section className="col-md-9">
              <TransHolder ssr={this.props.ssr}>

                <div className="row nav-filters text-md-right text-sm-center mb-4">
                  <PostTypeOrderFilters 
                    order={this.state.filters.order}
                    posttype={this.state.filters.postType}
                    handle={(key, value) => this.fetchItems({[key]: value})} />

                  <form onSubmit={e => {e.preventDefault(); this.fetchItems({filter: this.refs.filterInput.value})}} className="input-group col ml-5">
                    <input
                      ref="filterInput"
                      type="text"
                      className="form-control"
                      placeholder="Search..."
                      defaultValue={this.state.filters.filter} />
                    <span className="input-group-btn">
                      <button
                        type="submit"
                        className="btn btn-secondary"
                        style={{borderTopRightRadius: '.3rem', borderBottomRightRadius: '.3rem'}}>
                        <i className="fa fa-search"></i>
                      </button>
                    </span>
                  </form>
                </div>

                { loadingMessage ? <div className="row">{ loadingMessage }</div> : <table className="table table-striped table-hover mb-5">
                  <thead>
                    <tr>
                      <th>&nbsp;</th>
                      <th>Post Title</th>
                      <th>&nbsp;</th>
                      <th className="text-sm-right"><i className="fa fa-clock-o"></i>&nbsp;Latest update</th>
                    </tr>
                  </thead>
                  <tbody>
                    { this.state.items.data.map((p,i) => {
                      const momentUpdated = moment.utc(p.updatedAt);
                      const diff = moment().diff(momentUpdated, 'days');
                      const latestUpdates = diff > 5 ? momentUpdated.format('ll') : momentUpdated.fromNow();
                      return (
                        <tr key={i}
                          style={{cursor: 'pointer', opacity: p.status === 'completed' ? .7 : 1}}
                          onClick={e => Router.push(`/toy?id=${p.slug}`, `/toy/${p.slug}`)}>
                          <td>
                            <img width="32px" height="32px" className="rounded-circle" alt="Thumbnail" src={p.imageFile || config.defaultPostImage} />
                          </td>
                          <td className="align-middle">{p.title}</td>
                          <td className="align-middle">{ p.status !== 'active' ? <img src="/static/img/closed-stamp-sm.png" alt="Closed" /> : <span>&nbsp;</span> }</td>
                          <td className="text-sm-right align-middle">{latestUpdates}</td>
                        </tr>);
                    }) }
                  </tbody>
                </table> }

                
                <nav>
                  <Pagination 
                    pagin={this.state.items.paginator}
                    onChange={page => this.fetchItems({page})}
                  />
                </nav>
              </TransHolder>
            </section>
          </div>
        </div>
      </Layout>
    );
  }

}