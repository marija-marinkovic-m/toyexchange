import Page from '../../components/my-account-components/page';
import Layout from '../../components/layout';
import Sidebar from '../../components/my-account-components/account-sidebar';
import LogInWidget from '../../components/widget-login-or-signup';
import TransHolder from '../../components/component-transition-placeholder';

import ObjectAssign from 'object-assign';
import { MultiSelect } from 'react-selectize';
import AccountData from '../../api/account';
import ProfileBar from '../../components/my-account-components/account-profile-bar';

import { ReferralsForm, ReferralSentConfirmation } from '../../static/content/page-signup';

export default class extends Page {

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      emails: '',
      statement: '',
      loading: false
    });
  }

  handleChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.loading) return;
    let { emails, statement } = this.state;
    const exit = () => this.setState({loading: false});

    if (emails.trim() === '' || statement.trim() === '') {
      this.addNotification({title: 'Both fields are required'});
      return exit();
    }

    this.setState({loading: true});
    this.clearAllNotifications();

    AccountData.createReferrals({emails, statement})
      .then(res => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request Failed'});
          return exit();
        }
        this.addNotification({title: 'Success', message: res.message ? res.message : 'Referrals sent'});
        exit();
        this.setState({finish: true});
      });
  }

  render() {
    const user = this.state.session.user && this.state.session.user.data ? this.state.session.user.data : null;
    const handleSubmit = this.handleSubmit.bind(this);
    const handleChange = this.handleChange.bind(this);

    return (
    <Layout props={this.props} title="My Referrals" totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
      <div className="jumbotron">
          { this.notificationStack() }

          { this.props.loggedIn && user ? (
            <div>
              <ProfileBar user={user} />
              <div className="row">
                <aside className="col-md-3 mb-3">
                  <Sidebar 
                    user={user}
                    totalUnreadMessages={this.state.totalUnreadMessages}
                    pathname={this.props.pathname} />
                </aside>
                
                <section className="col-md-9">
                  <TransHolder>
                    <h5 className="text-success">{ ReferralsForm.subHeadline }</h5>

                    <p className="mb-5 font-weight-light">{ ReferralsForm.description }</p>

                    
                    { !this.state.finish ? <form onSubmit={handleSubmit}>
                      <div className="card mb-5">
                        <div className="card-block text-left">

                          <div className="form-group referral-emails">
                            <label>{ ReferralsForm.formInputEmailAddressesLabel }</label>
                            <MultiSelect
                              delimiters={[188,9]}
                              renderOption={() => ''}
                              onValuesChange={(values) => {
                                let value = values.map(v => v.label).join(',');
                                handleChange({target: {name: 'emails', value}})
                              }}
                              valuesFromPaste={(options, values, pastedText) => {
                                return pastedText
                                  .split(',')
                                  .filter(txt => values.map(i => i.label).indexOf(txt.trim()) == -1)
                                  .map(txt => ({label: txt.trim(), value: txt.trim()}));
                              }}
                              createFromSearch={(options, values, search) => {
                                let labels = values.map(value => value.label);
                                if (search.trim().length == 0 || labels.indexOf(search.trim()) != -1) return null;
                                return {label: search.trim(), value: search.trim()};
                              }}
                              theme="default"
                              renderNoResultsFound={() => ''}
                              renderToggleButton={() => ''} />
                          </div>

                          <div className="form-group">
                            <label>{ ReferralsForm.formInputStatementLabel }</label>
                            <textarea 
                              name="statement"
                              value={this.state.statement} 
                              onChange={handleChange} className="form-control">
                            </textarea>
                          </div>
                        </div>
                      </div>

                      <button 
                        type="submit" 
                        className="btn btn-primary"
                        disabled={this.state.loading}>
                        { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : ReferralsForm.formSubmitButtonLabel }
                        </button>
                    </form> : <h3 className="display-6 font-weight-light mb-4 pb-4">
                        <i className="fa fa-check rounded-ico text-success md mb-3 mr-2"></i>
                        { ReferralSentConfirmation.successNotice }
                      </h3> }
                  </TransHolder>
                </section>
              </div>
            </div>
          ) : (
            <LogInWidget />
          ) }
        </div>
    </Layout>
    );
  }
}