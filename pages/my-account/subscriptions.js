import Page from '../../components/my-account-components/page';
import Layout from '../../components/layout';
import Sidebar from '../../components/my-account-components/account-sidebar';
import LogInWidget from '../../components/widget-login-or-signup'
import TransHolder from '../../components/component-transition-placeholder';
import Pagination from '../../components/bootstrap/pagination';

import Account from '../../api/account';
import ProfileBar from '../../components/my-account-components/account-profile-bar';

import ObjectAssign from 'object-assign';
import moment from 'moment';

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      items: {data: null, paginator: null},
      loading: false
    });
    this.handlePaginate = this.handlePaginate.bind(this);
  }
  async componentDidMount() {
    super.componentDidMount();
    this.setState({
      items: await Account.getSubscriptions()
    })
  }

  async handlePaginate(page) {
    this.setState({
      items: await Account.getSubscriptions({page})
    })
  }

  render() {
    const user = this.props.loggedIn && this.state.session.user && this.state.session.user.data ? this.state.session.user.data : null;
    const { items } = this.state;

    return(
      <Layout props={this.props} title="My Subscriptions" totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <div className="jumbotron">
          { this.props.loggedIn ? (
            <div>
              <ProfileBar user={user} />
              <div className="row">
                <aside className="col-md-3 mb-3">
                  <Sidebar user={user} pathname={this.props.pathname} totalUnreadMessages={this.state.totalUnreadMessages} />
                </aside>
                <section className="col-md-9">
                  { items.data && <SubscriptionsTable items={items.data} /> }

                  { items.paginator && <Pagination pagin={items.paginator} onChange={this.handlePaginate} /> }

                  <hr />
                  <p className="text-muted mt-2">To cancel your membership to Toy-cycle.org and stop receiving email notifications, please contact <a href="mailto:admin@toy-cycle.org">admin@toy-cycle.org</a>. If you cancel prior to the end of a trial period, you will not be charged the annual membership fee. After the free trial period, the $12 annual membership fee is non-refundable.</p>
                </section>
              </div>
            </div>
          ) : <LogInWidget /> }
        </div>
      </Layout>
    );
  }
}


const SubscriptionsTable = ({items}) => (
  <table className="table table-hover">
    <thead>
      <tr>
        <th>Plan</th>
        <th>Date</th>
        <th>Free / Payed days</th>
      </tr>
    </thead>
    <tbody>
      { items && items.length && items.map((p,i) => <tr key={i}>
          <td>{ p.planData ? p.planData.title : `Referral (${p.referralId})` }</td>
          <td>{ moment.utc(p.startDate).format('LL') } - { moment.utc(p.endDate).format('LL') }</td>
          <td>{ p.freeDays } / { p.payedDays }</td>
        </tr>) }
    </tbody>
  </table>
);