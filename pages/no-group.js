import Link from 'next/link';

import Page from '../components/page';
import Layout from '../components/layout';

import Locations from '../api/locations';

import DataContent from '../static/content/page-no-group';
import ObjectAssign from 'object-assign';

export default class extends Page {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    if (ctx.query.zip) {
      props.zipCode = ctx.query.zip;
    }
    if (props.loggedIn) {
      let usr = props.session.user.data;

      props.countryId = usr.countryId;
      props.firstName = usr.firstName;
      props.lastName = usr.lastName;
      props.email = usr.email;
    }
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      firstName: props.firstName || '',
      lastName: props.lastName || '',
      email: props.email || '',
      zipCode: props.zipCode || '',
      countryId: props.countryId || 1,
      countries: null,
      loading: false
    });
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    super.componentDidMount();
    let allCountries = await Locations.listCountries({pagin: '0'});
    if (allCountries.data) {
      this.setState({
        countries: allCountries.data
      })
    }
  }

  handleInputChange(e) {
    const target = e.target;
    this.setState({[target.name]: target.value});
  }

  async handleSubmit(e) {
    e.preventDefault();
    console.log(this.state);

    this.clearAllNotifications();
    this.setState({loading: true});

    // createApplicant
    const { email, countryId, zipCode, firstName, lastName } = this.state;
    let response = await Locations.createApplicant({email, countryId, zipCode, firstName, lastName});

    console.log(response);

    if (response) {
      if (response.error) {
        this.addNotification({title: 'Error', message: response.error ? response.error.message : 'Request failed'});
      } else {
        this.addNotification({title: 'Success', message: response.message ? response.message : 'You will be notified the moment a Local Group launches in your area'});
        this.setState({requestSent: true})
      }
    }

    this.setState({
      loading: false
    });
  }

  render() {
    return (
      <Layout props={this.props} totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <div className="container-fluid py-5 text-center">
          <div className="col-md-8 mx-auto">
            <h1 className="col-md-10 mx-auto">{ DataContent.headline }</h1>
            <div dangerouslySetInnerHTML={{__html: DataContent.paragraph}}></div>
          </div>
          <div className="col-md-6 mx-auto">
            { !this.state.requestSent && <form onSubmit={this.handleSubmit}>

              <div className="card text-left mb-4">
                <div className="card-block">

                  <div className="row">
                    <div className="col form-group">
                      <label htmlFor="email">Email</label>
                      <input type="text" name="email" id="email" className="form-control" value={this.state.email} onChange={this.handleInputChange} />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col form-group">
                      <label htmlFor="firstName">First Name</label>
                      <input type="text" name="firstName" id="firstName" className="form-control" value={this.state.firstName} onChange={this.handleInputChange} />
                    </div>
                    <div className="col form-group">
                      <label htmlFor="lastName">Last Name</label>
                      <input type="text" name="lastName" id="lastName" className="form-control" value={this.state.lastName} onChange={this.handleInputChange} />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col form-group">
                      <label htmlFor="countryId">Country</label>
                      {/*<input type="text" name="countryId" id="countryId" className="form-control" value={this.state.countryId} onChange={this.handleInputChange} />*/}

                      <select
                        name="countryId"
                        value={this.state.countryId}
                        onChange={this.handleInputChange}
                        className="custom-select form-control">
                        { 
                          this.state.countries === null ?
                          <option>Loading...</option> : 
                          this.state.countries.map((c, i) => <option key={i} value={c.id}>{ c.name }</option>)
                        }
                      </select>
                    </div>
                    <div className="col form-group">
                      <label htmlFor="zipCode">Zip Code</label>
                      <input type="text" name="zipCode" id="zipCode" className="form-control" value={this.state.zipCode} onChange={this.handleInputChange} />
                    </div>
                  </div>

                </div>
              </div>

              <button disabled={this.state.loading} type="submit" className="btn btn-primary mb-5">{ this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : DataContent.formBtnLabel }</button>

            </form> }

            {this.state.requestSent && <p>Request successfully sent. You will be notified as soon as a Local Group launches in your area. Spread the word to help get your local group going!</p> }

            <Link href="/browse-groups"><a>{ DataContent.browseGroupsLinkLabel }</a></Link>

          </div>
        </div>
        { this.notificationStack() }
      </Layout>
    );
  }

}