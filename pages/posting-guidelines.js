import Page from '../components/page';
import Layout from '../components/layout';

import TransHolder from '../components/component-transition-placeholder';
import Data from '../static/content/page-posting-guidelines';

export default class extends Page {
  render() {
    return (
      <Layout props={this.props} title={Data.title} totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <TransHolder ssr={this.props.ssr}>
          <div className="jumbotron">
            <h1 className="mt-0 mb-3">{ Data.title }</h1>
            <hr />

            <div dangerouslySetInnerHTML={{ __html: Data.content }}></div>
                
          </div>
        </TransHolder>
      </Layout>
    );
  }
}