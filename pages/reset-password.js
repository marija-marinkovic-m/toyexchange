import Link from 'next/link';
import Page from '../components/page';
import Layout from '../components/layout';

import ObjectAssign from 'object-assign';

import Account from '../api/account';
import { Util } from '../api/util';

/**
 * if query.code === '' redirect to forgot password 
 * if query.code exists ask for password
 */
const defaultProps = {
  code: ''
}
export default class extends Page {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.defaultProps = ObjectAssign({}, defaultProps, ctx.query);
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      loading: false,
      passwordReset: false,
      query: props.defaultProps || {},
      newPassword: '',
      confirmNewPassword: ''
    })
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.loading) return;

    if (this.state.newPassword !== this.state.confirmNewPassword) {
      this.addNotification({title: 'Error', message: 'Password entries doesn\'t match.'});
      return;
    }
    
    this.clearAllNotifications();
    this.setState({loading: true});

    Account.resetPassword({
      code: this.state.query.code,
      password: this.state.newPassword
    })
      .then(res => {
        Util.handleResponse(this, res, () => {
          this.setState({passwordReset: true});
        });
      })
      .catch(err => console.log(err));

  }

  render() {
    const { query } = this.state;
    return(
      <Layout props={this.props} title="Reset Password" session={this.state.session}>
        { this.props.loggedIn ? (
          <div className="text-center"><h2>You are already logged in</h2></div>
        ) : (
          <div className="jumbotron col-lg-5 col-md-8 mx-auto text-center">
            <h3 className="display-5 mb-5">Reset your password</h3>
            { query.code === '' ? (
              <div>
                <p> --> forget password </p>
              </div>
            ) : (
              <div>
                { this.state.passwordReset ? (
                  <div>
                    <h4 className="display-6 font-weight-light mb-4 pb-4">
                      <i className="fa fa-check rounded-ico d-block mx-auto text-success md my-3"></i>
                      Your password is successfully updated. You may now <Link href="/login"><a>Log In</a></Link>
                    </h4>
                  </div>
                ) : (
                  <form onSubmit={this.handleSubmit}>
                    <div className="input-group mb-4">
                      <span className="input-group-addon">
                        <i className="fa fa-lock"></i>
                      </span>
                      <input
                        type="password"
                        className="form-control"
                        placeholder="New password"
                        autoFocus
                        name="newPassword"
                        value={this.state.newPassword}
                        onChange={this.handleInputChange} />
                    </div>
                    <div className="input-group mb-4">
                      <span className="input-group-addon">
                        <i className="fa fa-lock"></i>
                      </span>
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Retype new password"
                        name="confirmNewPassword"
                        value={this.state.confirmNewPassword}
                        onChange={this.handleInputChange} />
                    </div>
                    <button
                      type="submit"
                      disabled={this.state.loading}
                      className="btn btn-primary w-100">
                      { this.state.loading ? <span><i className="fa fa-circle-o-notch"></i>&nbsp;&nbsp;Loading...</span> : 'Reset my password' }
                    </button>
                  </form>
                ) }
              </div>
            ) }
          </div>
        ) }
        {this.notificationStack()}
      </Layout>
    );
  }
}