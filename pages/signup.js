import React from 'react';
import Scroll from 'react-scroll';
import Link from 'next/link';
import Router from 'next/router';
import ObjectAssign from 'object-assign';
import cn from 'classnames';

import Session from '../auth/session';
import Page from '../components/page';
import Layout from '../components/layout';

import ReactTooltip from 'react-tooltip';
import { MultiSelect } from 'react-selectize';

import FileUpload from '../components/file-upload';
import TransHolder from '../components/component-transition-placeholder';
import AccountData from '../api/account';
import LocationsData from '../api/locations';
import GroupsData from '../api/groups';

const animateScroll = Scroll.animateScroll;
const ScrollElement = Scroll.Element;
const scroller = Scroll.scroller;

// content
import Intro, { LegalNotice, ReferralsForm, ReferralSentConfirmation, NavigationLabels, Tooltips} from '../static/content/page-signup';

/**
 * helpers:
 * SignupNav ==> navigation
 * TermsAndConditions ==> text box
 * LogInInformationContent ==> LogInScreen main content 
 * SubscriptionPlanContent ==> SubscriptionPlanScreen main content
 * UserSettingsContent ==> UserSettingsScreen main content
 */

const SignupNavBar = ({activeScreen}) => (
  <ul className="mx-auto mb-5 col-md-11">
    <li className={activeScreen > 1 ? 'text-success' : 'text-primary'}>
      <span className="step-icon d-inline-block done-indicator"><i className="fa fa-lock"></i></span>
      {NavigationLabels.step1()}
    </li>
    <li className={activeScreen >= 2 ? activeScreen > 2 ? 'text-success' : 'text-primary' : 'text-muted'}>
      <span className="step-icon d-inline-block done-indicator"><i className="fa fa-usd"></i></span>
      {NavigationLabels.step2()}
    </li>
    <li className={activeScreen === 3 ? 'text-primary' : 'text-muted'}>
      <span className="step-icon d-inline-block progress-indicator"><i className="fa fa-user"></i></span>
      {NavigationLabels.step3()}
    </li> 
  </ul>
);

const SignupNav = ({activeScreen = 1}) => (
  <div className="signup-nav mb-5 pb-2">
    <h1 className="mx-auto mb-5 col-md-8">{ Intro.headline }</h1>

    <div className="mx-auto mb-5 pb-5 col-md-6">
      <h2 className="text-success">{ Intro.subHeadline }</h2>

      <p className="text-muted">{ Intro.description }</p>
    </div>
    <SignupNavBar activeScreen={activeScreen} />
  </div>
);
const TermsAndConditions = () => (
  <div className="py-4 mt-5 mx-auto col-md-8 text-muted">
    <p><small>By signing up you agree to the <Link href="/terms-of-use"><a>Terms of Use</a></Link> and <Link href="/privacy-notice"><a>Privacy Policy</a></Link> of TOY-CYCLE.ORG.</small></p>
    <p><small>{ LegalNotice.p2 }</small></p>
  </div>
);
const LogInInformationContent = ({appState, handleChange, handleSubmit}) => (
  <div className="col-md-8 mx-auto">
    <form onSubmit={handleSubmit} className="mb-5">
      <div className="card text-left mb-5">
        <div className="card-block">
          <div className="form-group">
            <label htmlFor="login-email">Email</label>
            <input type="email" className="form-control" id="login-email" onChange={handleChange} value={appState.email} name="email" />
          </div>
          <div className="form-group">
            <label htmlFor="login-password">Create Password</label>
            <input type="password" className="form-control" id="login-password" onChange={handleChange} value={appState.password} name="password" />
          </div>
        </div>
      </div>

      <button type="submit" className="btn btn-primary" disabled={appState.loading}>{ appState.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : 'Next Step' }</button>
    </form>
  </div>
);
const SubscriptionPlanContent = ({appState, handleSubmit}) => (
  <div className="subscriptions-wrap container col-md-7">
    
    <TransHolder ssr={appState.ssr}>
      <div className="row mb-5 pb-4">
        { appState.allSubscriptionPlans && appState.allSubscriptionPlans.data && appState.allSubscriptionPlans.data.map((plan, i) => <div key={i} className={cn({'col mb-4': true, 'active': plan.id === appState.subscription.plan})}>
          <div className="card text-center" onClick={handleSubmit.bind(null, plan.id, null)}>
            <div className="card-block text-muted">
              <h4 className="card-title mb-1">{ plan.title }</h4>
              <h6>{ plan.subtitle }</h6>
              <h3 className="my-4 text-primary">$ {plan.price}</h3>
              <hr />

              <p>{ plan.description }</p>
            </div>
            <button className={cn({
              'btn': true, 
              'btn-primary': appState.subscription.plan !== plan.id, 
              'btn-success': appState.subscription.plan === plan.id, 
              'rounded-0': true
              })}>Select Membership</button>
          </div>
        </div>) }

      </div>
    </TransHolder>

    { appState.subscription.plan &&
    <TransHolder ssr={false}>
      <ScrollElement name="payment-types">
        <h2 className="font-weight-light mb-5 pb-4">Select Payment Type</h2>
        <div className="row mb-5">
          <div className="col">
            <div className="card" onClick={handleSubmit.bind(null, appState.subscription.plan, 'stripe')}>
              <div className="card-block">Visa</div>
              <button className="btn btn-primary">Pay with Card</button>
            </div>
          </div>
          <div className="col">
            <div className="card" onClick={handleSubmit.bind(null, appState.subscription.plan, 'paypal')}>
              <div className="card-block">PayPal</div>
              <button className="btn btn-primary">Pay With PayPal</button>
            </div>
          </div>
        </div>
      </ScrollElement>
    </TransHolder>
    }

  </div>
);
const UserSettingsContent = ({appState, handleChange, handleFileDrop, handleSubmit}) => (
  <div>
    <TransHolder ssr={appState.ssr}>
      <form onSubmit={handleSubmit} className="container col-md-8 mx-auto">
        <div className="card mb-5">
          <div className="card-block text-left">
            <div className="form-group">
              <FileUpload handleFile={handleFileDrop} />
            </div>

            <div className="row">
              <div className="col-md-4 form-group">
                <label htmlFor="firstName">First Name</label>
                <input type="text" name="firstName" id="firstName" 
                  className="form-control"
                  value={appState.user.firstName} onChange={handleChange} />
              </div>
              <div className="col-md-4 form-group">
                <label htmlFor="lastName">Last Name</label>
                <input type="text" name="lastName" id="lastName" 
                  className="form-control"
                  value={appState.user.lastName} onChange={handleChange} />
              </div>
              <div className="col-md-4 form-group">
                <label htmlFor="displayName">Display Name/Alias</label>
                <input type="text" name="displayName" id="displayName" 
                  className="form-control"
                  value={appState.user.displayName} onChange={handleChange} />
              </div>
            </div>

            <div className="row">
              <div className="col-md-3 form-group">
                <label htmlFor="countryId">Country</label>
                <select 
                  name="countryId" 
                  value={appState.user.countryId} 
                  onChange={handleChange} 
                  className="form-control" 
                  id="countryId"
                  disabled={!appState.allCountries || !appState.allCountries.data}>
                  <option value="">Select...</option>
                  { 
                    appState.allCountries && appState.allCountries.data &&
                    appState.allCountries.data.map((country, i) => <option key={i} value={country.id}>{ country.name }</option>)
                  }
                </select>
              </div>
              <div className="col-md-3 form-group">
                <label htmlFor="stateId">State</label>
                <select 
                  name="stateId" 
                  value={appState.user.stateId} 
                  onChange={handleChange} 
                  disabled={appState.loadingLocations && !appState.allStates.data}
                  className="form-control" 
                  id="stateId">
                  <option value="">Select...</option>
                  {
                    appState.allStates && appState.allStates.data ?
                    appState.allStates.data.map((state, i) => <option key={i} value={state.id}>{ state.name }</option>) : 
                    <option disabled>Country must be selected...</option>
                  }
                </select>
              </div>
              <div className="col-md-3 form-group">
                <label htmlFor="zipCode">Zip Code</label>
                <select 
                  name="zipCode" 
                  value={appState.user.zipCode} 
                  onChange={handleChange} 
                  disabled={appState.loadingLocations}
                  className="form-control" id="zipCode">
                  <option value="">Select...</option>
                  { 
                    appState.allZips && appState.allZips.data ?
                    appState.allZips.data.map((zip, i) => <option key={i} value={zip.code}>{zip.code}</option>) : 
                    <option disabled>State must be selected</option>
                  }
                </select>
              </div>
              <div className="col-md-3 form-group">
                <label htmlFor="city">City</label>
                <input type="text" name="city" id="city" value={appState.user.city} onChange={handleChange} className="form-control" />
              </div>
            </div>
            {/* <div className="row">
              <div className="col form-group"></div>
            </div> */}

            <div className="row">
              <div className="col-md-6 form-group">
                <label htmlFor="pickupAddress">Toy/Game Pickup Street Address {Tooltips.pickupStreetAddress && <i className="fa fa-question-circle" aria-hidden="true" data-tip={Tooltips.pickupStreetAddress}></i>}</label>
                <input type="text" name="pickupAddress" id="pickupAddress"
                  value={appState.user.pickupAddress} onChange={handleChange} className="form-control" />
              </div>
              <div className="col-md-6 form-group">
                <label htmlFor="pickupWindow">Pickup Window {Tooltips.pickupWindow && <i className="fa fa-question-circle" aria-hidden="true" data-tip={Tooltips.pickupWindow}></i>}</label>
                <input type="text" name="pickupWindow" id="pickupWindow"
                  value={appState.user.pickupWindow} onChange={handleChange} className="form-control" />
              </div>
            </div>

            <div className="row">
              <div className="col-md-6 form-group">
                <label>Receive Notifications From Nearby Groups {Tooltips.nearbyLocalGroupNotifications && <i className="fa fa-question-circle" aria-hidden="true" data-tip={Tooltips.nearbyLocalGroupNotifications}></i>}</label>
                <label className="form-check-label d-block">
                  <input className="form-check-input" type="radio" name="nearbyNotifications" value="1" onChange={handleChange} checked={appState.user.nearbyNotifications == '1'} /> Receive
                </label>
                <label className="form-check-label d-block">
                  <input className="form-check-input" type="radio" name="nearbyNotifications" value="0" onChange={handleChange} checked={appState.user.nearbyNotifications == '0'} /> Do Not Receive
                </label>
              </div>
              <div className="col-md-6 form-group">
                <label>Notifications Type {Tooltips.notificationsType && <i className="fa fa-question-circle" aria-hidden="true" data-tip={Tooltips.notificationsType}></i>}</label>
                <label className="form-check-label d-block">
                  <input className="form-check-input" type="radio" name="notificationType" value="individual" onChange={handleChange} checked={appState.user.notificationType == 'individual'} /> Individual Posts
                </label>
                <label className="form-check-label d-block">
                  <input className="form-check-input" type="radio" name="notificationType" value="daily" onChange={handleChange} checked={appState.user.notificationType == 'daily'} /> Daily Digest
                </label>
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="pickupPlacement">General Toy/Game Placement For Pickup&nbsp;
                {Tooltips.generalPlacementForPickup && <i className="fa fa-question-circle" aria-hidden="true" data-tip={Tooltips.generalPlacementForPickup}></i>}</label>
              <textarea name="pickupPlacement" onChange={handleChange} className="form-control" value={appState.user.pickupPlacement}></textarea>
            </div>
          </div>
        </div>
        <button 
          type="submit" 
          className="btn btn-primary"
          disabled={appState.loading}
        >
          {appState.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : 'Complete Sign Up'}
        </button>
      </form>
      <ReactTooltip />
    </TransHolder>
  </div>
);

/**
 * screens:
 * 0. ask for user group by zip, save zip to the session
 * 1. log in information
 * 2. choose subscription Plan
 * 3. user settings
 * 4. frends referrer, signup complete thank you page
 * 5. referrals sent thank you page
 */
const UserGroupZipQueryScreen = ({appState, handleChange, handleSubmit}) => (<div>
  {/* <SignupNav activeScreen={appState.activeScreen} /> */}
  <div className="signup-nav mb-5 pb-2">
    <SignupNavBar activeScreen={appState.activeScreen} />
  </div>
  <div className="col-md-8 mx-auto">
    <form onSubmit={handleSubmit} className="mb-5">
      <div className="card text-left mb-5">
        <div className="card-block">
          <h6>Please, check if user group for your area exists </h6>
          <div className="form-group">
            {/* <label htmlFor="check-zip">Enter your area zip code</label> */}
            <input type="text" className="form-control" id="check-zip" onChange={handleChange} value={appState.userSearchZip} name="userSearchZip" placeholder="Enter your area zip code" />
          </div>
        </div>
      </div>

      <button type="submit" className="btn btn-primary" disabled={appState.loading}>{ appState.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : 'Next Step' }</button>
    </form>
  </div>
  <TermsAndConditions />
</div>);
const LogInInformationScreen = props => (
  <div>
    <SignupNav activeScreen={props.appState.activeScreen} />
    <LogInInformationContent {...props} />
    <TermsAndConditions />
  </div>
);
const SubscriptionPlanScreen = props => (
  <div>
    <SignupNav activeScreen={props.appState.activeScreen} />
    <SubscriptionPlanContent {...props} />
    <TermsAndConditions />
  </div>
);
const UserSettingsScreen = props => (
  <div>
    <SignupNav activeScreen={props.appState.activeScreen} />
    <UserSettingsContent {...props} />
    <TermsAndConditions />
  </div>
);
const SendReferralScreen = ({appState, handleChange, handleSubmit}) => (
  <div>
    <TransHolder ssr={appState.ssr}>
      <div className="mb-5">
        <h3 className="display-6 font-weight-light mb-5 pb-4 col-md-8 mx-auto">
          <i className="fa fa-check rounded-ico d-block mx-auto text-success md mb-3"></i>
          { ReferralsForm.signupCompleteNotice }
        </h3>

        <h2 className="font-weight-bold mb-5">{ ReferralsForm.headline }</h2>
        <h5 className="text-success">{ ReferralsForm.subHeadline }</h5>

        <p className="col-md-7 mx-auto mb-5 font-weight-light">{ ReferralsForm.description }</p>

        
        <form onSubmit={handleSubmit}>
          <div className="card col-md-8 mx-auto mb-5">
            <div className="card-block text-left">

              <div className="form-group referral-emails">
                <label>{ ReferralsForm.formInputEmailAddressesLabel }</label>
                <MultiSelect
                  delimiters={[188,9]}
                  renderOption={() => ''}
                  onValuesChange={(values) => {
                    let value = values.map(v => v.label).join(',');
                    handleChange({target: {name: 'emails', value}})
                  }}
                  valuesFromPaste={(options, values, pastedText) => {
                    return pastedText
                      .split(',')
                      .filter(txt => values.map(i => i.label).indexOf(txt.trim()) == -1)
                      .map(txt => ({label: txt.trim(), value: txt.trim()}));
                  }}
                  createFromSearch={(options, values, search) => {
                    let labels = values.map(value => value.label);
                    if (search.trim().length == 0 || labels.indexOf(search.trim()) != -1) return null;
                    return {label: search.trim(), value: search.trim()};
                  }}
                  theme="default"
                  renderNoResultsFound={() => ''}
                  renderToggleButton={() => ''} />
              </div>

              {/*<div className="form-group">
                <label>{ ReferralsForm.formInputEmailAddressesLabel }</label>
                <input className="form-control" />
              </div>*/}
              <div className="form-group">
                <label>{ ReferralsForm.formInputStatementLabel }</label>
                <textarea 
                  name="statement"
                  value={appState.referrals.statement} 
                  onChange={handleChange} className="form-control">
                </textarea>
              </div>
            </div>
          </div>

          <button 
            type="submit" 
            className="btn btn-primary"
            disabled={appState.loading}>
            { appState.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : ReferralsForm.formSubmitButtonLabel }
            </button>
        </form>
      </div>
    </TransHolder>
  </div>
);
const ThankYouScreen = props => (
  <div>
    <TransHolder ssr={props.appState.ssr}>
      <div className="col-md-4 mx-auto mb-5">
      <h3 className="display-6 font-weight-light mb-4 pb-4">
        <i className="fa fa-check rounded-ico d-block mx-auto text-success md mb-3"></i>
        { ReferralSentConfirmation.successNotice }
      </h3>
      <p className="mb-4 font-weight-light">{ ReferralSentConfirmation.description }</p>

      <Link href="/group?id=home-group" as="/group/home-group"><a className="btn btn-primary">{ ReferralSentConfirmation.homeGroupBtnLabel }</a></Link>
      </div>
    </TransHolder>
  </div>
);

export default class extends Page {

  static async getInitialProps(ctx) {
    let props = await super.getInitialProps(ctx);
    props.activeScreen = props.loggedIn ? props.session.user.data.regStep + 1 : 0;
    return props;
  }

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      email: '',
      password: '',
      subscription: {
        plan: null,
        type: null // payment type
      },
      user: {
        avatarFile: '',
        firstName: '',
        lastName: '',
        displayName: '',
        stateId: '',
        city: '',
        zipCode: '',
        pickupAddress: '',
        pickupWindow: '',
        nearbyNotifications: '1',
        notificationType: 'individual', // individual | daily
        pickupPlacement: '',
        countryId: '' // still not used by api
      },
      referrals: {
        emails: '',
        statement: ''
      },
      activeScreen: this.props.activeScreen,
      ssr: this.props.ssr,
      loading: false,
      allSubscriptionPlans: {data: null},
      allCountries: {data: null},
      allStates: {data: null},
      allZips: {data: null},
      loadingLocations: false,
      loader: false,
      userSearchZip: ''
    });

    this.handleChange = this.handleChange.bind(this);
    this.handleNestedChange = this.handleNestedChange.bind(this);
    this.handleFileDrop = this.handleFileDrop.bind(this);
    this.handleUpdateScreen = this.handleUpdateScreen.bind(this);
    this.getActiveScreen = this.getActiveScreen.bind(this);

    this.handleSubmitStep0 = this.handleSubmitStep0.bind(this);
    this.handleSubmitStep1 = this.handleSubmitStep1.bind(this);
    this.handleSubmitStep2 = this.handleSubmitStep2.bind(this);
    this.handleSubmitStep3 = this.handleSubmitStep3.bind(this);
    this.handleSubmitStep4 = this.handleSubmitStep4.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    this.setState({[target.name]: target.value});
  }
  handleNestedChange(prop, event) {
    const target = event.target;
    this.setState((prevState, props) => {
      const oldState = prevState[prop];
      let newState = prevState[prop];
      newState[target.name] = target.value;
      return {oldState: newState};
    }, async () => {
      if (target.name === 'countryId' && target.value !== '') {
        this.setState({
          loadingLocations: true,
          user: ObjectAssign({}, this.state.user, {stateId: '', zipCode: ''})
        }, async () => {
          let allStates = await LocationsData.listStates({pagin: '0', countryId: target.value});
          this.setState({allStates, loadingLocations: false});
        })
      } else if (target.name === 'stateId' && target.value !== '') {
        this.setState({
          loadingLocations: true,
          user: ObjectAssign({}, this.state.user, {zipCode: ''})
        }, async () => {
          let allZips = await LocationsData.listZips({pagin: '0', stateId: target.value});
          this.setState({allZips, loadingLocations: false});
        })
      }
    });
  }
  handleFileDrop(imgFile) {
    this.setState({user: ObjectAssign({}, this.state.user, {avatarFile: imgFile})});
  }

  handleSubmitStep0(e) {
    e.preventDefault();
    if (this.state.loading) return;
    if (this.state.userSearchZip === '') {
      return alert('Area zip code is required field');
    }
    this.setState({loading: true}, async () => {
      this.clearAllNotifications();

      // check if user group with the zip exists
      // if (exist) => update session; go to step 1
      // if (not exist) => redirect to group applicants page
      
      let groups = await GroupsData.getGroups({filterZip: this.state.userSearchZip});
      if (!groups.data || !groups.data.length) { // groups.data is the list of user groups
        Router.push('/no-group?zip=' + this.state.userSearchZip);
        return;
      }
      
      let sessionInstance = new Session();
      let updatedSessionData = await sessionInstance
        .updateTransientData('userSearchZip', this.state.userSearchZip);

      this.setState({
        loading: false,
        session: updatedSessionData
      }, this.handleUpdateScreen.bind(null, 1));
    });
  }

  handleSubmitStep1(e) {
    e.preventDefault();
    if (this.state.loading) return;
    if (this.state.email === '' || this.state.password === '') {
      return alert('Both email and password are required fields');
    }
    this.setState({loading: true});
    this.clearAllNotifications();

    const refCode = this.state.session.refCode || '';

    let sessionInstance = new Session();
    sessionInstance
      .signup(this.state.email, this.state.password, refCode)
      .then(session => {
        this.setState({loading: false});
        // if we session.user === null --> signup failed
        if (session.user === null) {
          this.addNotification({title: 'Error', message: 'Something went wrong. Please contact the administrator.', dismissAfter: false});
          return;
        }
        this.addNotification({title: 'Success', message: 'Please, select membership plan that best fits your needs.'});
        this.handleUpdateScreen(2);
      });
  }
  handleSubmitStep2(planID, typeID, e) {
    e.preventDefault();
    let approvalUrl = '';
    let updatedSubscriptionData = {
      plan: planID,
      type: typeID
    }
    this.setState({subscription: updatedSubscriptionData}, function() {

      // submit if conditions pass 
      if (!planID || !typeID) {
        scroller.scrollTo('payment-types', {
          duration: 600,
          delay: 150,
          smooth: true
        });
        return;
      }

      this.setState({loader: true});

      AccountData.signupStepComplete({
        regStep: 2,
        planId: this.state.subscription.plan,
        paymentType: this.state.subscription.type,
        _req_type_json: true
      })
        .then(response => {
          console.log(response);
          
          if (response.error) {
            this.setState({loading: false, loader: false});
            this.addNotification({title: 'Error', message: response.error.message});
            return;
          }

          // api should return `approvalUrl` param in the response 
          // user should be redirected to `approvalUrl`
          if (response.approvalUrl) {
            approvalUrl = response.approvalUrl;
          } else {
            return;
          }

          let sessionInstance = new Session();
          return sessionInstance.updateSession();
        })
        .then(response => {
          // session updated
          this.setState({loading: false, session: response && response.user ? response : this.state.session});
          // this.addNotification({title: 'Success', message: 'Settings saved successfully.'});
          console.log(`Step 2 submission: Membership - ${this.state.subscription.plan}; Payment Type - ${this.state.subscription.type}`);
          

          // now redirect user to approvalUrl
          if (typeof window !== 'undefined' && approvalUrl !== '') {
            window.location.assign(approvalUrl);
          } else {
            // @TODO: for beta
            // this.handleUpdateScreen(3);
            throw new Error('Approval Url not defined');
            
            this.setState({loader: false});
          }
          
          // temporary disable update to screen 3
          // this.handleUpdateScreen(3);
        })
        .catch(err => {
          console.log(err);
          this.setState({loading: false});
          this.addNotification({title: 'Error', message: err.name ? err.name : 'Something went wrong'});
        });
    });
  }
  handleSubmitStep3(e) {
    e.preventDefault();
    if (this.state.loading) return;
    this.setState({loading: true});
    this.clearAllNotifications();

    AccountData.signupStepComplete({
      ...this.state.user,
      regStep: 3
    })
      .then(response => {
        if (response.error) {
          this.setState({loading: false});
          this.addNotification({title: 'Error', message: response.error.message});
          throw new Error(response.error.message);
        }
        let sessionInstance = new Session();
        return sessionInstance.updateSession();
      })
      .then(response => {
        // session updated
        this.setState({loading: false, session: response && response.user ? response : this.state.session});
        this.addNotification({title: 'Success', message: 'Settings saved successfully.'});
        this.handleUpdateScreen(4);
      })
      .catch(err => {
        console.log(err);
        this.setState({loading: false});
      });
  }
  handleSubmitStep4(e) {
    e.preventDefault();
    if (this.state.loading) return;

    let { referrals } = this.state;
    
    const exit = () => {
      this.setState({loading: false})
    }

    if (referrals.emails.trim() === '' || referrals.statement.trim() === '') {
      this.addNotification({title: 'Both fields are requried'});
      return exit();
    }

    this.setState({loading: true});
    this.clearAllNotifications();

    AccountData.createReferrals(referrals)
      .then(res => {
        if (!res || res.error) {
          this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request Failed'});
          return exit();
        }
        this.addNotification({title: 'Success', message: res.message ? res.message : 'Referrals sent'});
        exit();
        this.handleUpdateScreen(5);
      });
  }

  handleUpdateScreen(screen) {
    this.setState({activeScreen: screen}, this.populateLocationFromSession.bind(this, screen));
    animateScroll.scrollToTop();
  }

  async populateLocationFromSession(screen) {
    if (screen !== 3) return;
    if (!this.state.session.transient || !this.state.session.transient.userSearchZip) return;

    // find zip ID 
    const searchZipsResponse = await LocationsData.listZips({filter: this.state.session.transient.userSearchZip});
    if (!searchZipsResponse || searchZipsResponse.error || !searchZipsResponse.data || !searchZipsResponse.data.length) return;

    const theZipID = searchZipsResponse.data[0].id;
    // get zip data
    const zipDataResponse = await LocationsData.getZip(theZipID);

    if (!zipDataResponse || zipDataResponse.error || !zipDataResponse.data) return;

    console.log(zipDataResponse.data);

    this.setState({loadingLocations: true}, async () => {

      // 1. grab states for countryId, and zips for state Id
      // 2. set state (user: {stateId, countryId, zipCode})

      const countryId = zipDataResponse.data.countryId;
      const stateId = zipDataResponse.data.stateId;
      const zipCode = zipDataResponse.data.code;

      const allStates = await LocationsData.listStates({pagin: '0', countryId});
      const allZips = await LocationsData.listZips({pagin: '0', stateId});

      this.setState({
        allZips,
        allStates,
        user: ObjectAssign({}, this.state.user, {countryId, stateId, zipCode}),
        loadingLocations: false
      });
    });
  }

  getActiveScreen() {
    switch (this.state.activeScreen) {
      case 1:
        return <LogInInformationScreen
        appState={this.state}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmitStep1} />;
      case 2:
        return <SubscriptionPlanScreen
          appState={this.state}
          handleSubmit={this.handleSubmitStep2} />;

      case 3:
        return <UserSettingsScreen
          appState={this.state}
          handleChange={this.handleNestedChange.bind(null, 'user')}
          handleFileDrop={this.handleFileDrop}
          handleSubmit={this.handleSubmitStep3} />;

      case 4:
        return <SendReferralScreen
          appState={this.state}
          handleChange={this.handleNestedChange.bind(null, 'referrals')}
          handleSubmit={this.handleSubmitStep4} />;

      case 5:
        return <ThankYouScreen appState={this.state} />;
    
      default: // 0. Check if user group exist
        return <UserGroupZipQueryScreen
          appState={this.state}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmitStep0} />
    }
  }

  async componentDidMount() {
    await super.componentDidMount();
    if (!this.state.allSubscriptionPlans.data) {
      this.setState({
        allSubscriptionPlans: await AccountData.getActiveSubscriptionPlans(),
        allCountries: await LocationsData.listCountries({pagin: '0'})
      });
    }

    if (this.props.loggedIn) {
      let activeScreen = 2; // this.state.session.user.data.regStep;
      
      if (this.state.session.user.data.paymentStatus === 'paid') {
        activeScreen = 3;
      }
      this.setState({activeScreen}, this.populateLocationFromSession.bind(this, activeScreen));
    }
  }

  render() {
    return(
      <Layout props={this.props} totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <div className={cn(['signup', 'jumbotron', 'text-center'], {'loading-sc': this.state.loader})}>
          { this.getActiveScreen() }
          <span className="loading-indicator" data-text="Loading...">Loading...</span>
        </div>
        { this.notificationStack() }
      </Layout>
    );
  }
}