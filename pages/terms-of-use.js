import React from 'react';
import Page from '../components/page';
import Layout from '../components/layout';
import TransHolder from '../components/component-transition-placeholder';

import Data from '../static/content/page-terms-of-use';

export default class extends Page {

  static async getInitialProps({req, pathname}) {
    let props = await super.getInitialProps({req, pathname});

    return props;
  }

  render() {
    return (
      <Layout props={this.props} title={Data.title} totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <TransHolder ssr={this.props.ssr}>
          <div className="jumbotron">
            <h1 className="mt-0 mb-3">{Data.title}</h1>
            <div dangerouslySetInnerHTML={{ __html: Data.content }}></div>
          </div>
        </TransHolder>
      </Layout>
    );
  }
}