import { Component } from 'react';
import Scroll from 'react-scroll'; 
import Link from 'next/link';
import Router from 'next/router';

import moment from 'moment';
import ObjectAssign from 'object-assign';
import cn from 'classnames';

import Page from '../components/page';
import Layout from '../components/layout';
import ToyEditForm, {ToyEditFormUncontrolled} from '../components/toy-post-form';
import config from '../static/config';

import { Modal, ModalBody, ModalFooter } from '../components/modal';
import TransHolder from '../components/component-transition-placeholder';
import { MapModal } from '../components/map-component';

import Posts, { MyPosts } from '../api/posts';
import { ageRangeLegend } from '../static/config';
import { Util } from '../api/util';

import MessagesComponent, {MessageAvatar} from '../components/my-account-components/widget-messages';
import { Messages } from '../api/account';

const Element = Scroll.Element;
const scroller = Scroll.scroller;

const defaultImg = config.defaultPostImage;

const mockupToy = {
  givenUserData: {
    id: 16,
    displayName: 'ImUser',
    avatarFile: 'https://dev.toy-cycle.org/api/images/avatar/16_avatar.jpg'
  }
}

export default class extends Page {

  constructor(props) {
    super(props);
    this.state = ObjectAssign({}, this.state, {
      toy: props.toy || {data: {}},
      messageContent: '',
      isOpenReportModal: false,
      isOpenDeleteModal: false,
      canEdit: false,
      isOpenEditModal: false,
      isOpenStatusModal: false,
      editToy: props.toy ? props.toy.data : null,
      loading: true,
      deleting: false,
      isMapOpen: false,
      reportMsg: '',
      toyStatus: null,
      toyTitleSubjectRef: '',
      reloadMessagesThread: false
    });
    this.handleEditFormFileDrop = this.handleEditFormFileDrop.bind(this);
    this.handleEditFormSubmit = this.handleEditFormSubmit.bind(this);
    this.handleReportAsInapropriate = this.handleReportAsInapropriate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

    this.handleCloseReportModal = this.handleCloseReportModal.bind(this);
    this.handleShowMap = this.handleShowMap.bind(this);
    this.updateStatus = this.updateStatus.bind(this);

    this.handleAdminDelete = this.handleAdminDelete.bind(this);
  }

  static async getInitialProps({req, pathname, query}) {
    let props = await super.getInitialProps({req, pathname});
    
    props.toyId = query.id;

    if (typeof window === 'undefined') {
      props.toy = await Posts.getPost(props.toyId);
    }

    // conversation id
    props.conversationId = query.conversation ? query.conversation : false;

    // page 
    props.page = query.page ? query.page : false;

    // section
    props.section = query.section ? query.section : false;

    return props;
  }

  async componentDidMount() {
    super.componentDidMount();
    const toy = await Posts.getPost(this.props.toyId);

    if (!toy || !toy.data) return false;
    
    const canEdit = this.props.loggedIn && (this.state.session.user.data.id === toy.data.userId || this.props.isAdmin);

    this.setState({
      toy,
      canEdit,
      editToy: toy.data,
      toyStatus: toy.data.status,
      loading: false
    });

    if (this.props.section) {
      scroller.scrollTo(this.props.section, {
        duration: 1500,
        smooth: true
      })
    }
  }

  componentDidUpdate(nextProps, nextState) {
    if (nextProps.conversationId != this.props.conversationId
      || nextProps.page != this.props.page
      || nextProps.section != this.props.section) {
      if (this.props.section) {
        scroller.scrollTo(this.props.section, {
          duration: 1500,
          smooth: true
        })
      }
    }
  }

  handleEditFormFileDrop(imageFile, src) {
    if (null === this.state.editToy) return;
    let data = ObjectAssign({}, this.state.editToy, {
      imageFile,
      imageSource: src
    });
    this.setState({editToy: data});
  }

  handleEditFormSubmit(e) {
    e.preventDefault();
    if (typeof this.toyEditForm === 'undefined' || !this.state.toy.data || !this.state.toy.data.id) return;

    this.clearAllNotifications();
    this.setState({loading: true});
    const editToy = ObjectAssign({}, this.state.editToy, Util.formToPlainObject(this.toyEditForm));

    // Make the request
    MyPosts.update(this.state.toy.data.id, editToy)
      .then(response => {
        if (response.message) {
          this.setState({
            toy: {data: ObjectAssign({}, editToy, {
              imageFile: editToy.imageSource || this.state.toy.data.imageFile
            })},
            isOpenEditModal: false
          }, () => this.addNotification({
            message: response.message
          }));
        } else if (response.error) {
          this.addNotification({
            title: 'Error',
            message: response.error.message,
            dismissAfter: 4500
          });
        } else {
          this.addNotification({
            title: 'Error',
            message: 'Something went wrong. Request failed. Please, try later.'
          });
        }
        this.setState({loading: false, editToy});
      });
  }

  handleDelete(e) {
    e.preventDefault();
    if (!this.props.loggedIn || !this.state.toy.data || !this.state.toy.data.id) return;

    this.clearAllNotifications();
    this.setState({deleting: true});

    // make the request
    MyPosts.delete(this.state.toy.data.id)
      .then(response => {
        this.setState({isOpenDeleteModal: false});
        if (response.message) {
          this.addNotification({message: `${response.message} You will soon be redirected to Home Group`, action: false});
          setTimeout(() => Router.push('/group?id=home-group', '/group/home-group'), 3500);
        } else if (response.error) {
          this.addNotification({
            title: 'Error',
            message: response.error.message,
            dismissAfter: 4500
          });
          this.setState({deleting: false});
        } else {
          this.addNotification({
            title: 'Error',
            message: 'Something went wrong. Request failed. Please, try later.'
          });
          this.setState({deleting: false})
        }
      });
  }

  handleAdminDelete(e) {
    e.preventDefault();
    if (!this.props.loggedIn || !this.state.toy.data || !this.state.toy.data.id) return;

    this.clearAllNotifications();
    this.setState({deleting: true});

    // make the request
    MyPosts.adminDeletePost(this.state.toy.data.id)
      .then(response => {
        this.setState({isOpenDeleteModal: false});
        if (response.message) {
          this.addNotification({message: `${response.message} You will soon be redirected to Browse groups`, action: false});
          setTimeout(() => Router.push('/group?id=home-group', '/group/home-group'), 3500);
        } else if (response.error) {
          this.addNotification({
            title: 'Error',
            message: response.error.message,
            dismissAfter: 4500
          });
          this.setState({deleting: false});
        } else {
          this.addNotification({
            title: 'Error',
            message: 'Something went wrong. Request failed. Please, try later.'
          });
          this.setState({deleting: false})
        }
      });
  }

  handleShowMap(e) {
    e.preventDefault();
    this.setState({isMapOpen: true});
  }

  handleReportAsInapropriate(e) {
    if (e) e.preventDefault();
    if (!this.reportMsgBox) return;

    this.clearAllNotifications();
    this.setState({loading: true});

    // make the requres
    MyPosts.reportPost({
      postId: this.state.toy.data.id,
      description: this.reportMsgBox.value
    })
      .then(response => {
        if (response.message) {
          this.setState({
            isOpenReportModal: false
          }, () => {
            this.addNotification({
              message: response.message
            });
          });
        } else if (response.error) {
          this.addNotification({
            title: 'Error',
            message: response.error.message,
            dismissAfter: 4500
          });
        } else {
          this.addNotification({
            title: 'Error',
            message: 'Something went wrong. Request failed. Please, try later.'
          });
        }
        this.setState({loading: false});
      })
      .catch(e => {
        this.addNotification({
          title: 'Error',
          message: 'Something went wrong. Request failed. Please, try later.'
        });
        this.setState({loading: false})
      });
  }

  handleCloseReportModal(e) {
    if (e) e.preventDefault();
    this.setState({
      isOpenReportModal: false
    })
  }

  updateStatus() {
    // if (this.state.toyStatus === this.state.toy.data.status) return;
    let body = {status: this.state.toyStatus};
    if (!this.props.isAdmin && this.state.givenUserId) {
      body.givenUserId = this.state.givenUserId;
    }

    this.setState({loading: true});
    this.clearAllNotifications();
    const exit = () => this.setState({loading: false});

    const updateCallback = res => {
      if (!res || res.error) {
        this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
        return exit();
      }
      Posts.getPost(this.state.toy.data.id)
        .then(res => {
          this.addNotification({title: 'Success', message: res.message ? res.message: 'Status sucessfully updated.'});
          this.setState({
            toy: {data: ObjectAssign({}, this.state.toy.data, {status: body.status}, res.data ? res.data : {})},
            loading: false,
            isOpenStatusModal: false,
            isOpenCompleteAdInFavorModal: false,
            reloadMessagesThread: true
          });
        });
    }


    if (this.props.isAdmin) {
      MyPosts.adminUpdateStatus(this.state.toy.data.id, body)
        .then(updateCallback);
    } else {
      MyPosts.updateStatus(this.state.toy.data.id, body)
        .then(updateCallback);
    }
  }

  completeAdInFavor(givenUserId, givenUserDisplayName) {
    this.setState({
      completeAdInFavorTemp: {givenUserDisplayName, givenUserId},
    }, () => this.setState({isOpenCompleteAdInFavorModal: true}));
  }
  handleCompleteAdInFavor() {
    if (!this.state.completeAdInFavorTemp) throw new Error('CompleteAdInFavorTemp not reachable');
    const { givenUserId } = this.state.completeAdInFavorTemp;
    if (!givenUserId) throw new Error('givenUserId not reachable');

    if (this.state.toy.data.status === 'completed') throw new Error('Ad is already completed');

    this.setState({
      toyStatus: 'active',
      loading: true,
      givenUserId
    }, () => {
      Messages.createMessage(this.state.toy.data.id, givenUserId, {
        subject: this.state.toy.data.title,
        content: `Message: Congratulations! You have been selected to receive “${this.state.toy.data.title}”. You can pick up this item at “${this.state.toy.data.pickupAddress}”. The pick up time has been designated as “${this.state.toy.data.pickupWindow}”. Where you can find the item: “${this.state.toy.data.pickupPlacement}”.<br /><small><em>If the pick up time or placement have not been specifically stated, please contact the posting member to coordinate pick up.</em></small><br /><hr />${this.state.messageContent}`
      })
        .then(res => {
          if (!res || res.error) {
            this.addNotification({title: 'Error', message: res.error ? res.error.message : 'Request failed'});
            this.setState({loading: false, isOpenCompleteAdInFavorModal: false});
          } else {
            this.updateStatus()
            this.updateMessageContent('');
          }
        });
    });
  }

  updateMessageContent(messageContent) {
    this.setState({
      messageContent
    });
  }

  render() {
    const toyExists = this.state.toy && typeof this.state.toy === 'object' && this.state.toy.data;
    let toy;

    if (toyExists) {
      toy = this.state.toy.data;
      if (toy.imageFile === '') {
        toy.imageFile = defaultImg;
      }
    }

    // does current user have groupId defined
    let userGroup;

    if (this.props.loggedIn) {
      userGroup = this.state.session.user && this.state.session.user.data.groupId;
    }

    return (
      <Layout props={this.props} title={toyExists ? toy.title : 'Toy-Exchange.org'} totalUnreadMessages={this.state.totalUnreadMessages} session={this.state.session}>
        <TransHolder ssr={this.props.ssr}>
          { toyExists && toy && (
            <div className="jumbotron">
              <div className="clearfix">
                <div className="float-left">
                  <h1 className="text-capitalize protruding-icon-holder">
                    <Link href="/group?id=home-group" as="/group/home-group"><a className="protruding-icon fa fa-caret-left rounded-ico md align-middle mr-0"></a></Link>{ toy.title }
                  </h1>
                  <p className="text-muted">by { toy.userData && toy.userData.displayName } | { moment(toy.updatedAt).format('LL') }</p>
                </div>

                { this.props.loggedIn && (
                  <div className="float-md-right">

                    {/* EDIT BLOCK */}
                    { this.state.canEdit && (
                      <div className="d-inline-block mr-2">
                        <button disabled={this.state.deleting} type="button" onClick={(e) => {e.preventDefault(); this.setState({isOpenEditModal: true})}} className="btn btn-primary r-ico text-uppercase font-weight-bold mb-4 text-white">Edit&nbsp;&nbsp;<i className="fa fa-pencil"></i></button>

                        {/* edit modal */}
                        <Modal
                          isOpen={this.state.isOpenEditModal}
                          onClose={() => this.setState({isOpenEditModal: false})}
                          modalTitle="Edit Post"
                          sizeClass="modal-lg"
                          >
                          <ModalBody>
                            <ToyEditFormUncontrolled
                              mainComponent={this}
                              defaults={this.state.editToy}
                              handleFileDrop={this.handleEditFormFileDrop}
                            />
                          </ModalBody>
                          <ModalFooter>
                            <button type="button" className="btn btn-danger" onClick={this.handleEditFormSubmit} disabled={this.state.loading}>{ this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Loading...</span> : 'Save' }</button>
                            <button type="button" className="btn btn-secondary" onClick={() => this.setState({isOpenEditModal: false})}>Cancel</button>
                          </ModalFooter>
                        </Modal>

                        {/* delete modal */}
                        <Modal
                          isOpen={this.state.isOpenDeleteModal}
                          onClose={() => this.setState({isOpenDeleteModal: false})}
                          modalTitle="Confirm"
                          >
                          <ModalBody><p className="text-left">Are you Sure you want to delete this post?</p></ModalBody>
                          <ModalFooter>
                            <button type="button" disabled={this.state.deleting} className="btn btn-danger" onClick={this.handleDelete}>{ this.state.deleting ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Deleting...</span> : 'Delete' }</button>
                            <button type="button" disabled={this.state.deleting} className="btn btn-secondary" onClick={() => this.setState({isOpenDeleteModal: false})}>Cancel</button>
                          </ModalFooter>
                        </Modal>

                        {/* status modal */}
                        <Modal
                          isOpen={this.state.isOpenStatusModal}
                          onClose={() => this.setState({isOpenStatusModal: false})}
                          modalTitle="Update ad status"
                          sizeClass="modal-sm">
                          <ModalBody>
                            <div className="custom-controls-stacked">
                              {/*active*/}
                              <label className="custom-control custom-radio">
                                <input 
                                  id="s-active" name="status" 
                                  type="radio" value="active" 
                                  className="custom-control-input"
                                  checked={this.state.toyStatus === 'active'}
                                  onChange={e => e.target.checked && this.setState({toyStatus: 'active'})} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Active</span>
                              </label>
                              {/*canceled*/}
                              <label className="custom-control custom-radio">
                                <input 
                                  id="s-canceled" name="status" 
                                  type="radio" value="canceled" 
                                  className="custom-control-input"
                                  checked={this.state.toyStatus === 'canceled'}
                                  onChange={e => e.target.checked && this.setState({toyStatus: 'canceled'})} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Canceled</span>
                              </label>
                              {/*completed*/}
                              <label className="custom-control custom-radio">
                                <input 
                                  id="s-completed" 
                                  name="status" type="radio" 
                                  value="completed" 
                                  className="custom-control-input"
                                  checked={this.state.toyStatus === 'completed'}
                                  onChange={e => e.target.checked && this.setState({toyStatus: 'completed'})} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Completed</span>
                              </label>

                              { this.props.isAdmin && (<div>
                                {/* suspended */}
                                <label className="custom-control custom-radio">
                                  <input 
                                    id="s-suspended" 
                                    name="status" type="radio" 
                                    value="suspended" 
                                    className="custom-control-input"
                                    checked={this.state.toyStatus === 'suspended'}
                                    onChange={e => e.target.checked && this.setState({toyStatus: 'suspended'})} />
                                  <span className="custom-control-indicator"></span>
                                  <span className="custom-control-description">Suspended</span>
                                </label><br />
                                {/* expired */}
                                <label className="custom-control custom-radio">
                                  <input 
                                    id="s-expired" 
                                    name="status" type="radio" 
                                    value="expired" 
                                    className="custom-control-input"
                                    checked={this.state.toyStatus === 'expired'}
                                    onChange={e => e.target.checked && this.setState({toyStatus: 'expired'})} />
                                  <span className="custom-control-indicator"></span>
                                  <span className="custom-control-description">Expired</span>
                                </label>
                              </div>) }
                            </div>
                          </ModalBody>
                          <ModalFooter>
                            <button type="button" disabled={this.state.loading || this.state.deleting || this.state.toyStatus === toy.status} className="btn btn-danger" onClick={this.updateStatus}>
                              { this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;Loading...</span> : 'Update status' }
                            </button>
                            <button type="button" disabled={this.state.loading} className="btn btn-secondary" onClick={() => this.setState({isOpenStatusModal: false})}>Cancel</button>
                          </ModalFooter>
                        </Modal>
                      </div>
                    ) }

                    {/* REPLY BLOCK */}
                    { userGroup && !this.state.canEdit && !this.props.isAdmin && <button type="button" className="btn btn-primary r-ico text-uppercase font-weight-bold mb-4 text-white" onClick={e => this.setState({toyTitleSubjectRef: toy.title})}>Reply to this post&nbsp;&nbsp;<i className="fa fa-paper-plane"></i></button> }

                    {/* REPORT AS INAPRPROPRIATE BLOCK */}
                    { userGroup && !this.state.canEdit && !this.props.isAdmin && <div>
                        <p><a href="#" onClick={(e) => {e.preventDefault(); this.setState({isOpenReportModal: true})}} className="text-muted px-3"><small>Report as Inappropriate&nbsp;&nbsp;<i className="fa fa-flag"></i></small></a></p>

                        <Modal
                          isOpen={this.state.isOpenReportModal}
                          onClose={this.handleCloseReportModal} 
                          modalTitle="Report as Inappropriate"
                        >
                          <ModalBody>
                            <div className="form-group">
                              <label htmlFor="reportMsg">Report Message</label>
                              <textarea 
                                className="form-control" 
                                id="reportMsg" 
                                rows="3" 
                                name="description"
                                defaultValue={this.state.reportMsg}
                                ref={(elem) => {this.reportMsgBox = elem;}}>
                              </textarea>
                            </div>
                          </ModalBody>
                          <ModalFooter>
                            <button 
                              type="button" className="btn btn-danger"
                              onClick={this.handleReportAsInapropriate}>
                              Submit Report
                            </button>
                            <button 
                              type="button" className="btn btn-secondary" 
                              onClick={this.handleCloseReportModal}>
                              Cancel
                            </button>
                          </ModalFooter>
                        </Modal>
                    </div> }

                  </div>
                ) }
              </div>

              <div className="row my-5">
                <div className={cn(['col-lg-7', 'col-md-8'], {closedAd: !this.state.loading && toy.status !== 'active'})}>

                  { this.props.loggedIn && (
                    <ul className="toy-props">
                      <li><i className="fa fa-bullhorn"></i>&nbsp;<span className="font-weight-bold text-uppercase">{ toy.postType }</span></li>
                      <li><i className="fa fa-child"></i>&nbsp;<span className="font-weight-bold text-uppercase">{ ageRangeLegend[toy.ageRange] }</span></li>
                      { toy.postType !== 'wanted' && <li>
                        <i className="fa fa-map-marker"></i>&nbsp;<span className="font-weight-bold">Location: </span> { toy.userData && toy.userData.zipCode } (<a href="#" onClick={this.handleShowMap}>Show Map</a>)

                        { toy.userData && toy.userData.zipCode && <MapModal
                          address={`${toy.pickupAddress} ${toy.userData.zipCode}`}
                          isOpen={this.state.isMapOpen}
                          onClose={() => this.setState({isMapOpen: false})} /> }
                      </li> }
                      { toy.postType !== 'wanted' && <li><i className="fa fa-calendar"></i>&nbsp;<span className="font-weight-bold">Pick Up Window: </span>{ toy.pickupWindow }</li> }

                      { toy.postType !== 'wanted' && this.state.canEdit && toy.givenUserData && toy.givenUserData.id && <li>
                        <MessageAvatar imgSrc={toy.givenUserData.avatarFile} className="given-user-data-avatar" />&nbsp;<span className="font-weight-bold">Selected user: </span> {toy.givenUserData.displayName}
                      </li> }

                      { this.props.isAdmin && <li>
                        <Link href={`/admin/reported-posts?toyId=${toy.id}`}><a><i className="fa fa-exclamation-triangle"></i>&nbsp;Manage Reports</a></Link>
                      </li> }

                      { this.props.isAdmin && <li>
                        <button onClick={e => this.setState({isOpenAdminDeleteModal: true})} className="btn btn-secondary mt-2"><i className="fa fa-trash"></i>&nbsp;Delete Post</button>
                      </li> }
                    </ul>
                  ) }

                  {/* Admin delete modal */}
                  { this.props.isAdmin && <Modal
                    isOpen={this.state.isOpenAdminDeleteModal}
                    onClose={() => this.setState({isOpenAdminDeleteModal: false})}
                    modalTitle="Confirm"
                    >
                    <ModalBody><p className="text-left">Are you Sure you want to delete this post?</p></ModalBody>
                    <ModalFooter>
                      <button type="button" disabled={this.state.deleting} className="btn btn-danger" onClick={this.handleAdminDelete}>{ this.state.deleting ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Deleting...</span> : 'Delete' }</button>
                      <button type="button" disabled={this.state.deleting} className="btn btn-secondary" onClick={() => this.setState({isOpenAdminDeleteModal: false})}>Cancel</button>
                    </ModalFooter>
                  </Modal> }

                  {/* Complete Ad in Favor modal */}
                  { this.state.completeAdInFavorTemp && <Modal
                    isOpen={this.state.isOpenCompleteAdInFavorModal}
                    onClose={() => this.setState({isOpenCompleteAdInFavorModal: false})}
                    modalTitle="Confirm">
                    <ModalBody>
                      <p>Are you sure you want to select user {this.state.completeAdInFavorTemp.givenUserDisplayName} as a match for this ad?</p>
                    </ModalBody>
                    <ModalFooter>
                      <button type="button" disabled={this.state.loading} className="btn btn-danger" onClick={this.handleCompleteAdInFavor.bind(this)}>
                        {this.state.loading ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Updating...</span> : 'Complete'}
                      </button>
                      <button type="button" disabled={this.state.loading} className="btn btn-secondary" onClick={() => this.setState({isOpenCompleteAdInFavorModal: false})}>Cancel</button>
                    </ModalFooter>
                  </Modal> }

                  <div className={this.props.loggedIn ? 'desc mb-5' : 'desc line-clamp'}><p>{ this.props.loggedIn ? toy.description : toy.excerptSeo }</p></div>

                  { 
                    this.state.canEdit && <ul className="nav">
                      <li className="nav-item">
                        <a href="#" 
                          className="nav-link pl-0" 
                          disabled={this.state.deleting}
                          onClick={(e) => {e.preventDefault(); this.setState({isOpenEditModal: true})}}
                        >
                          Edit Post
                        </a>
                      </li>
                      <li className="nav-item">
                        <a href="#" 
                          className="nav-link" 
                          disabled={this.state.deleting}
                          onClick={(e) => {e.preventDefault(); this.setState({isOpenDeleteModal: true})}}
                        >
                          { this.state.deleting ? <span><i className="fa fa-circle-o-notch fa-spin"></i>&nbsp;&nbsp;Deleting...</span> : 'Delete' }
                        </a>
                      </li>
                      { (this.props.isAdmin || ['suspended', 'expired'].indexOf(toy.status) === -1 ) && (<li>
                        <a href="#" 
                          className="nav-link"
                          disabled={this.state.loading || this.state.deleting}
                          onClick={e => {e.preventDefault(); this.setState({isOpenStatusModal: true})}}>Update Status ({toy.status})</a>
                      </li>) }
                    </ul>
                  }

                  { !this.props.loggedIn && (
                    <div className="jumbotron bg-faded row py-4 px-0 mt-5">
                      <h6 className="col-md-7">To connect with { toy.userData && toy.userData.displayName }, sign up for Toy-Exchange today.</h6>
                      <div className="col-md-5 text-md-right">
                        <Link href="/signup"><a className="btn btn-primary mr-3">Sign Up</a></Link>
                        <Link href="/login"><a className="btn btn-secondary">Log In</a></Link>
                      </div>
                    </div>
                  ) }
                </div>
                <div className="col-lg-5 col-md-4">
                  <img className="w-100 rounded" src={ toy.imageFile } alt={ toy.title } style={{opacity: toy.status !== 'active' ? 0.55 : 1}} />
                </div>
              </div>
              <Element name="reply">
              { this.props.loggedIn && toy && toy.id && this.state.session.user.data.groupId && <MessagesComponent
                  postId={toy.id}
                  postSlug={toy.slug}
                  postStatus={toy.status}
                  conversationId={this.state.canEdit ? this.props.conversationId : toy.userId}
                  currAvatar={this.state.session.user.data.avatarFile}
                  toy={toy}
                  page={this.props.page}
                  handleNotify={notificaton => this.addNotification(notificaton)}
                  updateSubjectRef={this.state.toyTitleSubjectRef}
                  handleCompleteFavor={this.completeAdInFavor.bind(this)}
                  messageContent={this.state.messageContent}
                  handleMessageContent={this.updateMessageContent.bind(this)}
                  reloadMessagesThread={this.state.reloadMessagesThread} /> }
              </Element>
            </div>
          ) }

          { !toyExists && !this.state.loading && (<h1>Not found</h1>) }
        </TransHolder>

        { this.notificationStack() }
        <style jsx>{`
          .desc {white-space: pre-wrap;}
          .line-clamp {
            position: relative;
            overflow:hidden;
          }
          .line-clamp:after {
            content: '';
            position: absolute; bottom: 0; right: 0;
            width: 100%; height: 4.2em;
            text-align: right;
            background: linear-gradient(to bottom, rgba(255, 255, 255, 0), rgba(255, 255, 255, 1) 100%);
          }
          .closedAd {
            background-image: url(${config.defaultClosedAdImage});
            background-position: center center; background-repeat: no-repeat;
            background-size: 80% auto;
          }
        `}</style>
      </Layout>
    );
  }
}