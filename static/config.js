const API_URI = 'https://toy-cycle.org/api';
const clientId = 2;
const _GA_KEY = 'UA-85765211-1';
const reloadUserDataRequestParam = 'mrp';

const ageRangeLegend = {
  '1': 'Age 0-3',
  '2': 'Age 4-8',
  '3': 'Age 9-up'
};

const _GAPI_KEY = 'AIzaSyAE89YhxNGE-kQnp-yWt1Mme2ljWNKlWHI';
const _GAPI_URI = 'https://maps.googleapis.com/maps/api';

const defaultPostImage = '/static/img/default.png';
const defaultAvatarImage = '/static/img/default-avatar.png';
const defaultClosedAdImage = '/static/img/closed-stamp.png';

module.exports = {
  API_URI,
  clientId,
  ageRangeLegend,
  defaultPostImage,
  defaultAvatarImage,
  defaultClosedAdImage,
  _GAPI_KEY,
  _GAPI_URI,
  _GA_KEY, // google analytics

  reloadUserDataRequestParam
}