export default {
  title: 'About Toy-Exchange.org',
  content: `<p><strong>Toy-Exchange.org</strong> is an online community dedicated to the free exchange of used toys and games. Much like <i>Freecycle</i>, you can post offers of free used toys and games for other members. You can also post wanted items, and you can receive toys and games offered by others.</p>

  <p>The community is organized into local user groups. Each group is geographically small enough that you won't have to travel far to pick up a toy or game. There is no shipping with <em>Toy-Exchange.org</em>. All posts in your home group will be within about a 5-mile radius, depending on your location. You may select to receive posts and notifications from nearby groups. This will increase the volume of used toys and games to choose from, but you may have to travel farther to get them.</p>

  <p>When you register with <em>Toy-Exchange.org</em>, you will receive a free trial period, after which you will be asked to pay a $12 annual membership fee. There is no charge for giving or getting used toys and games.</p>

  <p>Once registered, you can go to your <i>Settings Page</i> to create default settings for your experience on the site. You can select a default <i>Pick Up Address</i>, <i>Pick Up Window</i> and <i>Pick Up Placement</i>. This information will then autofill when you create posts. You can also choose to fill these out manually each time you create a new post. Your <i>Pick Up Address</i> will only be seen by the member you select to receive your item. Please read the <a href="/posting-guidelines">Posting Guidelines</a> for detailed information on how to create posts on our site.</p>

  <p>You will also be able to select to receive individual notifications or a daily digest. Every time a member posts a toy or game in your home group – or in a nearby group if you have selected to receive notifications from nearby groups – an email notification with the <i>Post Title</i>, a photo (if submitted) of the item and a link back to the post will go out to group members.</p>

  <p>If you want the item, you can follow the link and reply to the posting member that you are interested.</p>
  
  <p>The offering member will select from among respondents. If you are selected to receive the item, you will receive an email with the <i>Pick Up Address</i>, the <i>Pick Up Window</i> – time that the item will be available, for example 10:00-12:00 Saturday–and <i>Pick Up Placement</i>, for example Front Porch.  If the posting member has not indicated a specific <i>Pick Up Window</i>, but has instead stated something like, "Flexible," or "Email me," you will need to coordinate a pick up time with them.</p>
  
  <p>You can communicate further with the posting member via the item post on the site. Your communications will be preserved there in a thread for your reference.</p>
  
  <p>If you have any questions or concerns, you can contact the <em>Toy-cycle Team</em> <a target="_blank" href="mailto:admin@toy-cycle.org">here</a>.</p>
  
  <p>The <em>Toy-cycle.org</em> community was launched in July, 2017 and is continually growing. In order to accommodate this continual growth, we've developed a process that allows you to plug in immediately even before a group has launched in your area.  Simply register with your email address and zip code now, and you will be notified the minute a group is up and running.  You can help jump-start the process, and earn free time on your membership, by referring friends to <em>Toy-cycle</em>.</p>
  
  <p>The annual $12 membership fee allows us to operate and grow our community without relying on corporate sponsors or advertisers. We do, however, want <em>Toy-cycle</em> to be inclusive and accessible to all. If you find the fee to be a financial burden, please contact us <a target="_blank" href="mailto:admin@toy-cycle.org">here</a> to request a fee waiver. Or if you are an educator in a public school, or employed in a non-profit agency with a child-centered mission, please contact the Toy-cycle team <a target="_blank" href="mailto:admin@toy-cycle.org">here</a> to request a free membership.</p>
  
  
  <p><strong>You can find more information and answers to frequently asked questions <a href="/faq">here</a>.</strong></p>`
};