export default {
  title: 'Contact Us',
  content: `<p>
  Hi! Have questions or feedback? We would love to hear from you! Please contact us <a href="mailto:info@toy-cycle.org" target="_blank" rel="noopener noreferrer">here</a>. Experiencing technical difficulties? We’re so sorry, please let us know <a href="mailto:admin@toy-cycle.org" target="_blank" rel="noopener noreferrer">here</a>. We will do our best to get back to you within 24 hours.
  </p>`
}