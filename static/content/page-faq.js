export default {
  title: 'Frequently Asked Questions',
  content: `<div>
    <h3 class="question">What types of toys/games are accepted on Toy-cycle?</h3>
    <p class="answer">You can post all types of used toys and games on Toy-cycle. Toys like blocks or stuffies for infants and young children, or games for all ages, like scrabble or jigsaw puzzles.  Children's books, musical instruments, sports gear are all allowed. Other kid/baby gear though is not allowed. Just toys and games.</p>
  </div>

  <div>
    <h3 class="question">Can I give away other kids gear on Toy-cycle?</h3>
    <p class="answer">No, just toys and games.</p>
  </div>

  <div>
    <h3 class="question">Can I announce a yard sale on Toy-cycle?</h3>
    <p class="answer">No, Toy-cycle is designed exclusively for the free exchange of used toys and games between members.</p>
  </div>

  <div>
    <h3 class="question">What is a curb alert and why don't you accept them on Toy-cycle?</h3>
    <p class="answer">A curb alert is an announcement that items have already been placed outside at a certain address for anyone to come pick up if they want. In our experience, curb alert announcements can result in wasted trips and disappointment for some members who might arrive after the items are already taken. With Toy-cycle members make specific arrangements to gift or receive a given toy or game from other members, so that each can feel confident that the item(s) will be available when and where they are expected.</p>
  </div> 

  <div>
    <h3 class="question">How many local user groups are there?</h3>
    <p class="answer">Because Toy-cycle is continuously growing, on any given day the number of groups may be  different from the day before. Toy-cycle launched in July, 2017 with seven local groups in the San Francisco Bay Area.</p>
  </div> 

  <div>
    <h3 class="question">Are there local groups in every state?</h3>
    <p class="answer">As of July, 2017 there are only local groups in California. But the community is continually growing, with local groups being added frequently.</p>
  </div> 

  <div>
    <h3 class="question">Are there local groups in other countries?</h3>
    <p class="answer">Currently, there are only groups in the U.S., but the Toy-cycle team anticipates launching in Canada soon.</p>
  </div> 

  <div>
    <h3 class="question">What if no one wants the toy/game I am offering?</h3>
    <p class="answer">If no other member responds to your post, you can wait a brief time and post again, up to two times. If at that point you get no response, please search for another avenue for giving away your item, such as Craigslist, Freecycle or a local thrift shop.</p>
  </div> 

  <div>
    <h3 class="question">What if lots of people want the toy I am offering?</h3>
    <p class="answer">You may get many responses to your toy/game post. You are free to choose from among them who you would like to gift your item to. Please see the <a href="/posting-guidelines">Posting Guidelines</a> for further suggestions on selecting a respondent.</p>
  </div> 

  <div>
    <h3 class="question">What if my local group doesn't have many members or toys/games being offered?</h3>
    <p class="answer">You can select to Receive Notifications from Nearby Groups in your user settings.</p>
  </div> 

  <div>
    <h3 class="question">Why do I see posts from other groups on my Home Group Page when I haven't selected to Receive Notifications from Nearby Groups?</h3>
    <p class="answer">Posts from nearby groups will always appear on your Home Group Page, but when you have chosen not to receive notifications from nearby groups, you will not receive email notifications for those items.</p>
  </div> 

  <div>
    <h3 class="question">What does Pick-up Placement mean?</h3>
    <p class="answer">Pick-up Placement is a location at your home where you will place a posted item for pick up. Examples include Front porch, Side porch, Inside gate, etc.</p>
  </div> 

  <div>
    <h3 class="question">What is a Pick-up Window?</h3>
    <p class="answer">The Pick-up Window is the time frame during which you will make the posted item available to the member you have selected to receive it. For example, 9:00-11:00am Saturday. You may also choose to indicate Flexible, or Email Me if you would like to coordinate a time with the receiving member.</p>
  </div>
  
  <div>
    <h3 class="question">Can everyone see my name and address?</h3>
    <p class="answer">Only the members you select to receive your items can see your address, so that they can pick the items up from you.  Your Display Name is visible to all members.</p>
  </div>

  <p class="pl-30"><strong>If you have other questions not answered here, please contact the Toy-cycle team <a target="_blank" href="mailto:info@toy-cycle.org">here</a>.</strong></p>
  `
}


{/* 
<div>
  <h3 class="question"></h3>
  <p class="answer"></p>
</div> 
*/}