export default {
  metaPageTitle: 'Welcome',
  introHeadline: 'Exchange free used toys and games in your own community!',
  introSubHeadline: 'No packaging, no shipping, no money changing hands',
  introInputPlaceholder: 'Enter your Zip',
  introButtonLabel: 'Find My Group'
};

export const SlideshowImages = [
  '/static/img/home-slideshow/autos.jpg',
  '/static/img/home-slideshow/badminton.jpg',
  // '/static/img/home-slideshow/bag-toy.jpg',
  '/static/img/home-slideshow/books-cup-reframe.jpg',
  '/static/img/home-slideshow/chess.jpg',
  '/static/img/home-slideshow/guitar.jpg',
  // '/static/img/home-slideshow/lego.jpg',
  '/static/img/home-slideshow/music.jpg',
  // '/static/img/home-slideshow/marbles.jpg',
  '/static/img/home-slideshow/pexels.jpg',
  // '/static/img/home-slideshow/night.jpg',
  // '/static/img/home-slideshow/pexels2photo.jpg',
  '/static/img/home-slideshow/teddy.jpg',
];

export const VideoData = {
  videoId: '187558479',
  videoThumb: '/static/img/video-thumb.png'
};

export const Features = [
  {
    title: 'Free Yourself from the Tyranny of Toys',
    imgSrc: '/static/img/home-features/free-yourself.png',
    description: '<p>It’s a sunny Saturday afternoon, a beautiful day to be outside. But you are working diligently to clear the clutter of toys from the living room floor, your kid&rsquo;s bedroom and the hall closet. You locate toys your child hasn&rsquo;t played with since kindergarten. Never mind she is now in 3rd grade. What can you do? Sign up and offer those toys to your local TOY-CYCLE.ORG group. A nearby family wants them.</p><p>Now, it’s the following Saturday and you see an offer for a toy or game your kid would love.  It turns out the offering family lives just eight blocks from you!  You go pick it up and bring it home.  No shipping, no packaging, no money changing hands.</p>'
  },
  {
    title: 'Toys and Our Environment',
    imgSrc: '/static/img/home-features/toys-and-our-environment.png',
    description: '<p>Americans spend more than $20 billion annually on toys. And new toy packaging typically has a low recycle rate because the plastics used are not included in many municipal recycling programs. All that packaging uses precious resources and has a tremendous ecological impact.</p>'
  },
  {
    title: 'Try It Out',
    imgSrc: '/static/img/home-features/try-it-out.png',
    description: '<p>When you sign up for TOY-CYCLE.ORG, you can try out the site for the first 60 days free.  If it works for you, you&rsquo;ll be asked to pay a $12 annual membership fee to help defray the costs of the site&rsquo;s operation. That&rsquo;s it.  $12 a year. There is no charge for exchanging toys and games.  Your home group will include families within a reasonable distance of where you live.  You can also sign up to get notifications from other nearby groups if you choose.</p>'
  }
];

export const CallToActionWidget = {
  buttonLabel: 'Sign Up & Get A 60-day Free Trial',
  description: '<p>Toy-cycle is an online community dedicated to the free exchange of used toys and games.<br />Become a member and start exchanging free used toys and games today!</p>'
};