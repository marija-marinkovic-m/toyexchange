export default {
  headline: 'Welcome and congratulations!',
  paragraph: `
  <h4 class="font-weight-light text-muted col-md-10 mx-auto">You are among the first to show interest in a user group in your location.</h4>
  <h4>Join now and receive a free 6-month membership*</h4>
  <p class="font-weight-light mb-5 text-muted col-md-10 mx-auto">When we have enough members, we'll notify you that your local group is active and ready for exchanging free used toys and games.  Your free membership will begin at that time.<br><small>*Limited number</small></p>
  `,
  formBtnLabel: 'Notify Me',
  browseGroupsLinkLabel: 'Browse Existing Groups'
}