export default {
  title: 'Posting Guidelines',
  content: `<p>Welcome to TOY-exchange.ORG! We hope your experience on our site is a truly positive one. To aid in that goal, we have created these Posting Guidelines and Policies. Please read them carefully and in their entirety before creating posts on our site.</p>
	<p>You must register and become a member to use TOY-exchange.ORG.</p>
 
	<p>Posts are sorted by age group. Age 0-3, Age 4-8, Age 9 and Older. Within your User Settings you can select default filters for a specific age group(s) for email notifications. You can also set filters on your local group page for browsing.</p>
	 
	<p><strong>Age 0-3 group.</strong> As the name suggests, this group is for toys that might be suitable for children three and under. Blocks, stuffed animals, rattles, small ride-on toys, pull toys, activity centers, etc. But please don't assume that a toy offered in this group is automatically safe for all children. Use your own judgment when it comes to safety.</p>
	 
	<p><strong>Age 4-8 group.</strong> Any games or toys appropriate for children age 4-8 should be posted in this group. These are preschool to early elementary age kids. This can include anything from board games, puzzles and brain toys, to dolls, Lego sets, action figures, lawn games, sports gear, musical instruments, etc.</p>
	 
	<p><strong>Age 9 and Older.</strong> Play is fun for everyone. This category includes any toys or games suitable for kids 9 and older, including adults of all ages. Puzzles, card games, board games, science kits, electronic toys, robotics, kites, etc.</p>

	<h4>Sample Post:</h4>

	<figure><img src="/static/img/posting-guidelines.jpg" class="mw-100" alt="Toy-exchange how to post" /></figure>

	<p><strong>Post Title:</strong> Provide the name of the toy or game being offered.</p>
	<p><strong>Post Description:</strong> Try to provide a detailed description of the item and how it works, including an accurate description of its condition.</p>
	<p><strong>Photo:</strong> When possible, upload a snapshot of the item. The smaller the file size, the quicker and smoother the upload, though Toy-exchange.org can accept large file sizes. Crop your photo with the crop tool in the upper left of the upload box.</p>
	<p><strong>Post Type:</strong> Here you can select Offer or Wanted.</p>
	<p><strong>Age Range:</strong> Select an age range for the item.</p>
	<p><strong>Pick-up Window:</strong> If you have selected a default pick-up window in your settings, this box will auto fill. If not, you can state a time when you will make the item available for pick up. For example, Saturday 10:00AM – 12:00PM. If your pick-up window is flexible, please state, “Communicate with poster to coordinate".</p>
	<p><strong>Pick-up Address:</strong> This box will auto fill from the address provided in your settings. (Your address will only be seen by members with whom you arrange exchanges.)</p>
	<p><strong>Pick-up Placement:</strong> If you have stated a default placement in your settings, this box will auto fill. If not, you can state a specific location where the item will be placed for pick up. For example, Front Porch.</p>

	<p>Once you have completed the CREATE NEW POST form, click the SUBMIT NEW POST button. Your item will immediately post to your home group and an email notification will be sent to all other home group members, as well as members from nearby groups who have selected to receive them.</p>

	<p>Any member who wants a posted item can respond to the poster via the link provided in the email notification, or directly from the website. If you previously logged out of the site, you will be asked to log in before continuing to the posted item.</p>

	<h4>Multiple Responses for a Posted Item</h4>
	<p>It is very likely that when you post an item, you will receive multiple responses from other members who are interested. When this occurs you are free to choose the respondent you would like to gift your item to. Often people will select the first respondent, and that is a reasonable choice. However, that may disadvantage working parents. Parents with particularly demanding jobs may not be able to respond immediately to notifications. So selecting someone at random from among the responses you receive is also perfectly appropriate and even encouraged. To select a respondent, click on the SELECT <em>Toy-exchange member</em> TO RECEIVE THIS ITEM button.</p>

	<p>Selecting a respondent will automatically generate an email with the address, pickup window and pickup placement you provided when creating the post for the item. Please be considerate of other members by ensuring that you place the item where and when you have stated.</p>
	<p>Once you have selected a member to give your item to, the item will be automatically closed. If for whatever reason, you don’t end up giving the item to the selected member, you can return to your posts and reactivate it.</p>

	<h4>Personal Items Only - No Services, No Businesses</h4>
	<p>Offers of services and offers of items for sale are not accepted on TOY-exchange.ORG. These posts will be flagged as inappropriate and removed without notice.</p>
	<h4>Multiple Listings and Repeated Postings</h4>
	<p>There is no maximum on the number of posts that may be active for any member at any given time, but please do not post the same item multiple times. If you have not been able to give away your item after posting it twice, please try a different venue such as Freeexchange, Craigslist, or a local thrift shop.</p>

	<h4>Other Restrictions:</h4>
	<p><strong>Curb Alerts:</strong> Please do NOT post curb alerts on TOY-exchange.ORG. A curb alert is an announcement that items have already been left outside at a particular address. Experience suggests that this type of post can result in frustration and disappointment for our members. TOY-exchange.ORG expects that you make specific arrangements with another member to give/receive an offered item. Curb alerts should be flagged as inappropriate and will be removed without notice. You can choose to be at home when someone comes for a pick up, or to leave an item on a porch or other convenient location. Please be considerate of other members by ensuring that you place the item where and when you say you will.</p>
	<p><strong>Resale of items acquired on TOY-exchange.ORG:</strong> It is a violation of TOY-exchange.ORG's <a href="/terms-of-use" target="_blank">Terms of Use</a> to acquire free items on TOY-exchange.ORG in order to sell them. If we discover that a member is posting items for a business, or buying items for resale, we will cancel their membership immediately and without notice.</p>

	<h4>Safety</h4>
	<p>The overwhelming majority of TOY-exchange.ORG users are trustworthy and well-meaning. As with other community posting sites like Freeexchange, Facebook or Craigslist, please take reasonable measures to protect your safety and privacy when posting or when completing an Offer or Request exchange with another TOY-exchange.ORG member.</p>
	<h4>Privacy Policy</h4>
	<p>TOY-exchange.ORG's <a href="/privacy-notice" target="_blank">Privacy Policy</a> prohibits members from sending unwanted email to someone who posts on TOY-exchange.ORG, such as complaints about a posting, or anything that could be construed as unsolicited advertising for a business. If we discover that a member is in violation of our Privacy Policy, we may cancel their membership immediately and without notice.</p>
	<h4>Disclaimer</h4>
	<p>We ask people to please keep in mind that TOY-exchange.ORG has no control over the transactions that arise from postings between members. We have no way to guarantee that the condition of items posted is accurately reported nor that members will always follow through with their offer. We can only offer guidelines and trust that most members will be diligent in following them.</p>
`
};