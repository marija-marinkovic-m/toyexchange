export default {
  title: 'Privacy Policy',
  content: `<p>This website, Toy-Exchange.org (the “Site”), is owned and run by Toy-Exchange.org (“Toy-Exchange”, “we”, “us” or “our”). We are committed to maintaining your privacy as you navigate our Site and use our online registry, posting service and community exchange network (each a “Service” and collectively “Services”). Words capitalized and not defined in this Policy have the same meaning as they do in the <a href="/terms-of-use">Terms and Conditions of Service</a>.</p>

  <p>This Privacy Policy describes the types of information we may collect from you or that you may provide when you visit the Site. It also covers our practices for collecting, using, maintaining, protecting and disclosing that information. This policy applies to data collected: (i) by Toy-Exchange through the Site, and (ii) in email, text or other electronic messages between you and us as a result of using the Site. This Privacy Policy does not apply to information collected offline, by any third party or through our affiliate sites. <strong>Your continued use of the website and/or the Services constitutes your agreement</strong>.</p>

  <p>The use of technology on the Internet is rapidly evolving, as is Toy-Exchange’s use of new and evolving technology. We strongly encourage you to revisit this Privacy Policy from time to time for any updates regarding our use of this technology.</p>

  <h3>1. Information We Collect </h3>

	<p>In using our Site we may collect information that: (i) personally identifies you, such as name, address, phone number, credit card information or e-mail address (“Personal Information”); (ii) is about you but does not individually identify you; or (iii) tells us about your internet connection, the equipment you use to access our Site and your usage details. Collecting this information enables us to do administrative processing, provide our Services, and meet our legal obligations. You may refuse to provide some or all of your information (including Personal Information); however, keep in mind this may limit the ways in which we can interact with you, including providing you with our Services.</p>

	<p>The ways in which we collect information are:</p>

	<ul>
		<li>Directly from you via registration forms you fill out on our Site, by subscribing to a Service, requesting Services or when you report a problem with our Site.</li>
		<li>From records and copies of your correspondence (including e-mail addresses) if you contact us.</li>
		<li>Automatically as you use the Site where it relates to things like usage details, IP addresses and information collected </li>through cookies or other tracking technologies.
		<li>From third party business partners or affiliates.</li>
	</ul>

	<h4>Information That Does Not Personally Identify You</h4>

	<p>As you navigate our Site, we collect information about your equipment, browsing actions and patterns using automatic data collection technologies. Examples of this are information about your computer and internet connection, IP address, operating system and browser type. Keep in mind, information we collect automatically does not include Personal Information and helps us improve the user experience on our Site. The technologies we use for automatic data collection may include:</p>

	<p><strong><u>IP ADDRESS</u>.</strong> When you visit this Site, we may collect your Internet Protocol ("IP") address for marketing and customer experience purposes. Please note, this information is not personally identifiable, because it does not directly identify you (although it may identify the computer you are using.)</p>

	<p><strong><u>COOKIES AND WEB BEACONS</u>.</strong> A “cookie” is a piece of text that is placed on your computer's hard drive. A persistent cookie remains on your hard drive after you close your browser and may be used by your browser on subsequent visits to the site. Persistent cookies can be removed by following your web browser’s directions. A session cookie is temporary and disappears after you close your browser. You can reset your web browser to refuse all cookies or to indicate when a cookie is being sent. We use traffic log cookies to identify which pages are being used. This helps us analyze data about web page traffic and tailor the Site to customer needs. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us. However, you can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can modify your browser settings to decline cookies if you prefer. Note, this may prevent you from taking full advantage of the website and its features.</p>

	<p><strong><u>ANALYTICS COOKIES</u>.</strong> We use web analytic services to understand how people use the Site, as well as keep our Site services relevant, easy to use and up-to-date. Cookies allow web analytic services to recognize your browser or device, but do not contain Personal Information.</p>

	<h3>2. Third-Party Use of Cookies and Other Tracking Technologies</h3>

	<p>Some content or applications, including advertisements, on the Site are serviced by a third-party, including advertisers, ad networks and servers, content providers and application providers. These third parties may use cookies alone or in conjunction with web beacons or other tracking technologies to collect information about you when you use our Site. The information they collect may be associated with your Personal Information or they may collect information, including Personal Information, about your online activities over time and across different websites and other online services. They may use this information to provide you with interest-based (behavioral) advertising or other targeted content. We don’t control these third parties' tracking technologies or how they may be used. If you have any questions about an advertisement or other targeted content, you should contact the responsible provider directly.</p>

	<h3>3. What We Do with Your Information</h3>

	<p>We collect your information so we’re better able to understand your needs and provide you with our Services. More specifically, we collect your information to:</p>

	<ol>
		<li>Process your transactions, payments and service requests;</li>
		<li>Provide you with notices or service related emails; </li>
		<li>Administer, customize, personalize, analyze and improve our services, technologies, communications and relationships with you;</li>
		<li>Present the Site, the Services and its contents to you;</li>
		<li>Enforce our website terms with you to protect the security or integrity of this Site, our business, or our Services;</li>
		<li>Analyze user demographics, payment histories, and preferences;</li>
		<li>Comply with all reporting and other legal requirements; and</li>
		<li>Do as otherwise disclosed to you from time to time. </li>
	</ol>

	<h3>4. Updating Your Information </h3>

	<p>If you have any questions, would like access to your information, or need to correct/update your information please contact us at <a href="mailto:info@toy-Exchange.org">info@toy-Exchange.org</a>.</p>

	<h3>5. Sharing Your Personal Information</h3>

	<p>We do not sell, trade, or rent Personal Information to third parties for direct marketing. However, we may share generic aggregated demographic information (such as the total number of visitors) not linked to Personal Information with our business partners and trusted affiliates.</p>

	<p>However, we may:</p>

	<ul>
		<li>Use third party service providers to administer activities (such as payment processors, advertising, etc.) on our behalf but only disclose information for the limited purpose of performing those services. </li>
		<li>Disclose information: (i) To fulfill the purpose for which you provide it (for example, if you give us an e-mail address to use the "e-mail" feature of our Site); (ii) For any purpose disclosed by us when you provide the information; (iii) To comply with any court order, law or legal process, including a response to any government or regulatory request; (iv) To investigate, prevent, or take action regarding suspected or actual illegal activities or take precautions against liability; or (v) If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of us or others.</li>
	</ul>

	<p>Any Personal Information you voluntarily disclose for posting to the Service, becomes available to the public as controlled by any applicable privacy settings. If you remove information that you posted to the Service, copies may remain viewable in cached and archived pages of the Service, or if other users have copied or saved that information.</p>

	<h3>6. How We Secure Personal Information</h3>

	<p>We use a variety of security technologies and procedures to secure Personal Information. When you provide Personal Information we do our best to ensure it’s encrypted via SSL. Third party payment processors are used to process the financial information and we maintain physical, electronic, and procedural safeguards to guard your Personal Information. However, none of these are fail-proof, and we ask for your cooperation in ensuring your information stays safe. Please note we may discard any information: (i) after your account has been cancelled or terminated, (ii) upon your request; or (iii) any time in our sole discretion, whichever occurs earlier.</p>

	<h3>7. Collection and Use of Children’s Personal Information</h3>

	<p>Our Site is not intended for children under 13 years of age. If you are under 13, do not use or provide any information on this Site. If we learn we have collected or received personal information from a child under the age of 13 without verification of parental consent, we will delete that information. If you believe we might have any information from or about a child under 13, please contact us at <a href="mailto:info@toy-Exchange.org">info@toy-Exchange.org</a>.</p>

	<h3>8. Third Party Links</h3>

	<p>We are not responsible for the privacy practices or the content of third party websites and their use of information will be in accordance with their own privacy policies. <strong>Your use of a third party website is at your own risk</strong> and we cannot be held liable from any damages, costs, fees or penalties arising or resulting from your use of third party sites.</p>

	<h3>9. Complaint or Inquiry</h3>

	<p>If you have any questions, concerns, or comments about our Privacy Policy or practices, please contact us via email at <a href="mailto:info@toy-Exchange.org">info@toy-Exchange.org</a> with the words "PRIVACY POLICY" in the subject line.</p>

	<h3>10.	Internet Security and Possible Risks</h3>

	<p>Toy-Exchange cannot warrant the security of data while in transit to or from this Site. By using <strong>Toy-Exchange.org</strong> you will be deemed to have assumed such risks.</p>

	<h3>11.	Geographic Restrictions, Applicable Law</h3>

	<p>The Site may only be accessed within the USA and Canada. Any claim relating to the Site shall be governed by the internal laws of the State of California, without reference to its choice of law provisions, and shall be resolved solely through proceedings held within the State of California.</p>

	<h3>12.	Notification of Privacy Policy Changes</h3>

	<p>Toy-Exchange has the discretion to update this Privacy Policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage users to frequently check this page for any changes to stay informed about how we are helping to protect the Personal Information we collect. If we make material changes, we will notify you through a notice on the Site home page.</p>

	<h3>13.	Privacy Rights for California, North Dakota and Vermont Residents</h3>

	<p><strong>California</strong>. A California resident may request us not to share Personal Information with a third party. Existing California users can provide us with their order ID, name, email address and postal address by email at <a href="mailto:info@toy-Exchange.org">info@toy-Exchange.org</a>. In accordance with California Civil Code Sec. 1789.3, California resident users are entitled to know that they may file grievances and complaints with California Department of Consumer Affairs, 1625 North Market Blvd., Suite N 112 or by phone at  <u>800-952-5210</u>.</p>

	<p><strong>North Dakota</strong>. Pursuant to state law, we will only share information with our service providers and with third parties as required or permitted by law or as permitted by you.</p>

	<p><strong>Vermont</strong>. We will not disclose information about you with our affiliated companies or with Non-Affiliated Third Parties, other than as required or permitted by law or permitted by you.</p>

	<p>EFFECTIVE DATE: MAY 1, 2017 PLEASE PRINT AND RETAIN A COPY OF THIS AGREEMENT FOR YOUR RECORDS.</p>
`
}