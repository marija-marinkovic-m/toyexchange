export default {
  headline: 'Become a member and start exchanging free used toys today!',
  subHeadline: 'Enjoy a 60-day free trial.',
  description: 'The $12 annual fee will be applied at the end of your free trial. Cancel anytime before your trial ends to avoid being charged.'
}

export const NavigationLabels = {
  step1: () => <p><span className="hidden-sm-down">1. </span>Log In Information</p>, // '1. Log In Information',
  step2: () => <p><span className="hidden-sm-down">2. </span>Subscription Plan</p>, // '2. Subscription Plan',
  step3: () => <p><span className="hidden-sm-down">3. </span>User Settings</p>, //'3. User Settings'
};

export const LegalNotice = {
  p1: '',
  p2: 'Your small annual membership fee helps cover the cost of developing, maintaining and expanding our community. Thank you for your support!'
}

export const ReferralsForm = {
  signupCompleteNotice: 'Sign Up Complete! Thank You!',
  headline: 'Refer friends and extend your free membership!',
  subHeadline: 'For each new member you refer, earn three free months on your membership.',
  description: 'Your membership will be credited once your friend(s) sign up with TOY-CYCLE.ORG.',
  formInputEmailAddressesLabel: 'Your friend’s email address(es), separated by commas',
  formInputStatementLabel: 'Write a statement encouraging your friend(s) to join',
  formSubmitButtonLabel: 'Send Referral'
}

export const ReferralSentConfirmation = {
  successNotice: 'Referrals Sent! Thank You!',
  description: 'You are now ready to start exchanging free used toys and games.',
  homeGroupBtnLabel: 'Go To Home Group'
}

export const Tooltips = {
  pickupStreetAddress: '',
  pickupWindow: 'Please indicate when you will make the item available for pickup. Example: 9:00 - 12:00 Saturday',
  nearbyLocalGroupNotifications: '',
  notificationsType: '',
  generalPlacementForPickup: 'Please indicate where the item can be found during the pickup window.'
}