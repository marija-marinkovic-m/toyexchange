export default {
  title: 'TOY-exchange.ORG',
  content: `<h2>TERMS AND CONDITIONS OF SERVICE DATED: MAY 1, 2017</h2>

  <p>Please read the following prior to your use.</p>

  <h3>1. Important Rules</h3>

	<p>Toy-Exchange.org (also “Toy-Exchange”, “us”, “we”, or “our”) offers an online registry, posting service and community exchange network through this website Toy-exchange.org (the “Service”.) The rules that follow are <u>really important</u> and form part of the Terms and Conditions of Service (“Terms”). If you don’t agree with these rules or any other portion of the Terms, please don’t use the Service.</p>

	<div class="special-indent">
		<p><strong>A.</strong> By using this website, or any Services provided on, from, or through this website, you agree to follow these Terms and our <a href="/privacy-notice">Privacy Policy</a>.</p>

		<p><strong>B.</strong> Every item you list on the site must be free of charge, and must comply with Federal, State or local law.</p>

		<p><strong>C.</strong> Use of this website must be for personal, non-commercial use.</p>

		<p><strong>D.</strong> You must be at least 18 years or older to use this website.</p>

		<p><strong>E.</strong> You represent and warrant (promise) you have an unrestricted right to transfer, and full title in, all items you list at the time of posting. Please note, posting items you don’t own is a federal criminal offense.</p>

		<p><strong>F.</strong> Once an item is picked up you no longer have ownership in that item and you waive any right to say otherwise.</p>

		<p><strong>G.</strong> We believe every member of the Toy-Exchange community is committed to our mission of fairness and generosity. However, we can’t and don’t guarantee we check every listing or the items posted. You agree, as further described below, you won’t hold us responsible for any activity you undertake on or through the website. You are fully responsible for anything that happens, or does not happen, as a result of using the Service(s).</p>

		<p><strong>H.</strong> We ask you be careful with the information you disclose on the website and how you engage with other users. If something makes you uncomfortable, or seems suspicious, please stop communication and contact us immediately at <a href="mailto:info@toy-exchange.org">info@toy-exchange.org</a>.</p>

		<p><strong>I.</strong> Every member of the community must be kind, cordial and respectful of one another. Not only in their listings, but through communication, coordination and pickup. We reserve the right to remove disrespectful, rude, discriminatory, or inappropriate users as outlined below.</p>

		<p><strong>J.</strong> All listings must fairly represent items that are available for immediate pickup at the time of posting. To that end curb-side posts (where items are left on the curb) are not allowed. Posts may not be false, inaccurate or misleading. By the same token, once you respond to a post you must arrange for pickup quickly after. Failing to arrange for pickup, and actually pickup, the item as agreed between you and the poster may entitle the poster to repost.</p>
	</div>

	<p>We may, in our sole discretion, withdraw, modify or revise these Terms at any time, without prior notice. You agree to, from time to time, check for the most up-to-date version of Terms and Conditions of Use located on this page. Regardless, you will be bound by changes as soon as they are posted. All links or guidelines referenced in these Terms are made a part of these Terms.</p>

	<h3>The Service and Membership</h3>

	<p><u>In order to</u> use the Service, you will need a membership. As for memberships:</p>

	<ul>
		<li>Membership is open to users 18 years of age or older.</li>
		<li>Only one membership is permitted per individual. You may not use a membership that isn’t in your name or create a membership under a fictitious name.</li>
		<li>All information you supply must be accurate and complete and you solely are responsible for any activity that occurs under your account.</li>
		<li>If we provide you with a user name, password or any other piece of security information, do not disclose it to anyone. Let us know immediately if you discover a breach of security or an unauthorized use of your account. If there is unauthorized use of your account, you may be liable for damages or losses Toy-Exchange Group (defined below) suffers as a result.</li>
		<li>We may disable a user name, password or other identifier at any time in our sole discretion. If we do disable an identifier, we will not be liable for losses you suffer as a result.</li>
	</ul>

	<p>By using the Service, your membership has an initial and recurring payment feature. You accept responsibility for all recurring charges that occur prior to cancellation or termination. To cancel a membership, send us a message at <a href="mailto:info@toy-exchange.org">info@toy-exchange.org</a> before the renewal. Cancelling a subscription will not entitle you to a full refund. But, so long as you haven’t breached, or are in breach of, these terms you may be entitled to a pro-rated reimbursement for months paid and not used [along with a small cancellation fee.] We reserve the right to change the price of the membership fee, however, price increases will be done with advance written notice. Memberships are not transferable.</p>

	<h3>3. Third Party Links</h3>

	<p>The Service may contain links to third party websites not under our ownership or control. We are not responsible for the content, privacy policies, or practices of any third party websites and <strong>you relieve Toy-Exchange Group from any and all liability arising from your use of a third-party website</strong>.</p>

	<h3>4. Reliance on Information Posted and No Professional Advice</h3>

	<p>WE DO NOT WARRANT THE ACCURACY OR COMPLETENESS OF INFORMATION ON THE WEBSITE. YOUR RELIANCE IS STRICTLY AT YOUR OWN RISK. ALL STATEMENTS AND/OR OPINIONS EXPRESSED IN THIRD PARTY MATERIALS ARE THE OPINIONS AND RESPONSIBILITY OF THE PERSON OR ENTITY PROVIDING THEM. WE ARE NOT RESPONSIBLE, OR LIABLE TO YOU OR ANY THIRD PARTY, FOR THE CONTENT OR ACCURACY OF ANY MATERIALS PROVIDED BY THIRD PARTIES. AND WE ARE NOT RESPONSIBLE FOR ITEMS LOST, DAMAGED OR NOT AS REPRESENTED IN A LISTING.</p>

	<h3>5. Your Use of the Service—Permissions and Restrictions</h3>

	<p>You have our permission to use Services as outlined in these Terms provided that:</p>

	<div class="special-indent">
		<p><strong>A.</strong> You agree not to alter or modify any part of a Service or any copyright, trademark or other proprietary notices appearing in a Service.</p>

		<p><strong>B.</strong> You agree not to use a Service for commercial uses unless you obtain our prior written approval.</p>

		<p><strong>C.</strong> You agree not to use, or launch, an automated system that sends more request messages to our servers in a given period of time than a human can reasonably produce in the same period by using a conventional on-line web browser. This includes, but isn’t limited to, "robots," "spiders," or "offline readers."</p>

		<p><strong>D.</strong> You agree not to collect or harvest any personally identifiable information from the Service (e.g., account names from comments or email) or to use a Service’s Content or communication systems for solicitation purposes.</p>

		<p><strong>E.</strong> You don’t introduce viruses, trojan horses, worms, logic bombs or other material that is malicious or technologically harmful. And you do not attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of this website, the server on which the website is stored, or any server, computer or database connected to the website.</p> 

		<p><strong>F.</strong> You don’t engage in any other conduct that restricts or inhibits anyone's use or enjoyment of our Services, or which, as determined by us, may harm us or users of our Services.</p>
	</div>

	<h3>6. Your Content and Conduct</h3>

	<div class="special-indent">

		<p><strong>A.</strong>	This website may contain message boards, profiles, forums, bulletin boards and other interactive features (collectively, "Interactive Services") that allow users to post, submit, publish, display or transmit (hereinafter, "post") listings, messages, text, content or materials (collectively, "Content") on or through the website.</p>

		<p><strong>B.</strong>	You solely are responsible for the Content you post, along with the consequences of submitting and publishing Content. You represent and warrant (promise) you own all rights, consents, and permissions to post Content you submit; and if you don’t own these rights that you’ve obtained them from the legal owner.</p>

		<p><strong>C.</strong>	You hereby give Toy-Exchange Group a worldwide, non-exclusive, royalty-free, sub-licenseable and transferable license to use, reproduce, distribute, modify, prepare derivative works of, display and perform your Content in connection with this website and/or Toy-Exchange’s business. You also grant each user of the Service a non-exclusive license to access your Content.</p>

		<p><strong>D.</strong>	To be clear, the license you’ve agreed to above does not transfer ownership. It allows us to do things like highlight your listing on our homepage, promote the website and its Services, repurpose part or all of the Service in other media formats and otherwise carry out business in a way that allows users to benefit from being a part of our community. However, you may revoke the rights granted above at any time in a writing sent to us at <a href="mailto:info@toy-exchange.org">info@toy-exchange.org</a> and/or by deleting your account. Do keep in mind, though your Content may be removed from our Services we can’t guarantee Content will be removed from the Internet.</p>    

		<p><strong>E.</strong>	By connecting a Service with a third-party service, like a social media service, you give us permission to access and use your information from that service as permitted by that service, and to store your log-in credentials from that service.</p>

		<p><strong>F.</strong>	You may not use Interactive Services for (or post Content or other material that):</p>

		<div class="special-indent">
			<p><strong>i.</strong> is defamatory, obscene, indecent, abusive, offensive, harassing, violent, hateful, inflammatory or otherwise objectionable, promotes sexually explicit or pornographic material, violence, or discrimination based on race, sex, religion, nationality, disability, sexual orientation or age,</p>
			<p><strong>ii.</strong> infringes any patent, trademark, trade secret, copyright or other intellectual property or other rights of any other person,</p>

			<p><strong>iii.</strong> violates the legal rights of others, contains material that could give rise to any civil or criminal liability or that otherwise conflicts with these Terms or our Privacy Policy, </p>

			<p><strong>iv.</strong> violates applicable local, national, or international laws and regulations,</p>

			<p><strong>v.</strong> is likely to deceive any person,</p>

			<p><strong>vi.</strong> promotes any illegal activity, or advocates, promotes or assists in any unlawful act,</p>

			<p><strong>vii.</strong> causes annoyance, inconvenience or needless anxiety or likely to upset, embarrass, alarm or annoy any other person,</p>

			<p><strong>viii.</strong> impersonates any person, or misrepresents your identity or affiliation with any person or organization,</p>

			<p><strong>ix.</strong> involves (or includes a link to) commercial activities or sales, such as contests, sweepstakes or other sales promotions, barter or advertising, or</p>

			<p><strong>x.</strong> gives the impression that it comes from or is endorsed by us or any other person or entity, if this is not the case.</p>
		</div>	

		<p><strong>G.</strong>	We may disclose information about you to a third party who claims Content you post violates their rights, including intellectual property rights or privacy rights. We may also take legal action, including without limitation referring you to law enforcement, for any illegal or unauthorized use of the website. You understand we will fully cooperate with any law enforcement authorities or court order requesting or directing us to disclose the identity or other information of anyone posting any materials on or through our Services.</p>

	</div>

	<h3>7. Content in General</h3>

	<p>You agree:</p>

	<div class="special-indent">
		<p><strong>A.</strong> Content is provided to you AS IS and your use of Content is at your sole risk and expense. Toy-Exchange Group is not responsible for the accuracy, usefulness, safety, or intellectual property rights of or relating to such Content.</p>

		<p><strong>B.</strong> Not to circumvent, disable or otherwise interfere with security-related features of a Service, including features that restrict use or copying of Content or enforce limitations on use of a Service or its Content.</p>

		<p><strong>C.</strong> We do our best to monitor user accounts, however you may be exposed to Content that is inaccurate, offensive, indecent, or objectionable. Where this happens, report such activity to us immediately. Our contact information appears at the end of these Terms. Because we can’t possibly monitor every account at all times, you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against Toy-Exchange Group with respect to such contact.</p>

		<p><strong>D.</strong> If, without a request from us, you send creative ideas or suggestions, whether online, by email, by postal mail, or otherwise (collectively, 'comments'), you agree we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments you forward to us. We are under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments.</p>

		<p><strong>E.</strong> We may limit or modify your Content, for example by imposing a maximum number of days for a listing, the size of posts, etc.</p>
	</div>

	<p>We reserve all rights not expressly granted in or to a Service and its Content.</p>

	<h3>8. Termination of Use or of an Account</h3>

	<p>Please note, use of our Services does not give users a continued right to use or have access to such Services. We want everyone to enjoy their experience, and consequently we reserve the right to terminate a user’s access to a Service, including a user’s account, if the user: (1) engages in predatory, bullying or unlawful behavior, (2) violates these Terms, (3) infringes on the intellectual property rights of others, or (4) infringes on the privacy rights of others. This includes the ability to remove a post that we believe is in violation of these Terms.</p>

	<h3>9. Digital Millennium Copyright Act</h3>

	<div class="special-indent">
		<p><strong>A.</strong>	If notified, as outlined in this Section, that Content infringes on another's intellectual property rights we reserve the right to remove such Content without prior notice and without liability.</p>

		<p><strong>B.</strong>	If you are a copyright owner (or an agent of a copyright owner) and believe Content infringes your copyright(s), you may submit a notification under the Digital Millennium Copyright Act ("DMCA") by providing our Copyright Agent with the following information in writing (see 17 U.S.C 512(c)(3) for further detail):</p>

		<ul>
			<li>Identification of the copyrighted work claimed to have been infringed;</li>

			<li>The material claimed to be infringing that is to be removed or disabled and information reasonably sufficient to permit us to locate the material;</li>

			<li>Contact information, such as an address, telephone number, and, if available, an email address;</li>

			<li>A statement that: (1) you believe in good faith the material is not authorized by the copyright owner, its agent, or the law; and (2) the information you supply in the notice is accurate, and you are authorized to act on behalf of the owner that is allegedly infringed.</li>	
		</ul>
	</div>

	<p>Send copyright infringement notifications to <a href="mailto:info@toy-exchange.org">info@toy-exchange.org</a>. For clarity, only DMCA notices should go to the Copyright Agent. You acknowledge that if you fail to comply with all of the requirements of this Section your DMCA notice may not be valid.</p>

	<h3>10. Warranty Disclaimer</h3>

	<p><strong>YOU AGREE YOUR USE OF THE SERVICES IS AT YOUR SOLE RISK. <u>TO THE FULLEST EXTENT</u> PERMITTED BY LAW, TOY-exchange GROUP DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SERVICES AND YOUR USE OF THE SERVICES. TOY-exchange GROUP ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, OMISSIONS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL INJURY OR PROPERTY DAMAGE (OF ANY NATURE) RESULTING FROM YOUR ACCESS TO OR USE OF OUR SERVICES, (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED ON THEM, (IV) ANY INTERRUPTION OF OUR SERVICES, OR (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICES BY ANY THIRD PARTY. TOY-exchange DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY ITEM LISTED, ANY SERVICES FEATURED IN ANY BANNER OR OTHER ADVERTISING. TOY-exchange WILL NOT BE A PARTY TO, OR IN ANY WAY BE RESPONSIBLE FOR MONITORING, ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES SO PLEASE USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.</strong></p>

	<h3>11. Limitation of Liability</h3>

	<p><strong>IN NO EVENT WILL TOY-exchange GROUP BE LIABLE TO YOU FOR DAMAGES (THIS INCLUDES DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES) RESULTING FROM (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL INJURY OR PROPERTY DAMAGE RESULTING FROM ACCESS TO OR USE OF SERVICES, (III) UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, (IV) ANY INTERRUPTION OF OUR SERVICES, (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE, WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICES BY ANY THIRD PARTY, AND/OR (V) ANY ERRORS, OMISSIONS, INACCURACIES OR MISTAKES IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND THAT RESULTS FROM YOUR USE OF ANY CONTENT MADE AVAILABLE VIA THE SERVICES. THIS LIMITATION APPLIES <u>WHETHER OR NOT</u> A CLAIM IS BASED ON A THEORY OF WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY AND WHETHER OR NOT THE COMPANY IS AWARE SUCH DAMAGES ARE POSSIBLE. HOWEVER, THE FOREGOING LIMITATION APPLIES ONLY <u>TO THE FULLEST EXTENT</u> PERMITTED BY LAW IN THE APPLICABLE JURISDICTION. ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF OR RELATING TO THESE TERMS OR THE WEBSITE MUST BE COMMENCED WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES, OTHERWISE, THE CAUSE OF ACTION OR CLAIM IS PERMANENTLY BARRED.</strong></p>

	<h3>12. Indemnity</h3>

	<p>To the extent permitted by applicable law, you agree to defend, indemnify and hold harmless Toy-Exchange, its owners, operators, affiliates, and each of its or their directors, employees, licensors, licensees, service providers, successors, permitted assignees and agents <strong>(individually and collectively “Toy-Exchange Group”)</strong>  from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees) arising or resulting from: (i) your use of and access to a Service; (ii) your violation of these Terms; (iii) your violation of any third party right, including without limitation any intellectual property right or privacy right; or (iv) any claim that your Content caused damage to a third party. To be clear, this obligation to indemnify, defend and hold us harmless will survive these Terms and your use of the applicable Service(s).</p>

	<h3>13. Assignment</h3>

	<p>These Terms, and any rights and licenses granted hereunder, may not be transferred or assigned by you but may be assigned by us without restriction. Any attempt by you to assign that is not consented to will be considered null and void.</p>

	<h3>14. Trademarks and Intellectual Property</h3>

	<p>Toy-Exchange and all related names, logos, service names, designs and slogans are trademarks of Toy-Exchange or its affiliates or licensors. You may not use these marks without our written permission. All other names, logos, service names, designs and slogans on this website are the trademarks of their respective owners. This website and its features, functionality (Content, trademarks, service marks and logos) are owned by, or if not owned licensed to, Toy-Exchange and are subject to copyright, trademark, patent, trade-secret and other intellectual property or proprietary rights under the law. You may only use these as directed, to the extent directed. If you have any questions, please contact us.</p>

	<h3>15. General</h3>

	<p>You agree: <u>These Terms are governed by the laws of the State of California</u>, without respect to its conflict of laws principles. Any claim or dispute between you and us that arises (in whole or in part) from the Service will be decided exclusively by a court of competent <u>jurisdiction</u> located in the County of Alameda and you hereby waive the ability to assert otherwise. These Terms, together with the <a href="/privacy-notice">Privacy Policy</a> and any sites or guidelines referenced here, or other legal notices published by Toy-Exchange on a Service, constitute the entire agreement between you and Toy-Exchange concerning a Service. If any provision in these Terms is deemed invalid by a court of competent <u>jurisdiction</u>, its invalidity will not affect the validity of the remaining provisions, which remain in full force and effect. No waiver by Toy-Exchange of any term or condition set forth in these Terms shall be considered a further or continuing waiver of such term or condition or a waiver of any other term or condition, and any failure of Toy-Exchange to assert a right or provision under these Terms does not constitute a waiver of such right or provision that precludes us from enforcing at <u>a later date</u>.</p>

	<h3>16. Notice for California Users</h3>

	<p>If you are a California resident, you can request information regarding the disclosure of your personal information by Toy-Exchange to third parties for the third parties’ direct marketing purposes. To make such a request, please send an email to <a href="mailto:info@toy-exchange.org">info@toy-exchange.org</a> or write to us at P.O. Box 7165 Oakland, CA 94601. Under California Civil Code Section 1789.3, California Website users are entitled to the following specific consumer rights notice: The Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs may be contacted in writing at 1625 N. Market Blvd., Suite N 112, Sacramento, California 95834, or by telephone at (800) 952-5210.</p>

	<p>Any feedback, comments, requests for technical support, and other communications should be directed to our customer service through <a href="mailto:info@toy-exchange.org">info@toy-exchange.org</a>.</p>`
};