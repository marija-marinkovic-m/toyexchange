const socialLinks = [
  { iconClass: 'fa fa-facebook', label: 'facebook', href: '//www.facebook.com' },
  { iconClass: 'fa fa-pinterest-p', label: 'pinterest', href: '//www.pinterest.com' },
  // {iconClass: 'fa fa-google-plus', label: 'google plus', href: '//google.com'},
  { iconClass: 'fa fa-twitter', label: 'twitter', href: '//twitter.com' },
  // {iconClass: 'fa fa-youtube-play', label: 'youtube', href: '//youtube.com'},
  // {iconClass: 'fa fa-instagram', label: 'instagram', href: '//instagram.com'}
];

export default socialLinks;