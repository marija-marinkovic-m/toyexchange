export default {
  title: 'Become a member and start exchanging free used toys and games today!',
  signUpButtonLabel: 'Sign Up & Get a 60-day Free Trial',
  logInTitle: 'Already have an account?',
  logInLinkLabel: 'Log In'
}