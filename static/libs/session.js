/* global window */
/* global localStorage */
/* global XMLHttpRequest */
/**
 * A class to handle signing in and out and caching session data in sessionStore
 *
 * Note: We useXMLHttpRequest() here rather than fetch because fetch() uses
 * Service Workers and they cannot share cookies with the browser session
 * yet (!) so if we tried to get or pass the CSRF token it would mismatch.
 */

class Session {

  constructor({req} = {}) {
    this._session = {};
    try {
      if (req) {
        // if running on server access the server side envirnoment
        this._session = req.session;
      } else {
        // if running on client, attempt to load session from localstorage
        this._session = this._getLocalStore('session');
      }
    } catch(err) {
      return;
    }
  }

  static async getCsrfToken() {
    return new Promise((resolve, reject) => {
      if (typeof window === 'undefined') {
        return reject(Error('This method should only be called on the client (getCsrfToken)'));
      }

      let xhr = new XMLHttpRequest();
      xhr.open('GET', '/auth/csrf', true);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            const responseJson = JSON.parse(xhr.responseText);
            resolve(responseJson.csrfToken);
          } else {
            reject(Error('Unexpected response when trying to get CSRF token'));
          }
        }
      }
      xhr.onerror = () => {
        reject(Error('XMLHttpRequest error: Unable to get CSRF token'));
      };
      xhr.send();
    });
  }

  // async requst cant be done in the constructor so access is via async method
  // this allows use of XMLHttpRequest when running on the client to fetch it 
  async getSession(forceUpdate) {
    // if on the server, return session as will be loaded in constructor
    if (typeof window === 'undefined') {
      return new Promise(resolve => {
        resolve(this._session);
      });
    }

    // if force update is set, clear data from the store 
    if (forceUpdate === true) {
      this._session = {};
      this._removeLocalStore('session');
    }

    // Attempt to load session data from sessionStore on every call 
    this._session = this._getLocalStore('session');

    // if session data exists, has not expired and forceUpdate is not set
    // return the stored session we already have 
    if (this._session && Object.keys(this._session).length > 0 && this._session.expires && this._session.expires > Date.now()) {
      return new Promise(resolve => {
        resolve(this._session);
      });
    }

    // if don't have session data, or is expired or forceUpdate is set 
    // to true than revalidate it by fetching it again from the server 
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', '/auth/session', true);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            // update session with session info 
            this._session = JSON.parse(xhr.responseText);

            // set a value we will use to check this client 
            // this client should silently revalidate on the value of clientMaxAte set by the server
            this._session.expires = Date.now() + this._session.clientMaxAge;

            // save changes to session
            this._saveLocalStore('session', this._session);

            resolve(this._session);
          } else {
            reject(Error('XMLHttpRequest failed: Unable to get session'));
          }
        }
      }
      xhr.onerror = () => {
        reject(Error('XMLHttpRequest error: Unable to get session'));
      }
      xhr.send();
    });
  }

  async login(username, password) {
    return new Promise(async (resolve, reject) => {
      if (typeof window === 'undefined') {
        return reject(Error('This method should only be called on the client (login)'));
      }

      // make sure session is in memory 
      this._session = await this.getSession();
      // make sure the latest CSRF Token is in session
      this._session.csrfToken = await Session.getCsrfToken();

      let xhr = new XMLHttpRequest();
      xhr.open('POST', '/auth/login', true);
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = async () => {
        if (xhr.readyState === 4) {
          if (xhr.status !== 200) {
            return reject(Error('XMLHttpRequest error: Error while atempting to signin'));
          }
          this._session = JSON.parse(xhr.responseText);
          this._session.expires = Date.now() + this._session.clientMaxAge;

          // save changes to session
          this._saveLocalStore('session', this._session);

          return resolve(this._session);
        }
      }
      xhr.onerror = () => {
        return reject(Error('XMLHttpRequest error: Unable to login'));
      }
      xhr.send('_csrf=' + encodeURIComponent(this._session.csrfToken) + '&' +
                'username=' + encodeURIComponent(username) + '&' +
                'password=' + encodeURIComponent(password));
    });
  }

  async signup(email, password) {
    return new Promise(async(resolve, reject) => {
      if (typeof window === 'undefined') {
        return reject(Error('This method shoud only be called on the client (signup)'));
      }
      // we need csrf token
      this._session = await this.getSession();
      this._session.csrfToken = await Session.getCsrfToken();

      let xhr = new XMLHttpRequest();
      xhr.open('POST', '/auth/signup', true);
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = async () => {
        if (xhr.readyState === 4) {
          if (xhr.status !== 200) {
            return reject(Error('XMLHttpRequest error: Error while atempting to signup'));
          }
          this._session = JSON.parse(xhr.responseText);
          this._session.expires = Date.now() + this._session.clientMaxAge;

          // save changes to session
          this._saveLocalStore('session', this._session);
          return resolve(this._session);
        }
      };
      xhr.onerror = () => {
        return reject(Error('XMLHttpRequest error: Unable to signup'));
      }
      xhr.send('_csrf=' + encodeURIComponent(this._session.csrfToken) + '&' +
                'email=' + encodeURIComponent(email) + '&' +
                'password=' + encodeURIComponent(password));
    });
  }

  async logout() {
    // signout from the server 
    return new Promise(async (resolve, reject) => {
      if (typeof window === 'undefined') {
        return reject(Error('This method should only be called on the client (logout)'));
      }

      // make sure session is in memory 
      this._session = await this.getSession();
      // make sure the latest CSRF Token is in session
      this._session.csrfToken = await Session.getCsrfToken();

      let xhr = new XMLHttpRequest();
      xhr.open('POST', '/auth/logout', true);
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = async () => {
        if (xhr.readyState === 4) {
          // @TODO: I'm not checking for sucess, just completion

          // update local session
          this._session = await this.getSession(true);
          resolve(true);
        }
      };
      xhr.onerror = () => {
        reject(Error('XMLHttpRequest error: unable to signout'));
      };
      xhr.send('_csrf=' + encodeURIComponent(this._session.csrfToken));
    });
  }

  async refreshToken() {
    return new Promise(async (resolve, reject) => {

      if (typeof window === 'undefined') {
        return reject(Error('This method should only be called on the client (refreshToken)'));
      }

      // make sure session is in memory 
      this._session = await this.getSession();
      // make sure the latest CSRF Token is in session
      this._session.csrfToken = await Session.getCsrfToken();

      // we need accessToken and refreshToken from session.user.auth
      if (!this._session.user || !this._session.user.auth || !this._session.user.auth.refreshToken || !this._session.user.auth.accessToken) {
        return reject(Error('Tokens not in memory'));
      }

      let xhr = new XMLHttpRequest();
      xhr.open('POST', '/auth/refresh-token', true);
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = async () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            this._session = JSON.parse(xhr.responseText);
            this._session.expires = Date.now() + this._session.clientMaxAge;
            this._saveLocalStore('session', this._session);
            resolve(this._session);
          } else {
            reject(Error('XMLHttpRequest failed: Unable to refresh the token'));
          }
        }
      };
      xhr.onerror = () => {
        reject(Error('XMLHttpRequest error: Unable to refresh the token'));
      };
      xhr.send('_csrf=' + encodeURIComponent(this._session.csrfToken) + '&' +
                '_accessToken=' + encodeURIComponent(this._session.user.auth.accessToken) + '&' +
                '_refreshToken=' + encodeURIComponent(this._session.user.auth.refreshToken));
    });
  }

  async updateSession() {
    return new Promise(async (resolve, reject) => {
      if (typeof window === 'undefined') {
        return reject(Error('This method should only be called on the client (updateSession)'));
      }

      // make sure session is there
      this._session = await this.getSession();
      // make sure the latest CSRF Token is in session
      this._session.csrfToken = await Session.getCsrfToken();

      // access token needed for the request
      if (!this._session.user || !this._session.user.auth || !this._session.user.auth.accessToken) {
        return reject(Error('AccessToken missing from memory.'));
      }
      
      let xhr = new XMLHttpRequest();
      xhr.open('POST', '/auth/update-session-user-data', true);
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = async () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            this._session = JSON.parse(xhr.responseText);
            this._session.expires = Date.now() + this._session.clientMaxAge;
            this._saveLocalStore('session', this._session);
            resolve(this._session);
          } else {
            reject(Error('XMLHttpRequest failed: Unable to update the session'));
          }
        }
      };
      xhr.onerror = () => {
        reject(Error('XMLHttpRequest error: Unable to update session data'));
      };
      xhr.send('_csrf=' + encodeURIComponent(this._session.csrfToken) + '&' +
                '_accessToken=' + encodeURIComponent(this._session.user.auth.accessToken));
    });
  }


  // web storage api is widely supported, but not always available
  // its handled here by just returning null (silently)
  _getLocalStore(name) {
    try {
      return JSON.parse(localStorage.getItem(name));
    } catch (err) {
      return null;
    }
  } 
  _saveLocalStore(name, data) {
    try {
      localStorage.setItem(name, JSON.stringify(data));
      return true;
    } catch(err) {
      return false;
    }
  }
  _removeLocalStore(name) {
    try {
      localStorage.removeItem(name);
      return true;
    } catch(err) {
      return false;
    } 
  }

}