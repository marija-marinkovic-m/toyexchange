let module = {}, totalUnread = 0, intervalMs = 60000, notificationPullInterval;
importScripts('/static/config.js');
importScripts('/static/libs/session.js');

const fetchMyMessages = (accessToken, api_uri) => {
  let xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        try {
          const res = JSON.parse(xhr.responseText);
          self.postMessage(res.data.totalUnread);
        } catch(e) {
          console.log(e);
          console.log('refresh');
          self.postMessage(0);
          return;
          // try to refresh the token
          // const SessionInstance = new Session();
          // return SessionInstance.refreshToken()
          //   .then(res => {
          //     console.log(res);
          //     if (res.user && res.user.auth && res.user.auth.refreshToken) {
          //       const newToken = res.user.auth.refreshToken;
          //     } else {
          //       SessionInstance
          //         .logout()
          //         .then(() => window.location.reload());
          //       return;
          //     }
          //     // try just one more time
          //     // if fails logout user
          //     let reXhr = new XMLHttpRequest();
          //     reXhr.onreadystatechange = () => {
          //       try {
          //         const reRes = JSON.parse(reXhr.response);
          //         totalUnread = reRes.data.totalUnread;
          //         self.postMessage(totalUnread);
          //       } catch(reE) {
          //         SessionInstance
          //           .logout()
          //           .then(() => window.location.reload());
          //       }
          //     };
          //     xhr.onerror = () => {
          //       console.log('error while refreshing the token');
          //       self.postMessage(totalUnread);
          //     };
          //     xhr.open('GET', api_uri + '/my/messages', true);
          //     xhr.setRequestHeader('Authorization', 'Bearer ' + newToken);
          //     xhr.send();
          //   });
        }
      } else {
        self.postMessage(totalUnread);
      }
    }
  }

  xhr.onerror = () => {
    console.log('an error occured');
    self.postMessage(totalUnread);
  }

  xhr.open('GET', api_uri + '/my/messages', true);
  xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);
  xhr.send();
}

self.addEventListener('message', e => {
  const msg = e.data;
  switch (msg) {
    case 'stop':
      self.postMessage('I\'m about to close now. Bye, bye.');
      clearInterval(notificationPullInterval);
      self.close();
      break;
    default:
      // here we assume accessToken is sent
      if (module.exports && module.exports.API_URI) {
        fetchMyMessages(msg, module.exports.API_URI);

        if (notificationPullInterval) return;
        notificationPullInterval = setInterval(() => {
          fetchMyMessages(msg, module.exports.API_URI);
        }, intervalMs)
      }
  }
},false);